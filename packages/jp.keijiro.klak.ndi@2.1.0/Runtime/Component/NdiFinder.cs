using Klak.Ndi.Interop;
using System;
using System.Collections.Generic;

namespace Klak.Ndi {

    public static class NdiFinder
    {
        public static IEnumerable<string> sourceNames => EnumerateSourceNames();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<string> EnumerateSourceNames()
        {
            var list = new List<string>();
            foreach (var source in SharedInstance.Find.CurrentSources)
                list.Add(source.NdiName);
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<Source> EnumerateSources()
        {
            List<Source> sources = new List<Source>();
            foreach(var s in SharedInstance.Find.CurrentSources)
            {
                sources.Add(s);
            }
            return sources;
        }
    }

} // namespace Klak.Ndi
