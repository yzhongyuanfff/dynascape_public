using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;
#if BIBCAM_HAS_UNITY_VIDEO
using UnityEngine.Video;
#endif

namespace Bibcam.Decoder {

sealed class BibcamVideoFeeder_Archieve : MonoBehaviour
{
#if BIBCAM_HAS_UNITY_VIDEO

    #region Scene object reference

    [SerializeField] BibcamMetadataDecoder _decoder = null;
    [SerializeField] BibcamTextureDemuxer _demuxer = null;
    [SerializeField] bool _asynchronous = true;
    [SerializeField] public VisualEffect vfx;

    [SerializeField] bool stepMode = false;
    [SerializeField] bool stepNext = false;

    BibcamFrameFeeder _feeder;
    VideoPlayer _video;

    public ulong frameStart = 0;
    public ulong frameStop = 0;
    public ulong currentFrame = 0;
    public bool checkFrameStop = false;
    public bool autoRestart = false;
    public bool fbRestartVideo = false;

    [SerializeField] public Material enhancedStencilMaterial = null;
    [SerializeField] CustomRenderTexture altTexture = null;
    [Range(0, 20)]
    [SerializeField] float width = 2;
        public bool fbPauseTheVideo = false;
        public bool fbResumeTheVideo = false;


        #endregion

        #region MonoBehaviour implementation


        void OnDestroy()
    {
        _feeder?.Dispose();
        _feeder = null;
    }

    void Start()
    {
        _video = GetComponent<VideoPlayer>();
        altTexture = new CustomRenderTexture(1920, 1080, RenderTextureFormat.Default, RenderTextureReadWrite.Default);
        altTexture.updateMode = CustomRenderTextureUpdateMode.Realtime;
        altTexture.initializationSource = CustomRenderTextureInitializationSource.Material;
        altTexture.initializationMode = CustomRenderTextureUpdateMode.Realtime;
        if (_video)
        {
            _video.frameReady += videoPrepared;
            _video.sendFrameReadyEvents = true;
        }
        if(frameStop == 0) 
            frameStop = _video.frameCount;
    }

        bool AllowResume = true;

    public void PauseTheVideo()
    {
        _video.Pause();
        vfx.pause = true;
    }

    public void DeepPauseTheVideo()
    {
         AllowResume = false;
         PauseTheVideo();
    }

    public void ResumeTheVideo()
    {
        if (AllowResume)
        {
            _video.Play();
                vfx.pause = false;
        }
    }

    public void DeepResumeTheVideo()
    {
            AllowResume = true;
            ResumeTheVideo();
    }

    private void videoPrepared(VideoPlayer source, long frameIdx)
    {
       if (stepMode) 
             _video.Pause();
    }

        public void RestartVideo()
        {
            vfx.Stop();
            vfx.Reinit();
            _video.Stop();
            _video.frame = 0;
            _video.Play();
        }

        public void UpdatePlaybackSpeed(float v)
        {
            _video.playbackSpeed = v;
        }

        public void UpdateStartFrame(float sv)
        {
            frameStart = (ulong)(sv * _video.frameCount);
        }

        public void UpdateStopFrame(float sv)
        {
            frameStop = (ulong)(sv * _video.frameCount);
        }

    void Update()
    {
        if(_video == null)
        {
            _video = GetComponent<VideoPlayer>();
            if (_video)
            {
                _video.frameReady += videoPrepared;
                _video.sendFrameReadyEvents = true;
            }
            if (frameStop == 0) 
                frameStop = _video.frameCount;
        }
        if (fbPauseTheVideo)
        {
            fbPauseTheVideo = false;
            PauseTheVideo();
        }
        if (fbResumeTheVideo)
        {
            fbResumeTheVideo = false;
            ResumeTheVideo();
        }

        if (stepNext)
        {
            stepNext = false;
            _video.StepForward();
        }
        if (fbRestartVideo)
        {
            fbRestartVideo = false;
            RestartVideo();
        }
        else
        {
            if (checkFrameStop) {
                if (_video.frame > (long)frameStop) {
                    if (_video.isLooping)
                        _video.frame = 0;
                    else
                    {
                        _video.Pause();
                        if (autoRestart)
                        {
                            //vfx.Stop();
                            //vfx.Reinit();
                            _video.Stop();
                            _video.frame = 0;
                            _video.Play();
                        }
                        return;
                    }
                }
            }
        }
        if (_video.texture == null) return;
        if (_video.frame < (long)frameStart) return;

        currentFrame = (ulong)_video.frame;
        Texture currentTexture = _video.texture;

        if (enhancedStencilMaterial != null)
        {
            enhancedStencilMaterial.mainTexture = _video.texture;
            altTexture.initializationMaterial = enhancedStencilMaterial;
            altTexture.initializationMaterial.SetFloat("_Width", width);
            currentTexture = altTexture;
        }


        if (_asynchronous)
        {
            // Async mode: Use FrameFeeder.
            _feeder = _feeder ?? new BibcamFrameFeeder(_decoder, _demuxer);
            _feeder.AddFrame(currentTexture);
            _feeder.Update();
        }
        else
        {
            // Sync mode: Simply decode and demux.
            _decoder.DecodeSync(currentTexture);
            _demuxer.Demux(currentTexture, _decoder.Metadata);
            return;
        }
    }

    #endregion

#else

    void OnValidate()
      => Debug.LogError("UnityEngine.Video is missing.");

#endif
}

} // namespace Bibcam.Decoder
