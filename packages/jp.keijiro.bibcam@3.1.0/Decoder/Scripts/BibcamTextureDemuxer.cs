using UnityEngine;
using Bibcam.Common;

namespace Bibcam.Decoder {

[ExecuteInEditMode]
public sealed class BibcamTextureDemuxer : MonoBehaviour
{
    #region Editable attributes

    [SerializeField, Range(0, 8)] int _margin = 1;
        [SerializeField] bool DounDistortion = false;

    #endregion

    #region Hidden asset references

    [SerializeField] Shader _shader = null;
    //[SerializeField] Material _processingMaterial;
        //[SerializeField] bool DoEdgeDetection = true;

    #endregion

    #region Public members

    public RenderTexture ColorTexture => _color;
    public RenderTexture DepthTexture => _depth;

    public RenderTexture unDestortedColorTexture;

    public void Demux(Texture source, in Metadata meta)
    {
        // Laze initialization for the demux shader
        if (_material == null) _material = ObjectUtil.NewMaterial(_shader);

        // Lazy initialization for demuxing buffers
        var (w, h) = (source.width, source.height);
        if (_color == null) _color = GfxUtil.RGBARenderTexture(w / 2, h);
        if (DounDistortion)
            if (unDestortedColorTexture == null) unDestortedColorTexture = GfxUtil.RGBARenderTexture(w / 2, (int)(h * (540.0f / 1080.0f)));
        if (_depth == null) _depth = GfxUtil.RHalfRenderTexture(w / 2, h / 2);

        // Demux shader invocations
        _material.SetInteger(ShaderID.Margin, _margin);
        _material.SetVector(ShaderID.DepthRange, meta.DepthRange);
        Graphics.Blit(source, _color, _material, 0);
        //if (DoEdgeDetection)
        //{
        //if (_tmpdepth == null) _tmpdepth = GfxUtil.RHalfRenderTexture(w / 2, h / 2);
        //    Graphics.Blit(source, _tmpdepth, _material, 1);
        //        //Graphics.Blit(_tmpdepth, _depth, _processingMaterial); // depth edge detection etc. 
        //        _processingMaterial.mainTexture = _tmpdepth;
        //        _depth.initializationMaterial = _processingMaterial;
        //    }
        //else
        {
            Graphics.Blit(source, _depth, _material, 1);
        }
   
        //
        if(DounDistortion)
            Graphics.Blit(source, unDestortedColorTexture, _material, 2); // should be disabled to save power
    }

    #endregion

    #region Private members

    Material _material;
    RenderTexture _color;
    RenderTexture _depth;
        RenderTexture _tmpdepth;

    #endregion

        #region MonoBehaviour implementation

        void OnDestroy()
    {
        ObjectUtil.Destroy(_material);
        ObjectUtil.Destroy(_color);
        ObjectUtil.Destroy(_depth);
    }

    #endregion
}

} // namespace Bibcam.Decoder
