using UnityEditor;
using UnityEngine;
using System.IO;

public class CreateAssetBundles
{
    [MenuItem("Assets/Build AssetBundles BuildTarget.Android WithoutCompress")]
    static void BuildAllAssetBundles_Android_WithoutCompress()
    {
        if (!Directory.Exists(Application.persistentDataPath))
        {
            Directory.CreateDirectory(Application.persistentDataPath);
        }
        BuildPipeline.BuildAssetBundles(Application.persistentDataPath + "/AssetBundle_AndroidWithoutCompress", 
            BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.Android);

        Debug.Log("Baked to: " + Application.persistentDataPath);
    }

    [MenuItem("Assets/Build AssetBundles BuildTarget.Android")]
    static void BuildAllAssetBundles_Android()
    {
        if (!Directory.Exists(Application.persistentDataPath))
        {
            Directory.CreateDirectory(Application.persistentDataPath);
        }
        BuildPipeline.BuildAssetBundles(Application.persistentDataPath,
            BuildAssetBundleOptions.None, BuildTarget.Android);

        Debug.Log("Baked to: " + Application.persistentDataPath);
    }

    [MenuItem("Assets/Build AssetBundles BuildTarget.Win")]
    static void BuildAllAssetBundles_PCWin()
    {
        //
        string path = Application.persistentDataPath + "/AssetBundle_WinWithoutCompress";
        if (!Directory.Exists(path)) Directory.CreateDirectory(path);

        //
        BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.StandaloneWindows64);

        //
        Debug.Log("Baked to: " + path);
    }

}