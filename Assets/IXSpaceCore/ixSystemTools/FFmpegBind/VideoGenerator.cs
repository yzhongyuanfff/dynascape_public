// FFmpegOut - FFmpeg video encoding plugin for Unity
// https://github.com/keijiro/KlakNDI

using UnityEngine;
using System.Collections;
using System.IO;
using FFmpegOut;
using JfranMora.Inspector;
using System.Collections.Generic;

public enum VideoEncodingMode
{
    TempFrame, 
    RTArray
}

//[AddComponentMenu("FFmpegOut/Camera Capture")]
public class VideoGenerator : MonoBehaviour
{
   // Camera camera;

    #region Public properties

    [SerializeField] int ResFactor = 2; // 2k

    int _width = 1920;

    public int width
    {
        get { return _width; }
        set { _width = value; }
    }

    int _height = 1080;

    public int height
    {
        get { return _height; }
        set { _height = value; }
    }

    [SerializeField] FFmpegPreset _preset;

    public FFmpegPreset preset
    {
        get { return _preset; }
        set { _preset = value; }
    }

    [SerializeField] float _frameRate = 60;

    [SerializeField] public VideoEncodingMode videoEncodingMode = VideoEncodingMode.TempFrame;

    public float frameRate
    {
        get { return _frameRate; }
        set { _frameRate = value; }
    }

    bool DoCaptureVideo = false;

    #endregion

    #region Private members

    public FFmpegSession _session;
    public RenderTexture _tempRT;
    GameObject _blitter;

    RenderTextureFormat GetTargetFormat(Camera camera)
    {
        return camera.allowHDR ? RenderTextureFormat.DefaultHDR : RenderTextureFormat.Default;
    }

    int GetAntiAliasingLevel(Camera camera)
    {
        return camera.allowMSAA ? QualitySettings.antiAliasing : 1;
    }

    #endregion

    #region Time-keeping variables

    int _frameCount;
    float _startTime;
    int _frameDropCount;

    float FrameTime
    {
        get { return _startTime + (_frameCount - 0.5f) / _frameRate; }
    }

    void WarnFrameDrop()
    {
        if (++_frameDropCount != 10) return;

        Debug.LogWarning(
            "Significant frame droppping was detected. This may introduce " +
            "time instability into output video. Decreasing the recording " +
            "frame rate is recommended."
        );
    }

    #endregion

    #region MonoBehaviour implementation

    void OnValidate()
    {
        _width = Mathf.Max(8, _width);
        _height = Mathf.Max(8, _height);
    }

    void OnDisable()
    {
        if (_session != null)
        {
            // Close and dispose the FFmpeg session.
            _session.Close();
            _session.Dispose();
            _session = null;
        }

        if (_tempRT != null)
        {
            // Dispose the frame texture.
           // GetComponent<Camera>().targetTexture = null;
            Destroy(_tempRT);
            _tempRT = null;
        }

        if (_blitter != null)
        {
            // Destroy the blitter game object.
            Destroy(_blitter);
            _blitter = null;
        }
    }

    IEnumerator Start()
    {
        // Sync with FFmpeg pipe thread at the end of every frame.
        for (var eof = new WaitForEndOfFrame(); ;)
        {
            yield return eof;
            _session?.CompletePushFrames();
        }

        //camera = GetComponent<Camera>();
    }

    private Color[] GenerateGridPatternColors(int width, int height, int gridSize, Color color1, Color color2)
    {
        Color[] colors = new Color[width * height];
        int numGridX = Mathf.CeilToInt((float)width / gridSize);
        int numGridY = Mathf.CeilToInt((float)height / gridSize);

        for (int y = 0; y < height; y++)
        {
            int gridY = y / gridSize;

            for (int x = 0; x < width; x++)
            {
                int gridX = x / gridSize;
                bool isEvenGrid = (gridX + gridY) % 2 == 0;
                colors[y * width + x] = isEvenGrid ? color1 : color2;
            }
        }

        return colors;
    }

    [Header("///")]
    public int gridSize = 8;
    public Color color1 = Color.white;
    public Color color2 = Color.black;


    [Header("///")]
    public int numBars = 50;
    public Color barColor = Color.black;
    public Color backgroundColor = Color.white;
    public bool randomizeBarWidths = true;

    private Texture2D barcodeTexture;

    [SerializeField] List<RenderTexture> RTs = new List<RenderTexture>();

    [SerializeField] int NumTexturesForTesting = 900;

    void UpdateRenderTexture()
    {

        _tempRT = new RenderTexture(_width, _height, 0); // 24?
                                                         //Debug.Log("RenderTexture Created, with resolution: " + _width + "x" + _height);
        _tempRT.Create();

        // Set the RenderTexture as the active RenderTexture
        RenderTexture.active = _tempRT;

        //
       // GenerateBarcode();

        // Generate the grid pattern colors and apply them to the RenderTexture
        Texture2D texture = new Texture2D(width, height);
        Color[] colors = GenerateGridPatternColors(width, height, gridSize, color1, color2);
        texture.SetPixels(colors);
        texture.Apply();

        // Assign the generated texture to the RenderTexture
        Graphics.Blit(texture, _tempRT);

        // Reset the active RenderTexture
        RenderTexture.active = null;
    }

    public void StopRecording()
    {
        if (_session != null)
        {
            _session.CompletePushFrames();
            // Close and dispose the FFmpeg session.
            _session.Close();
            _session.Dispose();
            _session = null;
        }

        Debug.Log("Done. Video File Written To Disk.");
    }
    void InsertFrame()
    {
        var gap = Time.time - FrameTime;
        var delta = 1 / _frameRate;

        // 
        UpdateRenderTexture();

        if (gap < 0)
        {
            // Update without frame data.
            _session.PushFrame(null);
        }
        else if (gap < delta)
        {
            // Single-frame behind from the current time:
            // Push the current frame to FFmpeg.
            _session.PushFrame(_tempRT);
            _frameCount++;
        }
        else if (gap < delta * 2)
        {
            // Two-frame behind from the current time:
            // Push the current frame twice to FFmpeg. Actually this is not
            // an efficient way to catch up. We should think about
            // implementing frame duplication in a more proper way. #fixme
            _session.PushFrame(_tempRT);
            _session.PushFrame(_tempRT);
            _frameCount += 2;
        }
        else
        {
            // Show a warning message about the situation.
            WarnFrameDrop();

            // Push the current frame to FFmpeg.
            _session.PushFrame(_tempRT);

            // Compensate the time delay.
            _frameCount += Mathf.FloorToInt(gap * _frameRate);
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////
    [Button]
    void GenerateRTs()
    {
        RTs.Clear();

        for (int i = 0; i < NumTexturesForTesting; i++)
        {
            UpdateRenderTexture();
            RTs.Add(_tempRT);
        }
    }

    [Button]
    public void CreateSession()
    {
        _width = ResFactor * 540 / 9 * 16;
        _height = ResFactor * 540;

        // Lazy initialization
        //if (_session == null)
        {
            //UpdateRenderTexture();

            // filename 
            string folder = Application.persistentDataPath + "/Captures/";
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            Debug.Log("Writing Video File To: " + folder);

            // create session 
            _session = FFmpegSession.Create(
                folder + gameObject.name,
                _width,
                _height,
                _frameRate, preset
            );

            //_startTime = Time.time;
            //_frameCount = 0;
            //_frameDropCount = 0;
        }
    }

    [Button]
    public void StartPushingFrames()
    {
        //for (int i = 0; i < NumTexturesForTesting; i++)
        //{
        //    _session.PushFrame(RTs[i]);
        //    _frameCount++;
        //}

        //Debug.Log("All Frames Pushed! Num = " + NumTexturesForTesting);

        DoPushFrames = true;
        idx = 0;
    }

    bool DoPushFrames = false;
    int idx = 0;

    void Update()
    {
        //if (DoCaptureVideo) 
        //    InsertFrame();

        if (DoPushFrames)
        {
            //
            if (videoEncodingMode == VideoEncodingMode.RTArray) 
            {
                if (idx < NumTexturesForTesting)
                {
                    _session.PushFrame(RTs[idx]);
                    idx++;
                    Debug.Log("Pushing Frame: " + idx);
                }else
                {
                    DoPushFrames = false;
                    StopRecording();
                }
            }

            ////
            //if (videoEncodingMode == VideoEncodingMode.TempFrame)
            //{
            //    _session.PushFrame(_tempRT);
            //    Debug.Log("Pushing Frame! ");
            //}
        }
    }

    #endregion
}