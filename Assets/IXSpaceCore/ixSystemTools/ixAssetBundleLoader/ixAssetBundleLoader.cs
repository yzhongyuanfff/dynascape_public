// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ixAssetBundleLoader : MonoBehaviour
{

    private string loadSceneName;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(LoadSceneFromBundle(BundleFileName));
    }

    /// <summary>
    /// 
    /// </summary>
    public void LoadActiveSceneBundle()
    {
        if (!GameObject.FindGameObjectWithTag("StringManager"))
            return;

        string fn = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
        LogMe("Loading Asset From File: " + fn);
        StartCoroutine(LoadSceneFromBundle(fn));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator LoadSceneFromBundle(string fn)
    {
        AssetBundleCreateRequest asyncBundleRequest = AssetBundle.LoadFromFileAsync(fn);
        yield return asyncBundleRequest;

        AssetBundle localAssetBundle = asyncBundleRequest.assetBundle;

        if (localAssetBundle == null)
        {
            LogMe("Failed to load AssetBundle! localAssetBundle = null");
            yield break;
        }

        if (localAssetBundle.isStreamedSceneAssetBundle)
        {
            string[] scenePaths = localAssetBundle.GetAllScenePaths();
            string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
            loadSceneName = sceneName;
            LogMe("Loading Scene: " + sceneName);
            SceneManager.LoadSceneAsync(loadSceneName, LoadSceneMode.Additive);
        }

        localAssetBundle.Unload(false);
        LogMe("Scene Loaded!");
    }

    public void UnloadJustLoadScene()
    {
        LogMe("Removing Scene: " + loadSceneName + " with UnloadSceneAsync()");
        if (loadSceneName!="")
            SceneManager.UnloadSceneAsync(loadSceneName,UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator LoadAssetFromBundle(string fn, string assetName)
    {
        AssetBundleCreateRequest asyncBundleRequest = AssetBundle.LoadFromFileAsync(fn);
        yield return asyncBundleRequest;

        AssetBundle localAssetBundle = asyncBundleRequest.assetBundle;

        if (localAssetBundle == null)
        {
            LogMe("Failed to load AssetBundle! localAssetBundle = null");
            yield break;
        }

        AssetBundleRequest assetRequest = localAssetBundle.LoadAssetAsync<GameObject>(assetName);
        yield return assetRequest;

        GameObject prefab = assetRequest.asset as GameObject;
        Instantiate(prefab);

        localAssetBundle.Unload(false);
        LogMe("AssetBundle Load!");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }
}
