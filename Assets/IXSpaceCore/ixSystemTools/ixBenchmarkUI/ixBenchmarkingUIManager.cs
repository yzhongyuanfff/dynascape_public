// ------------------------------------------------------------------------------------
// <copyright company="Technische Universitšt Dresden">
//      Copyright (c) Technische Universitšt Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

//using SharpPcap;
//using SharpPcap.LibPcap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum Space
{
    Corridor,
    Stairs,
    Foyer
}

public enum Pattern
{
    SmallArea,
    BackAndForth,
    RectangleTest,
    Predefined
}

public enum Speed
{
    Fast,
    Slow
}

public enum HeadMovement
{
    LookingAround,
    LookingStraight
}

public enum RuntimeOptions
{
    KeepRunning,
    QuitApp,
    RefreshGuardian
}

public enum NumOfSpatialAnchors
{
    Only1,
    Sparse,
    Dense
}

public enum CorrectOption
{
    WithCorrection,
    WithoutCorrection
}

public class Condition
{
    public Space space;
    public Pattern pattern;
    public Speed speed;
    public HeadMovement headmovement;
    public RuntimeOptions runtimeoptions;
    public NumOfSpatialAnchors numofspatialanchors;
    public CorrectOption correctoption;
}

// Write a function & reflect it to UI 
public class ixBenchmarkingUIManager : MonoBehaviour
{
    [SerializeField] GameObject Content;

    List<Tuple<Condition, float, float>> data = new List<Tuple<Condition, float, float>>();
    Condition condition = new Condition();
    List<Type> conditionEnums = new List<Type>();

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
        InitUI();
    }

    void ShowCondition()
    {
        foreach (FieldInfo  fi in condition.GetType().GetFields())
        {
            ixUtility.LogMe(fi.GetValue(condition).ToString());
        }
    }

    void InitUI()
    {
        conditionEnums.Add(typeof(Space));
        conditionEnums.Add(typeof(Pattern));
        conditionEnums.Add(typeof(Speed));
        conditionEnums.Add(typeof(HeadMovement));
        conditionEnums.Add(typeof(RuntimeOptions));
        conditionEnums.Add(typeof(NumOfSpatialAnchors));
        conditionEnums.Add(typeof(CorrectOption));

        foreach (Type type in conditionEnums)
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("UIConditionOptionPrefeb", 
                typeof(GameObject)), Content.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = type.Name;

            List<GameObject> conditionBtnList = new List<GameObject>();

            foreach (int i in Enum.GetValues(type))
            {
                GameObject uibtn = Instantiate((GameObject)Resources.Load("UIBtn",
                    typeof(GameObject)), ui.transform);
                uibtn.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = Enum.GetName(type, i);

                // init the btn toggle state 
                if ((int)condition.GetType().GetField(type.Name.ToLower()).GetValue(condition) == i)
                    uibtn.transform.Find("ToggleState").gameObject.SetActive(true);
                else
                    uibtn.transform.Find("ToggleState").gameObject.SetActive(false);

                // call back 
                uibtn.GetComponent<Button>().onClick.AddListener(delegate {
                    condition.GetType().GetField(type.Name.ToLower()).SetValue(condition, i);// cast?
                    uibtn.transform.Find("ToggleState").gameObject.SetActive(true);
                    ShowCondition();
                });
                conditionBtnList.Add(uibtn);
            }

            foreach(GameObject currentBtn in conditionBtnList)// disable the other btns 
            {
                currentBtn.GetComponent<Button>().onClick.AddListener(delegate {
                    foreach (GameObject btn in conditionBtnList) {
                        if (!btn.Equals(currentBtn))
                        {
                            btn.transform.Find("ToggleState").gameObject.SetActive(false);
                        }
                    }
                });
            }
        }
    }
}
