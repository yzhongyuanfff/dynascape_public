using Klak.Ndi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ixNDIController : MonoBehaviour
{
    [HideInInspector] public RenderTexture RTToSend;
    WebCamTexture webcamtex;

    NdiSender sender;
    VideoPlayer videoplayer;

    public void SetNdiTex_WebcamTex()
    {
        WebCamDevice[] devices = WebCamTexture.devices;

        // for debugging purposes, prints available devices to the console
        for (int i = 0; i < devices.Length; i++)
        {
            ixUtility.LogMe("Webcam available: " + devices[i].name);
        }
        ixUtility.LogMe("num Of Webcam available: " + devices.Length);

        //
        if (webcamtex)
        {
            webcamtex.Stop(); // remember to stop before start! 
            Destroy(webcamtex);
        }

        webcamtex = new WebCamTexture(devices[0].name);
        webcamtex.Play();

        // For texture size info
        RTToSend = new RenderTexture(webcamtex.width, webcamtex.height, 0);

        sender.sourceTexture = webcamtex;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="url"></param>
    public void SetNdiTex_VideoTexFromUrl(string url)
    {
        videoplayer.url = url;
            //GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
        videoplayer.prepareCompleted += Videoplayer_prepareCompleted;
        videoplayer.isLooping = true;
        videoplayer.source = VideoSource.Url;
        videoplayer.renderMode = VideoRenderMode.RenderTexture;
        videoplayer.Prepare();
    }

    private void Videoplayer_prepareCompleted(VideoPlayer source)
    {
        RTToSend = new RenderTexture((int)videoplayer.width, (int)videoplayer.height, 0);
        videoplayer.targetTexture = RTToSend;

        //
        sender.sourceTexture = RTToSend;

        //
        videoplayer.Play();
    }

    void FindComponnets()
    {
        sender = GetComponent<NdiSender>();
        videoplayer = GetComponent<VideoPlayer>();
    }


    // Start is called before the first frame update
    void Start()
    {
        FindComponnets();
        SetNdiTex_WebcamTex();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
