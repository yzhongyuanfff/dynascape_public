using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Klak.Ndi;
using System.Linq;
using System;
using Klak.Ndi.Interop;
using TMPro;
using UnityEngine.UI;

public class ixNDIReceiverController : MonoBehaviour
{

    public GameObject Content;
    public GameObject DecoderControlPanelContent;

    NdiReceiver receiver;
    //List<string> _sourceNames;
    //List<Source> sources;
    List<GameObject> sourceUIList;
    List<Tuple<string, Action<GameObject>>> featureList;


    /// <summary>
    /// 
    /// </summary>
    public void ListNdiSources()
    {
        //_sourceNames.Clear();
        //_sourceNames = NdiFinder.sourceNames.ToList();
        //foreach(var sn in _sourceNames)
        //{
        //    ixUtility.LogMe("Ndi Sender Found!: Name = " + sn);
        //}
        //sources = NdiFinder.EnumerateSources();
        foreach (var ui in sourceUIList) Destroy(ui);
        sourceUIList.Clear();

        var sources = NdiFinder.sourceNames;
        foreach (var s in sources)
        {
            ixUtility.LogMe("Source found: " + s);

            GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), Content.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = s;
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { SetNdiSource(s); });
            sourceUIList.Add(ui);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    void SetNdiSource(string name)
    {
        receiver.ndiName = name;
        ixUtility.LogMe("Setting Ndi source to: " + receiver.ndiName);
    }

    /// <summary>
    /// 
    /// </summary>
    public void TrySetNdiSourceAsTheFirstFound()
    {
        List<string> sources = (List<string>)NdiFinder.sourceNames;
        if (sources.Count > 0)
        {
            receiver.ndiName = sources[0];
            ixUtility.LogMe("Setting Ndi source to: "  + receiver.ndiName);
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void ToggleDecodingNewFrame(GameObject ui)
    {
        receiver.ToggleReceiving();
        ixUtility.LogMe("Receving: " + receiver.GetRecvState());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void IncreaseFrameInterval(GameObject ui)
    {
        receiver.SetFrameInterval(receiver.GetFrameInterval() + 1);
        ixUtility.LogMe("IncreaseFrameInterval, interval = " + receiver.GetFrameInterval());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void DecreaseFrameInterval(GameObject ui)
    {
        receiver.SetFrameInterval(receiver.GetFrameInterval() - 1);
        ixUtility.LogMe("DecreaseFrameInterval, interval = " + receiver.GetFrameInterval());
    }

    // Start is called before the first frame update
    void Start()
    {
        //sources = new List<Source>();
        receiver = GetComponent<NdiReceiver>();
        sourceUIList = new List<GameObject>();
        ListNdiSources();
        TrySetNdiSourceAsTheFirstFound();

        //
        featureList = new List<Tuple<string, Action<GameObject>>>();
        featureList.Add(new Tuple<string, Action<GameObject>>("ToggleDecodingNewFrame", ToggleDecodingNewFrame));
        featureList.Add(new Tuple<string, Action<GameObject>>("IncreaseFrameInterval", IncreaseFrameInterval));
        featureList.Add(new Tuple<string, Action<GameObject>>("DecreaseFrameInterval", DecreaseFrameInterval));

        //
        foreach (var t in featureList)
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), DecoderControlPanelContent.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = t.Item1;
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { t.Item2(ui); });
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
