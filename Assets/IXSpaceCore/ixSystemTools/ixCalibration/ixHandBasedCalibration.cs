// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ixHandBasedCalibration : MonoBehaviour
{
    GameObject rwirst;
    GameObject lwirst;

    // Start is called before the first frame update
    void Start()
    {
        rwirst = GameObject.FindGameObjectWithTag("RHand").transform.FindChildRecursive("b_l_wrist").gameObject;
        lwirst = GameObject.FindGameObjectWithTag("LHand").transform.FindChildRecursive("b_r_wrist").gameObject;
        // leftWrist = OvrReferenceManager.Instance.LeftHandVisual.transform.FindChildRecursive("b_l_wrist").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        // GameObject.FindGameObjectWithTag("RHand").GetComponent<OVRHand>().HandConfidence
    }
}
