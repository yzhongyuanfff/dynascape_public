using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ixSpaceExploreManager : MonoBehaviour
{
    [Header("----------")]
    [SerializeField] GameObject UiTitle;
    [SerializeField] GameObject UIContent;
    [SerializeField] GameObject SceneGeometry;
    [SerializeField] GameObject MainCalibrationPoint;

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
        SceneGeometry.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitUI()
    {
        if(UiTitle.GetComponent<TextMeshProUGUI>())
            UiTitle.GetComponent<TextMeshProUGUI>().text = this.name;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ixUtility.MakeSepLine(UIContent, "------IXModel Alignment------");
        ixUtility.MakeAButton(UIContent,
            "Align To The First Avaliable Spatial Anchor",
            delegate {

                List<GameObject> SAGameObjects = GameObject.FindGameObjectWithTag("SpatialAnchorManager").GetComponent<SpatialAnchorManager>().GetAllSpatialAnchorRefs();

                bool foundSAGameObject = false;
                GameObject SAObject = null;
                foreach(GameObject o in SAGameObjects)
                {
                    if(o.GetComponent<SpatialAnchorPropertyManager>().anchorUsage == AnchorUsage.Calibration)
                    {
                        SAObject = o;
                        foundSAGameObject = true;
                    }
                }

                if (foundSAGameObject)
                {
                    // Calculate the offset needed to align the child object
                    //Vector3 offset = SAGameObjects[0].transform.position - SceneGeometry.transform.TransformPoint(MainCalibrationPoint.transform.localPosition);
                    //Quaternion rotationOffset = SAGameObjects[0].transform.rotation * Quaternion.Inverse(SceneGeometry.transform.rotation * MainCalibrationPoint.transform.localRotation);

                    //SceneGeometry.transform.position += offset;
                    //SceneGeometry.transform.rotation *= rotationOffset;

                    ixUtility.AlignGameObjectWithChild(SceneGeometry, MainCalibrationPoint, SAObject);
                }
            });
        ixUtility.MakeAButton(UIContent, 
            "Show/ Hide Scene Geometry", 
            delegate {
                SceneGeometry.SetActive(!SceneGeometry.activeSelf);
            });
    }
}
