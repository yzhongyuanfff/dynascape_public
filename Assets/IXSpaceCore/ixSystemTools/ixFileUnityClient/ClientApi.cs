﻿// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Photon.Pun;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ClientApi : MonoBehaviour
{
    [HideInInspector] public string url;
    public GameObject parent;
    public GameObject filterOptionContent;

    JSONNode fileList;
    List<UnityWebRequest> rqlist;
    List<GameObject> uiList;

    bool isEnabled;

    ixUtility.FilterOption filterOption = ixUtility.FilterOption.ALL;

    /// <summary>
    /// 
    /// </summary>
    void Start()
    {
        url = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().FileListServerPath;
        rqlist = new List<UnityWebRequest>();
        uiList = new List<GameObject>();

        //
        InitializeFileExtensions();

        //
        filterOption = ixUtility.FilterOption.ALL;
        TryFetchFileListFromServer();
    }

    /// <summary>
    /// 
    /// </summary>
    public void EnableDel()
    {
        isEnabled = !isEnabled;

        foreach (GameObject ui in uiList)
        {
            ui.transform.GetComponent<FileDownloader>().EnableDel = isEnabled;
            ui.transform.GetComponent<FileDownloader>().RefreshUI();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="this_ui"></param>
    void OnActiveClicked(GameObject this_ui)
    {
        foreach (GameObject ui in uiList)
            ui.GetComponent<FileDownloader>().SetInActive();
        this_ui.GetComponent<FileDownloader>().SetActive();
    }

    void OnSetLinkClicked(GameObject this_ui)
    {
        foreach (GameObject ui in uiList)
            ui.GetComponent<FileDownloader>().SetUnused();
        this_ui.GetComponent<FileDownloader>().SetUseLink();
    }

    void ClearUI()
    {
        foreach (GameObject u in uiList)
            GameObject.Destroy(u);
        uiList.Clear();
    }

    List<Tuple<string,ixUtility.FilterOption>> fileExtensions = new List<Tuple<string, ixUtility.FilterOption>>();

    void InitializeFileExtensions()
    {
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".", ixUtility.FilterOption.ALL));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".bytes", ixUtility.FilterOption.Bytes));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".mp4", ixUtility.FilterOption.MP4));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".mov", ixUtility.FilterOption.MOV));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>("Selected", ixUtility.FilterOption.SelectedClips));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".ply", ixUtility.FilterOption.PLY));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".csv", ixUtility.FilterOption.CSV));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".txt", ixUtility.FilterOption.TXT));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".winab", ixUtility.FilterOption.WinAB));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".androidab", ixUtility.FilterOption.AndroidAB));
        fileExtensions.Add(new Tuple<string, ixUtility.FilterOption>(".zip", ixUtility.FilterOption.ZIP));

        AddFilterOptionsToUI();
    }

    void AddFilterOptionsToUI()
    {
        foreach(var extb in fileExtensions)
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("UITextedBtn", typeof(GameObject)), filterOptionContent.transform);

            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = extb.Item1;
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
                filterOption = extb.Item2;
                TryFetchFileListFromServer();
            });
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void UpdateUI()
    {
        string FolderAddress = Application.persistentDataPath + 
            GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().RelativeDataFolderPath;

        if (!Directory.Exists(FolderAddress))
        {
            Directory.CreateDirectory(FolderAddress);
        }

        foreach (GameObject u in uiList)
            GameObject.Destroy(u);
        uiList.Clear();

        ixUtility.LogMe("Listing Files with: " + filterOption);

        for (int i = 0; i < fileList.Count; i++)
        {
            /*filtering*/
            string ext = Path.GetExtension(fileList[i][1]);

            // check extension 
            if (filterOption == ixUtility.FilterOption.ALL) 
            {
                // list all 
            } else if (filterOption == ixUtility.FilterOption.SelectedClips)
            {
                string httpaddress = fileList[i][0];
                if (httpaddress.StartsWith("http://141.76.64.17:8080/InAppAssetRepo/MediaExplore/SelectedClips")) { 
                    // list 
                } else
                {
                    continue;
                }
            } else
            {
                bool foundExtFilter = false;
                bool ignoreMe = false;
                foreach (var extb in fileExtensions)
                {
                    if (filterOption == extb.Item2)
                    {
                        foundExtFilter = true;
                        if (ext.Equals(extb.Item1) || ext.Equals(extb.Item1.ToUpper())) 
                        { 
                            // show 
                        } 
                        else
                        {
                            ignoreMe = true;
                            break; // ignore
                        }
                    }
                }
                if (!foundExtFilter)
                    continue;
                if (foundExtFilter && ignoreMe)
                    continue;
                if(foundExtFilter && !ignoreMe) { } // continue the following part
            }


            // link, name, size
            //GameObject ui = PhotonNetwork.Instantiate("UIDownloadBtnPrefeb", Vector3.zero, Quaternion.identity); // to get proper photon id 
            //ui.transform.SetParent(parent.transform);
            GameObject ui = Instantiate((GameObject)Resources.Load("UIDownloadBtnPrefeb", typeof(GameObject)), parent.transform);

            ui.transform.Find("ChkBtn").GetComponent<Button>().onClick.AddListener(delegate { OnActiveClicked(ui); });
            ui.transform.Find("SetBtn").GetComponent<Button>().onClick.AddListener(delegate { OnSetLinkClicked(ui); });

            ui.transform.Find("FileLink").GetComponent<TextMeshProUGUI>().text = fileList[i][0];
            ui.transform.Find("Percentage").GetComponent<TextMeshProUGUI>().text = "0%";
            ui.transform.Find("Size").GetComponent<TextMeshProUGUI>().text = fileList[i][2] + " MB";

            ui.transform.GetComponent<FileDownloader>().FileID = i;
            ui.transform.GetComponent<FileDownloader>().HttpAddress = fileList[i][0];
            ui.transform.GetComponent<FileDownloader>().DiskAddress = FolderAddress + fileList[i][1];
            ui.transform.GetComponent<FileDownloader>().FileNameWithoutExtension = fileList[i][1];
            ui.transform.GetComponent<FileDownloader>().FolderAddress = FolderAddress;
            ui.transform.GetComponent<FileDownloader>().RefreshUI();

            uiList.Add(ui);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void Update()
    {
        int rqid = 0;
        foreach(var rq in rqlist)
        {
            if(rq.downloadProgress!=1)
                Debug.Log("rq.downloadProgress of rqid " + rqid + " :" + rq.downloadProgress);

            rqid++;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void TryFetchFileListFromServer()
    {
        fileList = null;
        ClearUI();
        StartCoroutine(Get(url));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    IEnumerator Get(string url)
    {
        using(UnityWebRequest www = UnityWebRequest.Get(url)){

            //www.timeout = 5;
            www.useHttpContinue = false;
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                Debug.Log("URL = " + url);
                www.Dispose();
            }
            else
            {
                if (www.isDone)
                {
                    var result = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    fileList = JSON.Parse(result);
                    UpdateUI();
                }
                else
                {
                    //handle the problem
                    Debug.Log("Error! data couldn't get.");
                }
            }
        }
        
    }
}
