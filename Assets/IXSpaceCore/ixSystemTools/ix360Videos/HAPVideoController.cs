// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Klak.Hap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HAPVideoController : MonoBehaviour
{
    public GameObject samplePrefeb;
    public bool fbVisSamplePoints = false;
    List<Vector3> _sample_points;
    HapPlayer _video = null;
    public GameObject _videoHolder;
    public Vector3 snapOffset;

    public long targetFrame = 100;
    public float Framerate = 30;
    public bool enableSnapping = false;
    public GameObject snapTarget_NN;
    long lastframe = 100;
    public bool bGoToTargetFrame = false;

    public float totalMeters = 5;
    public float speed = 2.5f;// sample points per meter = fps * speed
    public float ImmersionRange = 0.4f;
    public float ImmersionScale = 2;
    public float FarScale = 0.5f;

    //
    public RenderTexture rt;
    public Renderer targetRenderer;
    public string targetMaterialProperty = "_BaseMap";

    void CreateHAPComponent()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        //_video = GetComponent<HapPlayer>();
        //CreateHAPComponent();

        if (snapTarget_NN == null)
            snapTarget_NN = Camera.main.gameObject;

        float density = Framerate * speed;
        _sample_points = new List<Vector3>();
        for (int i = 0; i < density * totalMeters; i++)
        {
            _sample_points.Add(this.transform.localPosition - new Vector3(i / density, 0, 0));
        }
    }

    public void OpenActiveVideo()
    {
        string fpath = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curActiveFilePath;

        if (_video) Destroy(_video);

        _video = gameObject.AddComponent<HapPlayer>();
        _video.targetTexture = rt;
        _video.targetRenderer = targetRenderer;
        _video.targetMaterialProperty = targetMaterialProperty;
        _video._filePath = fpath;
        _video._pathMode = HapPlayer.PathMode.LocalFileSystem;
        _video.speed = 0;

        _video.ClearMessage();
        //_video.Open(fpath,HapPlayer.PathMode.LocalFileSystem); // Do Not Call 
        //LogMe("_video.Message: " + _video.Message);

        //LogMe("_video.CheckIfDemuxerWorks(): " + _video.CheckIfDemuxerWorks().ToString());
        //LogMe("_video.CheckIfStreamWorks(): " + _video.CheckIfStreamWorks().ToString());
        //CreateHAPComponent();
        //if (_video) _video.Open(activeFile, HapPlayer.PathMode.LocalFileSystem);
    }

    public void LogInternalMessage()
    {
       ixUtility.LogMe("_video.Message: " + _video.Message);
    }

    // Update is called once per frame
    void Update()
    {
        if (bGoToTargetFrame)
        {
            bGoToTargetFrame = false;
            float targetTime = targetFrame / Framerate;
            if(_video) _video.time = targetTime;
            Debug.Log("seeking!");
        }

        if (enableSnapping)
        {
            int nearestFrameIdx = 0;
            float mindist = float.MaxValue;
            int idx = 0;
            Vector3 snapPosition = Vector3.zero;
            foreach (var p in _sample_points)
            {
                float d = Vector3.Distance(p, this.transform.InverseTransformPoint(snapTarget_NN.transform.position));
                if (d < mindist)
                {
                    mindist = d;
                    nearestFrameIdx = idx;
                    snapPosition = p;
                }
                idx++;
            }

            targetFrame = nearestFrameIdx;
            float targetTime = targetFrame / Framerate;
            if (_video) _video.time = targetTime;
            _videoHolder.transform.position = this.transform.TransformPoint(snapPosition + snapOffset);

            //Debug.Log("Num of frames = " + _video.frameCount);

            if (mindist > ImmersionRange)
                targetRenderer.transform.localScale = new Vector3(FarScale, FarScale, FarScale);
            else
                targetRenderer.transform.localScale = new Vector3(ImmersionScale, ImmersionScale, ImmersionScale);
        }

        if (fbVisSamplePoints)
        {
            fbVisSamplePoints = false;

            foreach (var p in _sample_points)
            {
                Instantiate(samplePrefeb, this.transform.TransformPoint(p), Quaternion.identity);
            }
        }
    }

    float _PlaybackSpeed = 1;
    ulong _FrameStart = 0;

    public void PlayTheVideo()
    {
        _video.time = _FrameStart / Framerate;
        _video.speed = _PlaybackSpeed;
    }

    public void PauseTheVideo()
    {
        _video.speed = 0;
    }
    public void ResumeTheVideo()
    {
        _video.speed = _PlaybackSpeed;
    }

    public void ToggleSnap()
    {
        enableSnapping = !enableSnapping;
    }
}
