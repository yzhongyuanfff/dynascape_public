// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoFrameControl360 : MonoBehaviour
{
    public GameObject samplePrefeb;
    public bool fbVisSamplePoints = false;
    List<Vector3> _sample_points;
    VideoPlayer _video;

    public long targetFrame = 100;
    public bool enableSnapping = false;
    public GameObject snapTarget;
    long lastframe = 100;
    public bool bGoToTargetFrame = false;

    // Start is called before the first frame update
    void Start()
    {
        _video = GetComponent<VideoPlayer>();
        _video.frameReady += OnFrameReady;
        _video.sendFrameReadyEvents = true;

        _sample_points = new List<Vector3>();
        float meters = 5;
        float density = 30;
        for (int i = 0; i < density * meters; i++) 
        {
            _sample_points.Add(this.transform.position + new Vector3(i / density, 0, 0));
        }
    }

    private void OnFrameReady(VideoPlayer source, long frameIdx)
    {
        Debug.Log("Frame " + frameIdx + " ready!");
        //_video.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        if (bGoToTargetFrame)
        {
            bGoToTargetFrame = false;
            _video.frame = targetFrame;
            _video.Play();
            Debug.Log("_video.frame = " + _video.frame);
        }

        if (enableSnapping) {
            int nearestFrameIdx = 0;
            float dist = float.MaxValue;
            int idx = 0;
            foreach (var p in _sample_points)
            {
                float d = Vector3.Distance(p, snapTarget.transform.position);
                if (d < dist)
                {
                    dist = d;
                    nearestFrameIdx = idx;
                }
                idx++;
            }

            targetFrame = nearestFrameIdx;
            _video.frame = targetFrame;
            if (targetFrame != lastframe)
                _video.Play();
            lastframe = _video.frame;
        }

        if (fbVisSamplePoints)
        {
            fbVisSamplePoints = false;
            
            foreach(var p in _sample_points)
            {
                Instantiate(samplePrefeb,p,Quaternion.identity);
            }
        }
    }
}
