Shader "Hidden/Bibcam/Demux"
{
    Properties
    {
        _MainTex("", 2D) = "black"{}
    }

    CGINCLUDE

#include "UnityCG.cginc"
#include "Packages/jp.keijiro.bibcam/Common/Shaders/Common.hlsl"

Texture2D _MainTex;
uint _Margin;
float2 _DepthRange;
float edge = 0.1; 

void VertexColor(float4 position : POSITION,
                 float2 texCoord : TEXCOORD,
                 out float4 outPosition : SV_Position,
                 out float4 outTexCoord : TEXCOORD)
{
    outPosition = UnityObjectToClipPos(position);
    outTexCoord = texCoord.xyxy * BibcamFrameSize.xyxy / float4(2, 1, 2, 2);
    outTexCoord.x += BibcamFrameSize.x / 2;
}

float4 FragmentColor(float4 position : SV_Position,
                     float4 texCoord : TEXCOORD0) : SV_Target
{
    float3 c = _MainTex[texCoord.xy].rgb;
    float  s = _MainTex[texCoord.zw].r;
    #ifndef UNITY_NO_LINEAR_COLORSPACE
    s = LinearToGammaSpace(s);
    #endif
    s = saturate(lerp(-0.1, 1, s)); // Compression noise filter
    return float4(c, s);
}
void VertexColorU(float4 position : POSITION,
    float2 texCoord : TEXCOORD,
    out float4 outPosition : SV_Position,
    out float4 outTexCoord : TEXCOORD)
{
    outPosition = UnityObjectToClipPos(position);
    outTexCoord = texCoord.xyxy * BibcamFrameSize.xyxy / float4(2, 1, 2, 2);
    outTexCoord.x += BibcamFrameSize.x / 2;
}

float4 FragmentColorU(float4 position : SV_Position,
    float4 texCoord : TEXCOORD0) : SV_Target
{
    float3 c = _MainTex[texCoord.xy].rgb;
    float  s = _MainTex[texCoord.zw].r;
    #ifndef UNITY_NO_LINEAR_COLORSPACE
    s = LinearToGammaSpace(s);
    #endif
    s = saturate(lerp(-0.1, 1, s)); // Compression noise filter
    return float4(c, s);
}

void VertexDepth(float4 position : POSITION,
                 float2 texCoord : TEXCOORD,
                 out float4 outPosition : SV_Position,
                 out float2 outTexCoord : TEXCOORD)
{
    outPosition = float4(position.x * 2 - 1, 1 - position.y * 2, 1, 1);
    outTexCoord = texCoord * BibcamFrameSize / 2;
    outTexCoord.y += BibcamFrameSize.y / 2;
}

float4 FragmentDepth(float4 position : SV_Position,
                     float2 texCoord : TEXCOORD) : SV_Target
{
    uint2 dtc = texCoord;
    dtc.x = min(dtc.x, BibcamFrameSize.x / 2 - 1 - _Margin);
    dtc.y = max(dtc.y, BibcamFrameSize.y / 2 + _Margin); 

    uint2 ctc = texCoord.xy * BibcamFrameSize.xy / float2(2, 1);
    ctc.x += BibcamFrameSize.x / 2;
    float3 crgb = _MainTex[ctc].rgb;

    float x = 0;
    float y = 0;
    uint2 texelSize = uint2(5,5);
    uint2 uv = ctc;

    x += _MainTex[uv + uint2(-texelSize.x, -texelSize.y)].r * -1.0;
    x += _MainTex[uv + uint2(-texelSize.x, 0)].r * -2.0;
    x += _MainTex[uv + uint2(-texelSize.x, texelSize.y)].r * -1.0;

    x += _MainTex[uv + uint2(texelSize.x, -texelSize.y)].r * 1.0;
    x += _MainTex[uv + uint2(texelSize.x, 0)].r * 2.0; //
    x += _MainTex[uv + uint2(texelSize.x, texelSize.y)].r * 1.0; // 

    y += _MainTex[uv + uint2(-texelSize.x, -texelSize.y)].r * -1.0; // 
    y += _MainTex[uv + uint2(0, -texelSize.y)].r * -2.0; // 
    y += _MainTex[uv + uint2(texelSize.x, -texelSize.y)].r * -1.0; // 

    y += _MainTex[uv + uint2(-texelSize.x, texelSize.y)].r * 1.0; // 
    y += _MainTex[uv + uint2(0, texelSize.y)].r * 2.0; // 
    y += _MainTex[uv + uint2(texelSize.x, texelSize.y)].r * 1.0; // 

    /*x += _MainTex[uv + float2(-texelSize.x, -texelSize.y)].r * -1.0;
    x += tex2D(_MainTex, uv + float2(-texelSize.x, 0)) * -2.0;
    x += tex2D(_MainTex, uv + float2(-texelSize.x, texelSize.y)) * -1.0;

    x += tex2D(_MainTex, uv + float2(texelSize.x, -texelSize.y)) * 1.0;
    x += tex2D(_MainTex, uv + float2(texelSize.x, 0)) * 2.0;
    x += tex2D(_MainTex, uv + float2(texelSize.x, texelSize.y)) * 1.0;

    y += tex2D(_MainTex, uv + float2(-texelSize.x, -texelSize.y)) * -1.0;
    y += tex2D(_MainTex, uv + float2(0, -texelSize.y)) * -2.0;
    y += tex2D(_MainTex, uv + float2(texelSize.x, -texelSize.y)) * -1.0;

    y += tex2D(_MainTex, uv + float2(-texelSize.x, texelSize.y)) * 1.0;
    y += tex2D(_MainTex, uv + float2(0, texelSize.y)) * 2.0;
    y += tex2D(_MainTex, uv + float2(texelSize.x, texelSize.y)) * 1.0;*/

    float delta = sqrt(x * x + y * y);

    float3 drgb = _MainTex[dtc].rgb;

    if (delta > edge) 
        drgb = float3(1, 0, 0);

    #ifndef UNITY_NO_LINEAR_COLORSPACE
    drgb = LinearToGammaSpace(drgb);
    #endif
    return DecodeDepth(drgb, _DepthRange);
}

    ENDCG

    SubShader
    {
        Pass
        {
            Cull Off ZWrite Off ZTest Always
            CGPROGRAM
            #pragma vertex VertexColor
            #pragma fragment FragmentColor
            ENDCG
        }
        Pass
        {
            Cull Off ZWrite Off ZTest Always
            CGPROGRAM
            #pragma vertex VertexDepth
            #pragma fragment FragmentDepth
            ENDCG
        }
        Pass
        {
            Cull Off ZWrite Off ZTest Always
            CGPROGRAM
            #pragma vertex VertexColorU
            #pragma fragment FragmentColorU
            ENDCG
        }
    }
}
