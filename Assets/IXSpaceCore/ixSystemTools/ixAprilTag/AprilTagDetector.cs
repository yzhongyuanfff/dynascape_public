using Bibcam.Decoder;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AprilTagDetector : MonoBehaviour
{
    [SerializeField] int _decimation = 2;
    [SerializeField] float _tagSize = 0.25f;
    [SerializeField] Material _tagMaterial = null;

    RenderTexture _texture;
    AprilTag.TagDetector _detector;
    TagDrawer _drawer;

    [HideInInspector] public Vector3 QRPosition;
    [HideInInspector] public Quaternion QRRotation;

    // Start is called before the first frame update
    void Start()
    {
        _drawer = new TagDrawer(_tagMaterial);
    }

    // Update is called once per frame
    void Update()
    {
        // _drawer.Draw(0, Vector3.zero, Quaternion.identity, _tagSize);
    }
    void OnDestroy()
    {
        if(_detector!=null) _detector.Dispose();
        _drawer.Dispose();
    }

    public void SetTexture(RenderTexture rt)
    {
        _texture = rt;
    }

    public bool DetectQR(float FOV)
    {
        bool hasQR = false;

        //
        var image = _texture.AsSpan();
        if (image.IsEmpty) return false;

        // AprilTag detection
        //var fov = FOV * Mathf.Deg2Rad; // Camera.main.fieldOfView
        _detector = new AprilTag.TagDetector(_texture.width, _texture.height, _decimation);
        _detector.ProcessImage(image, FOV, _tagSize);

        // Detected tag visualization
        foreach (var tag in _detector.DetectedTags)
        {
            _drawer.Draw(tag.ID, tag.Position, tag.Rotation, _tagSize);
            Debug.Log("QR Detected!, tag.ID = "  + tag.ID + " , tag.Position = " + tag.Position + " , tag.Rotation = " + tag.Rotation);

            QRPosition = tag.Position;
            QRRotation = tag.Rotation;
            hasQR = true;
        }
        return hasQR;
    }
}
