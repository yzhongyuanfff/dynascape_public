using JfranMora.Inspector;
using Klak.Ndi;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public enum Resolution { 
    Res480p,
    Res720p,
    Res1080p,
    Res1440p
}

public class ixCamCapManager : MonoBehaviour
{
    [Header("----------")]
    [SerializeField] bool DoPerspectiveCap = true;
    [SerializeField] bool DoSendFrameViaNDI = true;
    [SerializeField] bool DoUpdateAccordingToMainCamera = true;

    Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
        UpdateFrameDimention(Resolution.Res720p);
    }

    // Update is called once per frame
    void Update()
    {
        if (DoUpdateAccordingToMainCamera)
        {
            this.transform.position = Camera.main.transform.position;
            this.transform.rotation = Camera.main.transform.rotation;
        }
    }

    public void UpdateFrameDimention(Resolution res) {


        if (res.Equals(Resolution.Res480p))
        {
            camera.targetTexture = new RenderTexture(720, 480, 1, RenderTextureFormat.Default);
        }
        if (res.Equals(Resolution.Res720p))
        {
            camera.targetTexture = new RenderTexture(1280, 720, 1, RenderTextureFormat.Default);
        }
        if (res.Equals(Resolution.Res1080p))
        {
            camera.targetTexture = new RenderTexture(1920, 1080, 1, RenderTextureFormat.Default);
        }
        if (res.Equals(Resolution.Res1440p))
        {
            camera.targetTexture = new RenderTexture(2560, 1440, 1, RenderTextureFormat.Default);
        }

        UpdateFrameTarget();
    }


    void UpdateFrameTarget()
    {
        if(GetComponent<NdiSender>())
            GetComponent<NdiSender>().sourceTexture = camera.targetTexture;
    }

    public void ToggleFrameSending()
    {
        DoSendFrameViaNDI = !DoSendFrameViaNDI;
        GetComponent<NdiSender>().enabled = DoSendFrameViaNDI;
    }

    public bool GetFrameSendingState()
    {
        return DoSendFrameViaNDI;
    }

    void CaptureNKImageWithThisCamera(int factor)
    {
        if (camera == null) camera = GetComponent<Camera>();

        Vector2Int res = new Vector2Int(540 * factor / 9 * 16, 540 * factor);
        camera.targetTexture = new RenderTexture(res.x, res.y, 1, RenderTextureFormat.Default);

        RenderTexture currentRT = RenderTexture.active;
        RenderTexture.active = camera.targetTexture;

        camera.Render();

        Texture2D Image = new Texture2D(camera.targetTexture.width, camera.targetTexture.height);
        Image.ReadPixels(new Rect(0, 0, camera.targetTexture.width, camera.targetTexture.height), 0, 0);
        Image.Apply();
        RenderTexture.active = currentRT;

        var Bytes = Image.EncodeToPNG();
        Destroy(Image);


        string fileNameFromTimeStamp = (new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()).ToString();
        string folderPath = Application.persistentDataPath + "/Captures/";
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);
        string fileName = "Cap_" + fileNameFromTimeStamp + "_" + factor + "K.png";
        File.WriteAllBytes(folderPath + fileName, Bytes);

        //
        ixUtility.LogMe("Captured! File Written To: " + Application.persistentDataPath + "/Captures/");
        Debug.Log("Captured! File Written To: " + Application.persistentDataPath + "/Captures/");
    }
    void CaptureImageWithThisCamera(int ResX, int ResY)
    {
        if (camera == null) camera = GetComponent<Camera>();

        camera.targetTexture = new RenderTexture(ResX, ResY, 1, RenderTextureFormat.Default);

        RenderTexture currentRT = RenderTexture.active;
        RenderTexture.active = camera.targetTexture;

        camera.Render();

        Texture2D Image = new Texture2D(camera.targetTexture.width, camera.targetTexture.height);
        Image.ReadPixels(new Rect(0, 0, camera.targetTexture.width, camera.targetTexture.height), 0, 0);
        Image.Apply();
        RenderTexture.active = currentRT;

        var Bytes = Image.EncodeToPNG();
        Destroy(Image);


        string fileNameFromTimeStamp = (new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()).ToString();
        string folderPath = Application.persistentDataPath + "/Captures/";
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);
        string fileName = "Cap_" + fileNameFromTimeStamp + "_" + ResX + "x" + ResY + ".png";
        File.WriteAllBytes(folderPath + fileName, Bytes);

        //
        ixUtility.LogMe("Captured! File Written To: " + Application.persistentDataPath + "/Captures/");
        Debug.Log("Captured! File Written To: " + Application.persistentDataPath + "/Captures/");
    }


    [Button]
    void Capture4KImageWithThisCamera()
    {
        CaptureNKImageWithThisCamera(4);
    }


    [Button]
    void Capture8KImageWithThisCamera()
    {
        CaptureNKImageWithThisCamera(8);
    }

    [Button]
    void Capture16KImageWithThisCamera()
    {
        CaptureNKImageWithThisCamera(16);
    }

    [Button]
    void CaptureMaxResImageWithThisCamera()
    {
        //16384 x 16384
        CaptureImageWithThisCamera(16384, 16384);
    }
}
