// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassthroughAdapter : MonoBehaviour
{
    OVRPassthroughLayer oVRPassthroughLayer = null;
    public Texture passthroughLeft;
    public Texture passthroughRight;

    // Start is called before the first frame update
    void Start()
    {
        //oVRPassthroughLayer = GetComponent<OVRPassthroughLayer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(oVRPassthroughLayer == null)
        {
            oVRPassthroughLayer = GetComponent<OVRPassthroughLayer>();
        }

        if (oVRPassthroughLayer)
        {
            //passthroughLeft = oVRPassthroughLayer.passthroughOverlay.textures[0];
            //passthroughRight = oVRPassthroughLayer.passthroughOverlay.textures[1];
        }
    }
}
