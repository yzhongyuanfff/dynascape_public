using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ixSpaceConfigurator : MonoBehaviour
{
    [SerializeField] List<GameObject> ControllingObjectList;
    [SerializeField] List<Material> ControllingObjectMaterialList;

    [SerializeField] Material PassthroughMaterial;

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
        CollectMaterialList();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void MakeAButtonMRTK(GameObject parent, string text, UnityAction callback)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UIPressableButton", typeof(GameObject)), parent.transform);
        ui.transform.GetComponent<ButtonConfigHelper>().MainLabelText = text;
        ui.transform.GetComponent<ButtonConfigHelper>().OnClick.AddListener(callback);
    }

    void CollectMaterialList()
    {
        ControllingObjectMaterialList.Clear();
        for (int i = 0; i < ControllingObjectList.Count; i++)
        {
            //
            if (ControllingObjectList[i])
                ControllingObjectMaterialList.Add(ControllingObjectList[i].GetComponent<MeshRenderer>().material);
            else
                ControllingObjectMaterialList.Add(null);
        }
    }

    void RecursiveMaterialAssign(Transform o, Material m)
    {
        if(o.GetComponent<MeshRenderer>())
            o.GetComponent<MeshRenderer>().material = m;

        foreach(Transform c in o.transform)
            RecursiveMaterialAssign(c, m);
    }

    void InitUI()
    {
        // 
        MakeAButtonMRTK(this.transform.Find("ButtonCollection").gameObject, "Snap Slides", delegate { 
                
        });
        // 
        MakeAButtonMRTK(this.transform.Find("ButtonCollection").gameObject, "Snap Videos", delegate {

        });
        // 
        MakeAButtonMRTK(this.transform.Find("ButtonCollection").gameObject, "Powerwall", delegate {
            foreach(GameObject o in ControllingObjectList)
            {
                if (o == null) continue;
                o.gameObject.SetActive(!o.gameObject.activeSelf);
            }
        });
        // how to toggle?
        MakeAButtonMRTK(this.transform.Find("ButtonCollection").gameObject, "Passthrough", delegate {
            foreach (GameObject o in ControllingObjectList)
            {
                if (o != null && PassthroughMaterial != null)
                {
                    RecursiveMaterialAssign(o.transform, PassthroughMaterial);
                }
            }
        });
        // 
        MakeAButtonMRTK(this.transform.Find("ButtonCollection").gameObject, "More", delegate {

        });

        this.transform.Find("ButtonCollection").GetComponent<GridObjectCollection>().UpdateCollection();

    }
}
