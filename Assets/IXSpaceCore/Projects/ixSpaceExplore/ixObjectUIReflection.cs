using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ixObjectUIReflection : MonoBehaviour
{
    [SerializeField] GameObject UIContextMenuParent;

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void MakeAButtonMRTK(GameObject parent, string text, UnityAction callback)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UIPressableButton", typeof(GameObject)), parent.transform);
        ui.transform.GetComponent<ButtonConfigHelper>().MainLabelText = text;
        ui.transform.GetComponent<ButtonConfigHelper>().OnClick.AddListener(callback);
    }

    GameObject MakeContextMenuMRTK(GameObject RefAnchor)
    {
        GameObject panel = Instantiate((GameObject)Resources.Load("UIContextMenu", typeof(GameObject)), UIContextMenuParent.transform);
        panel.transform.position = RefAnchor.transform.position;
        panel.transform.rotation = RefAnchor.transform.rotation;
        return panel.transform.Find("ButtonCollection").gameObject;
    }

    void InitUI()
    {
        GameObject UIAnchor = this.transform.Find("UIAnchor").gameObject;
        GameObject btnCollection = MakeContextMenuMRTK(UIAnchor);

        MakeAButtonMRTK(btnCollection, "Snap Slides", delegate {
            // set as slides snapper 
        });
        MakeAButtonMRTK(btnCollection, "Snap Videos", delegate {
            // set as video snapper 
        });
        MakeAButtonMRTK(btnCollection, "Powerwall", delegate {
            //this.gameObject.SetActive(!gameObject.activeSelf);
            this.GetComponent<MeshRenderer>().enabled = !this.GetComponent<MeshRenderer>().enabled;
        });
        MakeAButtonMRTK(btnCollection, "More", delegate {
            // 
        });
    }
}
