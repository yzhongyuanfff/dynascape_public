using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkedVideoLoader : MonoBehaviour, IPunInstantiateMagicCallback
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Will assign local url to the video player -> remember to donwload all assets before running this
    /// </summary>
    /// <param name="info"></param>    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        object[] instantiationData = info.photonView.InstantiationData;
        if (instantiationData.Length > 0)
        {
            ixUtility.LogMe("instantiationData[0] = (Link?)" + instantiationData[0]); 
            GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath = (string)instantiationData[0];
            this.GetComponent<ixVideoPlayerController_TeachingXR>().LoadActiveVideoRPC(); // Just Once 
            this.GetComponent<ixVideoPlayerController_TeachingXR>().PlayVideoRPC(); // Just Once
        }
    }
}
