using Bibcam.Decoder;
using JfranMora.Inspector;
using Klak.Hap;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;
using UnityEngine.Video;

public class ixVideoPlayerController_TeachingXR: MonoBehaviour
{
    [Header("----------")]
    [SerializeField] bool ReflactToUI = true;
    [SerializeField] GameObject UIContent;
    [SerializeField] GameObject ProgressBarContent;
    [Header("========== Plain Video Renderer====")]
    [Header("----------")]
    [SerializeField] bool RenderToRenderer = false;
    [SerializeField] Renderer targetRenderer;
    [SerializeField] string targetMaterialProperty = "_BaseMap";
    [Header("----------")]
    [SerializeField] bool DoFlipWithHap = true;
    [SerializeField] Material ProcessingMaterial;
    [Header("========== VFX Renderer====")]
    [Header("----------")]
    [SerializeField] bool RenderToVFX = false;
    [SerializeField] BibcamVideoFeeder videoFeeder;
    [SerializeField] VisualEffect VFX;
    [SerializeField] bool ApplyCameraTransformations = false;
    [Header("----------")]
    [SerializeField] bool FollowMode = false;

    VideoPlayer _H264Video = null; // H264
    HapPlayer _HAPVideo = null; // HAP 
    VideoPreprocessing _processor; // 
    Texture outTexture;

    float _PlaybackSpeed = 1;
    GameObject UGUITextedSlider;
    ixUtility.VideoFormat videoFormat;
    GameObject mixedRealityPlayspace;

    // Start is called before the first frame update
    void Start()
    {
        mixedRealityPlayspace = GameObject.FindGameObjectWithTag("MixedRealityPlayspace");
        InitUI();
        InitProcessor();
    }
    // Update is called once per frame
    void Update()
    {
        UpdateUI();
        UpdateFrame();
        UpdateMRTKPlayspaceFollowMode();
    }

    public void SwitchVFX(VisualEffect newVFX)
    {
        StopVFX();
        VFX = newVFX;
    }

    public VisualEffect RefVFX()
    {
        return VFX;
    }

    /////////////////////////////////////////////////////////////////////
    /// <summary>
    /// 
    /// </summary>
    [Button]
    public void ShallowPauseVFXPlay()
    {
        FollowMode = false;
        PauseVideoAndVFXPlay();
    }

    [Button]
    public void ShallowResumeVFXPlay()
    {
        if (VFX.pause)
        {
            FollowMode = true;
            ResumeVideoAndVFXPlay();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void VideoPlayer_errorReceived(VideoPlayer source, string message)
    {
        ixUtility.LogMe("_H264Video, Error message: " + message);
    }

    [PunRPC]
    public void LoadActiveVideoRPC()
    {
        string videoUrl = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
        ixUtility.LogMe("Playing video from: " + videoUrl);

        //
        if (!System.IO.File.Exists(videoUrl))
        {
            ixUtility.LogMe("Video File Not Exist! ");
            return;
        }

        //
        if (Path.GetExtension(videoUrl).Equals(".mp4") || Path.GetExtension(videoUrl).Equals(".MP4"))
        {
            ixUtility.LogMe("Setting Url and Preparing the H264 videoplayer");
            videoFormat = ixUtility.VideoFormat.H264;
            PrepareH264VideoPlayer(videoUrl);
        }
        else if (Path.GetExtension(videoUrl).Equals(".mov") || Path.GetExtension(videoUrl).Equals(".MOV"))
        {
            ixUtility.LogMe("Setting Url and Preparing the HAP videoplayer");
            videoFormat = ixUtility.VideoFormat.HAP;
            PrepareHapVideoPlayer(videoUrl);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void LoadActiveVideo()
    {
        LoadActiveVideoRPC();
        this.GetComponent<PhotonView>().RPC("LoadActiveVideoRPC", RpcTarget.Others);
    }

    void PrepareH264VideoPlayer(string url)
    {
        _H264Video = gameObject.AddComponent<VideoPlayer>();
        _H264Video.source = VideoSource.Url;
        _H264Video.playOnAwake = false;
        _H264Video.waitForFirstFrame = true;
        _H264Video.skipOnDrop = true;
        _H264Video.playbackSpeed = 1;
        _H264Video.renderMode = VideoRenderMode.MaterialOverride;
        //_H264Video.targetMaterialRenderer = targetRenderer;
        _H264Video.targetMaterialProperty = targetMaterialProperty;

        _H264Video.errorReceived += VideoPlayer_errorReceived;
        _H264Video.url = url;
        _H264Video.Prepare();

        //
        if (UGUITextedSlider)
        {
            UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 0;
            ixUtility.LogMe("_H264Video.frameCount.ToString() = "  + _H264Video.frameCount.ToString());
            UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = 
                _H264Video.frameCount.ToString();
        }

        //
        if (_HAPVideo) Destroy(_HAPVideo);
    }

    void PrepareHapVideoPlayer(string url)
    {
        if (_HAPVideo) Destroy(_HAPVideo);

        _HAPVideo = gameObject.AddComponent<HapPlayer>();
        //_HAPVideo.targetRenderer = targetRenderer; // disable automatic render texture assign, do it manually
        _HAPVideo.targetMaterialProperty = targetMaterialProperty;
        _HAPVideo._filePath = url;
        _HAPVideo._pathMode = HapPlayer.PathMode.LocalFileSystem;
        _HAPVideo.speed = 0;

        //
        if (UGUITextedSlider)
        {
            UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 0;
            ixUtility.LogMe("_HAPVideo.frameCount.ToString() = " + _HAPVideo.frameCount.ToString());
            UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = 
                _HAPVideo.frameCount.ToString();
        }

        //
        _H264Video.Stop();
    }

    ///////////////////////////////////////////////////////////////////////////////Video Control ////
    /// <summary>
    /// 
    /// </summary>
    [PunRPC]
    public void PlayVideoRPC()
    {
        ixUtility.LogMe("Playing Video");
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            _H264Video.Prepare();
            _H264Video.Play();
        }

        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            _HAPVideo.time = 0;
            _HAPVideo.speed = _PlaybackSpeed;
        }

        if (RenderToVFX && VFX)
        {
            StopVFX();
            StartVFX();
        }
    }


    public void PlayVideo()
    {
        PlayVideoRPC();
        this.GetComponent<PhotonView>().RPC("PlayVideoRPC", RpcTarget.Others);
    }

    void StopVFX()
    {
        VFX.Stop();
        VFX.Reinit();
        VFX.SendEvent("OnStop");
    }

    void StartVFX()
    {
        VFX.Play();
        VFX.SendEvent("OnPlay");
        if(VFX.HasBool("ApplyCameraTransformations"))
            VFX.SetBool("ApplyCameraTransformations", false);
    }

    void PauseVideoAndVFXPlay()
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
            _H264Video.Pause(); 
        if (videoFormat == ixUtility.VideoFormat.HAP)
            _HAPVideo.speed = 0;
        VFX.pause = true;
        VFX.SendEvent("OnStop");
    }

    void StopVideo()
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            _H264Video.Stop();
            _H264Video.frame = 0;
            _H264Video.Prepare();
        }

        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            _HAPVideo.speed = 0;
            _HAPVideo.time = 0;
        }
    }

    void StopALLRemoveAllPoints()
    {
        StopVideo();
        StopVFX();
    }

    void ResumeVideoAndVFXPlay()
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
            _H264Video.Play();
        if (videoFormat == ixUtility.VideoFormat.HAP)
            _HAPVideo.speed = _PlaybackSpeed;
        VFX.pause = false;
        VFX.SendEvent("OnPlay");
    }

    [PunRPC]
    public void PauseContinueVideoPlayRPC()
    {

        bool DoPlay = false;
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            if (!_H264Video.isPlaying)
            {
                _H264Video.Play();
                DoPlay = true;
            }
            else
            {
                _H264Video.Pause();
                DoPlay = false;
            }
        }

        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            if (_HAPVideo.speed == 0)
            {
                _HAPVideo.speed = _PlaybackSpeed;
                DoPlay = true;
            }
            else
            {
                _HAPVideo.speed = 0;
                DoPlay = false;
            }
        }

        if (RenderToVFX && VFX)
        {
            if (DoPlay)
            {
                VFX.pause = false;
                VFX.SendEvent("OnPlay");
            }
            else
            {
                VFX.pause = true;
                VFX.SendEvent("OnStop");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void PauseContinueVideoPlay()
    {
        PauseContinueVideoPlayRPC();
        this.GetComponent<PhotonView>().RPC("PauseContinueVideoPlayRPC", RpcTarget.Others);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="per"></param>
    public void UpdateVideoPlayPercentage(float per)
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            int targetFrame = (int)(per * _H264Video.frameCount);
            _H264Video.Pause();
            _H264Video.frame = targetFrame;
            _H264Video.Play();
        }
        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            //int targetFrame = (int)(per * _HAPVideo.frameCount);
            float targetTime = (float)(per * _HAPVideo.streamDuration);
            _HAPVideo.time = targetTime;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="speed"></param>
    public void UpdateVideoPlaySpeed(float speed)
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            _H264Video.playbackSpeed = speed;
        }
        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            _HAPVideo.speed = speed;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void LogVideoState()
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            ixUtility.LogMe("_H264Video.isPrepared = " + _H264Video.isPrepared);
            ixUtility.LogMe("_H264Video.isPlaying = " + _H264Video.isPlaying);
            ixUtility.LogMe("_H264Video.isActiveAndEnabled = " + _H264Video.isActiveAndEnabled);
            ixUtility.LogMe("_H264Video.width = " + _H264Video.width);
            ixUtility.LogMe("_H264Video.height = " + _H264Video.height);
        }
        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            ixUtility.LogMe("_HAPVideo.isValid = " + _HAPVideo.isValid);
        }
    }


    /////////////////////////////////////////////////////////////////////////////// UI Control ////
    void InitUI()
    {
        if (!ReflactToUI) return;

        if (ProgressBarContent)
        {
            UGUITextedSlider = Instantiate(
                (GameObject)Resources.Load("UGUITextedSlider", typeof(GameObject)),
                ProgressBarContent.transform);

            UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 0;
            UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = "00:00";
        }

        if (UIContent)
        {
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = 
                    "Load Active Video";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { 
                    LoadActiveVideo(); });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = 
                    "Play";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
                    delegate { PlayVideo(); });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = 
                    "Pause/ Resume";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
                    delegate { PauseContinueVideoPlay(); });
            }
            //{
            //    GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
            //        "Stop ALL";
            //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
            //        delegate { StopALLRemoveAllPoints(); });
            //}
            //{
            //    GameObject ui = Instantiate((GameObject)
            //        Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
            //        "Toggle Camera Following";
            //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
            //        FollowMode = !FollowMode;
            //    });
            //}
        }
    }

    void UpdateUI()
    {
        if (UGUITextedSlider == null) return;

        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            if (_H264Video && _H264Video.isPlaying) { 
                UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 
                    ((float)_H264Video.frame / _H264Video.frameCount);
                UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text =
                    _H264Video.frame.ToString() + "/" + _H264Video.frameCount.ToString();           
            }
        }
            
        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            if (_HAPVideo && _HAPVideo.isValid) { 
                UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 
                    (float)(_HAPVideo.time / _HAPVideo.streamDuration);
                UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = 
                    _HAPVideo.time.ToString("F2") + "/" + _HAPVideo.streamDuration.ToString("F2");           
            }
        }

    }

    ///////////////////////////////////////////////////////////
    void InitProcessor()
    {
        if (ProcessingMaterial == null) return;

        _processor = gameObject.AddComponent<VideoPreprocessing>();
        _processor.SetProcessingMaterial(ProcessingMaterial);
    }

    long lastFrameCount;

    void UpdateFrame()
    {
        if (videoFormat == ixUtility.VideoFormat.H264) {
            bool newFrameComes = false;
            if (_H264Video && _H264Video.isPlaying)
            {
                newFrameComes = _H264Video.frame != lastFrameCount;
                lastFrameCount = _H264Video.frame;
                outTexture = _H264Video.texture;
            }

            //if (newFrameComes)
            {
                if (RenderToRenderer && targetRenderer)
                    targetRenderer.material.mainTexture = outTexture;
                if (RenderToVFX && videoFeeder)
                    videoFeeder.DecodeFrame(outTexture);
            }
        }

        if (videoFormat == ixUtility.VideoFormat.HAP) {
            if (_HAPVideo && _HAPVideo.isValid) {
                if (DoFlipWithHap)
                {
                    Texture currentTexture = _processor.GetProcessedTexture(_HAPVideo.texture);
                    outTexture = currentTexture;
                }
                else
                {
                    outTexture = _HAPVideo.texture;
                }
            }

            if (RenderToRenderer && targetRenderer)
                targetRenderer.material.mainTexture = outTexture;
            if (RenderToVFX && videoFeeder)
                videoFeeder.DecodeFrame(outTexture);
        }
    }

    void UpdateMRTKPlayspaceFollowMode()
    {
        if (FollowMode && mixedRealityPlayspace && videoFeeder && (videoFeeder.RefDecoder() != null))
        {
            mixedRealityPlayspace.transform.position =
                this.transform.TransformPoint(videoFeeder.RefDecoder().Metadata.CameraPosition);
        }
    }
}
