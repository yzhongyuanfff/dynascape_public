// ------------------------------------------------------------------------------------
// <copyright company="Technische Universitšt Dresden">
//      Copyright (c) Technische Universitšt Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using Random = UnityEngine.Random;

public class ixTeachingDashboard : MonoBehaviour
{
    [Header("----------")]
    [SerializeField] GameObject UiTitle;
    [SerializeField] GameObject UIContent;

    [Header("----------Slides: XR Spec. ---")]
    //[SerializeField] GameObject SlidesParent; //[SerializeField] GameObject SlidePrefeb;
    [SerializeField] float DefaultScale = 0.4f;

    [Header("----------Slides: Baseline ---")]
    [SerializeField] GameObject UGUIImagePrefeb;
    [SerializeField] Material ImageMaterialDemo;
    [SerializeField] GameObject UGUIScrollContent;
    [SerializeField] float UGUIDefaultScale = 400.0f;

    [Header("----------Videos: XR Spec. ---")]
    [Header("----------Videos: Baseline ---")]

    [Header("----------3D Models: XR Spec. ---")]
    [Header("----------3D Models: Videos: Baseline ---")]

    //
    List<string> fplist = new List<string>();
    List<string> linkList = new List<string>();
    List<Texture2D> textureList = new List<Texture2D>();
    List<GameObject> imgs = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {

    }


    void UnzipActiveFile()
    {
        //
        fplist.Clear();

        //
        string zipfile = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curActiveFilePath;
        string targetPathBase = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curFolderPath;
        string fileNameWithoutExtension = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curFileName;

        //
        fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileNameWithoutExtension);
        string targetPath = targetPathBase + "/" + fileNameWithoutExtension + "/";
        if (!Directory.Exists(targetPath))
            Directory.CreateDirectory(targetPath);

        //
        fplist = ZipFile.UnZip(targetPath, zipfile);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    [PunRPC]
    void LoadToTexturesInMemory()
    {
        // Empty, but maybe already unziped! Check folder 
        if (fplist.Count == 0)
        {
            string zipfile = GameObject.FindGameObjectWithTag("StringManager")
                .GetComponent<StringManager>().curActiveFilePath;
            string targetPathBase = GameObject.FindGameObjectWithTag("StringManager")
                .GetComponent<StringManager>().curFolderPath;
            string fileNameWithoutExtension = GameObject.FindGameObjectWithTag("StringManager")
                .GetComponent<StringManager>().curFileName;
            fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileNameWithoutExtension);
            string targetPath = targetPathBase + "/" + fileNameWithoutExtension + "/";
            if (Directory.Exists(targetPath)) // already zipped! 
            {
                string[] files = Directory.GetFiles(targetPath);
                foreach (var f in files)
                {
                    fplist.Add(f);
                }
            }
            else
            {
                return; // not zipped! file not downloaded even. 
            }
        }

        // Clear first 
        foreach (var tex in textureList) Destroy(tex);
        textureList.Clear();

        //
        foreach (string fp in fplist)
        {
            Random.InitState(fp.Length);
            Vector3 rp = new Vector3(
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f));
            Quaternion ro = new Quaternion(
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1));
            byte[] bytes = System.IO.File.ReadAllBytes(fp);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);

            textureList.Add(texture);
        }
    }

    [PunRPC]
    void InstantiateImageObjects()
    {
        //
        foreach (var img in imgs) Destroy(img);
        imgs.Clear();

        //
        int texID = 0;
        foreach (var tex in textureList)
        {
            Random.InitState(texID);
            Vector3 rp = new Vector3(
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f));
            Quaternion ro = new Quaternion(
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1));

            GameObject img = Instantiate((GameObject)
                Resources.Load("SlidePrefeb", typeof(GameObject)), this.transform);
            img.GetComponent<MeshRenderer>().material.mainTexture = tex;
            img.transform.localScale = new Vector3(DefaultScale, (float)tex.height / tex.width * DefaultScale, 1);
            img.transform.position = rp;
            img.transform.rotation = ro;
            imgs.Add(img);

            texID++;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void GeatherLinkLists()
    {
        linkList.Clear();

        linkList.Add("http://141.76.64.17:8080/InAppAssetRepo/MediaExplore/FVRSlides/06.FVR.Haptics/page_0.png");

        for (int i = 0; i < 68; i++)
        {
            linkList.Add("http://141.76.64.17:8080/InAppAssetRepo/MediaExplore/FVRSlides/06.FVR.Haptics/page_" + i + ".png");
        }
    }

    //
    void InstatiateVideoPanel()
    {
        string LocalVideoFilePath = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curActiveFilePath;
        object[] data = new object[1];
        data[0] = LocalVideoFilePath; // video link on local disk  
        PhotonNetwork.Instantiate("ixPlainVideoPrefeb", Vector3.zero, Quaternion.identity, 0, data);
    }

    //
    void InstantiateImageQuads()
    {
        int id = 0;
        foreach (var texLink in linkList)
        {
            Random.InitState(id);
            Vector3 rp = new Vector3(
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f));
            Quaternion ro = new Quaternion(
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1));

            object[] data = new object[1];
            data[0] = texLink;
            PhotonNetwork.Instantiate("SlidePrefeb", rp, ro, 0, data); // -> no post change avaliable

            //object[] networkedData = this.gameObject.GetPhotonView().InstantiationData;
            //string networkedLink = (string)networkedData[0];
            //ixUtility.LogMe("networkedLink = " + networkedLink);
            //ixUtility

            //img.transform.parent = ImagesParent.transform; // -> this only works in local? 
            //img.GetComponent<MeshRenderer>().material.mainTexture = tex;
            //img.transform.localScale = new Vector3(DefaultScale, (float)tex.height / tex.width * DefaultScale, 1);
            ////imgs.Add(img);

            id++;
        }
    }

    void InstantiateSlideCanvas()
    {

    }

    void InstantiateImageObjects_NetworkedInstantiate()
    {
        //
        // foreach (var img in imgs) Destroy(img);
        //imgs.Clear();

        //
        int texID = 0;
        foreach (var tex in textureList)
        {
            Random.InitState(texID);
            Vector3 rp = new Vector3(
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f));
            Quaternion ro = new Quaternion(
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1));

            PhotonNetwork.Instantiate("SlidePrefeb", rp, ro); // -> no post change avaliable
            //img.transform.parent = ImagesParent.transform; // -> this only works in local? 
            //img.GetComponent<MeshRenderer>().material.mainTexture = tex;
            //img.transform.localScale = new Vector3(DefaultScale, (float)tex.height / tex.width * DefaultScale, 1);
            ////imgs.Add(img);

            texID++;
        }
    }

    void CreateImagesFromFileList_Random()
    {
        //
        LoadToTexturesInMemory();

        //
        InstantiateImageObjects();
    }

    void RenderImagesToCanvas()
    {
        // Empty, but maybe already unziped! Check folder 
        if (fplist.Count == 0)
        {
            string zipfile = GameObject.FindGameObjectWithTag("StringManager")
                .GetComponent<StringManager>().curActiveFilePath;
            string targetPathBase = GameObject.FindGameObjectWithTag("StringManager")
                .GetComponent<StringManager>().curFolderPath;
            string fileNameWithoutExtension = GameObject.FindGameObjectWithTag("StringManager")
                .GetComponent<StringManager>().curFileName;
            fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileNameWithoutExtension);
            string targetPath = targetPathBase + "/" + fileNameWithoutExtension + "/";
            if (Directory.Exists(targetPath)) // already zipped! 
            {
                string[] files = Directory.GetFiles(targetPath);
                foreach (var f in files)
                {
                    fplist.Add(f);
                }
            }
            else
            {
                return; // not zipped! file not downloaded even. 
            }
        }

        //
        foreach (var img in imgs) Destroy(img);
        imgs.Clear();

        //
        foreach (string fp in fplist)
        {
            byte[] bytes = System.IO.File.ReadAllBytes(fp);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);

            //
            GameObject img = Instantiate(UGUIImagePrefeb, UGUIScrollContent.transform);
            Material tmpMat = new Material(ImageMaterialDemo);
            tmpMat.mainTexture = texture;
            img.GetComponent<Image>().material = tmpMat;
            //img.transform.localScale = new Vector3(UGUIDefaultScale, (float)texture.height / texture.width * DefaultScale, 1);
            //img.transform.position = rp;
            //img.transform.rotation = ro;
            img.GetComponent<RectTransform>().sizeDelta =
                new Vector2(UGUIDefaultScale, (float)texture.height / texture.width * UGUIDefaultScale);
            imgs.Add(img);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void MakeAButton(GameObject parent, string text, UnityAction callback)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), parent.transform);
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = text;
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(callback);
    }

    void MakeSepLine(GameObject parent, string text)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UGUIText", typeof(GameObject)), parent.transform);
        ui.GetComponent<TextMeshProUGUI>().text = text;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void InitUI()
    {
        //if(UiTitle) UiTitle.GetComponent<TextMeshProUGUI>().text = this.name;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        MakeSepLine(UIContent, "------[Slides] Networked Loader------");
        MakeAButton(UIContent,
            "Instantiate Image Objects",
            delegate {
                GeatherLinkLists();
                InstantiateImageQuads();
            });
        MakeAButton(UIContent,
            "Snap All Locally -> Will be sync. to remote users",
            delegate {
                foreach(Transform c in this.transform)
                {
                    if (c.GetComponent<SnapToTheNearestSpatialAnchor>())
                    {
                        c.GetComponent<SnapToTheNearestSpatialAnchor>().UpdateThreshold(float.MaxValue); // discard the distance issue 
                        c.GetComponent<SnapToTheNearestSpatialAnchor>().SnapOnce();
                    }
                }
            });
        MakeAButton(UIContent,
            "Networked Physics Object Demo",
            delegate {
                GameObject netObj = PhotonNetwork.Instantiate("TrafficConePrefeb", Vector3.zero, Quaternion.identity);
                netObj.transform.parent = this.transform;
            });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        MakeSepLine(UIContent, "------[Video] Networked Loader------");
        MakeAButton(UIContent,
            "Instatiate Video Panel",
            delegate {
                //GeatherLinkLists();
                InstatiateVideoPanel();
            });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        MakeSepLine(UIContent, "------[Slides] Local Loader------");
        MakeAButton(UIContent,
            "Unzip Active File"
            ,
            delegate {
                UnzipActiveFile();
            });
        MakeAButton(UIContent,
            "Create Images From File List - Random"
            ,
            delegate {
                CreateImagesFromFileList_Random();
            });
        MakeAButton(UIContent,
            "RenderImagesToCanvas"
            ,
            delegate {
                RenderImagesToCanvas();
            });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        MakeSepLine(UIContent, "------Archieve------");
        MakeAButton(UIContent,
            "InstantiateImageObjects - RPC",
            delegate {
                LoadToTexturesInMemory();
                InstantiateImageObjects();
                this.GetComponent<PhotonView>().RPC("LoadToTexturesInMemory", RpcTarget.Others);
                this.GetComponent<PhotonView>().RPC("InstantiateImageObjects", RpcTarget.Others);
            });
    }
}
