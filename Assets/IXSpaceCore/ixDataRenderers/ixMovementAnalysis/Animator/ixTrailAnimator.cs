using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ixTrailAnimator : MonoBehaviour
{
    CustomTubeRenderer customTubeRenderer;

    public GameObject UIContent;
    public GameObject AvatarUI;

    bool EnableAnim = true;
    public int curFrame = 0;

    // Start is called before the first frame update
    void Start()
    {
        customTubeRenderer = this.transform.parent.transform.GetComponent<CustomTubeRenderer>();
        this.gameObject.SetActive(false);
        AvatarUI.gameObject.SetActive(false);
        InitUI();
    }

    bool IsDatareadyLastFrame = false;

    // Update is called once per frame
    void Update()
    {
        if (EnableAnim && customTubeRenderer.IsDataReady) // double check 
        {
            CheckAndActiveUI();
            ApplyAnim();
        }
    }

    void ApplyAnim()
    {
        if (curFrame <= customTubeRenderer.pointList.Count - 1)
        {
            Vector3 animPosition = customTubeRenderer.pointList[curFrame];
            Quaternion animRotation = Quaternion.identity;
            if (customTubeRenderer.rotationList.Count == customTubeRenderer.pointList.Count)
                animRotation = customTubeRenderer.rotationList[curFrame];

            //// assume this is only one scene geometry or the scale/ transform are the same 
            //Transform sceneGeometry = GameObject.FindGameObjectWithTag("SceneGeometry").transform;
            //if (sceneGeometry)
            //{
            //    animPosition = sceneGeometry.TransformPoint(animPosition);
            //    animRotation = sceneGeometry.rotation * animRotation;
            //}

            this.transform.position = animPosition;
            this.transform.rotation = animRotation;

            UpdateFrameTextUI("Current Frame = " + curFrame);
            UpdateAvatarUIPose(animPosition);
            curFrame++;
        }
        else
        {
            curFrame = 0;
        }
    }

    public void PauseResumeAnim()
    {
        EnableAnim = !EnableAnim;
    }

    public void UpdateAnimFrame(int f)
    {
        curFrame = f;
    }

    GameObject frameTextUI;

    void InitUI()
    {
        frameTextUI = Instantiate((GameObject)Resources.Load("UGUIText", typeof(GameObject)), UIContent.transform);
        frameTextUI.transform.GetComponent<TextMeshProUGUI>().text = "<Text>";
    }

    void UpdateFrameTextUI(string t)
    {
        frameTextUI.transform.GetComponent<TextMeshProUGUI>().text = t;
    }

    void UpdateAvatarUIPose(Vector3 p)
    {
        AvatarUI.transform.position = p;
        AvatarUI.transform.rotation = Quaternion.LookRotation(Vector3.zero - this.transform.position, Vector3.up);
    }

    void CheckAndActiveUI()
    {
        if (!AvatarUI.gameObject.activeSelf) 
            AvatarUI.gameObject.SetActive(true);
    }
}
