using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.VFX;

public class ixTrailAnimatorVFX : MonoBehaviour
{
    VFXTrailRenderer vfxTrailRenderer;

    //[SerializeField] VisualEffect VFX;
    VisualEffect VFX;

    [SerializeField] GameObject UIContent;
    [SerializeField] GameObject AvatarUI;

    public bool EnableAnim = false;
    [HideInInspector] public bool ShowContextAvatarUI = false;
    public int curFrame = 0;

    // Start is called before the first frame update
    void Start()
    {
        VFX = this.transform.parent.transform.GetComponent<VisualEffect>();
        vfxTrailRenderer = this.transform.parent.transform.GetComponent<VFXTrailRenderer>();
        InitUI();
    }

    bool IsDatareadyLastFrame = false;

    // Update is called once per frame
    void Update()
    {
        if (EnableAnim && vfxTrailRenderer.IsDataReady) // double check 
        {
            CheckAndActiveUI();
            ApplyAnim();
        }
    }

    int AnimFrameLimit = 0;

    void ApplyAnim()
    {
        //
        if (vfxTrailRenderer.sampledPointList.Count > 0) // if use the sampled point list 
            AnimFrameLimit = vfxTrailRenderer.sampledPointList.Count;
        else
            AnimFrameLimit = vfxTrailRenderer.pointList.Count;

        //
        if (curFrame < AnimFrameLimit)
        {
            Vector3 animPosition = Vector3.zero;
            Quaternion animRotation = Quaternion.identity;

            if(vfxTrailRenderer.sampledPointList.Count > 0) // if use the sampled point list 
            {
                animPosition = vfxTrailRenderer.sampledPointList[curFrame];
                if (vfxTrailRenderer.sampledRotationList.Count > 0)     
                    animRotation = vfxTrailRenderer.sampledRotationList[curFrame];
            }
            else
            {
                animPosition = vfxTrailRenderer.pointList[curFrame];
                if (vfxTrailRenderer.rotationList.Count > 0)
                    animRotation = vfxTrailRenderer.rotationList[curFrame];
            }


            //// assume this is only one scene geometry or the scale/ transform are the same  -> use the origin transformation 
            //Transform sceneGeometry = GameObject.FindGameObjectWithTag("SceneGeometry").transform;
            //if (sceneGeometry)
            //{
            //    animPosition = sceneGeometry.TransformPoint(animPosition);
            //    animRotation = sceneGeometry.rotation * animRotation;
            //}

            this.transform.position = animPosition;
            this.transform.rotation = animRotation;

            UpdateFrameTextUI("Current Frame = " + curFrame);
            UpdateAvatarUIPose(animPosition);
            if(VFX.HasInt("AnimFrame"))
                VFX.SetInt("AnimFrame", curFrame);
            curFrame++;
        }
        else
        {
            curFrame = 0;
        }
    }

    public void StartAnim()
    {
        EnableAnim = true;
        VFX.SetBool("IsAnim", true);
    }

    public void PauseResumeAnim()
    {
        EnableAnim = !EnableAnim; // avatar anim
        if(EnableAnim)
            VFX.SetBool("IsAnim", true); // traj. anim
        else
            VFX.SetBool("IsAnim", false);
    }

    public void UpdateAnimFrame(float per)
    {
        curFrame = (int)(per * vfxTrailRenderer.sampledPointList.Count);
    }

    public void UpdateAnimFrame(int f)
    {
        curFrame = f;
    }

    GameObject frameTextUI;

    void InitUI()
    {
        frameTextUI = Instantiate((GameObject)Resources.Load("UGUIText", typeof(GameObject)), UIContent.transform);
        frameTextUI.transform.GetComponent<TextMeshProUGUI>().text = "<Text>";
    }

    void UpdateFrameTextUI(string t)
    {
        frameTextUI.transform.GetComponent<TextMeshProUGUI>().text = t;
    }

    void UpdateAvatarUIPose(Vector3 p)
    {
        if (AvatarUI)
        {
            AvatarUI.transform.position = p;
            AvatarUI.transform.rotation = Quaternion.LookRotation(Vector3.zero - this.transform.position, Vector3.up);
        }
    }

    void CheckAndActiveUI()
    {
        if (!ShowContextAvatarUI) return;

        if(AvatarUI)
            if (!AvatarUI.gameObject.activeSelf) 
                AvatarUI.gameObject.SetActive(true);
    }
}
