using Bibcam.Decoder;
using JfranMora.Inspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Video;
using Random = System.Random;

public enum PathEncoding
{
	Random,
	CameraSpeed,
	CrowdState
}

public class VFXTrailRenderer : MonoBehaviour
{
	[SerializeField] public VisualEffect TrailVFX;
	[SerializeField] List<VisualEffectAsset> VFXAssetCandidates = new List<VisualEffectAsset>();
	[SerializeField] public GameObject Avatar;
	[SerializeField] public GameObject TimeScrubber;
	[SerializeField] public GameObject ClipStartPrefeb;
	[SerializeField] public GameObject ClipEndPrefeb;
	GameObject ClipStartIndocator;
	GameObject ClipEndIndicator;
	uint VFXCapacity = 1000000; // default value for rendering vfxs


	[HideInInspector] public List<float> timeList = new List<float>();
	[HideInInspector] public List<Vector3> pointList = new List<Vector3>();
	[HideInInspector] public List<Vector3> pointListNonDul = new List<Vector3>();
	[HideInInspector] public List<float> speedList = new List<float>();
	[HideInInspector] public List<Color> colorList = new List<Color>();
	[HideInInspector] public List<Color> randomColorList = new List<Color>();
	[HideInInspector] public List<float> timeValueList = new List<float>();
	[HideInInspector] public List<Quaternion> rotationList = new List<Quaternion>();
	[HideInInspector] public List<Vector3> eulerAngleList = new List<Vector3>();

	[HideInInspector] public List<Vector3> sampledPointList = new List<Vector3>();
	[HideInInspector] public List<Color> sampledColorList = new List<Color>();
	[HideInInspector] public List<float> sampledTimeValueList = new List<float>();
	[HideInInspector] public List<Quaternion> sampledRotationList = new List<Quaternion>();
	[HideInInspector] public List<Vector3> sampledEulerAngleList = new List<Vector3>();

	[HideInInspector] public List<int> KeyFrames = new List<int>();
	[HideInInspector] public List<Vector3> TransformationListP = new List<Vector3>();
	[HideInInspector] public List<Quaternion> TransformationListR = new List<Quaternion>();

	[HideInInspector] public bool IsDataReady = false;
	[HideInInspector] public bool DidSampled = false;

	Color[] colors = new Color[6];

	public GraphicsBuffer timeValueBuffer;
	public GraphicsBuffer colorBuffer;
	public GraphicsBuffer pointBuffer;
	public GraphicsBuffer eulerAngleBuffer;
	public GraphicsBuffer humanCountBuffer;
	public GraphicsBuffer randomColorBuffer;

	VideoPlayer VideoReference;
	BibcamVideoFeeder VideoFeeder;

	PathEncoding pathEncoding = PathEncoding.CameraSpeed;

	public bool RenderColorTrail = true;
	public bool RenderTimeTick = true;
	public bool RenderTexturedFrustumVis = true;
	public bool RenderTexturedStripVis = true;
	public bool RenderPointBasedTrail = true;
	public bool RenderCameraOrientaion_LineBased = true;
	public bool RenderPathView = true;
	public bool RenderWithLines = true;

	public bool EncodePathWithSpeed = false;
	public bool EncodePathWithHumanCount = true;

	public void SetVideoReference(VideoPlayer video)
    {
		VideoReference = video;
	}

	public void SetVideoFeeder(BibcamVideoFeeder feeder)
    {
		VideoFeeder = feeder;
	}

	[SerializeField] GameObject HolderPrefeb;
	[SerializeField] public bool DoCreateHolders = false;
	public void CreateHolders()
    {
		for(int i =0;i< pointList.Count; i += 60)
		{
			GameObject holder = Instantiate(HolderPrefeb, this.transform);
			holder.GetComponent<Microsoft.MixedReality.Toolkit.UI.ObjectManipulator>().HostTransform =
				this.transform.parent.transform.parent.transform; // assign host at run time
			holder.transform.position = this.transform.TransformPoint(pointList[i]);
		}
    }

	// Start is called before the first frame update
	void Start()
    {
		//TrailVFX = GetComponent<VisualEffect>();
		//VFXCapacity = VFXInfo.capacity;
		//VFXCapacity = Math.Max(TrailVFX.GetParticleSystemInfo("PointbasedTrail").capacity, TrailVFX.GetParticleSystemInfo("StripTrails").capacity);
		//VFXCapacity = 1000000;

		colors[0] = Color.cyan;
		colors[1] = Color.red;
		colors[2] = Color.green;
		colors[3] = new Color(255, 165, 0);
		colors[4] = Color.yellow;
		colors[5] = Color.magenta;

		if (VFXAssetCandidates.Count > 0)
			TrailVFX.visualEffectAsset = VFXAssetCandidates[0];

		ixUtility.LogMe("VFXCapacity = " + VFXCapacity);
	}

	[SerializeField] Gradient SpeedMapping;

	[SerializeField] Gradient HumanCountMapping;

	[SerializeField] int NumOfHumanMax = 7;

	void GenerateColorList(PathEncoding pe)
	{
        //if (pe == PathEncoding.Random)
        //{

		//
		randomColorList.Clear();

		//
        if (humanCountArray.Count > 0)
        {
			for (int i = 0; i < pointList.Count; i++)
            {
				if (i < humanCountArray.Count)
                {
					float r = humanCountArray[i] / ((float)NumOfHumanMax);
					randomColorList.Add(HumanCountMapping.Evaluate(r));
                }
			}
			Debug.Log("Do have human count information.");
		}
   //     else
   //     {
			//// CREATE COLOR DUMMY DATA
			//Random rnd = new Random();
			//for (int i = 0; i < pointList.Count; i++)
			//{
			//	int colorLength = rnd.Next(50, 200);
			//	//int rndColor = rnd.Next(0, colors.Length);
			//	float r = UnityEngine.Random.Range(0, 1.0f);
			//	Color c = HumanCountMapping.Evaluate(r);
			//	for (int j = 0; j < colorLength; j++)
			//	{
			//		if (randomColorList.Count >= pointList.Count - 1)
			//			break;
			//		randomColorList.Add(c);
			//	}
			//}
   //     }

        //}
        //else if(pe == PathEncoding.CameraSpeed)
        //{
			//
			colorList.Clear();

			//// remove dulplicated points
			//pointListNonDul.Clear();
			//Vector3 lastPoint;
			float delta = 0.0001f;
			//pointListNonDul.Add(pointList[0]);
			//for (int i =0;i< pointList.Count-1; i++)
			//{
			//	float dist = Vector3.Distance(pointList[i + 1], pointList[i]);
			//	if (dist > delta)
   //             {
			//		pointListNonDul.Add(pointList[i + 1]);
   //             }
   //         }

			// compute speed list
			speedList.Clear();
			float lastNonZeroDist = delta;
			float minSpeed = float.MaxValue;
			float maxSpeed = 0;
			for (int i = 0; i < pointList.Count - 1; i++)
			{
				float dist = Vector3.Distance(pointList[i + 1], pointList[i]);
				float speed = dist;
				if (dist < delta) speed = lastNonZeroDist;
				speedList.Add(speed);
				if (dist > 0.0001f) lastNonZeroDist = dist;
				if (speed < minSpeed)
					minSpeed = speed;
				if (speed > maxSpeed)
					maxSpeed = speed;
			} speedList.Add(0);

			// normalize speed to 0-1
			for(int i =0;i< speedList.Count; i++)
			{
				speedList[i] = (speedList[i] - minSpeed) / (maxSpeed - minSpeed);
			}

			//
			for(int i = 0; i < speedList.Count; i++)
			{
				colorList.Add(SpeedMapping.Evaluate(speedList[i]));
			}
        //}
	}

	void GenerateTimeValues(int pointCount)
	{
		//CREATE AUDIO DUMMY DATA
		timeValueList.Clear();
		for (int i = 0; i < pointCount; i++)
		{
			//int noise = rnd.Next(0, 1);
			//float loudnessValue = Mathf.Sin(i / 10.0f);
			timeValueList.Add( i);
		}
	}

	void GenerateEulerAngleList(List<Quaternion> quaternionList)
	{
		//Convert quaternion to eulerAngles for VFX graph
		eulerAngleList.Clear();
		foreach (var quaternion in quaternionList)
		{
			eulerAngleList.Add(quaternion.eulerAngles);
		}
	}

	[SerializeField] GameObject PreviewQuad;
	[SerializeField] Texture VideoTexture;
	[SerializeField] TMPro.TMP_Text TimeText;
	[SerializeField] TMPro.TMP_Text FrameText;

	int lastFrame = 0;

	void PutItToTheNearestPointOnTrail(ref GameObject o)
    {
		if (IsDataReady && (pointList.Count > 0) && (rotationList.Count > 0) && o) {
			int nearestPointIdx = 0;
			float minDist = Vector3.Distance(o.transform.position, this.transform.TransformPoint(pointList[0]));
			for (int i = 0; i < pointList.Count; i++)
			{
				float dist = Vector3.Distance(o.transform.position, this.transform.TransformPoint(pointList[i]));
				if (dist < minDist)
                {
					nearestPointIdx = i;
					minDist = dist;
				}
			}
			o.transform.position = this.transform.TransformPoint(pointList[nearestPointIdx]);
			o.transform.rotation = this.transform.rotation * rotationList[nearestPointIdx];

            if (VideoReference && PreviewQuad)
            {
				VideoReference.frame = nearestPointIdx;
				if (nearestPointIdx != lastFrame)
					VideoReference.StepForward();
				if (nearestPointIdx == lastFrame && VideoReference.isPrepared)
					VideoReference.Pause();
				if (nearestPointIdx != lastFrame) {
					VideoTexture = VideoReference.texture;
					VideoFeeder.DecodeFrame(VideoTexture);
					PreviewQuad.GetComponent<MeshRenderer>().material.mainTexture = VideoFeeder.RefDemuxer().ColorTexture;
					FrameText.text = nearestPointIdx.ToString();
					TimeText.text = FrameToTimeString(nearestPointIdx);
				}

				lastFrame = nearestPointIdx;
			}
		}
	}

	public bool DoSnap = false;
	public bool DoAnim = true;
	public bool AllowHandSeek = false;

	// Update is called once per frame
	void Update()
    {
		if (AllowHandSeek)
		{
			// checking distance bet. hand and time scrubber
			GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
			if (rhand)
            {
				float dist = Vector3.Distance(rhand.transform.position, TimeScrubber.transform.position);
				if(dist < 0.4 * this.transform.parent.transform.parent.transform.localScale.x)
				{
					TimeScrubber.transform.position = rhand.transform.position; // moving the time pointer with rhand. If near, do snapping like always 
					DoSnap = true;
					DoAnim = false;
				}
				//else
				//{
				//	DoSnap = false; // if far away, will stop snapping and stop animation
				//	DoAnim = true;
				//	//VideoReference.Pause(); // pause video, pause anim, pause vfx
				//}
            }
        }
        if (DoSnap && IsDataReady && (pointList.Count > 0) && (rotationList.Count > 0) && TimeScrubber)
        {
			//PutStartToTheNearestPointOnTrail(ClipStart); // pass the reference
			//PutEndToTheNearestPointOnTrail(ClipEnd);
			PutItToTheNearestPointOnTrail(ref TimeScrubber);
		}
        if (DoAnim && IsDataReady && (pointList.Count > 0) && (rotationList.Count > 0) && TimeScrubber && VideoReference)
        {
			int frame = (int)VideoReference.frame;
			if (frame >= 0 && frame < pointList.Count)
            {
				TimeScrubber.transform.position = this.transform.TransformPoint(pointList[frame]);
				TimeScrubber.transform.rotation = this.transform.rotation * rotationList[frame];

				if (VideoReference && PreviewQuad)
				{
					PreviewQuad.GetComponent<MeshRenderer>().material.mainTexture = VideoFeeder.RefDemuxer().ColorTexture;
					FrameText.text = frame.ToString();
					TimeText.text = FrameToTimeString(frame);
					VideoReference.Play();
				}
            }
		}
    }

	public void SnapTimeScrubberToPosition(Vector3 newPosition)
    {
		TimeScrubber.transform.position = newPosition;
		PutItToTheNearestPointOnTrail(ref TimeScrubber);
	}

	public int GetCurrentFrameNumber()
    {
		int nearestPointIdx = 0;
		if (IsDataReady && (pointList.Count > 0) && (rotationList.Count > 0) && TimeScrubber)
		{
			float minDist = Vector3.Distance(TimeScrubber.transform.position, this.transform.TransformPoint(pointList[0]));
			for (int i = 0; i < pointList.Count; i++)
			{
				float dist = Vector3.Distance(TimeScrubber.transform.position, this.transform.TransformPoint(pointList[i]));
				if (dist < minDist)
				{
					nearestPointIdx = i;
					minDist = dist;
				}
			}
		}
		return nearestPointIdx;
	}

	public Vector3 GetPositionGivenFrame(int frame)
    {
		return this.transform.TransformPoint(pointList[frame]); // global position
	}

	public Quaternion GetRotationGivenFrame(int frame)
	{
		return this.transform.rotation * rotationList[frame]; // global rotation
	}

	public void InstantiateOrMoveStartIndicator(int startFrame)
    {
        if (!ClipStartIndocator)
        {
			ClipStartIndocator = Instantiate(ClipStartPrefeb, this.transform);
        }
		Vector3 position = this.transform.TransformPoint(pointList[startFrame]);
		ClipStartIndocator.transform.position = position;

		//
		TrailVFX.SetInt("FrameStart", startFrame);
	}

	public void InstantiateOrMoveEndIndicator(int endFrame)
	{
		if (!ClipEndIndicator)
		{
			ClipEndIndicator = Instantiate(ClipEndPrefeb, this.transform);
		}
		Vector3 position = this.transform.TransformPoint(pointList[endFrame]);
		ClipEndIndicator.transform.position = position;

		// update vfx
		TrailVFX.SetInt("FrameEnd", endFrame);
	}

	public VisualEffect RefVFX()
    {
		return TrailVFX;
    }

	/////////////////////////////////////////////////////////////////////////////////////// Creation
	public void SwitchToStripBasedVFXRenderer()
    {
		TrailVFX.Stop();
		TrailVFX.SendEvent("OnStop");
		TrailVFX.visualEffectAsset = VFXAssetCandidates[0];
		TrailVFX.Reinit();
		TrailVFX.Play();
		TrailVFX.SendEvent("OnPlay");
	}

	public void SwitchToPointBasedVFXRenderer()
    {
        TrailVFX.Stop();
		TrailVFX.SendEvent("OnStop");
		TrailVFX.visualEffectAsset = VFXAssetCandidates[1];
		TrailVFX.Reinit();
		TrailVFX.Play();
		TrailVFX.SendEvent("OnPlay");
	}

	public void CreateTrailBaseOnPointList(List<Vector3> points, Color c)
    {
		pointList = points;

		//GenerateEulerAngleList(pointList.Count);
		GenerateTimeValues(pointList.Count);
		GenerateColorList(pathEncoding);

		TrailVFX.SetVector4("Color", new Vector3(c.r, c.g, c.b));

		UpdateGPU();
	}

	public void CreateTrailBaseOnPointAndRotationList(List<Vector3> points, List<Quaternion> rotations, Color c)
    {


		pointList = points;
		rotationList = rotations;

		GenerateEulerAngleList(rotations);
		GenerateTimeValues(pointList.Count);
		GenerateColorList(pathEncoding);

		TrailVFX.SetVector4("Color", new Vector3(c.r, c.g, c.b));
		UpdateGPU();
	}

	[Button]
	public void ApplyDefaultRenderingParameters()
    {
		RenderWithLines = true;
		RenderColorTrail = true;
		RenderTimeTick = false;
		RenderTexturedFrustumVis = false;
		RenderTexturedStripVis = false;
		RenderPointBasedTrail = false;
		RenderCameraOrientaion_LineBased = false;
		RenderPathView = false;

		EncodePathWithSpeed = true;
		EncodePathWithHumanCount = false;

		SwitchRendering();
	}

	[Button]
	public void SwitchRendering()
    {
		StopVFX();
		TrailVFX.SetBool("RenderColorTrail", RenderColorTrail);
		TrailVFX.SetBool("RenderTimeTick", RenderTimeTick);
		TrailVFX.SetBool("RenderTexturedFrustumVis", RenderTexturedFrustumVis);
		TrailVFX.SetBool("RenderTexturedStripVis", RenderTexturedStripVis); //
		TrailVFX.SetBool("RenderPointBasedTrail", RenderPointBasedTrail);
		TrailVFX.SetBool("RenderCameraOrientaion_LineBased", RenderCameraOrientaion_LineBased);
		TrailVFX.SetBool("RenderPathView", RenderPathView);
		TrailVFX.SetBool("RenderWithLines", RenderWithLines);

		TrailVFX.SetBool("EncodePathWithSpeed", EncodePathWithSpeed);
		TrailVFX.SetBool("EncodePathWithHumanCount", EncodePathWithHumanCount);
		StartVFX();
    }

	void UpdateGPU()
	{
		TrailVFX.Stop();
		TrailVFX.SendEvent("OnStop");
		TrailVFX.Reinit();
		//
		if (pointList.Count > VFXCapacity)
		{
			DidSampled = true;
			float sf = ((float)VFXCapacity) / pointList.Count;
			int pointInterval = (int)(sf * 100); // percentage

			ixUtility.LogMe("Too Many Points! Sampling Before Sending To GPU: Rate = " + sf);

			if (pointInterval < 1)
			{
				ixUtility.LogMe("Error Sampling Rate: " + sf);
				//yield return null;
				return;
			}

			sampledPointList.Clear();
			sampledRotationList.Clear();
			sampledColorList.Clear();
			sampledTimeValueList.Clear();
			sampledEulerAngleList.Clear();
			for (int i = 0; i < pointList.Count; i += 1)
			{
				if (i % pointInterval == 0)
				{

				}
				else
				{
					sampledColorList.Add(colorList[i]);
					sampledPointList.Add(pointList[i]);
					sampledRotationList.Add(rotationList[i]);
					sampledTimeValueList.Add(timeValueList[i]);
					sampledEulerAngleList.Add(eulerAngleList[i]);

				}
			}

			//
			pointBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, sampledPointList.Count, sizeof(float) * 3);
			pointBuffer.SetData(sampledPointList);
			TrailVFX.SetGraphicsBuffer("PositionBuffer", pointBuffer);

			colorBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, sampledColorList.Count, sizeof(float) * 4);
			colorBuffer.SetData(sampledColorList);
			TrailVFX.SetGraphicsBuffer("ColorBuffer", colorBuffer);

			timeValueBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, sampledTimeValueList.Count, sizeof(float) * 3);
			timeValueBuffer.SetData(sampledTimeValueList);
			TrailVFX.SetGraphicsBuffer("TimeValueBuffer", timeValueBuffer);

			eulerAngleBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, sampledEulerAngleList.Count, sizeof(float) * 3);
			eulerAngleBuffer.SetData(sampledEulerAngleList);
			TrailVFX.SetGraphicsBuffer("EulerAngleBuffer", eulerAngleBuffer);

			// add human count

			TrailVFX.SetInt("MaxPointIdx", sampledPointList.Count);
			TrailVFX.SetInt("MinPointIdx", 0);
			TrailVFX.SetBool("IsAnim", false);
		}
		else
		{
			DidSampled = false;
			//
			pointBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, pointList.Count, sizeof(float) * 3);
			pointBuffer.SetData(pointList);
			TrailVFX.SetGraphicsBuffer("PositionBuffer", pointBuffer);


			colorBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, colorList.Count, sizeof(float) * 4);
			colorBuffer.SetData(colorList);
			TrailVFX.SetGraphicsBuffer("ColorBuffer", colorBuffer);

			timeValueBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, timeValueList.Count, sizeof(float));
			timeValueBuffer.SetData(timeValueList);
			TrailVFX.SetGraphicsBuffer("TimeValueBuffer", timeValueBuffer);

			eulerAngleBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, eulerAngleList.Count, sizeof(float) * 3);
			eulerAngleBuffer.SetData(eulerAngleList);
			TrailVFX.SetGraphicsBuffer("EulerAngleBuffer", eulerAngleBuffer);

            if (humanCountArray.Count > 0)
            {
				humanCountBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, humanCountArray.Count, sizeof(float));
				humanCountBuffer.SetData(humanCountArray);
				//if(TrailVFX.HasGraphicsBuffer("HumanCountBuffer"))
				//TrailVFX.SetGraphicsBuffer("HumanCountBuffer", humanCountBuffer);
            }

            if (randomColorList.Count > 0)
            {
				randomColorBuffer = new GraphicsBuffer(
					GraphicsBuffer.Target.Structured, randomColorList.Count, sizeof(float) * 4);
				randomColorBuffer.SetData(randomColorList);
				TrailVFX.SetGraphicsBuffer("RandomColorBuffer", randomColorBuffer);
			}

			TrailVFX.SetInt("MaxPointIdx", pointList.Count);
			if(TrailVFX.HasInt("MinPointIdx"))
				TrailVFX.SetInt("MinPointIdx", 0);
			TrailVFX.SetBool("IsAnim", false);
		}

		//TrailVFX.SetBool("RenderColorTrail", RenderColorTrail);
		//TrailVFX.SetBool("RenderTimeTick", RenderTimeTick);
		//TrailVFX.SetBool("RenderTexturedFrustumVis", RenderTexturedFrustumVis);
		//TrailVFX.SetBool("RenderTexturedStripVis", RenderTexturedStripVis); //
		//TrailVFX.SetBool("RenderPointBasedTrail", RenderPointBasedTrail);
		//TrailVFX.SetBool("RenderCameraOrientaion_LineBased", RenderCameraOrientaion_LineBased);

		//TrailVFX.Play();
		//TrailVFX.SendEvent("OnPlay");

		// init
		EncodePathWithSpeed = false;
		EncodePathWithHumanCount = true;

		SwitchRendering();

		IsDataReady = true;
		if (Avatar)
			Avatar.SetActive(false);

        // initialize
        if (pointList.Count > 0 && TimeScrubber)
			TimeScrubber.transform.position = this.transform.TransformPoint(pointList[0]);

		// after creation
		if(DoCreateHolders) 
			CreateHolders();

		// apply after creation
		ApplyDefaultRenderingParameters();

		//
	}

	IEnumerator ReadPointsFromCsvCoroutine(string file)
	{
		pointList.Clear();
		rotationList.Clear();

		StreamReader reader = new StreamReader(file);
		var header = reader.ReadLine();
		while (!reader.EndOfStream)
		{
			var line = reader.ReadLine();

			//
			//float randomNumber = Random.Range(0.0f, 1.0f);
			//if (randomNumber > downScaleFactor) continue;

			var values = line.Split(',');

			Vector3 point = new Vector3(
				float.Parse(values[1].ToString()),
				float.Parse(values[2].ToString()),
				float.Parse(values[3].ToString()));

			pointList.Add(point);

			Quaternion rotation = new Quaternion(
				float.Parse(values[5].ToString()), // x
				float.Parse(values[6].ToString()),
				float.Parse(values[7].ToString()),
				float.Parse(values[4].ToString())); // w

			rotationList.Add(rotation);

			//yield return new WaitForSeconds(0.001f); // might be too slow
		}

		yield return new WaitForSeconds(0.001f);

		Debug.Log("Start Building Trajectories,  pointList.Count = " + pointList.Count);
		ixUtility.LogMe("Start Building Trajectories,  pointList.Count = " + pointList.Count);
		ixUtility.LogMe("VFXCapacity = " + VFXCapacity);


		GenerateColorList(pathEncoding);
		GenerateTimeValues(pointList.Count);
		GenerateEulerAngleList(rotationList);

		UpdateGPU();

		yield return null;
	}

	public void CreateTrailBaseOnFileAndColor(string fn, Color c) {
		IsDataReady = false;
		if (Avatar) Avatar.SetActive(false);
		StartCoroutine(ReadPointsFromCsvCoroutine(fn));
		TrailVFX.SetVector4("Color", new Vector3(c.r, c.g, c.b));
	}


	List<RenderTexture> TrailTextures = new List<RenderTexture>();
	public Bounds TrailBounds = new Bounds();

	public void LoadMetaDataFromFolder(string folderPath)
    {
		string fileToRead = folderPath + "/VideoMetaData.csv";

		Vector3 minPoint = -Vector3.one;
		Vector3 maxPoint = Vector3.one;
		int frameStart = 0;
		int frameStop = int.MaxValue;

		StreamReader reader = new StreamReader(fileToRead);
		while (!reader.EndOfStream)
		{
			var line = reader.ReadLine();
			var values = line.Split(',');
			if (values[0].ToString() == "VideoBoundsMin")
			{
				minPoint = new Vector3(
					float.Parse(values[1].ToString()),
					float.Parse(values[2].ToString()),
					float.Parse(values[3].ToString()));
			}
			if (values[0].ToString() == "VideoBoundsMax")
			{
				maxPoint = new Vector3(
					float.Parse(values[1].ToString()),
					float.Parse(values[2].ToString()),
					float.Parse(values[3].ToString()));
			}
			if (values[0].ToString() == "VideoStartFrame")
			{
				frameStart = int.Parse(values[1].ToString());
			}
			if (values[0].ToString() == "VideoStopFrame")
			{
				frameStop = int.Parse(values[1].ToString());
			}
		}

		TrailBounds.SetMinMax(minPoint, maxPoint);
	}

	public void LoadTrailTexturesFromFolder(string folderPath)
    {
		/// Load frame textures
		long numFrames = pointList.Count;
		if (numFrames == 0)
		{
			Debug.Log("Tube has no points! ");
			return;
		}
		for (int i = 0; i < numFrames; i++)
		{
			string fn = folderPath + i + ".jpg";
			if (System.IO.File.Exists(fn))
			{
				// Load the image data into a Texture2D
				byte[] imageBytes = File.ReadAllBytes(fn);
				Texture2D frametex = new Texture2D(2, 2);
				frametex.LoadImage(imageBytes);

				// Create a temporary RenderTexture with the same dimensions as the loaded image
				RenderTexture tempRT = new RenderTexture(frametex.width, frametex.height, 0, RenderTextureFormat.ARGB32);
				tempRT.enableRandomWrite = true;
				tempRT.Create();

				// Set the target RenderTexture as the active RenderTexture
				RenderTexture.active = tempRT;

				// Copy the Texture2D data into the RenderTexture
				Graphics.Blit(frametex, tempRT);

				TrailTextures.Add(tempRT);

				// Clean up temporary objects
				Destroy(frametex);
			}
		}

		//
		Texture2DArray texture2DArray = new Texture2DArray(TrailTextures[0].width, TrailTextures[0].height, TrailTextures.Count,
			TextureFormat.RGBA32, false, false);
		for (int i = 0; i < TrailTextures.Count; i++)
			Graphics.CopyTexture(TrailTextures[i], 0, 0, 0, 0, TrailTextures[i].width, TrailTextures[i].height, texture2DArray, i, 0, 0, 0);

		//
		TrailVFX.SetTexture("TextureArray", texture2DArray);
	}

	/////////////////////////////////////////////////////////////////////////////////////// Modify
	public void AdjustColor(Color c) {
		TrailVFX.SetVector4("Color", new Vector3(c.r, c.g, c.b));
	}

	public void ComputeTrackingJumps()
    {

    }

	[SerializeField] float GeometryOffset = 0.1f;
	[SerializeField] float offPercentage = 0.2f;


	[Button]
	public void AddDummyGeometryIndicator()
    {
		List<Vector3> newPointList = new List<Vector3>();
		for(int i = 0; i < pointList.Count - 1; i++)
        {
			newPointList.Add(pointList[i]);
			//
			// bet. pointList[i] pointlist[i+1]
			float r = UnityEngine.Random.Range(0.0f, 1.0f);
			if(r < offPercentage)
            {
				Vector3 avg = (pointList[i] + pointList[i + 1]) / 2.0f;
				Vector3 first = (pointList[i] + avg) / 2.0f;
				Vector3 second = (pointList[i + 1] + avg) / 2.0f;
				Vector3 higher = new Vector3(first.x, first.y + GeometryOffset, first.z);
				Vector3 lower = new Vector3(second.x, second.y - GeometryOffset, second.z);
				newPointList.Add(higher);
				newPointList.Add(avg);
				newPointList.Add(lower);
			}
		}
		pointList = newPointList;
		UpdateGPU();
	}

	public void RestartVFX()
	{
		TrailVFX.SendEvent("OnStop");
		TrailVFX.Stop();
		TrailVFX.Reinit();
		TrailVFX.Play();
		TrailVFX.SendEvent("OnPlay");
	}

	public void StopVFX()
	{
		TrailVFX.SendEvent("OnStop");
		TrailVFX.Stop();
		TrailVFX.Reinit();
	}

	public void StartVFX()
	{
		TrailVFX.Play();
		TrailVFX.SendEvent("OnPlay");
	}
	/////////////////////////////////////////////////////////////////////////////////////// IO
	public void WriteToFile(string folderPath, string filePath)
	{
		//string folderPath = GameObject.FindGameObjectWithTag("StringManager")
		//	.GetComponent<StringManager>().curFolderPath;
		//string fileName = GameObject.FindGameObjectWithTag("StringManager")
		//	.GetComponent<StringManager>().curFileName;

		string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(filePath) + "_ExtractedData/";
		if (!System.IO.Directory.Exists(extractedDataFolder))
			System.IO.Directory.CreateDirectory(extractedDataFolder);
		string fileToWrite = extractedDataFolder + "/CameraTrails.csv";

		StreamWriter writer = new StreamWriter(fileToWrite);

		// write header
		writer.WriteLine("Time,PositionX,PositionY,PositionZ,QuatW,QuatX,QuatY,QuatZ,");

		//
		if (timeList.Count != pointList.Count)
		{
			timeList.Clear();
			for (int i = 0; i < pointList.Count; i++)
			{
				timeList.Add(i);
			}
		}

		//
		for (int i = 0; i < pointList.Count; i++)
		{
			writer.WriteLine(
					timeList[i] + ","
				+ pointList[i].x + "," + pointList[i].y + "," + pointList[i].z + ","
				+ rotationList[i].x + "," + rotationList[i].y + "," + rotationList[i].z + "," + rotationList[i].w);
		}
		writer.Close();

		Debug.Log("Written To: " + fileToWrite);
	}

	public void WritePathToFile()
	{
		string folderPath = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFolderPath;
		string fileName = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFileName;

		string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
		if (!System.IO.Directory.Exists(extractedDataFolder))
			System.IO.Directory.CreateDirectory(extractedDataFolder);
		string fileToWrite = extractedDataFolder + "/VideoPath.csv";

		StreamWriter writer = new StreamWriter(fileToWrite);

		// write header
		writer.WriteLine("Time,PositionX,PositionY,PositionZ,QuatW,QuatX,QuatY,QuatZ,");

		//
		if (timeList.Count != pointList.Count)
		{
			timeList.Clear();
			for (int i = 0; i < pointList.Count; i++)
			{
				timeList.Add(i);
			}
		}

		//
		for (int i = 0; i < pointList.Count; i++)
		{
			writer.WriteLine(
					timeList[i] + ","
				+ pointList[i].x + "," + 0 + "," + pointList[i].z + "," // 0 instead of pointList[i].y here!
				+ rotationList[i].x + "," + rotationList[i].y + "," + rotationList[i].z + "," + rotationList[i].w);
		}
		writer.Close();

		Debug.Log("Written To: " + fileToWrite);
	}

	public void TryReadFromFile()
	{
		string folderPath = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFolderPath;
		string fileName = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFileName;


		string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
		string fn = extractedDataFolder + "/CameraTrails.csv";  // using another extension for reading diff. related files

		if (System.IO.File.Exists(fn))
			CreateTrailBaseOnFileAndColor(fn, Color.green);
		else
			Debug.Log("CSV File Not Exist!");
	}

	public void TryReadPathFromFile()
	{
		string folderPath = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFolderPath;
		string fileName = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFileName;


		string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
		string fn = extractedDataFolder + "/VideoPath.csv";  // using another extension for reading diff. related files

		if (System.IO.File.Exists(fn))
			CreateTrailBaseOnFileAndColor(fn, Color.green);
		else
			Debug.Log("CSV File Not Exist!");
	}

	List<float> humanCountArray = new List<float>();

	public void TryReadHumanCountFile()
	{
		string folderPath = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFolderPath;
		string fileName = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFileName;


		string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
		string fn = extractedDataFolder + "/HumanCount.csv";  // using another extension for reading diff. related files


		if (System.IO.File.Exists(fn))
		{
			humanCountArray.Clear();
			StreamReader reader = new StreamReader(fn);
			var line = reader.ReadLine(); // header
			while (!reader.EndOfStream)
			{
				line = reader.ReadLine();
				var values = line.Split(',');
				humanCountArray.Add((float)int.Parse(values[1].ToString()));
			}
			Debug.Log("Human Count File Loaded!");
		}
		else
			Debug.Log("CSV File Not Exist!");
	}

	public void ClearTrail()
	{
		timeList.Clear();
		pointList.Clear();
		rotationList.Clear();
		UpdateGPU();
	}

	public static string FrameToTimeString(int frame)
	{
		const int frameRate = 60; // Currently the bibcam video format is using this number

		var totalSeconds = (float)frame / frameRate;

		var hours = (int)totalSeconds / 3600;
		var minutes = (int)(totalSeconds % 3600 / 60);
		var seconds = (int)totalSeconds % 60;

		var fractionalSeconds = (int)((totalSeconds - (int)totalSeconds) * 100);

		var hoursString = hours.ToString().PadLeft(2, '0');
		var minutesString = minutes.ToString().PadLeft(2, '0');
		var secondsString = seconds.ToString().PadLeft(2, '0');
		var fractionalSecondsString = fractionalSeconds.ToString();

		return hoursString + ":" + minutesString + ":" + secondsString + "." + fractionalSecondsString;
	}
}
