using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.IO;
using System;

public class ixJumpPointManager : MonoBehaviour
{
    [Header("----------")]
    [SerializeField] GameObject UIContent;
    [Header("----------")]
    [SerializeField] bool RenderJumps;
    [SerializeField] GameObject JumpStartObjectPrefeb;
    [SerializeField] GameObject JumpEndObjectPrefeb;
    [SerializeField] Color StartColor;
    [SerializeField] Color EndColor;
    [Header("----------")]
    [SerializeField] GameObject TubePrefeb;

    [HideInInspector]public List<Tuple<string, Vector3, Vector3, Color>> JumpList = new List<Tuple<string, Vector3, Vector3, Color>>();
    List<GameObject> JumpObjectList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClearJumpData()
    {
        JumpList.Clear();
    }

    public void AppendJumpData(Vector3 startJump, Vector3 endJump)
    {
        JumpList.Add(new Tuple<string, Vector3, Vector3, Color>("-1", startJump, endJump, Color.green));
    }

    void LoadTrackingJumpData()
    {
        //
        JumpList.Clear();

        //
        string filepath = GameObject.FindGameObjectWithTag("StringManager")
                .GetComponent<StringManager>().curActiveFilePath;

        StreamReader reader = new StreamReader(filepath);
        var header = reader.ReadLine();
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();

            var values = line.Split(',');

            string UserID = values[0].ToString();
            Vector3 JumpStart = new Vector3(
                float.Parse(values[1].ToString()),
                float.Parse(values[2].ToString()),
                float.Parse(values[3].ToString()));
            Vector3 JumpEnd = new Vector3(
                float.Parse(values[4].ToString()),
                float.Parse(values[5].ToString()),
                float.Parse(values[6].ToString()));

            JumpList.Add(new Tuple<string, Vector3, Vector3, Color>(UserID, JumpStart, JumpEnd, Color.green));
        }
    }

    void RenderJumpObjects()
    {
        //
        foreach (var jo in JumpObjectList)
        {
            Destroy(jo);
        }
        JumpObjectList.Clear();

        // adjust color 
        for(int i = 0; i < JumpList.Count; i++)
        {
            float per = (float)i / JumpList.Count;
            Color c = Color.Lerp(StartColor, EndColor, per);
            JumpList[i] = new Tuple<string, Vector3, Vector3, Color>(
                JumpList[i].Item1,
                JumpList[i].Item2,
                JumpList[i].Item3, 
                c
            );
        }

        //
        foreach (var j in JumpList)
        {
            GameObject jumpStartObject = Instantiate(JumpStartObjectPrefeb, this.transform);
            jumpStartObject.transform.position = j.Item2;
            jumpStartObject.GetComponent<MeshRenderer>().material.color = j.Item4;
            JumpObjectList.Add(jumpStartObject);

            GameObject jumpEndObject = Instantiate(JumpEndObjectPrefeb, this.transform);
            jumpEndObject.transform.position = j.Item3;
            jumpEndObject.GetComponent<MeshRenderer>().material.color = j.Item4;
            JumpObjectList.Add(jumpEndObject);
        }
    }

    void RenderJumpsInStartEndColor()
    {
        //
        foreach (var jo in JumpObjectList)
        {
            Destroy(jo);
        }
        JumpObjectList.Clear();

        //
        foreach (var j in JumpList)
        {
            GameObject jumpStartObject = Instantiate(JumpStartObjectPrefeb, this.transform);
            jumpStartObject.transform.position = j.Item2;
            jumpStartObject.GetComponent<MeshRenderer>().material.color = StartColor;
            JumpObjectList.Add(jumpStartObject);

            GameObject jumpEndObject = Instantiate(JumpEndObjectPrefeb, this.transform);
            jumpEndObject.transform.position = j.Item3;
            jumpEndObject.GetComponent<MeshRenderer>().material.color = EndColor;
            JumpObjectList.Add(jumpEndObject);
        }
    }

    float deltaDist = 0.1f; // 10cm
    List<GameObject> tubes = new List<GameObject>();

    void RenderLinesBetweenJumpObjects()
    {
        //
        foreach (var tube in tubes)
        {
            Destroy(tube);
        }
        tubes.Clear();

        //
        foreach (var j in JumpList)
        {
            Vector3 dir = Vector3.Normalize(j.Item3 - j.Item2); // from start to end 
            float totalDist = Vector3.Distance(j.Item3, j.Item2);

            List<Vector3> InnerPoints = new List<Vector3>();
            InnerPoints.Add(j.Item2);
            float dist = 0;
            while (dist<totalDist)
            {
                Vector3 nextPoint = j.Item2 + dist * dir;
                InnerPoints.Add(nextPoint);
                dist += deltaDist;
            }

            var tube = Instantiate(TubePrefeb, this.transform);
            tube.GetComponent<CustomTubeRenderer>().CreateTrailBaseOnPointList(InnerPoints, j.Item4); 
            tubes.Add(tube);
        }
    }

    void DeleteAllRenderedObjects()
    {
        //
        JumpList.Clear();

        //
        foreach (var jo in JumpObjectList)
        {
            Destroy(jo);
        }
        JumpObjectList.Clear();

        //
        foreach (var tube in tubes)
        {
            Destroy(tube);
        }
        tubes.Clear();
    }


    void InitUI()
    {
        if (UIContent)
        {
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Load And Render Tracking Jumps From Active File";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    LoadTrackingJumpData();
                    RenderJumpObjects();
                    //RenderLinesBetweenJumpObjects(); // RenderJumpsInStartEndColor
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Render Jumps In Start And End Color";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    RenderJumpsInStartEndColor();
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Render Lines Between Jump Objects";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    RenderLinesBetweenJumpObjects();
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Delete All Rendered Objects";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    DeleteAllRenderedObjects();
                });
            }
        }
    }
}
