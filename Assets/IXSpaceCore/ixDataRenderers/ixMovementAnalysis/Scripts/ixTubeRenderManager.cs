using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public class ixTubeRenderManager : MonoBehaviour
{
	//
	[Header("----------")]
	[SerializeField] GameObject UIContent;
	[SerializeField] GameObject AnimUIContent; // Animation related 
	[Header("----------")]
	[SerializeField] bool RenderWithMeshRenderer;
	[SerializeField] GameObject TubePrefeb;
	[Header("----------")]
	[SerializeField] bool RenderWithVFXRenderer;
	[SerializeField] GameObject VFXTrailRendererPrefeb;
	[SerializeField] Color startColor = Color.green;
	[SerializeField] Color endColor = Color.red;

	//
	List<Tuple<string, Action<GameObject>>> featureList;
	List<Tuple<string, Action, Action>> advancedFeatureList; // name, init, update function
	List<GameObject> tubes = new List<GameObject>();
	GameObject UGUITextedSlider;

    // Start is called before the first frame update
    void Start()
    {
		InitializeUI();
		InitializeUIAdvanced();
	}

    // Update is called once per frame
    void Update()
	{
		foreach (var t in advancedFeatureList)
			t.Item3();
	}

	/// <summary>
	/// 
	/// </summary>
	void InitializeUI()
	{
		if (UIContent == null) return;

		featureList = new List<Tuple<string, Action<GameObject>>>();
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"[CustomTubeRenderer] Append Trail Base On Active File",
			delegate {
				AppendTrailsBaseOnActiveFile();
			}
		));
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"[VFXTrailRenderer] Append Trail Base On Active File",
			delegate {
				string path = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
				if (Path.GetExtension(path).Equals(".csv") || Path.GetExtension(path).Equals(".CSV"))
				{
					var tube = Instantiate(VFXTrailRendererPrefeb, this.transform);
					tube.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(path, startColor); // dummy color 
					tubes.Add(tube);

					LoopAdjustColor(); // post processing 
				}
			}
		));
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"Unzip Active File and Render", 
			delegate { UnzipActiveFileAndRender(); }
		));
		featureList.Add(new Tuple<string, Action<GameObject>>("Bundle Adjust Color", delegate { LoopAdjustColor(); }));
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"Compute Tracking Jumps", delegate { 
			 
			}));
		featureList.Add(new Tuple<string, Action<GameObject>>("Larger Radius", delegate { LargerRadius(); }));
		featureList.Add(new Tuple<string, Action<GameObject>>("Smaller Radius", delegate { SmallerRadius(); }));
		featureList.Add(new Tuple<string, Action<GameObject>>("Finer Resolution", delegate { FinerResolution(); }));
		featureList.Add(new Tuple<string, Action<GameObject>>("Coarser Resolution", delegate { CoarserResolution(); }));
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"Double DownScaling Factor", delegate { DoubleDownScalingFactor(); }));
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"Half DownScaling Factor", delegate { HalfDownScalingFactor(); }));
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"Apply Active POI Position and Scaling Factor", delegate {
			if(this.transform.parent.tag == "SceneGeometry")
            {
				float f = GameObject.Find("StringManager").GetComponent<StringManager>().ActiveScalingFactor;
				Vector3 p = GameObject.Find("StringManager").GetComponent<StringManager>().ActivePOIPosition;
				Quaternion r = GameObject.Find("StringManager").GetComponent<StringManager>().ActivePOIRotation;
				this.transform.parent.transform.position = p;
				this.transform.parent.transform.rotation = r;
				this.transform.parent.transform.localScale = new Vector3(f,f,f);
			}
		}));
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"Toggle Dynamic Update", delegate {
				foreach (var t in tubes) {
					t.GetComponent<CustomTubeRenderer>().ToggleDynamicUpdate();
					ixUtility.LogMe("Tube " + t.name + "is updating dynamically = " 
						+ t.GetComponent<CustomTubeRenderer>().GetDynamicUpdateState());
				}
			}));
		featureList.Add(new Tuple<string, Action<GameObject>>(
			"Toggle Dynamic Update Trail Version", delegate {
				foreach (var t in tubes)
				{
					t.GetComponent<CustomTubeRenderer>().ToggleDynamicUpdate_Trails();
					ixUtility.LogMe("Tube " + t.name + "is updating dynamically = "
						+ t.GetComponent<CustomTubeRenderer>().GetDynamicUpdateState_Trails());
				}
			}));

		// Instantiate buttons 
		foreach (var t in featureList)
		{
			GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
			ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = t.Item1;
			ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
			ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { t.Item2(ui); });
		}
	}

	/// <summary>
	/// 
	/// </summary>
	void InitializeUIAdvanced()
    {
		advancedFeatureList = new List<Tuple<string, Action, Action>>();
		advancedFeatureList.Add(new Tuple<string, Action, Action>(
			"AnimSlider", // name
			delegate { // init function 
				if (AnimUIContent == null) return;

				UGUITextedSlider = Instantiate(
					(GameObject)Resources.Load("UGUITextedSlider_DetailPanel", typeof(GameObject)),
					AnimUIContent.transform);

				UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 0;
				UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = "00:00";
			}, 
			delegate { // update function

                if (RenderWithMeshRenderer)
                {
					if (tubes.Count > 0 && tubes[0].GetComponent<CustomTubeRenderer>().IsDataReady)
					{
						UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value =
							((float)tubes[0].transform.Find("Avatar").GetComponent<ixTrailAnimator>().curFrame)
							/ tubes[0].GetComponent<CustomTubeRenderer>().pointList.Count;
						UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text =
							tubes[0].transform.Find("Avatar").GetComponent<ixTrailAnimator>().curFrame + "/"
							+ tubes[0].GetComponent<CustomTubeRenderer>().pointList.Count;
					}
                }

                if (RenderWithVFXRenderer)
                {
					if (tubes.Count > 0 && tubes[0].GetComponent<VFXTrailRenderer>().IsDataReady)
					{
                        if (tubes[0].transform.Find("Avatar").GetComponent<ixTrailAnimatorVFX>().EnableAnim)
                        {
							// update slider 
							UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value =
								((float)tubes[0].transform.Find("Avatar").GetComponent<ixTrailAnimatorVFX>().curFrame)
								/ tubes[0].GetComponent<VFXTrailRenderer>().pointList.Count;
							UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text =
								tubes[0].transform.Find("Avatar").GetComponent<ixTrailAnimatorVFX>().curFrame + "/"
								+ tubes[0].GetComponent<VFXTrailRenderer>().pointList.Count;
                        }

					}
				}
			}));
		advancedFeatureList.Add(new Tuple<string, Action, Action>(
			"",
			delegate
			{
				GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), AnimUIContent.transform);
				ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Start Animation VFX";
				ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
				ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
					delegate {

						if (RenderWithVFXRenderer)
						{
							foreach (var t in tubes)
							{
								t.transform.Find("Avatar").GetComponent<ixTrailAnimatorVFX>().StartAnim();
							}
						}
					}
				);
			},
			delegate
			{

			}
			));
		advancedFeatureList.Add(new Tuple<string, Action, Action>(
			"",
            delegate
            {
				GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), AnimUIContent.transform);
				ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Pause/ Resume Animation";
				ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
				ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
					delegate {
						if (RenderWithMeshRenderer)
                        {
							foreach (var t in tubes)
							{
								t.transform.Find("Avatar").GetComponent<ixTrailAnimator>().PauseResumeAnim();
							}
                        }

						if (RenderWithVFXRenderer) {
							foreach (var t in tubes)
							{
								t.transform.Find("Avatar").GetComponent<ixTrailAnimatorVFX>().PauseResumeAnim();
							}
						}
					}
				);
			},
            delegate
            {

            }
			));
		advancedFeatureList.Add(new Tuple<string, Action, Action>(
			"",
			delegate
			{
				GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), AnimUIContent.transform);
				ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Apply Animation Percentage";
				ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
				ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
					delegate {

						if (RenderWithVFXRenderer)
						{
							foreach (var t in tubes)
							{
								float per = UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value;
								t.transform.Find("Avatar").GetComponent<ixTrailAnimatorVFX>().UpdateAnimFrame(per);
							}
						}
					}
				);
			},
			delegate
			{

			}
			));
		advancedFeatureList.Add(new Tuple<string, Action, Action>(
			"",
			delegate
			{
				GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), AnimUIContent.transform);
				ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Render With Points";
				ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
				ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
					delegate {

						if (RenderWithVFXRenderer)
						{
							foreach (var t in tubes)
							{
								t.GetComponent<VFXTrailRenderer>().SwitchToPointBasedVFXRenderer();
							}
						}
					}
				);
			},
			delegate
			{

			}
			));
		advancedFeatureList.Add(new Tuple<string, Action, Action>(
			"",
			delegate
			{
				GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), AnimUIContent.transform);
				ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Render With Strips";
				ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
				ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
					delegate {

						if (RenderWithVFXRenderer)
						{
							foreach (var t in tubes)
							{
								t.GetComponent<VFXTrailRenderer>().SwitchToStripBasedVFXRenderer();
							}
						}
					}
				);
			},
			delegate
			{

			}
			));

		// Instantiate buttons 
		foreach (var t in advancedFeatureList) 
			t.Item2();
	}

	/////////////////////////////////////////////////////////// Rendering UI Callbacks ////
	/// <summary>
	/// 
	/// </summary>
	void AppendTrailsBaseOnActiveFile()
	{
		string path = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
		if (Path.GetExtension(path).Equals(".csv") || Path.GetExtension(path).Equals(".CSV"))
		{
			var tube = Instantiate(TubePrefeb, this.transform);
			tube.GetComponent<CustomTubeRenderer>().CreateTrailBaseOnFileAndColor(path, startColor); // dummy color 
			tubes.Add(tube);

			LoopAdjustColor(); // post processing 
		}
	}

	void LoopAdjustColor()
    {
		int NumOfTubes = tubes.Count;
		int idx = 0;
		foreach (var t in tubes)
        {
			float per = (float)idx / (float)NumOfTubes;
			if(RenderWithMeshRenderer)
				t.GetComponent<CustomTubeRenderer>().AdjustColor(Color.Lerp(startColor, endColor, per));
			if(RenderWithVFXRenderer)
				t.GetComponent<VFXTrailRenderer>().AdjustColor(Color.Lerp(startColor, endColor, per));
			idx++;
        }
	}

	void LoopComputeTrackingJumps()
    {
		foreach (var t in tubes)
		{
			//t.GetComponent<VFXTrailRenderer>().LargerRadius();
		}
	}

	void LargerRadius()
    {
		foreach(var t in tubes)
        {
			t.GetComponent<CustomTubeRenderer>().LargerRadius();
        }
    }

	void SmallerRadius()
	{
		foreach (var t in tubes)
		{
			t.GetComponent<CustomTubeRenderer>().SmallerRadius();
		}
	}

	void UnzipActiveFileAndRender()
	{
		//
		string zipfile = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curActiveFilePath;
		string targetPathBase = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFolderPath;
		string fileNameWithoutExtension = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFileName;

		//
		fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileNameWithoutExtension);
		string targetPath = targetPathBase + "/" + fileNameWithoutExtension + "/";

		List<string> fplist = new List<string>();
		if (!Directory.Exists(targetPath))
		{
			Directory.CreateDirectory(targetPath);
			fplist = ZipFile.UnZip(targetPath, zipfile);
			ixUtility.LogMe("Unzipped! zipfile = " + zipfile + " targetPath = " + targetPath);
		}
		else
		{
			var info = new DirectoryInfo(targetPath);
			var fileInfo = info.GetFiles();
			foreach (var file in fileInfo)
				fplist.Add(file.FullName);
		}

		//
		foreach(var t in tubes)
        {
			Destroy(t);
        }
		tubes.Clear();

		//
		foreach (string f in fplist)
		{
			ixUtility.LogMe("Loading File: " + f);
			Debug.Log("Loading File: " + f);

			if (Path.GetExtension(f).Equals(".csv") || Path.GetExtension(f).Equals(".CSV"))
			{
                if (RenderWithMeshRenderer)
                {
					var tube = Instantiate(TubePrefeb, this.transform);
					tube.GetComponent<CustomTubeRenderer>().CreateTrailBaseOnFileAndColor(f, startColor); 
					tubes.Add(tube);
                }

                if (RenderWithVFXRenderer)
                {
					var tube = Instantiate(VFXTrailRendererPrefeb, this.transform);
					tube.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(f, startColor); 
					tubes.Add(tube);
                }
			}
		}
	}

	void FinerResolution()
	{
		foreach (var t in tubes)
		{
			t.GetComponent<CustomTubeRenderer>().FinerResolution();
		}
	}

	void CoarserResolution()
    {
		foreach (var t in tubes)
		{
			t.GetComponent<CustomTubeRenderer>().CoarserResolution();
		}
	}

	void DoubleDownScalingFactor()
	{
		foreach (var t in tubes)
		{
			float s = t.GetComponent<CustomTubeRenderer>().GetDownScalingFactor();
			t.GetComponent<CustomTubeRenderer>().UpdateDownScalingFactor(s * 2.0f);

			ixUtility.LogMe("New scaling factor = " + s * 2.0f);
		}
	}

	void HalfDownScalingFactor()
	{
		foreach (var t in tubes)
		{
			float s = t.GetComponent<CustomTubeRenderer>().GetDownScalingFactor();
			t.GetComponent<CustomTubeRenderer>().UpdateDownScalingFactor(s / 2.0f);

			ixUtility.LogMe("New scaling factor = " + s / 2.0f);
		}
	}

	/////////////////////////////////////////////////////////// Anim UI ////
	/// <summary>
	/// 
	/// </summary>
	void PauseResumeAnim()
    {
		foreach (var t in tubes)
		{
			t.transform.Find("Avatar").GetComponent<ixTrailAnimator>().PauseResumeAnim();
		}
	}
}
