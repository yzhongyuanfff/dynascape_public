﻿// ------------------------------------------------------------------------------------
// <copyright file="CustomTubeRenderer.cs">
//      Copyright (c) Mathias Soeholm, Technische Universität Dresden
//		Licensed under the MIT License.
// </copyright>
// <author>
//      Mathias Soeholm, modifications by Wolfgang Büschel. Then, modified by Zhongyuan Yu
// </author>
// <comment>
//		Source: https://gist.github.com/mathiassoeholm/15f3eeda606e9be543165360615c8bef
//		Original file comment:
//		Author: Mathias Soeholm
//		Date: 05/10/2016
//		No license, do whatever you want with this script
// </comment>
// ------------------------------------------------------------------------------------

using JfranMora.Inspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class CustomTubeRenderer : MonoBehaviour
{
	// for the renderer
	Vector3[] positions;
	Color[] colors;
	float[] radii;

	[SerializeField] int Sides;
	[SerializeField] float startWidth = 0.1f;
	[SerializeField] float endWidth = 0.1f;
	public Color color;
	[SerializeField] GameObject AnimAvatar;
	[SerializeField] GameObject DirectionIndicatorPrefeb;
	[SerializeField] Material MeshTubeMaterial;

	[SerializeField] private float downsamplerDeltaDistance = 0.5f; // meter
	[SerializeField] private float downsamplerDeltaTime = 1f; // second
	[SerializeField] private float downsamplerDeltaCurvature = 135f; // angle max: 180.0

	[SerializeField] private int timeTickFrameSkip = 30;
	[SerializeField] private GameObject timeTickPrefab;

	private Vector3[] vertices;
	private Color[] vertexColors;
	private Mesh mesh;
	private MeshFilter meshFilter;
	private MeshRenderer meshRenderer;

	// Core Data
	public List<Vector3> pointList = new List<Vector3>();
	public List<Quaternion> rotationList = new List<Quaternion>();
	public List<float> timeList = new List<float>();
	public List<Vector3> SampledPoints = new List<Vector3>();
	List<GameObject> DirectionIndicators = new List<GameObject>();
	public List<Vector3> JumpPoints = new List<Vector3>();

	public bool IsDataReady = false;
	public int NumOfSamplePoints = 0;
	string filepath = "";
	float downScaleFactor = 0.1f;
	int IndicatorInterval = 100;
	float speedThreshold = 50.0f;
	bool DoDynamicUpdate = false;
	bool DoDynamicUpdate_Trails = false;

	public Material material
	{
		get { return meshRenderer.material; }
		set { meshRenderer.material = value; }
	}

	void Awake()
	{
		//
		meshFilter = GetComponent<MeshFilter>();
		if (meshFilter == null)
		{
			meshFilter = gameObject.AddComponent<MeshFilter>();
		}
		mesh = new Mesh();
		meshFilter.mesh = mesh;

		//
		meshRenderer = GetComponent<MeshRenderer>();
		if (meshRenderer == null)
		{
			meshRenderer = gameObject.AddComponent<MeshRenderer>();
		}
		meshRenderer.material = MeshTubeMaterial;
	}

    private void Start()
    {
		//InitializeUI();
    }
    private void Update()
    {
  //      if (DoDynamicUpdate)
  //      {
		//	SetPositionsDynamic(AnimAvatar.transform.GetComponent<ixTrailAnimator>().curFrame);
		//}
  //      if (DoDynamicUpdate_Trails)
  //      {
		//	SetPositionsDynamic_Trail(AnimAvatar.transform.GetComponent<ixTrailAnimator>().curFrame, 0, 100);
		//}
    }

	public void ToggleDynamicUpdate()
    {
		DoDynamicUpdate = !DoDynamicUpdate;

	}

	public bool GetDynamicUpdateState()
    {
		return DoDynamicUpdate;
    }

	public void ToggleDynamicUpdate_Trails()
	{
		DoDynamicUpdate_Trails = !DoDynamicUpdate_Trails;

	}

	public bool GetDynamicUpdateState_Trails()
	{
		return DoDynamicUpdate_Trails;
	}


	private void OnEnable()
	{
		meshRenderer.enabled = true;
	}

	private void OnDisable()
	{
		meshRenderer.enabled = false;
	}

	private void OnValidate()
	{
		Sides = Mathf.Max(3, Sides);
	}

	[Button]
	void DrawExampleTrail()
	{
		List<Vector3> exampleTrail = new List<Vector3>();
		exampleTrail.Add(new Vector3(0, 0, 0));
		exampleTrail.Add(new Vector3(0, 1, 0));
		exampleTrail.Add(new Vector3(0, 2, 2));
		exampleTrail.Add(new Vector3(0, 3, 3));

		//List<Color>
		this.SetColor(Color.green);
		pointList = exampleTrail;
		this.SetPositions();
	}

	public void CreateTrailBaseOnPointList(List<Vector3> points, Color c)
    {
		pointList = points;
		SetPositions();
		SetColor(c);
	}

	public void CreateTrailBaseOnPointAndRotationList(List<Vector3> points, List<Quaternion> rots, Color c)
	{
		pointList = points;
		rotationList = rots;
		SetPositions();
		SetColor(c);
	}

	IEnumerator Thread_CreateTrailBaseOnFileAndColor(string fn, Color c)
	{
		filepath = fn;
		StartCoroutine(ReBuildTrailFromFile());
		SetColor(c);
		yield return null;
	}

	IEnumerator ReBuildTrailFromFile()
	{
		IsDataReady = false;
		if (Path.GetExtension(filepath).Equals(".csv") || Path.GetExtension(filepath).Equals(".CSV"))
		{
			StartCoroutine(ReadPointsFromCsvCoroutine(filepath));
			yield return null;
		}
    }

	public void WriteToFile()
    {
		string folderPath = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFolderPath;
		string fileName = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFileName;

		string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
		if (!System.IO.Directory.Exists(extractedDataFolder))
			System.IO.Directory.CreateDirectory(extractedDataFolder);
		string fileToWrite = extractedDataFolder + "/CameraTrails.csv";

		StreamWriter writer = new StreamWriter(fileToWrite);

		// write header
		writer.WriteLine("Time,PositionX,PositionY,PositionZ,QuatW,QuatX,QuatY,QuatZ,");

		//
		if (timeList.Count != pointList.Count)
		{
			timeList.Clear();
			for (int i = 0; i < pointList.Count; i++)
            {
				timeList.Add(i);
			}
		}

		//
		for(int i = 0; i < pointList.Count; i++)
        {
			writer.WriteLine(
					timeList[i] + ","
				+ pointList[i].x + "," + pointList[i].y + "," + pointList[i].z + ","
				+ rotationList[i].x + "," + rotationList[i].y + "," + rotationList[i].z + "," + rotationList[i].w);
        }
		writer.Close();

		Debug.Log("Written To: " + fileToWrite);
	}

	public void TryReadFromFile()
    {
		string folderPath = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFolderPath;
		string fileName = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFileName;


		string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
		string fn = extractedDataFolder + "/CameraTrails.csv";  // using another extension for reading diff. related files

		if (System.IO.File.Exists(fn))
			CreateTrailBaseOnFileAndColor(fn, Color.green);
		else
			Debug.Log("CSV File Not Exist!");
	}

	public void ClearTrail()
	{
		timeList.Clear();
		pointList.Clear();
		rotationList.Clear();
		meshFilter.mesh = new Mesh();
	}

	IEnumerator ReadPointsFromCsvCoroutine(string file)
	{
		timeList.Clear();
		pointList.Clear();
		rotationList.Clear();
		//pointList = new List<Vector3>();
		//rotationList = new List<Quaternion>();

		StreamReader reader = new StreamReader(file);
		var header = reader.ReadLine();
		while (!reader.EndOfStream)
		{
			var line = reader.ReadLine();

			//
			//float randomNumber = Random.Range(0.0f, 1.0f);
			//if (randomNumber > downScaleFactor) continue;

			var values = line.Split(',');

			float time = float.Parse(values[0].ToString());
			timeList.Add(time);

			Vector3 point = new Vector3(
				float.Parse(values[1].ToString()),
				float.Parse(values[2].ToString()),
				float.Parse(values[3].ToString()));

			pointList.Add(point);

			Quaternion rotation = new Quaternion(
				float.Parse(values[5].ToString()), // x
				float.Parse(values[6].ToString()),
				float.Parse(values[7].ToString()),
				float.Parse(values[4].ToString())); // w

			rotationList.Add(rotation);

			//yield return new WaitForSeconds(0.001f); // might be too slow
		}

		yield return new WaitForSeconds(0.001f);

		Debug.Log("Start Building Trajectories,  pointList.Count = " + pointList.Count);
		//ixUtility.LogMe("Start Building Trajectories,  pointList.Count = " + pointList.Count);

		SetPositions();

		yield return null;
	}

	/// <summary>
	///
	/// </summary>
	/// <param name="fn"></param>
	/// <param name="c"></param>
	public void CreateTrailBaseOnFileAndColor(string fn, Color c)
    {
		StartCoroutine(Thread_CreateTrailBaseOnFileAndColor(fn,c));
	}


	public void AdjustColor(Color c)
    {
		SetColor(c);
	}

	[Button]
	public void SmallerRadius()
	{
		UpdateRadius(startWidth - 0.01f, endWidth - 0.01f);
	}

	[Button]
	public void LargerRadius()
    {
		UpdateRadius(startWidth + 0.01f, endWidth + 0.01f);
	}

	public void UpdateRadius(float s, float e)
    {
		startWidth = s;
		endWidth = e;

		RebuildTrail();
    }

	[Button]
	public void RebuildTrail()
    {
		SetPositions();
	}

	/// <summary>
	///
	/// </summary>
	/// <param name="c"></param>
	void SetColor(Color c)
    {
		this.color = c;
		material.color = c;
		foreach (var di in DirectionIndicators)
			di.transform.Find("Geometry").GetComponent<MeshRenderer>().material.color = c;
	}

	public void FinerResolution()
    {
		Sides += 1;
		RebuildTrail();
	}

	public void CoarserResolution()
    {
		Sides -= 1;
		RebuildTrail();
	}

	public void UpdateDownScalingFactor(float s) // takes some time
    {
		downScaleFactor = s;
		RebuildTrail();
		//StartCoroutine(ReBuildTrailFromFile());
    }

	public void SetDownScaleFactor(float s)
    {
		downScaleFactor = s;
	}

	public float GetDownScalingFactor()
    {
		return downScaleFactor;
    }

	void SamplePoints()
    {
		SampledPoints.Clear();
		int pointInterval = (int)(1 / downScaleFactor);
			//(int)(downScaleFactor * pointList.Count);
		for (int i = 0; i < pointList.Count; i+= pointInterval)
        {
			SampledPoints.Add(pointList[i]);
        }

		ixUtility.LogMe("pointInterval = " + pointInterval);
		ixUtility.LogMe("SampledPoints.Count = " + SampledPoints.Count);
	}
	void SamplePoints(int maxFrame)
	{
		SampledPoints.Clear();
		int pointInterval = (int)(1 / downScaleFactor);
		//(int)(downScaleFactor * pointList.Count);
		for (int i = 0; i < maxFrame; i += pointInterval)
		{
			SampledPoints.Add(pointList[i]);
		}

		ixUtility.LogMe("pointInterval = " + pointInterval);
		ixUtility.LogMe("SampledPoints.Count = " + SampledPoints.Count);
	}
	void SamplePoints(int currentFrane, int offset, int trailLength)
	{
		SampledPoints.Clear();
		//int pointInterval = (int)(1 / downScaleFactor);
		//(int)(downScaleFactor * pointList.Count);

		int maxIndex = (currentFrane + trailLength) < pointList.Count ? (currentFrane + trailLength) : pointList.Count;
		int minIndex = (currentFrane + offset) > 0 ? currentFrane + offset : 0;

		for (int i = minIndex; i < maxIndex; i += 1)
		{
			SampledPoints.Add(pointList[i]);
		}

		//ixUtility.LogMe("pointInterval = " + pointInterval);
		ixUtility.LogMe("SampledPoints.Count = " + SampledPoints.Count);
	}

	void TrackingJumpDetector()
    {
		JumpPoints.Clear();
		List<float> speedList = new List<float>();
		speedList.Add(0);
		for (int i = 1; i < pointList.Count; i += 1)
		{
			float speed = (Vector3.Distance(pointList[i], pointList[i - 1])) / (timeList[i] - timeList[i - 1]);
			if (speed > speedThreshold)
				JumpPoints.Add(pointList[i - 1]);
			speedList.Add(speed);
		}

		ixUtility.LogMe("Num Of JumpPoints: " + JumpPoints.Count);
	}

	///////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// wrapper function
	/// </summary>
	void SetPositions()
	{
		// subsampling
		//TrackingJumpDetector();
		// SamplePoints();

		PointsDownsamplerAndTimeTick();

		List<List<Vector3>> segmentsList = new List<List<Vector3>>();
		segmentsList.Add(pointList);

		SetPositions(segmentsList, null);
	}

	void SetPositionsDynamic(int maxFrame)
    {
		// subsampling
		SamplePoints(maxFrame);
		//TrackingJumpDetector();

		List<List<Vector3>> segmentsList = new List<List<Vector3>>();
		segmentsList.Add(SampledPoints);

		SetPositions(segmentsList, null);
	}

	void SetPositionsDynamic_Trail(int currentFrame, int offset, int trailLength)
	{
		// subsampling
		SamplePoints(currentFrame,offset,trailLength);
		//TrackingJumpDetector();

		List<List<Vector3>> segmentsList = new List<List<Vector3>>();
		segmentsList.Add(SampledPoints);

		SetPositions(segmentsList, null);
	}

	public void SetPositions(List<List<Vector3>> segmentsList, List<List<Color>> colorSegmentsList = null)
	{
		bool IsUsingPerVertexColor = true;
		if (segmentsList == null || segmentsList.Count < 1)
		{
			return;
		}

		int TotalPoints = 0;
		for (int i = 0; i < segmentsList.Count; i++)
		{
			TotalPoints += segmentsList[i].Count+2;
			if (colorSegmentsList == null || colorSegmentsList.Count != segmentsList.Count || segmentsList[i].Count != colorSegmentsList[i].Count)
			{
				IsUsingPerVertexColor = false;
			}
		}

		positions = new Vector3[TotalPoints];
		radii = new float[TotalPoints];

		int CurrentPoint = 0;
		for (int i = 0; i < segmentsList.Count; i++)
		{
			var Segment = segmentsList[i];
			if (Segment.Count < 2)
			{
				return;
			}

			Vector3 v0offset = (Segment[0] - Segment[1]) * 0.01f;
			positions[CurrentPoint] = v0offset + Segment[0];
			radii[CurrentPoint] = 0.0f;
			CurrentPoint++;

			for (int p = 0; p < Segment.Count; p++)
			{
				positions[CurrentPoint] = Segment[p];
				radii[CurrentPoint] = Mathf.Lerp(startWidth, endWidth, (float)CurrentPoint / TotalPoints);
				CurrentPoint++;
			}

			Vector3 v1offset = (Segment[Segment.Count - 1] - Segment[Segment.Count - 2]) * 0.01f;
			positions[CurrentPoint] = v1offset + Segment[Segment.Count - 1];
			radii[CurrentPoint] = 0.0f;
			CurrentPoint++;
		}

		colors = new Color[TotalPoints];
		CurrentPoint = 0;
		if (IsUsingPerVertexColor)
		{
			for (int i = 0; i < segmentsList.Count; i++)
			{
				var ColorList = colorSegmentsList[i];
				if (ColorList.Count < 2)
				{
					return;
				}

                colors[CurrentPoint] = ColorList[0];
				CurrentPoint++;

				for (int p = 0; p < ColorList.Count; p++)
				{
					colors[CurrentPoint] = ColorList[p];
					CurrentPoint++;
				}

				colors[CurrentPoint] = ColorList[ColorList.Count - 1];
				CurrentPoint++;
			}
		}
		else
		{
			for(int i = 0; i < TotalPoints; i++)
            {
				colors[i] = color;
            }
		}

		// Set tiling param for the timeline lines
		material.SetVector("_Tiling", new Vector2(1f, TotalPoints));

		StartCoroutine(GenerateMesh());
	}

	IEnumerator GenerateMesh()
	{
		if (mesh == null || positions == null || positions.Length <= 1)
		{
			mesh = new Mesh();
			// yield return null;
		}

		var verticesLength = Sides*positions.Length;
		if (vertices == null || vertices.Length != verticesLength)
		{
			vertices = new Vector3[verticesLength];
			vertexColors = new Color[verticesLength];

			var indices = GenerateIndices();
			var uvs = GenerateUVs();

			if (verticesLength > mesh.vertexCount)
			{
				mesh.vertices = vertices;
				mesh.triangles = indices;
				mesh.uv = uvs;
			}
			else
			{
				mesh.triangles = indices;
				mesh.vertices = vertices;
				mesh.uv = uvs;
			}
		}

		var currentVertIndex = 0;

		yield return new WaitForSeconds(0.001f);

		for (int i = 0; i < positions.Length; i++)
		{
			var circle = CalculateCircle(i);
			foreach (var vertex in circle)
			{
				vertices[currentVertIndex] = vertex;
				vertexColors[currentVertIndex] = colors[i];
				currentVertIndex++;
			}

			//yield return new WaitForSeconds(0.001f); // might be too slow
		}

		mesh.vertices = vertices;
		mesh.colors = vertexColors;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		meshFilter.mesh = mesh;

		IsDataReady = true;
		NumOfSamplePoints = pointList.Count;
		//AnimAvatar.gameObject.SetActive(true);
		//InstantiateDirectionIndicators();
		SetColor(this.color); // apply again, indicators were not avaliable while loading from file (async)
	}

	private Vector2[] GenerateUVs()
	{
		var uvs = new Vector2[positions.Length*Sides];

		for (int segment = 0; segment < positions.Length; segment++)
		{
			for (int side = 0; side < Sides; side++)
			{
				var vertIndex = (segment * Sides + side);
				var u = side/(Sides-1f);
				var v = segment/(positions.Length-1f);

				uvs[vertIndex] = new Vector2(u, v);
			}
		}

		return uvs;
	}

	private int[] GenerateIndices()
	{
		// Two triangles and 3 vertices
		var indices = new int[positions.Length*Sides*2*3];

		var currentIndicesIndex = 0;
		for (int segment = 1; segment < positions.Length; segment++)
		{
			for (int side = 0; side < Sides; side++)
			{
				var vertIndex = (segment*Sides + side);
				var prevVertIndex = vertIndex - Sides;

				// Triangle one
				indices[currentIndicesIndex++] = prevVertIndex;
				indices[currentIndicesIndex++] = (side == Sides - 1) ? (vertIndex - (Sides - 1)) : (vertIndex + 1);
				indices[currentIndicesIndex++] = vertIndex;


				// Triangle two
				indices[currentIndicesIndex++] = (side == Sides - 1) ? (prevVertIndex - (Sides - 1)) : (prevVertIndex + 1);
				indices[currentIndicesIndex++] = (side == Sides - 1) ? (vertIndex - (Sides - 1)) : (vertIndex + 1);
				indices[currentIndicesIndex++] = prevVertIndex;
			}
		}

		return indices;
	}

	private Vector3[] CalculateCircle(int index)
	{
		var dirCount = 0;
		var forward = Vector3.zero;

		// If not first index
		if (index > 0)
		{
			forward += (positions[index] - positions[index - 1]).normalized;
			dirCount++;
		}

		// If not last index
		if (index < positions.Length-1)
		{
			forward += (positions[index + 1] - positions[index]).normalized;
			dirCount++;
		}

		// Forward is the average of the connecting edges directions
		forward = (forward/dirCount).normalized;
		var side = Vector3.Cross(forward, forward+new Vector3(.123564f, .34675f, .756892f)).normalized;
		var up = Vector3.Cross(forward, side).normalized;

		var circle = new Vector3[Sides];
		var angle = 0f;
		var angleStep = (2*Mathf.PI)/Sides;

		var t = index / (positions.Length-1f);
		var radius = radii[index];

		for (int i = 0; i < Sides; i++)
		{
			var x = Mathf.Cos(angle);
			var y = Mathf.Sin(angle);

			circle[i] = positions[index] + side*x* radius + up*y* radius;

			angle += angleStep;
		}

		return circle;
	}

	void InstantiateDirectionIndicators()
    {
		foreach(var di in DirectionIndicators)
        {
			Destroy(di);
        }
		DirectionIndicators.Clear();
		for (int i = 0; i < pointList.Count; i += IndicatorInterval)
        {
			GameObject di = Instantiate(DirectionIndicatorPrefeb,
				pointList[i], rotationList[i], this.transform.Find("DirectionIndicators"));
			//// use the same color as the trail -> set color afterwards
			//di.transform.Find("Geometry").GetComponent<MeshRenderer>().material.color
			//	= this.material.color;
			DirectionIndicators.Add(di);
        }
	}

	private void PointsDownsampler()
	{
		SampledPoints.Clear();

		var downsamplerDeltaTimeInFrames = downsamplerDeltaTime * 60;

		SampledPoints.Add(pointList[0]);
		var lastDirection = Vector3.zero;
		var lastEmittedPoint = pointList[0];
		var lastEmittedIndex = 0;

		for (var i = 1; i < pointList.Count; i++)
		{
			// Always emit the last point
			var emit = i == pointList.Count - 1;

			var point = pointList[i];

			// Delta distance
			if (Vector3.Distance(point, lastEmittedPoint) > downsamplerDeltaDistance)
				emit = true;

			// Delta time
			if (i - lastEmittedIndex > downsamplerDeltaTimeInFrames)
				emit = true;

			// Delta curvature
			if (lastDirection != Vector3.zero)
			{
				var currentDirection = point - lastEmittedPoint;
				if (Vector3.Angle(currentDirection, lastDirection) < downsamplerDeltaCurvature)
					emit = true;
			}

			if (!emit) continue;

			SampledPoints.Add(point);
			lastDirection = lastEmittedPoint - point;
			lastEmittedPoint = point;
			lastEmittedIndex = i;

		}
	}

	List<GameObject> TimeTicks = new List<GameObject>();

	public void UpdateTimeTicksRenderingAccordingToFrameRange(int frameStart, int frameEnd)
    {
		foreach(var T in TimeTicks)
        {
			if(T.GetComponent<TimeTickController>().FrameID < frameStart)
			{
				T.gameObject.SetActive(false);
			}
			else if (T.GetComponent<TimeTickController>().FrameID > frameEnd)
			{
				T.gameObject.SetActive(false);
			}
            else
            {
				T.gameObject.SetActive(true);
            }
		}
    }

	private void PointsDownsamplerAndTimeTick()
	{
		//Instantiate(timeTickPrefab, pointList[0], Quaternion.identity);
		var lastEmittedTimeTick = 0;

		SampledPoints.Clear();

		var downsamplerDeltaTimeInFrames = downsamplerDeltaTime * 60;

		SampledPoints.Add(pointList[0]);
		var lastDirection = Vector3.zero;
		var lastEmittedPoint = pointList[0];
		var lastEmittedIndex = 0;
		TimeTicks.Clear();

		for (var i = 1; i < pointList.Count; i++)
		{
			// Time tick
			if (i - lastEmittedTimeTick > timeTickFrameSkip)
			{
				var timeTick = Instantiate(timeTickPrefab, transform.TransformPoint(pointList[i]), Quaternion.identity, this.transform);
				timeTick.GetComponent<TimeTickController>().timeText.text = VFXTrailRenderer.FrameToTimeString(i);
				timeTick.GetComponent<TimeTickController>().FrameID = i;
				TimeTicks.Add(timeTick);
				lastEmittedTimeTick = i;
			}

			// Always emit the last point
			var emit = i == pointList.Count - 1;

			var point = pointList[i];

			// Delta distance
			if (Vector3.Distance(point, lastEmittedPoint) > downsamplerDeltaDistance)
				emit = true;

			// Delta time
			if (i - lastEmittedIndex > downsamplerDeltaTimeInFrames)
				emit = true;

			// Delta curvature
			if (lastDirection != Vector3.zero)
			{
				var currentDirection = point - lastEmittedPoint;
				if (Vector3.Angle(currentDirection, lastDirection) < downsamplerDeltaCurvature)
					emit = true;
			}

			if (!emit) continue;

			SampledPoints.Add(point);
			lastDirection = lastEmittedPoint - point;
			lastEmittedPoint = point;
			lastEmittedIndex = i;

		}
	}
}
