// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class EmitPointsFromImage : MonoBehaviour
{
    public Texture2D sourceTexture; 
    VisualEffect _vfx;

    // list of points  
    private Vector3[] points;
    // corresp. colors 
    private Color[] colors;
    //
    public int vertexCount;
    // Byte size of the point element.
    private const int elementSize = sizeof(float) * 4;
    // buffer to hold point positions 
    public GraphicsBuffer _position_buffer;
    // buffer to hold point colors 
    public GraphicsBuffer _color_buffer;

    // Start is called before the first frame update
    void Start()
    {
        _vfx = this.GetComponent<VisualEffect>();
        genPoints();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// upload points from cpu to gpu - reshaping the original points! Just one time ... 
    public void updateGPUGraphicsBuffer()
    {
        _position_buffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, vertexCount, sizeof(float) * 3);
        _position_buffer.SetData(points);
        _vfx.SetGraphicsBuffer("PointBuffer", _position_buffer);

        _color_buffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, vertexCount, sizeof(float) * 4);
        _color_buffer.SetData(colors);
        _vfx.SetGraphicsBuffer("ColorBuffer", _color_buffer);

        _vfx.SetInt("PointCount", vertexCount);
    }

    void genPoints()
    {
        vertexCount = sourceTexture.width * sourceTexture.height;

        //
        points = new Vector3[vertexCount];
        colors = new Color[vertexCount];

        //
        for(int i = 0;i< sourceTexture.width; i++)
        {
            for (int j = 0; j < sourceTexture.height; j++)
            {
                points[i + sourceTexture.width * j] = new Vector3(
                    (float)i / sourceTexture.width, (float)j / sourceTexture.height, 0);
                colors[i + sourceTexture.width * j] = sourceTexture.GetPixel(i, j);
            }
        }

        //
        updateGPUGraphicsBuffer();
    }
}
