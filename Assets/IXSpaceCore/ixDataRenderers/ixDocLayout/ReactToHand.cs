// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactToHand : MonoBehaviour
{
    public float liftOffset = 0.1f;
    public List<Vector3> originalPositions;

    List<bool> pageHoverState;
    List<bool> lastpageHoverState;
    List<bool> pageState; // listed out or not 

    // Start is called before the first frame update
    void Start()
    {
        Resetlayout();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameObject.FindGameObjectWithTag("IndexTip")) return;

        if ((originalPositions.Count == 0) 
            || (pageHoverState.Count == 0) 
            || (lastpageHoverState.Count == 0)
            || (pageState.Count == 0)
            ) 
            Resetlayout();

        Vector3 tip = GameObject.FindGameObjectWithTag("IndexTip").transform.position;
        int cidx = 0;
        foreach(Transform t in this.transform)
        {
            pageHoverState[cidx] = PointInOABB(tip, t.GetComponent<BoxCollider>());
            if(pageHoverState[cidx] && (!lastpageHoverState[cidx]))
            {
                pageState[cidx] = !pageState[cidx];
            }
            if (pageState[cidx])
            {
                Vector3 liftedPosition = new Vector3(
                    originalPositions[cidx].x + liftOffset, originalPositions[cidx].y, originalPositions[cidx].z);
                t.localPosition = liftedPosition;
            }
            else
            {
                t.localPosition = originalPositions[cidx];
            }
            lastpageHoverState[cidx] = pageHoverState[cidx];
            cidx++;
        }
    }

    void Resetlayout()
    {
        originalPositions = new List<Vector3>();
        pageHoverState = new List<bool>();
        lastpageHoverState = new List<bool>();
        pageState = new List<bool>();
        foreach (Transform t in this.transform)
        {
            originalPositions.Add(t.localPosition);
            pageHoverState.Add(false);
            lastpageHoverState.Add(false);
            pageState.Add(false);
        }
    }

    bool PointInOABB(Vector3 point, BoxCollider box)
    {
        point = box.transform.InverseTransformPoint(point) - box.center;

        float halfX = (box.size.x * 0.5f);
        float halfY = (box.size.y * 0.5f);
        float halfZ = (box.size.z * 0.5f);
        if (point.x < halfX && point.x > -halfX &&
           point.y < halfY && point.y > -halfY &&
           point.z < halfZ && point.z > -halfZ)
            return true;
        else
            return false;
    }
}
