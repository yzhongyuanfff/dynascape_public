using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.VFX;

public class ixPointbasedVolumeRenderer : MonoBehaviour
{
    [Header("======")]
    [SerializeField] GameObject UIContent;
    [SerializeField] VisualEffect VolumeVFX;

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
        //VolumeVFX.Stop();
    }

    private void Awake()
    {
        //meshFilter = GetComponent<MeshFilter>();
        //meshRenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        // Update the indirect buffer with the number of points to render
        //uint[] args = new uint[] { (uint)numPoints, 1, 0, 0 };
        //indirectBuffer.SetData(args);
    }

    //[SerializeField] int numPoints = 1000; // Number of points to generate
    //[SerializeField] float radius = 1.0f; // Radius of the sphere
    //[SerializeField] float pointSize = 0.1f; // Size of the points

    //private MeshFilter meshFilter;
    //private MeshRenderer meshRenderer;
    //private ComputeBuffer vertexBuffer;
    //private ComputeBuffer indirectBuffer;

    private void GeneratePointsOnSphere()
    {
        //// Generate points on the sphere surface
        //Vector3[] points = new Vector3[numPoints];
        //for (int i = 0; i < numPoints; i++)
        //{
        //    // Generate random spherical coordinates
        //    float theta = UnityEngine.Random.Range(0f, Mathf.PI * 2f); // Random angle around Y-axis
        //    float phi = UnityEngine.Random.Range(0f, Mathf.PI); // Random angle from top to bottom
        //    float x = radius * Mathf.Sin(phi) * Mathf.Cos(theta);
        //    float y = radius * Mathf.Cos(phi);
        //    float z = radius * Mathf.Sin(phi) * Mathf.Sin(theta);
        //    Vector3 point = new Vector3(x, y, z);
        //    points[i] = point;
        //}

        //// Create vertex buffer
        //vertexBuffer = new ComputeBuffer(numPoints, sizeof(float) * 3);
        //vertexBuffer.SetData(points);

        //// Create indirect buffer
        //indirectBuffer = new ComputeBuffer(1, sizeof(uint) * 4, ComputeBufferType.IndirectArguments);
        //uint[] args = new uint[] { (uint)numPoints, 1, 0, 0 };
        //indirectBuffer.SetData(args);

        //// Assign buffers to the shader
        //meshRenderer.material.SetBuffer("_Vertices", vertexBuffer);
        //meshRenderer.material.SetBuffer("_IndirectBuffer", indirectBuffer);
        //meshRenderer.material.SetFloat("_PointSize", pointSize);

    }

    void CreateVolumeFromActiveFile()
    {
        string fn = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
        byte[] byteArray = File.ReadAllBytes(fn);

        ixUtility.LogMe("fn = " + fn);
        ixUtility.LogMe("byteArray.Length = " + byteArray.Length);

        Vector3 volumeDim = new Vector3(512, 512, 39);

        // Convert byte[] to float[]
        float[] floatBuffer = new float[byteArray.Length / 2]; // Assuming each float is 4 bytes
        //Buffer.BlockCopy(fileBytes, 0, floatBuffer, 0, fileBytes.Length);
        //for(int i = 0; i < byteArray.Length; i++)
        //{
        //    floatBuffer[i] = byteArray[i] / 65535.0f; // normalize 
        //}

        for (int i = 0; i < byteArray.Length; i += 2)
        {
            byte[] bytes = new byte[] { byteArray[i], byteArray[i + 1] }; // Take two bytes from byte array
            byte[] buffer = new byte[4];
            Array.Copy(bytes, 0, buffer, 0, 2);
            floatBuffer[i / 2] = BitConverter.ToInt32(buffer, 0) / 1500.0f; // Convert bytes to float and store in float array
        }

        //
        GraphicsBuffer volumeBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, floatBuffer.Length, sizeof(float));
        volumeBuffer.SetData(floatBuffer, 0, 0, floatBuffer.Length);

        VolumeVFX.Stop();
        VolumeVFX.Reinit();
        VolumeVFX.SetGraphicsBuffer("volumeBuffer", volumeBuffer);
        VolumeVFX.SetVector3("volumeDim", volumeDim);
        VolumeVFX.Play();
    }

    void InitUI()
    {
        ixUtility.MakeAButton(UIContent,
            "Load Volume",delegate {
                CreateVolumeFromActiveFile();
            });
    }

}
