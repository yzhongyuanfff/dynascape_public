using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkedImageLoader : MonoBehaviour, IPunInstantiateMagicCallback
{
    [SerializeField] float DefaultScale = 0.4f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator LoadImageFromUrlAndAssignToMeshRenderer_Coroutine(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        // rqlist.Add(www);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            ixLogger.LogMe(www.error);
        }
        else
        {
            byte[] bytes = www.downloadHandler.data;
            ulong numbytes = www.downloadedBytes;

            ixUtility.LogMe("Download To Memory with Success! www.downloadedBytes = " + numbytes);

            //File.WriteAllBytesAsync(DiskAddress, results);


            yield return new WaitForSeconds(0.5f);
            Texture2D tex = new Texture2D(1, 1);
            tex.LoadImage(bytes);


            yield return new WaitForSeconds(0.5f);
            this.transform.Find("Geometry").GetComponent<MeshRenderer>().material.mainTexture = tex;
            transform.localScale = new Vector3(DefaultScale, (float)tex.height / tex.width * DefaultScale, 1);
            //GetComponent<BoxCollider>().size = new Vector3(DefaultScale, (float)tex.height / tex.width * DefaultScale, 0.01f);
        }
    }

    void DownloadFromUrl()
    {

    }

    public void LoadImageFromUrlAndAssignToMeshRenderer(string url)
    {
        StartCoroutine(LoadImageFromUrlAndAssignToMeshRenderer_Coroutine(url));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="info"></param>
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        //
        object[] instantiationData = info.photonView.InstantiationData;
        if (instantiationData.Length > 0)
        {
            ixUtility.LogMe("instantiationData[0] = (Link?)" + instantiationData[0]); //Trying to see whats coming in instantiation data
            LoadImageFromUrlAndAssignToMeshRenderer((string)instantiationData[0]);
        }

        // find parent 
        GameObject parent = GameObject.FindGameObjectWithTag("TeachingContent");
        this.transform.parent = parent.transform;
    }
}
