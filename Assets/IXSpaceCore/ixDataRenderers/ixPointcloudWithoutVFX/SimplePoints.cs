using UnityEngine;

public class SimplePoints : MonoBehaviour
{
    public Material pointMaterial; // Assign the "Custom/PointShader" material to this field
    //public ComputeShader pointComputeShader; // Assign a compute shader that generates the point positions to this field

    public int pointCount = 10000; // Number of points to render

    private ComputeBuffer pointBuffer; // Buffer to hold the point positions

    void Start()
    {
        // Generate point positions on CPU
        Vector3[] pointPositions = new Vector3[pointCount];
        for (int i = 0; i < pointCount; i++)
        {
            pointPositions[i] = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));
        }

        // Create a ComputeBuffer to hold the point positions
        pointBuffer = new ComputeBuffer(pointCount, sizeof(float) * 3);
        pointBuffer.SetData(pointPositions);

        // Set the point buffer and point count in the point material
        pointMaterial.SetBuffer("_PointPositions", pointBuffer);
        pointMaterial.SetInt("_PointCount", pointCount);
    }

    void Update()
    {
        pointMaterial.SetPass(0);
        // Use Graphics.DrawProceduralNow with material and topology
        Graphics.DrawProceduralNow(MeshTopology.Points, pointCount);
    }

    void OnDestroy()
    {
        // Release the ComputeBuffer when the object is destroyed
        if (pointBuffer != null)
        {
            pointBuffer.Release();
            pointBuffer = null;
        }
    }
}
