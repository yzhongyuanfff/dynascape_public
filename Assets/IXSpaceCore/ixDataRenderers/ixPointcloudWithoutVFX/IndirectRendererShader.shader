Shader "Custom/PointShader" {
    Properties{
        _MainTex("Point Texture", 2D) = "white" {}
        _PointSize("Point Size", Range(1, 10)) = 1
    }

        SubShader{
            Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
            LOD 100

            Pass {
                CGPROGRAM
                #pragma vertex Vert
                #pragma target 5.0
                #pragma fragment Frag
                #include "UnityCG.cginc"

                struct appdata {
                    float4 vertex : POSITION;
                };

                struct v2f {
                    float4 vertex : SV_POSITION;
                    float2 uv : TEXCOORD0;
                };

                sampler2D _MainTex;
                float _PointSize;
                RWStructuredBuffer<float3> _PointPositions;

                v2f Vert(appdata v, uint id : SV_InstanceID) {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(float4(_PointPositions[id], 1.0));
                    o.uv = v.vertex.xy;
                    return o;
                }

                fixed4 Frag(v2f i) : SV_Target {
                    //// Discard fragments outside the point size
                    //float2 uv = i.uv * 2 - 1;
                    //float dist = length(uv);
                    //if (dist > _PointSize * 0.5) {
                    //    discard;
                    //}

                    //// Sample the point texture
                    //return tex2D(_MainTex, i.uv);
                    return float4(1.0, 1.0, 1.0, 1.0);
                }
                ENDCG
            }
        }
}
