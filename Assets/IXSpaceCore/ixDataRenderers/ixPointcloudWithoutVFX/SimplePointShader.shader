Shader "Custom/PointShaderSimple" {
    SubShader {
        Tags {"Queue"="Transparent" "RenderType"="Transparent"}
        LOD 100

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : SV_POSITION;
            };

            RWStructuredBuffer<float3> _PointPositions;
            int _PointCount;

            v2f vert (appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                //// Discard fragments outside the point buffer
                //if (i.vertex.x < 0 || i.vertex.x >= _PointCount) discard;

                //// Fetch the point position from the point buffer
                //float3 pointPos = _PointPositions[int(i.vertex.x)];

                // Set the point color to red
                return float4(1, 0, 0, 1);
            }
            ENDCG
        }
    }
}
