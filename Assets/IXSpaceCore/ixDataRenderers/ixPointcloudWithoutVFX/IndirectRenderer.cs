using UnityEngine;

public class IndirectRenderer : MonoBehaviour
{
    public Material pointMaterial;
    public ComputeBuffer argsBuffer;
    public int pointCount = 1000;

    private ComputeBuffer positionBuffer;
    private Vector3[] positions;

    void Start()
    {
        // Generate random point positions on a sphere
        positions = GenerateRandomSpherePoints(pointCount, 5.0f);

        // Create a buffer to hold the arguments for the draw call
        argsBuffer = new ComputeBuffer(1, 16, ComputeBufferType.IndirectArguments);

        // Set the arguments for the draw call (vertex count, instance count, start vertex index, start instance index)
        int[] args = new int[] { pointCount, 1, 0, 0 };
        argsBuffer.SetData(args);

        // Create a buffer to hold the point positions and set the data
        positionBuffer = new ComputeBuffer(pointCount, 12);
        positionBuffer.SetData(positions);

        // Set the buffer to the pointMaterial shader
        pointMaterial.SetBuffer("_PointPositions", positionBuffer);
    }

    void Update()
    {
        pointMaterial.SetPass(0);
        // Render points using indirect rendering
        Graphics.DrawProceduralIndirectNow(MeshTopology.Points, argsBuffer, 0);
    }

    void OnDestroy()
    {
        // Release the compute buffers
        argsBuffer.Release();
        positionBuffer.Release();
    }

    // Utility function to generate random points on a sphere surface
    private Vector3[] GenerateRandomSpherePoints(int count, float radius)
    {
        Vector3[] points = new Vector3[count];
        for (int i = 0; i < count; i++)
        {
            float theta = Random.Range(0.0f, Mathf.PI * 2.0f);
            float phi = Random.Range(0.0f, Mathf.PI);
            float x = radius * Mathf.Sin(phi) * Mathf.Cos(theta);
            float y = radius * Mathf.Sin(phi) * Mathf.Sin(theta);
            float z = radius * Mathf.Cos(phi);
            points[i] = new Vector3(x, y, z);
        }
        return points;
    }
}
