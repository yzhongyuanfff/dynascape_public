using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHandManager : MonoBehaviour
{
    public OVRControllerHelper ControllerL;
    public OVRControllerHelper ControllerR;
    public GameObject HandL;
    public GameObject HandR;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bool LControllerConnected = OVRInput.IsControllerConnected(ControllerL.m_controller);
        bool RControllerConnected = OVRInput.IsControllerConnected(ControllerR.m_controller);
        if (!LControllerConnected) HandL.SetActive(true);
        if (!RControllerConnected) HandR.SetActive(true);
    }
}
