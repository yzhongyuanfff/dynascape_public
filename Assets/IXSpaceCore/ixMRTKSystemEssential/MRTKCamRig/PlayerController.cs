using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject uiContent;
    public GameObject toggleBtnPrefeb;

    [HideInInspector] public GameObject player = null;
    bool isVirtual = false;
    bool physicsHandsEnabled = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        SetupGUI();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// call back function when slider data updated 
    /// </summary>
    /// <param name="eventData"></param>
    public void OnSliderUpdated(SliderEventData eventData)
    {
        if (!player)
            player = GameObject.FindGameObjectWithTag("Player");
        else
            player.GetComponent<OVRPassthroughLayer>().textureOpacity = eventData.NewValue;
    }

    /// <summary>
    /// 
    /// </summary>
    public void SetCompletelyVirtual()
    {
        if (!player)
            player = GameObject.FindGameObjectWithTag("Player");
        else
            player.GetComponent<OVRPassthroughLayer>().textureOpacity = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    public void MaximumPassthroughLevel()
    {
        if (!player)
            player = GameObject.FindGameObjectWithTag("Player");
        else
            player.GetComponent<OVRPassthroughLayer>().textureOpacity = 1;
    }

    /// <summary>
    /// 
    /// </summary>
    public void ToggleVirtualExperience()
    {
        isVirtual = player.GetComponent<OVRPassthroughLayer>().textureOpacity < 0.1f;
        if (isVirtual)
            player.GetComponent<OVRPassthroughLayer>().textureOpacity = 1;
        else
            player.GetComponent<OVRPassthroughLayer>().textureOpacity = 0;
        isVirtual = !isVirtual;
    }

    /// <summary>
    /// 
    /// </summary>
    void SetupGUI()
    {
        // Physics Hands
        GameObject newUIElement = Instantiate(toggleBtnPrefeb, uiContent.transform);
        ToggleBtnConfigHelper toggleBtnConfigHelper = newUIElement.GetComponent<ToggleBtnConfigHelper>();
        if (toggleBtnConfigHelper)
        {
            toggleBtnConfigHelper.title.text = "Brick At Hand";
            toggleBtnConfigHelper.btnName.text = "Enable";
            toggleBtnConfigHelper.btn.onClick.AddListener(delegate {
                physicsHandsEnabled = !physicsHandsEnabled;

                GameObject lhand = GameObject.FindGameObjectWithTag("LHand");
                GameObject rhand = GameObject.FindGameObjectWithTag("RHand");

                //if (lhand && lhand.transform.Find("OVRCustomHandPrefab_L"))
                //    lhand.transform.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().setPhyicsHandsEnabled(physicsHandsEnabled);
                //if (rhand && rhand.transform.Find("OVRCustomHandPrefab_R"))
                //    rhand.transform.Find("brick").gameObject.SetActive(!rhand.transform.Find("brick").gameObject.activeSelf);

                GameObject b = GameObject.FindGameObjectWithTag("brick");
                if (b)
                {
                    b.GetComponent<MeshRenderer>().enabled = !b.GetComponent<MeshRenderer>().enabled;
                    b.GetComponent<BoxCollider>().enabled = !b.GetComponent<BoxCollider>().enabled;
                }

                if (physicsHandsEnabled)
                    toggleBtnConfigHelper.backPlate.SetActive(true);
                else
                    toggleBtnConfigHelper.backPlate.SetActive(false);
            });
        }
    }
}
