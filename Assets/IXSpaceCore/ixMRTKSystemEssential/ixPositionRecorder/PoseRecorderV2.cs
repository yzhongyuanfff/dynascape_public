// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using JfranMora.Inspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using TMPro;
using UnityEngine;

public class PoseRecorderV2 : MonoBehaviour
{
    //
    public GameObject ExperienceOrigin;
    public int StartFrame = 70 * 2;
    public bool EnableAutoRecord = true;
    [SerializeField] private CsvWriterConfig csvWriterConfig;

    long frameCount = 0;
    long lineCount = 0;
    string fpath;
    string fn;
    string startTime;
    GameObject HeadObject;  
    GameObject LHandObject; 
    GameObject RHandObject;
    private StreamWriter writer;

    void TryFindPlayer()
    {
        HeadObject = GameObject.FindGameObjectWithTag("MainCamera");
        LHandObject = GameObject.FindGameObjectWithTag("LHand");
        RHandObject = GameObject.FindGameObjectWithTag("RHand");
    }

    void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }

    [Serializable]
    public class PoseDataPerFrame
    {
        public long Count { get; set; }
        public Vector3 HeadPosition { get; set; }
        public Quaternion HeadRotation { get; set; }
        public Vector3 LHandPosition { get; set; }
        public Quaternion LHandRotation { get; set; }
        public Vector3 RHandPosition { get; set; }
        public Quaternion RHandRotation { get; set; }
    }

    [Serializable]
    public class CsvWriterConfig
    {
        public bool WriteCount = true;
        public bool TraceHeadPose = true;
        public bool TraceLHandPose = false;
        public bool TraceRHandPose = false;
        public bool TraceHardwareStatus = true;
    }

    private string DeviceInFoHeader
    {
        get
        {
            var output = "";
            output += "TimeHumanReadable, ";
            output += "TrackingStartTime, ";
            output += "DeviceID, ";
            output += "wrapperVersion, ";
            output += "nativeSDKVersion, ";
            output += "UserIPD, ";
            return output;
        }
    }
    private string DeviceInFo
    {
        get
        {
            var output = "";
            output += System.DateTime.Now.ToString() + ", ";
            output += startTime + ", ";
            output += SystemInfo.deviceUniqueIdentifier + ", ";
            output += OVRPlugin.wrapperVersion + ", ";
            output += OVRPlugin.nativeSDKVersion + ", ";
            output += OVRPlugin.ipd + ", ";
            return output;
        }
    }


    private PoseDataPerFrame BuildPoseData()
    {
        PoseDataPerFrame poseData = new PoseDataPerFrame();

        poseData.Count = lineCount;

        //
        if (HeadObject)
        {
            poseData.HeadPosition = HeadObject.transform.position;
            poseData.HeadRotation = HeadObject.transform.rotation;
        }
        else
        {
            poseData.HeadPosition = Vector3.zero;
            poseData.HeadRotation = Quaternion.identity;
        }

        //
        if (LHandObject)
        {
            poseData.LHandPosition = LHandObject.transform.position;
            poseData.LHandRotation = LHandObject.transform.rotation;
        }
        else
        {
            poseData.LHandPosition = Vector3.zero;
            poseData.LHandRotation = Quaternion.identity;
        }

        //
        if (RHandObject)
        {
            poseData.RHandPosition = RHandObject.transform.position;
            poseData.RHandRotation = RHandObject.transform.rotation;
        }
        else
        {
            poseData.RHandPosition = Vector3.zero;
            poseData.RHandRotation = Quaternion.identity;
        }
        return poseData;
    }

    private string SensorDataCsvHeader
    {
        get
        {
            var output = "";
            if (csvWriterConfig.WriteCount)
                output += "TimeStamp,";
            if (csvWriterConfig.TraceHeadPose)
                output += "HeadPositionX,HeadPositionY,HeadPositionZ,HeadQuatW,HeadQuatX,HeadQuatY,HeadQuatZ,";
            if (csvWriterConfig.TraceLHandPose)
                output += "LHandPositionX,LHandPositionY,LHandPositionZ,LHandQuatW,LHandQuatX,LHandQuatY,LHandQuatZ,";
            if (csvWriterConfig.TraceRHandPose)
                output += "RHandPositionX,RHandPositionY,RHandPositionZ,RHandQuatW,RHandQuatX,RHandQuatY,RHandQuatZ,";
            if (csvWriterConfig.TraceHardwareStatus)
                output +=
                    "App_CpuTime_Float," +
                    "App_GpuTime_Float," +
                    "Compositor_CpuTime_Float," +
                    "Compositor_GpuTime_Float," +
                    "Compositor_DroppedFrameCount_Int," +
                    "System_GpuUtilPercentage_Float," +
                    "System_CpuUtilAveragePercentage_Float," +
                    "System_CpuUtilWorstPercentage_Float," +

                    "Device_CpuClockFrequencyInMHz_Float," +
                    "Device_GpuClockFrequencyInMHz_Float," +
                    "Compositor_SpaceWarp_Mode_Int," +

                    "Device_CpuCore0UtilPercentage_Float," +
                    "Device_CpuCore1UtilPercentage_Float," +


                    "suggestedCpuPerfLevel," +
                    "suggestedGpuPerfLevel," +
                    "batteryLevel," +
                    "batteryStatus," +
                    "latency," +

                    "powerSaving," +
                    "shouldRecenter," +
                    "systemDisplayFrequency," +
                    "vsyncCount," +
                    "userPresent";
            
            // +Add New Trace Here, Define Header 
            return output;
        }
    }

    /*
            builder.Append(OVRPlugin.powerSaving); builder.Append(",");
            builder.Append(OVRPlugin.shouldRecenter); builder.Append(",");
            builder.Append(OVRPlugin.systemDisplayFrequency); builder.Append(",");
            builder.Append(OVRPlugin.vsyncCount); builder.Append(",");
            builder.Append(OVRPlugin.userPresent); builder.Append(",");
     */

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string BuildCsvRow()
    {
        var builder = new StringBuilder();
        PoseDataPerFrame poseData = BuildPoseData();

        if (csvWriterConfig.WriteCount)
        {
            builder.Append(lineCount); 
            builder.Append(",");
        }
        if (csvWriterConfig.TraceHeadPose)
        {
            builder.Append(poseData.HeadPosition.x.ToString()); builder.Append(",");
            builder.Append(poseData.HeadPosition.y.ToString()); builder.Append(",");
            builder.Append(poseData.HeadPosition.z.ToString()); builder.Append(",");

            builder.Append(poseData.HeadRotation.w.ToString()); builder.Append(",");
            builder.Append(poseData.HeadRotation.x.ToString()); builder.Append(",");
            builder.Append(poseData.HeadRotation.y.ToString()); builder.Append(",");
            builder.Append(poseData.HeadRotation.z.ToString()); builder.Append(",");
        }
        if (csvWriterConfig.TraceLHandPose)
        {
            // not implemented 
        }
        if (csvWriterConfig.TraceRHandPose)
        {
            // not implemented 
        }
        if (csvWriterConfig.TraceHardwareStatus)
        {
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .App_CpuTime_Float).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .App_GpuTime_Float).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .Compositor_CpuTime_Float).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .Compositor_GpuTime_Float).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .Compositor_DroppedFrameCount_Int).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .System_GpuUtilPercentage_Float).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .System_CpuUtilAveragePercentage_Float).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .System_CpuUtilWorstPercentage_Float).GetValueOrDefault()); builder.Append(",");

            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .Device_CpuClockFrequencyInMHz_Float).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .Device_GpuClockFrequencyInMHz_Float).GetValueOrDefault()); builder.Append(",");

            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .Compositor_SpaceWarp_Mode_Int).GetValueOrDefault()); builder.Append(",");


            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .Device_CpuCore0UtilPercentage_Float).GetValueOrDefault()); builder.Append(",");
            builder.Append(OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics
                .Device_CpuCore1UtilPercentage_Float).GetValueOrDefault()); builder.Append(",");

            builder.Append(OVRPlugin.suggestedCpuPerfLevel); builder.Append(",");
            builder.Append(OVRPlugin.suggestedGpuPerfLevel); builder.Append(",");

            builder.Append(SystemInfo.batteryLevel); builder.Append(",");
            builder.Append(SystemInfo.batteryStatus.ToString()); builder.Append(",");

            builder.Append(OVRPlugin.latency); builder.Append(",");


            builder.Append(OVRPlugin.powerSaving); builder.Append(",");
            builder.Append(OVRPlugin.shouldRecenter); builder.Append(",");
            builder.Append(OVRPlugin.systemDisplayFrequency); builder.Append(",");
            builder.Append(OVRPlugin.vsyncCount); builder.Append(",");
            builder.Append(OVRPlugin.userPresent); builder.Append(",");

            // +Add Keep the Order! 
        }

        lineCount++;
        return builder.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        frameCount = 0;
        TryFindPlayer();
        fpath = Application.persistentDataPath + "/CollectedData/Trails/";
        startTime = (new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()).ToString();
        fn = "TracedData_" + startTime + ".csv";
    }


    // Update is called once per frame
    void Update()
    {
        if (EnableAutoRecord)
        {
            // Record 
            if (frameCount > StartFrame)
            {
                if(HeadObject != null)
                {
                    string path = fpath + fn;

                    if (!System.IO.Directory.Exists(fpath))
                        System.IO.Directory.CreateDirectory(fpath);

                    // Cache value
                    var fileExisted = File.Exists(path);

                    // Append the file and Create if not existent yet.
                    using (var stream = new FileStream(path, FileMode.Append, FileAccess.Write))
                    using (writer = new StreamWriter(stream))
                    {
                        // Write header if first time:
                        if (!fileExisted)
                        {
                            writer.WriteLine(DeviceInFoHeader);
                            writer.WriteLine(DeviceInFo);
                            writer.WriteLine(SensorDataCsvHeader);
                        }

                        writer.WriteLine(BuildCsvRow());
                    }

                }
            }
            else
            {
                TryFindPlayer();
            }
        }
        frameCount++;
    }

    [Button]
    void ForceFlushToFile()
    {
        EnableAutoRecord = false;
        writer.Flush();
        writer.Close();
    }

    /// <summary>
    /// 
    /// </summary>
    public void PauseContinueRecording()
    {
        EnableAutoRecord = !EnableAutoRecord;
    }
}
