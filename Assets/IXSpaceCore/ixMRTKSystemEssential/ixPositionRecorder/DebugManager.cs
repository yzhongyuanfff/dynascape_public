using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugManager : MonoBehaviour
{
    public static DebugManager instance;
    bool inMenu = true;
    Text logText;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        var rt = DebugUIBuilder.instance.AddLabel("debug log");
        //rt.GetComponent<RectTransform>().sizeDelta = new Vector2(1, 1);
        logText = rt.GetComponent<Text>(); // anchor the pointer 
        logText.alignment = TextAnchor.MiddleLeft;
        DebugUIBuilder.instance.Show();
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Two) || OVRInput.GetDown(OVRInput.Button.Start))
        {
            if (inMenu) DebugUIBuilder.instance.Hide();
            else DebugUIBuilder.instance.Show();
            inMenu = !inMenu;
        }
    }

    public void log(string msg)
    {
        logText.text = msg;
    }
}
