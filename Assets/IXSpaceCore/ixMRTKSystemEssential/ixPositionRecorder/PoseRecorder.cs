// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using TMPro;
using UnityEngine;

public class PoseRecorder : MonoBehaviour
{
    //
    public GameObject TrajectoryKeyPoint_Left;
    public GameObject TrajectoryKeyPoint_Right;
    public GameObject ExperienceOrigin;
    public bool EnableAutoRecord = true;
    public bool EnableVisualizer = true;
    public bool RecordHands = false;
    public bool SaveAsTXTFile = true;
    public bool SaveAsCSVFile = false;
    public int StartFrame = 70 * 2;
    public int FrameSpanSec = 10;

    //
    int FrameSpan;
    Vector3 lastPoseR;
    Vector3 lastPoseL;
    Vector3 lastPoseH;
    Vector3 currPoseR;
    Vector3 currPoseL;
    Vector3 currPoseH;
    GameObject HeadObject;
    GameObject LHandObject;
    GameObject RHandObject; 
    GameObject trajectoryKeyPoint_Head;
    List<Vector3> headPath;
    List<Vector3> LHandPath;
    List<Vector3> RHandPath;
    List<Quaternion> headOriList;
    List<Quaternion> LHandOriList;
    List<Quaternion> RHandOriList;
    List<GameObject> trajectoryVisualizerList_Right;
    List<GameObject> trajectoryVisualizerList_Left;
    List<GameObject> trajectoryVisualizerList_Head;
    string fn;
    float thresholdDist = 0.03f;
    bool lastVisEnabled = true;
    bool isRecording = true;
    int frameCount = 0;
    string exportFilePath;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    private void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }

    // Start is called before the first frame update
    void Start()
    {
        frameCount = 0;
        FrameSpan = FrameSpanSec * 72;
        TryFindPlayer();

        headPath = new List<Vector3>();
        LHandPath = new List<Vector3>();
        RHandPath = new List<Vector3>();
        headOriList = new List<Quaternion>();
        LHandOriList = new List<Quaternion>();
        RHandOriList = new List<Quaternion>();

        trajectoryVisualizerList_Right = new List<GameObject>();
        trajectoryVisualizerList_Left = new List<GameObject>();
        trajectoryVisualizerList_Head = new List<GameObject>();

        exportFilePath = Application.persistentDataPath + "/CollectedData/Trails/";
    }

    // Update is called once per frame
    void Update()
    {
        if (EnableAutoRecord)
        {
            if(frameCount > (StartFrame + FrameSpan))
            {
                RestartRecording();
            }

            // Record 
            if (true && (frameCount > 60))
            {
                if(HeadObject != null)
                {
                    headPath.Add(HeadObject.transform.position);
                    headOriList.Add(HeadObject.transform.rotation);

                    if (RecordHands)
                    {
                        if (LHandObject)
                        {
                            LHandPath.Add(ExperienceOrigin.transform.InverseTransformPoint(LHandObject.transform.position));
                            LHandOriList.Add(Quaternion.Inverse(ExperienceOrigin.transform.rotation) * LHandObject.transform.rotation);
                        }

                        if (RHandObject)
                        {
                            RHandPath.Add(ExperienceOrigin.transform.InverseTransformPoint(RHandObject.transform.position));
                            RHandOriList.Add(Quaternion.Inverse(ExperienceOrigin.transform.rotation) * RHandObject.transform.rotation);
                        }

                        // visualize key points
                        if (EnableVisualizer) {
                            currPoseH = HeadObject.transform.position;
                            if (Vector3.Distance(currPoseH, lastPoseH) > thresholdDist)
                            {
                                if (trajectoryKeyPoint_Head != null)
                                {
                                    GameObject kp_H = Instantiate(
                                        trajectoryKeyPoint_Head, HeadObject.transform.position, HeadObject.transform.rotation);
                                    trajectoryVisualizerList_Head.Add(kp_H);
                                }
                                lastPoseH = HeadObject.transform.position;
                            }

                            //
                            if (LHandObject)
                            {
                                currPoseL = LHandObject.transform.position;
                                if (Vector3.Distance(currPoseL, lastPoseL) > thresholdDist)
                                {
                                    if (TrajectoryKeyPoint_Left != null)
                                    {
                                        GameObject kp_L = Instantiate(
                                            TrajectoryKeyPoint_Left, LHandObject.transform.position, LHandObject.transform.rotation);
                                        trajectoryVisualizerList_Left.Add(kp_L);
                                    }
                                    lastPoseL = LHandObject.transform.position;
                                }
                            }


                            //
                            if (RHandObject)
                            {
                                currPoseR = RHandObject.transform.position;
                                if (Vector3.Distance(currPoseR, lastPoseR) > thresholdDist)
                                {
                                    if (TrajectoryKeyPoint_Right != null)
                                    {
                                        GameObject kp_R = Instantiate(
                                            TrajectoryKeyPoint_Right, RHandObject.transform.position, RHandObject.transform.rotation);
                                        trajectoryVisualizerList_Right.Add(kp_R);
                                    }
                                    lastPoseR = RHandObject.transform.position;
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                TryFindPlayer();
            }
        }

        frameCount++;
    }

    /// <summary>
    /// 
    /// </summary>
    void TryFindPlayer()
    {
        HeadObject = GameObject.FindGameObjectWithTag("MainCamera");
        LHandObject = GameObject.FindGameObjectWithTag("LHand");
        RHandObject = GameObject.FindGameObjectWithTag("RHand");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fn"></param>
    void ExportAsTXTFile(string fn)
    {
        StreamWriter tw = new StreamWriter(fn, false);

        // write head& hand movements 
        for (int i = 0; i < headPath.Count; i++)
        {
            Vector3 sample = ExperienceOrigin.transform.InverseTransformPoint(headPath[i]);
            tw.WriteLine("v " + sample.x + " " + sample.y + " " + sample.z);
        }
        for (int i = 0; i < LHandPath.Count; i++)
        {
            Vector3 sample = ExperienceOrigin.transform.InverseTransformPoint(LHandPath[i]);
            tw.WriteLine("v " + sample.x + " " + sample.y + " " + sample.z);
        }
        for (int i = 0; i < RHandPath.Count; i++)
        {
            Vector3 sample = ExperienceOrigin.transform.InverseTransformPoint(RHandPath[i]);
            tw.WriteLine("v " + sample.x + " " + sample.y + " " + sample.z);
        }

        // write orientations 
        for (int i = 0; i < headOriList.Count; i++)
        {
            Quaternion sample = Quaternion.Inverse(ExperienceOrigin.transform.rotation) * headOriList[i];
            tw.WriteLine("o " + sample.x + " " + sample.y + " " + sample.z + " " + sample.w);
        }
        for (int i = 0; i < LHandOriList.Count; i++)
        {
            Quaternion sample = Quaternion.Inverse(ExperienceOrigin.transform.rotation) * LHandOriList[i];
            tw.WriteLine("o " + sample.x + " " + sample.y + " " + sample.z + " " + sample.w);
        }
        for (int i = 0; i < RHandOriList.Count; i++)
        {
            Quaternion sample = Quaternion.Inverse(ExperienceOrigin.transform.rotation) * RHandOriList[i];
            tw.WriteLine("o " + sample.x + " " + sample.y + " " + sample.z + " " + sample.w);
        }

        // 
        tw.Write("l ");
        for (int i = 0; i < headPath.Count; i++) 
            tw.Write((i + 1).ToString() + " ");
        tw.WriteLine();

        if (RecordHands)
        {
            tw.Write("l ");
            for (int i = 0; i < headPath.Count; i++)
                tw.Write((headPath.Count + i + 1).ToString() + " ");
            tw.WriteLine();
            tw.Write("l ");
            for (int i = 0; i < headPath.Count; i++)
                tw.Write((headPath.Count * 2 + i + 1).ToString() + " ");
            tw.WriteLine();
        }


        //
        tw.Close();
    }

    /// <summary>
    /// 
    /// </summary>
    void SaveToFile()
    {
        //
        if (headPath.Count == 0) return;
        System.IO.Directory.CreateDirectory(exportFilePath);
        fn = exportFilePath + "/trackedData_" + Time.time.ToString() + ".txt";

        //
        if(SaveAsTXTFile) ExportAsTXTFile(fn);
        if(SaveAsCSVFile) ExportAsCSVFile();

        //
        LogMe("File Written To Path!: " + exportFilePath);
        clearData();
    }

    /// <summary>
    /// 
    /// </summary>
    void clearData()
    {
        // clear data 
        headPath.Clear();
        LHandPath.Clear();
        RHandPath.Clear();
        headOriList.Clear();
        LHandOriList.Clear();
        RHandOriList.Clear();

        // destory all visualizers
        foreach (GameObject objet in trajectoryVisualizerList_Right) { Destroy(objet); }
        foreach (GameObject objet in trajectoryVisualizerList_Left) { Destroy(objet); }
        foreach (GameObject objet in trajectoryVisualizerList_Head) { Destroy(objet); }
    }

    /// <summary>
    /// 
    /// </summary>
    void SendDataViaNetwork()
    {
        if (headPath.Count == 0)
            return;
        fn = Application.persistentDataPath + "/trackedData_" + Time.time.ToString() + ".txt";
        ExportAsTXTFile(fn);
        Debug.Log("written! path: " + fn);

        // send via network, send as binary file
        (new WebClient()).UploadFile(new Uri("http://zhirongtec.top:3000/upload"), "POST", fn);
        Debug.Log("file " + fn + " has been posted to: http://zhirongtec.top:3000/upload");

        // clear data 
        headPath.Clear();
        LHandPath.Clear();
        RHandPath.Clear();
        headOriList.Clear();
        LHandOriList.Clear();
        RHandOriList.Clear();

        // destory all visualizers
        foreach (GameObject objet in trajectoryVisualizerList_Right) { Destroy(objet); }
        foreach (GameObject objet in trajectoryVisualizerList_Left) { Destroy(objet); }
        foreach (GameObject objet in trajectoryVisualizerList_Head) { Destroy(objet); }
    }

    /// <summary>
    /// 
    /// </summary>
    public void ExportAsCSVFile()
    {
        string timestamp = Time.time.ToString();
        using (StreamWriter sw = new StreamWriter(exportFilePath + "/" + timestamp + "_head.csv"))
        {
            int objectIdx = 0;
            sw.WriteLine("timestamp,poseX,poseY,poseZ,oriX,oriY,oriZ,oriW");
            for (int i = 0; i < headPath.Count; i++)
            {
                sw.WriteLine(i * 142857
                    + "," + headPath[i].x
                    + "," + headPath[i].y
                    + "," + headPath[i].z

                    + "," + headOriList[i].x
                    + "," + headOriList[i].y
                    + "," + headOriList[i].z
                    + "," + headOriList[i].w);
            }
        }
        using (StreamWriter sw = new StreamWriter(exportFilePath + "/" + timestamp + "_LHand.csv"))
        {
            int objectIdx = 1;
            sw.WriteLine("timestamp,poseX,poseY,poseZ,oriX,oriY,oriZ,oriW");
            for (int i = 0; i < LHandPath.Count; i++)
            {
                sw.WriteLine(i * 142857
                    + "," + LHandPath[i].x
                    + "," + LHandPath[i].y
                    + "," + LHandPath[i].z

                    + "," + LHandOriList[i].x
                    + "," + LHandOriList[i].y
                    + "," + LHandOriList[i].z
                    + "," + LHandOriList[i].w);
            }
        }
        using (StreamWriter sw = new StreamWriter(exportFilePath + "/" + timestamp + "_RHand.csv"))
        {
            int objectIdx = 2;
            sw.WriteLine("timestamp,poseX,poseY,poseZ,oriX,oriY,oriZ,oriW");
            for (int i = 0; i < RHandPath.Count; i++)
            {
                sw.WriteLine(i * 142857
                    + "," + RHandPath[i].x
                    + "," + RHandPath[i].y
                    + "," + RHandPath[i].z

                    + "," + RHandOriList[i].x
                    + "," + RHandOriList[i].y
                    + "," + RHandOriList[i].z
                    + "," + RHandOriList[i].w);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void RestartRecording()
    {
        SaveToFile();
        frameCount = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    public void PauseContinueRecording()
    {
        EnableAutoRecord = !EnableAutoRecord;
        LogMe("Pose: EnableAutoRecord = "  + EnableAutoRecord);
    }

    /// <summary>
    /// 
    /// </summary>
    public void RecreateAllTrailPoints()
    {
        foreach (var o in trajectoryVisualizerList_Left)
            Destroy(o);
        foreach (var o in trajectoryVisualizerList_Right)
            Destroy(o);
        trajectoryVisualizerList_Left.Clear();
        trajectoryVisualizerList_Right.Clear();

        Vector3 lastPointL = new Vector3();
        lastPointL = LHandPath[0];
        for (int i = 0; i < LHandPath.Count; i++) 
        {
            if (TrajectoryKeyPoint_Left != null && Vector3.Distance(LHandPath[i],lastPointL) > thresholdDist)
            {
                GameObject kp = Instantiate(TrajectoryKeyPoint_Left, LHandPath[i], LHandOriList[i]);
                trajectoryVisualizerList_Left.Add(kp);
                lastPointL = LHandPath[i];
            }
        }

        Vector3 lastPointR = new Vector3();
        lastPointR = RHandPath[0];
        for (int i = 0; i < RHandPath.Count; i++)
        {
            if (TrajectoryKeyPoint_Right != null && Vector3.Distance(RHandPath[i], lastPointR) > thresholdDist)
            {
                GameObject kp = Instantiate(TrajectoryKeyPoint_Right, RHandPath[i], RHandOriList[i]);
                trajectoryVisualizerList_Right.Add(kp);
                lastPointR = RHandPath[i];
            }
        }
    }
}
