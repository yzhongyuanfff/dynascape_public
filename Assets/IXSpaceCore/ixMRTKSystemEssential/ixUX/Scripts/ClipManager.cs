using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipManager : MonoBehaviour
{
    [SerializeField] GameObject Segment;
    [SerializeField] GameObject StartPoint;
    [SerializeField] GameObject EndPoint;

    [SerializeField] bool DoRealtimeUpdate = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(DoRealtimeUpdate)
            UpdateSegmentAccordingly();
    }

    public void SetDim()
    {

    }

    public void UpdateSegmentAccordingly()
    {
        Vector3 center = (StartPoint.transform.localPosition + EndPoint.transform.localPosition) / 2.0f;
        float len = Mathf.Abs((StartPoint.transform.localPosition - EndPoint.transform.localPosition).x);

        Segment.transform.localPosition = center;
        Segment.transform.localScale = new Vector3(len, Segment.transform.localScale.y, Segment.transform.localScale.z);
    }
}
