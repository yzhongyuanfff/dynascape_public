using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustChildPanels : MonoBehaviour
{
    [SerializeField] GameObject RGBDRendererParent;
    [SerializeField] GameObject FileBrowserUISlot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AdjustUIToSlots()
    {
        foreach(Transform video in RGBDRendererParent.transform)
        {
            video.transform.Find("UIPanels").transform.position = FileBrowserUISlot.transform.position;
            video.transform.Find("UIPanels").transform.rotation = FileBrowserUISlot.transform.rotation;
        }
    }
}
