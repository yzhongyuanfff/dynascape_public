// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using JfranMora.Inspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComeInFrontOfMe : MonoBehaviour
{
    //public List<Transform> LocalUIAnchors;
    GameObject head;
    List<bool> AnchorOccupied;

    bool isInFront = false;

    int FrameInterval = 72;
    int CurrFrameCount = 0;
    public bool IsToast = false;

    // Start is called before the first frame update
    void Start()
    {
        head = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.activeSelf && IsToast)
        {
            CurrFrameCount++;
            if (CurrFrameCount > FrameInterval)
            {
                CurrFrameCount = 0;
                comeToMe();
            }
        }
    }

    // UI solution for large spaces 
    [Button]
    public void comeToMe()
    {
        this.gameObject.SetActive(true);

        head = GameObject.FindGameObjectWithTag("MainCamera"); // try again to query head position, start may not run 
        if (!head) return;

        Vector3 targetPosition = head.transform.TransformPoint(new Vector3(0, 0, 0.9f));
            // LocalUIAnchors[0].transform.position
        targetPosition.y = head.transform.position.y;

        Quaternion t = head.transform.rotation;
        Vector3 rot = t.eulerAngles;
        rot.x = 0;
        rot.z = 0;
        Quaternion targetRotation = Quaternion.Euler(rot);

        this.transform.position = targetPosition;
        this.transform.rotation = targetRotation;
    }

    public void comeToMe(float dist)
    {
        this.gameObject.SetActive(true);

        head = GameObject.FindGameObjectWithTag("MainCamera"); // try again to query head position, start may not run 
        if (!head) return;

        Vector3 targetPosition = head.transform.TransformPoint(new Vector3(0, 0, dist));
        // LocalUIAnchors[0].transform.position
        targetPosition.y = head.transform.position.y;

        Quaternion t = head.transform.rotation;
        Vector3 rot = t.eulerAngles;
        rot.x = 0;
        rot.z = 0;
        Quaternion targetRotation = Quaternion.Euler(rot);

        this.transform.position = targetPosition;
        this.transform.rotation = targetRotation;
    }
}
