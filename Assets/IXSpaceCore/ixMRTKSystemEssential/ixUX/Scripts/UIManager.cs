using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public FollowMeToggle settingsUI;
    public FollowMeToggle mainMenuUI;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void callSettingsUI()
    {
        settingsUI.ToggleFollowMeBehavior();
    }

    void callMainMenuUI()
    {
        mainMenuUI.ToggleFollowMeBehavior();
    }
}
