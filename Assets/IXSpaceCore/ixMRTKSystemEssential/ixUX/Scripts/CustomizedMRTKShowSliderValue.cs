﻿// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

using Microsoft.MixedReality.Toolkit.UI;
using TMPro;
using UnityEngine;

public class CustomizedMRTKShowSliderValue : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro textMesh = null;

    public float factor = 0.15f;

    public void OnSliderUpdated(SliderEventData eventData)
    {
        if (textMesh == null)
        {
            textMesh = GetComponent<TextMeshPro>();
        }
        
        if (textMesh != null)
        {           
            float value = factor * eventData.NewValue;
            textMesh.text = $"{value:F2}"; 
        }
    }
}

