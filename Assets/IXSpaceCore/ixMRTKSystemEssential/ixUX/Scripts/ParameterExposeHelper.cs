using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;
using UnityEngine.Video;

public class ParameterExposeHelper : MonoBehaviour
{
    public List<BibcamRGBDVideoConfigure> BibcamRGBDVideoPlayers;

    public GameObject UIContent;

    public GameObject UIButtonWithText;
    public GameObject UISliderWithText;
    public GameObject UIToggleButtonWithText;

    void Start()
    {
        showMeOnGUI();
    }

    public void showMeOnGUI()
    {
        GameObject ui = Instantiate(UIButtonWithText, UIContent.transform);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Hide";
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { onBtnClicked(); });
    }

    void onBtnClicked()
    {
        if (this.GetComponent<PhotonView>())
            this.GetComponent<PhotonView>().RPC("onBtnClickedSync", RpcTarget.All);
        else
            onBtnClickedSync();
    }

    [PunRPC]
    void onBtnClickedSync()
    {
        this.transform.parent.gameObject.SetActive(false);
    }
}
