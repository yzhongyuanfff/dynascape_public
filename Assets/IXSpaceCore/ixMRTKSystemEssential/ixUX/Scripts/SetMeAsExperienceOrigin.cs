using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMeAsExperienceOrigin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setMeAsExperienceOrigin()
    {
        //
        GameObject.FindGameObjectWithTag("ExperienceOrigin").transform.position = this.transform.position;
        GameObject.FindGameObjectWithTag("ExperienceOrigin").transform.rotation = this.transform.rotation;

        // update sub scenes 
        foreach(GameObject o in GameObject.FindGameObjectsWithTag("SceneGeometry"))
        {
            o.transform.position = this.transform.position;
            o.transform.rotation = this.transform.rotation;
        }
    }

    public void BringUserToCalibrationStation(int x)
    {
        GameObject.FindGameObjectWithTag("CalibrationManager").GetComponent<ixCalibrationManager>().BringMeToStationX(x, this.transform);
    }
}
