using Bibcam.Decoder;
using JfranMora.Inspector;
using Klak.Hap;
using PathCreation;
using PathCreation.Examples;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;
using UnityEngine.Video;
using Random = UnityEngine.Random;
using System.Threading;

[AttributeUsage(AttributeTargets.All, Inherited = true)]
public class DisplayTextAttribute : PropertyAttribute
{
    public string text;

    public DisplayTextAttribute(string text)
    {
        this.text = text;
    }
}

public class ixVideoPlayerController : MonoBehaviour
{
    [Header("----------")]
    [SerializeField] bool ReflactToUI = true;
    [SerializeField] GameObject UIContent;
    [SerializeField] GameObject ProgressBarContent;

    [SerializeField] GameObject UIContent_Previewer;
    [SerializeField] GameObject UIContent_SpatialAuthoring;
    [SerializeField] GameObject UIContent_ClipAuthoring;
    [SerializeField] GameObject UIContent_Advanced;
    [Header("========== Plain Video Renderer====")]
    [Header("----------")]
    [SerializeField] bool RenderToRenderer = false;
    [SerializeField] Renderer targetRenderer;
    [SerializeField] string targetMaterialProperty = "_BaseMap";
    [Header("----------")]
    [SerializeField] bool DoPreprocessing = true;
    [SerializeField] Material ProcessingMaterial;
    [Header("========== VFX Renderer====")]
    [Header("----------")]
    [SerializeField] bool RenderToVFX = false;
    [SerializeField] GameObject VisualEffectParent;
    [SerializeField] BibcamVideoFeeder videoFeeder;
    [SerializeField] VisualEffect VFX; // default vfx for env rendering
    [SerializeField] VisualEffect HumanVFX;
    [SerializeField] bool ApplyCameraTransformations = false;
    [Header("----------")]
    [SerializeField] bool FollowMode = false;
    [Header("========== QR Detection From Video ====")]
    [Header("----------")]
    [SerializeField] AprilTagDetector aprilTagDetector;
    [SerializeField] GameObject QRMask;
    [SerializeField] GameObject RGBDCam;
    [SerializeField] bool ContinuesDetection = false;
    [Header("========== Render Tube From Video ====")] // Tube, Path Extraction
    [Header("----------")]
    [SerializeField] bool RenderWithTubeRenderer = false;
    [SerializeField] GameObject TubePrefeb;
    [SerializeField] bool RenderWithVFXRenderer = true;
    [SerializeField] GameObject VFXTrailRendererPrefeb;
    [SerializeField] PathCreator FloorPathCreator;
    //[SerializeField] GameObject VFXPathRendererPrefeb;

    VideoPlayer _H264Video = null; // H264
    HapPlayer _HAPVideo = null; // HAP
    VideoPreprocessing _processor; //
    [SerializeField]Texture outTexture;

    float _PlaybackSpeed = 1;
    GameObject UGUITextedSlider;
    ixUtility.VideoFormat videoFormat;
    GameObject mixedRealityPlayspace;

    // Start is called before the first frame update
    void Start()
    {
        mixedRealityPlayspace = GameObject.FindGameObjectWithTag("MixedRealityPlayspace");
        VFXPreviewControlVaribleNames = new List<string>();
        InitUI();
        InitProcessor();
    }

    List<Vector3> TubePoints = new List<Vector3>();
    List<Quaternion> TubeRotations = new List<Quaternion>();
    public GameObject Tube;
    public GameObject MeshTube;
    //GameObject VideoPath;

    [SerializeField] List<RenderTexture> TextureAtlas = new List<RenderTexture>();
    [SerializeField] private Texture2DArray texture2DArray;
    [SerializeField] private Texture2DArray texture2DPatternArray;
    [SerializeField] public bool ExtractingTrail = false;
    [SerializeField] Texture2D tex2d;

    List<long> TextureFrameIdx = new List<long>();
    RenderTexture TextureIn3D;
    int videoLoops = 0;
    int targetVideoLoops = -1;
    int frameInterval = 20;
    int frameCount = 0;
    string VideoDataFolderPath;
    string VideoPath;
    float ReplayDistance = 1;
    float PauseDistance = 2;
    bool DoApplyTimelineTransform = false;
    List<GameObject> quads = new List<GameObject>();
    int frameStart = 0;
    int frameStop = int.MaxValue;
    GameObject RotCtrlPoint;
    bool DoRotWithCtrlPoint = false;
    bool CacheFrames = false;
    bool RunCacheFrames = false;

    [SerializeField] Vector3 ImageOffset = new Vector3(0.2f, 0, 0.2f);
    [SerializeField] VisualEffect DynaScapeVFX;
    [SerializeField] VisualEffect DynaScapeVFX_Reduced;
    [SerializeField] VisualEffect DynaScapeVFX_Style_FineTune;
    [SerializeField] VisualEffect DynaScapeVFX_Style_FineTune_ZY;
    [SerializeField] VisualEffect VoxelVFX;
    [SerializeField] Bounds VideoBounds = new Bounds();
    [SerializeField] public GameObject SpatialPlacer;
    [SerializeField] GameObject DragableRotPointPrefeb;
    [SerializeField] public bool DoCheckProximity = false;
    [SerializeField] VFXStyleObject HumanHeatMapStyle;
    [SerializeField] VFXStyleObject EnvViewStyle;
    [SerializeField] int CurrentFrame = -1;
    [SerializeField] int FrameCountSinceStart = 0;
    [SerializeField] int TotalFrameTarget = 0;
    [SerializeField] bool StopGeneratingPointsForEnvWhenTargetPointCountHit = false;

    [SerializeField] int CacheInterval = 10;
    [SerializeField] float DesiredPlaybackSpeedForExtracting = 0.5f;
    [SerializeField] List<RenderTexture> CachedTextures = new List<RenderTexture>();

    //////////////// Test ////
    void TubeRenderTest()
    {
        TubePoints.Clear();
        TubePoints.Add(new Vector3(0, 0, 0));
        TubePoints.Add(new Vector3(0, 1, 0));
        TubePoints.Add(new Vector3(0, 1, 1));
        TubePoints.Add(new Vector3(0, 2, 3));
        TubePoints.Add(new Vector3(0, 4, 6));
        Tube = Instantiate(TubePrefeb, VFX.transform);
        Tube.GetComponent<CustomTubeRenderer>().SetDownScaleFactor(1); //
        Tube.GetComponent<CustomTubeRenderer>().CreateTrailBaseOnPointList(TubePoints, Color.green);
    }

    void VFXTubeRenderTest()
    {
        TubePoints.Clear();
        TubePoints.Add(new Vector3(0, 0, 0));
        TubePoints.Add(new Vector3(0, 1, 0));
        TubePoints.Add(new Vector3(0, 1, 1));
        TubePoints.Add(new Vector3(0, 2, 3));
        TubePoints.Add(new Vector3(0, 4, 6));
        Tube = Instantiate(VFXTrailRendererPrefeb, VFX.transform);
        Tube.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnPointList(TubePoints, Color.green);
    }

    //MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();

    long LastProcessedFrame = 0;

    // Update is called once per frame
    void Update()
    {
        UpdateUI();
        UpdateFrame();
        UpdateMRTKPlayspaceFollowMode();
        ContinuesDetectQR();

        if (MeshTube)
        {
            //propertyBlock = new MaterialPropertyBlock();
            //float inper = (float)_H264Video.frame / (float)_H264Video.frameCount;
            //Debug.Log("InPer = " + inper);
            //MeshTube.GetComponent<CustomTubeRenderer>().material.SetFloat("_InPer", inper);
            //propertyBlock.SetFloat("_InPer", inper);
            //MeshTube.GetComponent<MeshRenderer>().SetPropertyBlock(propertyBlock);
            //MeshTube.GetComponent<CustomTubeRenderer>().material.color = new Color(inper, 0, 0);
        }

        if (RunCacheFrames) // cache frames with interval 
        {
            if (_H264Video.frame < (long)_H264Video.frameCount - 1)
            {
                if (_H264Video.frame > LastProcessedFrame && _H264Video.isPrepared)
                {
                    //_H264Video.Pause();
                    if (_H264Video.frame >= 0)
                    {
                        // decode
                        videoFeeder.RefDecoder().DecodeSync(_H264Video.texture);
                        videoFeeder.RefDemuxer().Demux(_H264Video.texture, videoFeeder.RefDecoder().Metadata);

                        // save textures
                        if (_H264Video.frame % CacheInterval == 0)
                        {
                            RenderTexture tmpRT = new RenderTexture(
                                videoFeeder.RefDemuxer().ColorTexture.width,
                                videoFeeder.RefDemuxer().ColorTexture.height, 1)
                            { wrapMode = TextureWrapMode.Clamp };
                            Graphics.CopyTexture(videoFeeder.RefDemuxer().ColorTexture, tmpRT);
                            CachedTextures.Add(tmpRT);
                        }
                    }
                    LastProcessedFrame = _H264Video.frame;

                    //_H264Video.StepForward();
                }
                else
                {
                    // wait
                }
            }
            else
            {
                RunCacheFrames = false;
            }
        }

        if (ExtractingTrail) {
            if (_H264Video.frame < (long)_H264Video.frameCount - 5) {
                _H264Video.Pause();

                // Core logic for video extraction
                if (_H264Video.frame >= 5) {
                    // decode
                    videoFeeder.RefDecoder().DecodeSync(_H264Video.texture);
                    videoFeeder.RefDemuxer().Demux(_H264Video.texture, videoFeeder.RefDecoder().Metadata);

                    // save camera trails
                    TubePoints.Add(videoFeeder.RefDecoder().Metadata.CameraPosition);
                    TubeRotations.Add(videoFeeder.RefDecoder().Metadata.CameraRotation);
                    Debug.Log("Extracting camera pose from frame = " + _H264Video.frame +
                        "/" + _H264Video.frameCount + ", campose = " + videoFeeder.RefDecoder().Metadata.CameraPosition);

                    // save textures
                    if(_H264Video.frame % 30 == 0)
                    {
                        RenderTexture tmpRT = new RenderTexture(
                            videoFeeder.RefDemuxer().ColorTexture.width,
                            videoFeeder.RefDemuxer().ColorTexture.height, 1){ wrapMode = TextureWrapMode.Clamp };
                        Graphics.CopyTexture(videoFeeder.RefDemuxer().ColorTexture, tmpRT);
                        TextureAtlas.Add(tmpRT);
                        TextureFrameIdx.Add(_H264Video.frame);
                    }

                    // append bounds
                    Vector3 cameraPosition = videoFeeder.RefDecoder().Metadata.CameraPosition;
                    Vector3 corner0 = cameraPosition + videoFeeder.RefDecoder().Metadata.CameraRotation * new Vector3(2.5f, 2.5f, 5);
                    Vector3 corner1 = cameraPosition + videoFeeder.RefDecoder().Metadata.CameraRotation * new Vector3(-2.5f, 2.5f, 5);
                    Vector3 corner2 = cameraPosition + videoFeeder.RefDecoder().Metadata.CameraRotation * new Vector3(2.5f, -2.5f, 5);
                    Vector3 corner3 = cameraPosition + videoFeeder.RefDecoder().Metadata.CameraRotation * new Vector3(-2.5f, -2.5f, 5);
                    VideoBounds.Encapsulate(corner0);
                    VideoBounds.Encapsulate(corner1);
                    VideoBounds.Encapsulate(corner2);
                    VideoBounds.Encapsulate(corner3);
                }

                _H264Video.StepForward();
            }
            else if(_H264Video.frame >= (long)_H264Video.frameCount - 5) // drop off first and last 5 frames
            {
                Debug.Log("Extraction Finished!");
                ExtractingTrail = false;
                RenderToVFX = true;
                _H264Video.isLooping = true;


                if (RenderWithTubeRenderer)
                {
                    MeshTube = Instantiate(TubePrefeb, this.transform);
                    MeshTube.GetComponent<CustomTubeRenderer>().SetDownScaleFactor(1); //
                    MeshTube.GetComponent<CustomTubeRenderer>().CreateTrailBaseOnPointAndRotationList(
                        TubePoints, TubeRotations, Color.green);
                }
                if (RenderWithVFXRenderer)
                {
                    Tube = Instantiate(VFXTrailRendererPrefeb, VFX.transform);
                    Tube.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnPointAndRotationList(
                        TubePoints, TubeRotations, Color.green);
                }

                WriteALL();
            }
        }

        if (DoRotWithCtrlPoint)
        {
            Vector3 center = this.transform.parent.Find("VisualEffects").transform.TransformPoint(Tube.GetComponent<VFXTrailRenderer>().pointList[frameStart]); // the green ball
            int frame = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
            Vector3 ontubepoint = this.transform.parent.Find("VisualEffects").transform.TransformPoint(Tube.GetComponent<VFXTrailRenderer>().pointList[frame]);
            Vector3 ontubedir = new Vector3((ontubepoint - center).x, 0, (ontubepoint - center).z);
            Vector3 targetdir = new Vector3((RotCtrlPoint.transform.position - center).x, 0, (RotCtrlPoint.transform.position - center).z); // yellow ball
            float angle = Vector3.SignedAngle(ontubedir, targetdir, Vector3.up);

            //Debug.Log("angle = " + angle);
            //if(angle > 180)
            //{
            //    angle = -(360 - angle);
            //}


            this.transform.parent.Find("VisualEffects").transform.RotateAround(center, Vector3.up, angle);

            //this.transform.parent.Find("VisualEffects").transform.rotation = Quaternion.LookRotation(

            //// Calculate the direction from the center point to the target position
            //Vector3 targetDirection = RotCtrlPoint.transform.position - center;

            //// Rotate the parent object to align the child object with the target position
            //this.transform.parent.Find("VisualEffects").transform.rotation = Quaternion.LookRotation(-targetDirection, Vector3.up);

            // Move the parent object so that the child object is at the target position
            //Tube.transform.position += RotCtrlPoint.transform.position - OriRotCtrlPoint;
        }

        if (DoApplyTimelineTransform)
        {
            int frame = (int)_H264Video.frame;
            if(frame>frameStart && frame < frameStop)
            {
                this.transform.parent.Find("VisualEffects").position = Tube.GetComponent<VFXTrailRenderer>().TransformationListP[frame];
                this.transform.parent.Find("VisualEffects").rotation = Tube.GetComponent<VFXTrailRenderer>().TransformationListR[frame];
            }
        }

        if (DoCheckProximity)
        {
            Vector3 campose = this.transform.parent.Find("VisualEffects").TransformPoint(videoFeeder.RefDecoder().Metadata.CameraPosition);
            GameObject head = GameObject.FindGameObjectWithTag("MainCamera");
            Vector3 headpose = head.transform.position;

            float proximity = Vector3.Distance(campose, headpose);
            if (proximity < ReplayDistance)
            {
                ResumeVideoAndVFXPlay();
            }else if(proximity > PauseDistance)
            {
                PauseVideoAndVFXPlay();
            }
        }

        if (Input.GetKeyDown("space"))
        {
            //PauseContinueVideoPlayAndVFX();
        }
    }

    ///////////////////////////////////////////////////////////////////// Processing ////
    
    void PrepareProcessingVideo()
    {
        // disable rendering 
        StopVFX();
        RenderToVFX = false;
        _H264Video.isLooping = false;

        // prepare video to go through 
        _H264Video.frame = 0;
        //_H264Video.skipOnDrop = false;
        _H264Video.frameReady += VideoFrameReady;
        _H264Video.frameDropped += VideoFrameDrop;
        _H264Video.Play(); 

        //
        LastProcessedFrame = 0;
        _H264Video.playbackSpeed = DesiredPlaybackSpeedForExtracting;
        NumFrameDropped = 0;

        //
        Debug.Log("Start Processing Video, frames = " + _H264Video.frameCount);
    }

    [Button("Cache Frames")]
    void DoCacheFrames()
    {
        //
        PrepareProcessingVideo();

        // clear data 
        CachedTextures.Clear();

        // set flag 
        CacheFrames = true;
    }

    [Button]
    void TranscodeFrames()
    {
        //
        PrepareProcessingVideo();

        //
        TransCode = true;
    }

    [Button]
    void ForceFlushVideo()
    {
        //
        TransCode = false;
        videoGenerator.StopRecording(); // this will save video to disk 
    }

    [Button]
    void StopCache()
    {
        // set flag 
        CacheFrames = false;

        //
        _H264Video.frame = 0;
        _H264Video.Pause();
    }

    [Button]
    void ClearCachedFrames()
    {
        foreach (var img in CachedTextures)
            Destroy(img);
        CachedTextures.Clear();
    }

    List<GameObject> CachedFrameQuads = new List<GameObject>();

    [Button("[Debug] Show All Extracted Frames")]
    void ShowAllExtractedFrames()
    {
        CachedFrameQuads.Clear();
        int texID = 0;
        foreach (var tex in CachedTextures)
        {
            Random.InitState(texID);
            Vector3 rp = new Vector3(
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f),
                Random.Range(-1.0f, 1.0f));
            Quaternion ro = new Quaternion(
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1));

            GameObject img = Instantiate((GameObject)
                Resources.Load("ImagePrefeb", typeof(GameObject)), this.transform);
            img.transform.Find("Geometry").GetComponent<MeshRenderer>().material.mainTexture = tex;
            img.transform.localScale = new Vector3(0.4f, (float)tex.height / tex.width * 0.4f, 1);
            img.transform.position = rp;
            img.transform.rotation = ro;
            CachedFrameQuads.Add(img);

            texID++;
        }
    }

    [Button]
    void ClearAllGeneratedQuads()
    {
        foreach (var o in CachedFrameQuads)
            Destroy(o);
        CachedFrameQuads.Clear();
    }
    ///////////////////////////////////////////////////////////////////// Previewing ////

    [Button("////////")] void d000() { }

    [Button("Previewing - Disable All Camera Trail View")]
    public void Previewing_DisableAllCameraTrailView()
    {
        Tube.GetComponent<VFXTrailRenderer>().RenderColorTrail = false;
        Tube.GetComponent<VFXTrailRenderer>().RenderTimeTick = false;
        Tube.GetComponent<VFXTrailRenderer>().RenderTexturedFrustumVis = false;
        Tube.GetComponent<VFXTrailRenderer>().RenderTexturedStripVis = false;
        Tube.GetComponent<VFXTrailRenderer>().RenderPointBasedTrail = false;
        Tube.GetComponent<VFXTrailRenderer>().RenderCameraOrientaion_LineBased = false;
        Tube.GetComponent<VFXTrailRenderer>().RenderPathView = false;
        Tube.GetComponent<VFXTrailRenderer>().RenderWithLines = false;

        Tube.GetComponent<VFXTrailRenderer>().EncodePathWithSpeed = false;
        Tube.GetComponent<VFXTrailRenderer>().EncodePathWithHumanCount = true;

        Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
    }

    [Button("Previewing - Show All Camera Trail View")]
    public void Previewing_ShowAllCameraTrailView()
    {
        Tube.GetComponent<VFXTrailRenderer>().RenderColorTrail = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderTimeTick = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderTexturedFrustumVis = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderTexturedStripVis = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderPointBasedTrail = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderCameraOrientaion_LineBased = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderPathView = true;

        Tube.GetComponent<VFXTrailRenderer>().EncodePathWithSpeed = false;
        Tube.GetComponent<VFXTrailRenderer>().EncodePathWithHumanCount = true;

        Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
    }

    [Button("Previewing - Semantic")]
    public void PreviewSemantic()
    {
        Previewing_DisableAllCameraTrailView();
        Tube.GetComponent<VFXTrailRenderer>().RenderPathView = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderWithLines = true; // baseline
        Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
    }

    [Button("Previewing - Camera Pose")]
    public void PreviewCameraPose()
    {
        Previewing_DisableAllCameraTrailView();
        Tube.GetComponent<VFXTrailRenderer>().RenderPointBasedTrail = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderCameraOrientaion_LineBased = true;
        Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
    }

    [Button("Previewing - Time")]
    public void PreviewTime()
    {
        Previewing_DisableAllCameraTrailView();
        Tube.GetComponent<VFXTrailRenderer>().RenderTimeTick = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderWithLines = true; // baseline
        Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
    }

    [Button("Previewing - Keyframe with Frustum")]
    public void PreviewKeyframeWithFrustum()
    {
        Previewing_DisableAllCameraTrailView();
        Tube.GetComponent<VFXTrailRenderer>().RenderTexturedFrustumVis = true;
        Tube.GetComponent<VFXTrailRenderer>().RenderWithLines = true; // baseline
        Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
    }

    [Button("Previewing - With Path")]
    public void PreviewingWithPath()
    {
        FloorPathCreator.gameObject.SetActive(true);

        FloorPathCreator.bezierPath = new BezierPath(MeshTube.GetComponent<CustomTubeRenderer>().SampledPoints);

        FloorPathCreator.GetComponent<RoadMeshCreator>().TriggerUpdate();
    }

    [Button]
    public void ColorizePath()
    {
        GraphicsBuffer cbuffer = Tube.transform.GetComponent<VFXTrailRenderer>().randomColorBuffer;
        FloorPathCreator.transform.Find("Road Mesh Holder").GetComponent<MeshRenderer>().materials[0].SetBuffer("_PathColor", cbuffer);
    }

    public void UpdatePreviewHERate(float rate)
    {
        VFX.GetComponent<VFXStyle>().UpdatePreviewHERate(rate);
        VFX.GetComponent<VFXStyle>().UseStylesFromScript = true;
    }

    ///////////////////////////////////////////////////////////////////////////////VFX ////
    [Button("VFX - Render Env")]
    public void RenderEnv()
    {
        StartVFX(VFX);
        FrameCountSinceStart = 0;
        TotalFrameTarget = (int)(_H264Video.frameCount * 2.0f);
    }

    [Button("VFX - Pause Env Rendering")]
    public void PauseEnvRendering()
    {
        VFX.pause = true;
    }

    [Button("VFX - Disable Env Rendering")]
    public void DisableEnvRendering()
    {
        StopVFX(VFX);
    }

    [Button("VFX - Render Dynamic Human")]
    public void RenderDynamicHuman()
    {
        StartVFX(HumanVFX);
    }

    [Button("VFX - Pause Dynamic Human")]
    public void PauseDynamicHumanRendering ()
    {
        HumanVFX.pause = true;
    }

    [Button("VFX - Disable Dynamic Human")]
    public void DisableDynamicHumanRendering()
    {
        StopVFX(HumanVFX);
    }


    [Button("VFX - Show Dynamic Human")]
    public void ShowDynamicHuman()
    {
        DisableEnvRendering();
        DisableDynamicHumanRendering();
        HumanVFX.transform.GetComponent<VFXStyle>().currentStyles = HumanHeatMapStyle;
        StartVFX(HumanVFX);
    }

    [Button("VFX - Show Env View")]
    public void ShowEnvView()
    {
        DisableEnvRendering();
        DisableDynamicHumanRendering();
        VFX.transform.GetComponent<VFXStyle>().currentStyles = EnvViewStyle;
        StartVFX(VFX);
    }

    ///////////////////////////////////////////////////////////////////////////////Previewing/ Playback Control ////
    [Button("////////")] void d001() { }
    /// <summary>
    ///
    /// </summary>
    [Button("Playback - PlayVideo")]
    public void PlayVideo()
    {
        ixUtility.LogMe("Playing Video");
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            _H264Video.frame = frameStart;
            FrameCountSinceStart = 0;
            NumFrameDropped = 0;
            TotalFrameTarget = (int)(_H264Video.frameCount * 2.0f);
            _H264Video.Prepare();
            _H264Video.Play();
        }

        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            _HAPVideo.time = 0;
            _HAPVideo.speed = _PlaybackSpeed;
        }

        if (RenderToVFX && VFX)
        {
            StopVFX();
            StartVFX();
        }

        Tube.GetComponent<VFXTrailRenderer>().DoAnim = true;
        Tube.GetComponent<VFXTrailRenderer>().DoSnap = false;
    }

    [Button("Playback - ResetPlayParameters")]
    public void ResetPlayParameters()
    {
        _H264Video.playbackSpeed = 1.0f;
        DisableAllOtherExtractionVFXBooleans();
        PlayVideo();
    }

    [Button("Playback - Pause Video And VFX Play")]
    public void PauseVideoAndVFXPlay()
    {
        _H264Video.Pause();
        VFX.pause = true;
        //VFX.SendEvent("OnStop");

        Tube.GetComponent<VFXTrailRenderer>().DoSnap = false;
        Tube.GetComponent<VFXTrailRenderer>().DoAnim = false;
    }

    [Button("Playback - Resume Video And VFX Play")]
    public void ResumeVideoAndVFXPlay()
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
            _H264Video.Play();
        if (videoFormat == ixUtility.VideoFormat.HAP)
            _HAPVideo.speed = _PlaybackSpeed;
        VFX.pause = false;
        VFX.SendEvent("OnPlay");

        Tube.GetComponent<VFXTrailRenderer>().DoSnap = false;
        Tube.GetComponent<VFXTrailRenderer>().DoAnim = true;
    }

    [Button("Playback - PauseContinueVideoPlayAndVFX")]
    public void PauseContinueVideoPlayAndVFX()
    {
        if (_H264Video.isPlaying)
        {
            PauseVideoAndVFXPlay();
        }
        else
        {
            ResumeVideoAndVFXPlay();
        }
    }

    [Button("Playback - ToggleCheckProximity")]
    public void ToggleCheckProximity()
    {
        DoCheckProximity = !DoCheckProximity;
    }

    [Button("Playback - StopALLRemoveAllPoints")]
    public void StopALLRemoveAllPoints()
    {
        StopVideo();
        StopVFX();
    }

    public void UpdateVideoPlayPercentage(float per)
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            int targetFrame = (int)(per * _H264Video.frameCount);
            _H264Video.Pause();
            _H264Video.frame = targetFrame;
            _H264Video.Play();
        }
        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            //int targetFrame = (int)(per * _HAPVideo.frameCount);
            float targetTime = (float)(per * _HAPVideo.streamDuration);
            _HAPVideo.time = targetTime;
        }
    }

    public void UpdateVideoPlaySpeed(float speed)
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            _H264Video.playbackSpeed = speed;
        }
        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            _HAPVideo.speed = speed;
        }
    }

    public void LogVideoState()
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            ixUtility.LogMe("_H264Video.isPrepared = " + _H264Video.isPrepared);
            ixUtility.LogMe("_H264Video.isPlaying = " + _H264Video.isPlaying);
            ixUtility.LogMe("_H264Video.isActiveAndEnabled = " + _H264Video.isActiveAndEnabled);
            ixUtility.LogMe("_H264Video.width = " + _H264Video.width);
            ixUtility.LogMe("_H264Video.height = " + _H264Video.height);
        }
        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            ixUtility.LogMe("_HAPVideo.isValid = " + _HAPVideo.isValid);
        }
    }

    Vector3 LocalToWorldPosition(in Matrix4x4 inverseView, Vector3 localPosition)
    {
        Vector4 tmp02 = inverseView * new Vector4(localPosition.x, localPosition.y, localPosition.z, 1);
        return new Vector3(tmp02.x, tmp02.y, tmp02.z);
    }


    public void StopVideo()
    {
        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            _H264Video.Stop();
            _H264Video.frame = 0;
            _H264Video.Prepare();
        }

        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            _HAPVideo.speed = 0;
            _HAPVideo.time = 0;
        }
    }

    private void VideoPlayer_errorReceived(VideoPlayer source, string message)
    {
        ixUtility.LogMe("_H264Video, Error message: " + message);
    }

    ///////////////////////////////////////////////////////////////////// Playback Control - VFX ////
    [Button("Preview - ExtractArch")]
    public void ExtractArch()
    {
        DisableAllOtherExtractionVFXBooleans();
        RefVFX().SetBool("ExtractArch", true);

        //targetVideoLoops = 1; // reset frame target
        //videoLoops = 0;
        //DoDynamicHumanRenderingAfterTargetFrameHit = false;
        _H264Video.playbackSpeed = 1.0f;

        StopALLRemoveAllPoints();// start to play
        PlayVideo();
    }

    [Button("Preview - ExtractHuman")]
    public void ExtractHuman()
    {
        DisableAllOtherExtractionVFXBooleans();
        RefVFX().SetBool("ExtractHuman", true);

        //targetVideoLoops = 1; // reset frame target
        //videoLoops = 0;
        //DoDynamicHumanRenderingAfterTargetFrameHit = false;
        _H264Video.playbackSpeed = 1.0f;

        StopALLRemoveAllPoints();// start to play
        PlayVideo();
    }

    [Button("Preview - ExtractDynamicHumanWhileContinueVideoPlay (ExtractArch First)")]
    public void ExtractDynamicHumanWhileContinueVideoPlay()
    {
        DisableAllOtherExtractionVFXBooleans();
        _H264Video.playbackSpeed = 1.0f;
        RefVFX().SetBool("ExtractDynamicHuman", true);
        // continue to play!

    }

    [Button("Preview - ExtractThinBorder")]
    public void ExtractThinBorder()
    {
        DisableAllOtherExtractionVFXBooleans();
        RefVFX().SetBool("ExtractThinBorder", true);

        //targetVideoLoops = 1; // reset frame target
        //videoLoops = 0;
        //DoDynamicHumanRenderingAfterTargetFrameHit = false;
        _H264Video.playbackSpeed = 1.0f;

        StopALLRemoveAllPoints();// start to play
        PlayVideo();
    }

    [Button("+Navigate To VFX for More")] void d004() { }

    [Button("Playback - PauseResumeVFX")]
    public void PauseResumeVFX()
    {
        VFX.pause = !VFX.pause;
    }

    //[Button("+Navigate To VFX for More")] void d004() { }
    [Button("Switching VFX - Voxels")]
    public void SwitchToVFX_Voxels()
    {
        SwitchVFX(VoxelVFX);
        StopALLRemoveAllPoints();// start to play
        PlayVideo();
    }

    [Button("Switching VFX - DynaScapeVFX")]
    public void SwitchToVFX_DynaScapeVFX()
    {
        SwitchVFX(DynaScapeVFX);
        StopALLRemoveAllPoints();// start to play
        PlayVideo();
    }

    [Button("Switching VFX - DynaScapeVFX_Reduced")]
    public void SwitchToVFX_DynaScapeVFX_Reduced()
    {
        SwitchVFX(DynaScapeVFX_Reduced);
        StopALLRemoveAllPoints();// start to play
        PlayVideo();
    }

    [Button("Switching VFX - DynaScapeVFX_Style_FineTune")]
    public void SwitchToVFX_DynaScapeVFX_Style_FineTune()
    {
        SwitchVFX(DynaScapeVFX_Style_FineTune);
        StopALLRemoveAllPoints();// start to play
        PlayVideo();
    }
    [Button("Switching VFX - DynaScapeVFX_Style_FineTune ZY")]
    public void SwitchToVFX_DynaScapeVFX_Style_FineTune_ZY()
    {
        SwitchVFX(DynaScapeVFX_Style_FineTune_ZY);
        StopALLRemoveAllPoints();// start to play
        PlayVideo();
    }

    bool isApplyPointSizeVaryingEffect = false;
    [Button("VFX - Apply Point Size Varying Effect")]
    public void ApplyPointSizeVaryingEffect()
    {
        isApplyPointSizeVaryingEffect = !isApplyPointSizeVaryingEffect;
        VFX.SetBool("ApplyPointSizeVaryingEffect", isApplyPointSizeVaryingEffect);
    }

    public void DisableAllOtherExtractionVFXBooleans()
    {
        RefVFX().SetBool("ExtractArch", false);
        RefVFX().SetBool("ExtractHuman", false);
        RefVFX().SetBool("ExtractDynamicHuman", false);
        RefVFX().SetBool("ExtractThinBorder", false);

        RefVFX().SetBool("DoViewDepCheck", true);
    }

    public VisualEffect RefVFX()
    {
        return VFX;
    }

    public void StopVFX()
    {
        VFX.pause = true;
        VFX.Stop();
        VFX.Reinit();
        VFX.SendEvent("OnStop");
    }

    public void StopVFX(VisualEffect vfx)
    {
        vfx.pause = true;
        vfx.Stop();
        vfx.Reinit();
        vfx.SendEvent("OnStop");
    }

    public void StartVFX()
    {
        VFX.pause = false;
        VFX.gameObject.SetActive(true);
        VFX.SetBool("DoViewDepCheck", true);
        VFX.Play();
        VFX.SendEvent("OnPlay");
    }

    public void StartVFX(VisualEffect vfx)
    {
        vfx.pause = false;
        vfx.gameObject.SetActive(true);
        vfx.Play();
        vfx.SendEvent("OnPlay");
    }

    public void SwitchVFX(VisualEffect newVFX)
    {
        StopVFX();
        VFX = newVFX;
    }

    ///////////////////////////////////////////////////////////////////////////////Spatial Authoring - Constraint Based Spatial Placer ////
    [Button("////////")] void d002() { }

    [Button("Spatial Authoring - Spatial Placer")]
    public void ShowSpatialPlacerAndPlaceItInFrontOfMe()
    {
        SpatialPlacer.gameObject.SetActive(true);
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        SpatialPlacer.transform.position = rhand.transform.position;
    }

    [Button("Spatial Authoring - Align Spatial Placer")]
    public void AlignVFXAccordingToTheSpatialPlacer()
    {
        Vector3 targetScale = SpatialPlacer.transform.Find("AOIIllustrator").localScale;
        Vector3 targetPosition = SpatialPlacer.transform.Find("AOIIllustrator").position;
        Quaternion targetRotation = SpatialPlacer.transform.Find("AOIIllustrator").rotation;

        Vector3 origScale = VideoBounds.size;
        // compute the scaling factor - choose the smallest
        Vector3 scalingFactorVec3 = new Vector3(
            targetScale.x / origScale.x,
            targetScale.y / origScale.y,
            targetScale.z / origScale.z
        );
        float scalingFactor = Mathf.Min(
            Mathf.Min(scalingFactorVec3.x, scalingFactorVec3.y),
            scalingFactorVec3.z);

        //scalingFactor = 2 * scalingFactor;

        this.transform.parent.Find("VisualEffects").transform.localScale = new Vector3(scalingFactor, scalingFactor, scalingFactor);
        this.transform.parent.Find("VisualEffects").transform.position = targetPosition;
        this.transform.parent.Find("VisualEffects").transform.rotation = targetRotation;

        this.transform.parent.Find("VisualEffects").transform.localPosition += new Vector3(0, 0, targetScale.z / 2.0f); // align to the center
    }

    public void AlignVFXAccordingToTheSpatialPlacer(GameObject spatialPlacer)
    {
        SpatialPlacer = spatialPlacer;

        Vector3 targetScale = SpatialPlacer.transform.Find("AOIIllustrator").localScale;
        Vector3 targetPosition = SpatialPlacer.transform.Find("AOIIllustrator").position;
        Quaternion targetRotation = SpatialPlacer.transform.Find("AOIIllustrator").rotation;

        Vector3 origScale = VideoBounds.size;
        // compute the scaling factor - choose the smallest
        Vector3 scalingFactorVec3 = new Vector3(
            targetScale.x / origScale.x,
            targetScale.y / origScale.y,
            targetScale.z / origScale.z
        );
        float scalingFactor = Mathf.Min(
            Mathf.Min(scalingFactorVec3.x, scalingFactorVec3.y),
            scalingFactorVec3.z);

        //scalingFactor = 2 * scalingFactor;

        this.transform.parent.Find("VisualEffects").transform.localScale = new Vector3(scalingFactor, scalingFactor, scalingFactor);
        this.transform.parent.Find("VisualEffects").transform.position = targetPosition;
        this.transform.parent.Find("VisualEffects").transform.rotation = targetRotation;

        this.transform.parent.Find("VisualEffects").transform.localPosition += new Vector3(0, 0, targetScale.z / 2.0f); // align to the center
    }

    /// <summary>
    ///  New API
    /// </summary>
    [Button("Spatial Authoring - Direction Indicator Prefeb")]
    public void InstantiateDirectionIndicatorPrefeb()
    {
        GameObject indicator = Instantiate((GameObject)Resources.Load("SpatialStartDirectionIndicator", typeof(GameObject)), this.transform);
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        if(rhand) indicator.transform.position = rhand.transform.position; // not always avaliable
        indicator.GetComponent<VideoPlayerAdapter>().SetVideoPlayerController(this);
    }

    public void AlignAsStartPoint(GameObject indicator)
    {
        this.transform.parent.Find("VisualEffects").transform.position = indicator.transform.position;
        this.transform.parent.Find("VisualEffects").transform.rotation = indicator.transform.rotation;
        //this.transform.parent.Find("VisualEffects").transform.localScale = Vector3.one;
        //indicator.transform.parent = VFX.transform; // moves with the
    }

    public void AlignAsEndPoint(GameObject indicator)
    {
        Vector3 clocalp = Tube.GetComponent<VFXTrailRenderer>().pointList[Tube.GetComponent<VFXTrailRenderer>().pointList.Count - 1];
        Quaternion clocalr = Quaternion.identity;
        //Tube.GetComponent<VFXTrailRenderer>().rotationList[Tube.GetComponent<VFXTrailRenderer>().rotationList.Count - 1];
        ixUtility.AlignGameObjectWithChild(this.transform.parent.Find("VisualEffects").gameObject, clocalp, clocalr, indicator);
        this.transform.parent.Find("VisualEffects").transform.localScale = Vector3.one; // re-set the scale
        //indicator.transform.parent = VFX.transform; // moves with the
    }

    [Button("Spatial Authoring - Start Point")]
    public void InstantiateTrailDirectionIndicatorAtHand()
    {
        if (TrailStartIndicator == null) TrailStartIndicator =
                 Instantiate((GameObject)Resources.Load("SpatialStartDirectionIndicator", typeof(GameObject)), this.transform);
        else
            TrailStartIndicator = this.transform.Find("SpatialStartDirectionIndicator").gameObject;
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        TrailStartIndicator.transform.position = rhand.transform.position;
    }
    [Button("Spatial Authoring - Align Start")]
    public void AlignTrajectoryWithIndicator()
    {
        this.transform.parent.Find("VisualEffects").transform.position = TrailStartIndicator.transform.position;
        this.transform.parent.Find("VisualEffects").transform.rotation = TrailStartIndicator.transform.rotation;
        this.transform.parent.Find("VisualEffects").transform.localScale = Vector3.one;
        TrailStartIndicator.transform.parent = VFX.transform; // moves with the
    }

    [Button("Spatial Authoring - End Point")]
    public void InstantiateTrailEndIndicatorAtHand()
    {
        if (TrailEndIndicator == null)
            TrailEndIndicator = Instantiate((GameObject)Resources.Load("SpatialEndDirectionIndicator", typeof(GameObject)), this.transform);
        else
            TrailEndIndicator = this.transform.Find("SpatialEndDirectionIndicator").gameObject;
        //TrailEndIndicator.GetComponent<>
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        TrailEndIndicator.transform.position = rhand.transform.position;
    }
    [Button("Spatial Authoring - Align End")]
    public void AlignTrajectoryWithEndIndicator()
    {
        Vector3 clocalp = Tube.GetComponent<VFXTrailRenderer>().pointList[Tube.GetComponent<VFXTrailRenderer>().pointList.Count - 1];
        Quaternion clocalr = Quaternion.identity;
            //Tube.GetComponent<VFXTrailRenderer>().rotationList[Tube.GetComponent<VFXTrailRenderer>().rotationList.Count - 1];
        ixUtility.AlignGameObjectWithChild(this.transform.parent.Find("VisualEffects").gameObject, clocalp, clocalr, TrailEndIndicator);
        this.transform.parent.Find("VisualEffects").transform.localScale = Vector3.one; // re-set the scale
    }

    [Button("Spatial Authoring - Middle Point")]
    public void InstantiateTrailMiddleIndicatorAtHand()
    {
        TrailMiddleIndicator = Instantiate((GameObject)Resources.Load("SpatialPointIndicator", typeof(GameObject)), this.transform);
        //TrailEndIndicator.GetComponent<>
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        TrailMiddleIndicator.transform.position = rhand.transform.position;
    }

    [Button("Spatial Authoring - Align Middle")]
    public void AlignTrajectoryWithMiddlePointIndicator()
    {
        Vector3 clocalp = Tube.GetComponent<VFXTrailRenderer>().pointList[Tube.GetComponent<VFXTrailRenderer>().pointList.Count/2];
        Quaternion clocalr = Quaternion.identity;
        //Tube.GetComponent<VFXTrailRenderer>().rotationList[Tube.GetComponent<VFXTrailRenderer>().rotationList.Count - 1];
        ixUtility.AlignGameObjectWithChild(this.transform.parent.Find("VisualEffects").gameObject, clocalp, clocalr, TrailEndIndicator);
        this.transform.parent.Find("VisualEffects").transform.localScale = Vector3.one; // re-set the scale
    }

    //[Button("Spatial Authoring - Align Start + Middle")]
    //void AlignTrajectoryWithStartPointAndMiddlePoint()
    //{
    //    this.transform.parent.Find("VisualEffects").transform.position = TrailStartIndicator.transform.position;
    //    this.transform.parent.Find("VisualEffects").transform.rotation = Quaternion.FromToRotation(new Vector3(0,0,-1),
    //        (TrailMiddleIndicator.transform.position - TrailStartIndicator.transform.position).normalized);
    //    //TrailStartIndicator.transform.rotation;
    //    this.transform.parent.Find("VisualEffects").transform.localScale = Vector3.one; // re-set the scale
    //}

    /// <summary>
    /// New API
    /// </summary>
    [Button("Spatial Authoring - Create A Quad Prefeb")]
    public void InstantiateQuadPrefeb()
    {
        GameObject quad = Instantiate((GameObject)Resources.Load("SourceQuad", typeof(GameObject)), this.transform);
        quad.GetComponent<VideoPlayerAdapter>().SetVideoPlayerController(this);
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        if (rhand) quad.transform.position = rhand.transform.position;
    }
    public void SetMeAsSourceQuad(GameObject quad)
    {
        SourceQuad = quad;
        SourceQuad.transform.parent = this.transform.parent.Find("VisualEffects").transform; // follows the point cloud
    }

    public void SetMeAsTargetQuad(GameObject quad)
    {
        TargetQuad = quad;
        TargetQuad.transform.parent = this.transform;
    }


    ///
    [Button("Spatial Authoring - Create Source Quad")]
    public void CreateStartQuad()
    {
        if (!SourceQuad)
            SourceQuad = Instantiate((GameObject)Resources.Load("SourceQuad", typeof(GameObject)), this.transform.parent.Find("VisualEffects").transform);
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        SourceQuad.transform.position = rhand.transform.position;
    }

    [Button("Spatial Authoring - Create Target Quad")]
    public void CreateTargetQuad()
    {
        if (!TargetQuad)
            TargetQuad = Instantiate((GameObject)Resources.Load("TargetQuad", typeof(GameObject)), this.transform.parent.transform);
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        TargetQuad.transform.position = rhand.transform.position;
    }

    [Button("Spatial Authoring - Align Quads")]
    public void AlignQuads()
    {
        ixUtility.AlignGameObjectWithChild(this.transform.parent.Find("VisualEffects").gameObject, SourceQuad, TargetQuad);

        //TargetQuad.gameObject.SetActive(false);// disable target
    }

    GameObject TrailStartIndicator;
    GameObject TrailEndIndicator;
    GameObject TrailMiddleIndicator;
    GameObject SourceQuad = null;
    GameObject TargetQuad = null;

    ///////////////////////////////////////////////////////////////////////////////Spatial Authoring - QR ////
    [Button("Detect QR")]
    public void DetectQRFromBibcamAndAlign()
    {
        aprilTagDetector.SetTexture(videoFeeder.RefDemuxer().unDestortedColorTexture);
        bool hasQR = aprilTagDetector.DetectQR(videoFeeder.RefDecoder().Metadata.FieldOfView);
        if (hasQR)
        {
            Matrix4x4 inverseView = BibcamRenderUtils.InverseView(videoFeeder.RefDecoder().Metadata);
            Vector3 p = LocalToWorldPosition(inverseView, aprilTagDetector.QRPosition);
            Quaternion r = inverseView.rotation * aprilTagDetector.QRRotation;
            UpdateQRMask(p, r);
            UpdateCameraPose();
            QRWorldPosition = p;
            QRWorldRotation = r;

            AlignWithQRCodes();
        }
    }

    public void AlignWithQRCodes()
    {
        GameObject Origin = new GameObject("Origin");
        //Origin.transform.position = Vector3.zero;
        //Origin.transform.rotation = Quaternion.identity;

        ixUtility.AlignGameObjectWithChild(VFX.gameObject, QRMask, Origin);
    }

    public void UpdateQRMask(Vector3 p, Quaternion q)
    {
        QRMask.transform.position = p;
        QRMask.transform.rotation = q;
    }

    public void UpdateCameraPose()
    {
        RGBDCam.transform.position = videoFeeder.RefDecoder().Metadata.CameraPosition;
        RGBDCam.transform.rotation = videoFeeder.RefDecoder().Metadata.CameraRotation;
    }

    Vector3 QRWorldPosition;
    Quaternion QRWorldRotation;

    public void ContinuesDetectQR()
    {
        if (!ContinuesDetection) return;
        if (videoFeeder && videoFeeder.RefDemuxer().ColorTexture != null)
        {
            frameCount++;
            if (frameCount > frameInterval)
            {
                frameCount = 0;
                DetectQRFromBibcamAndAlign();
            }
        }
    }

    ///////////////////////////////////////////////////////////////////// Clip Authoring ////
    [Button("////////")] void d003() { }
    [Button("Clip - Enable Snapping")]
    public void EnableSnapping()
    {
        Tube.GetComponent<VFXTrailRenderer>().DoSnap = true;
        Tube.GetComponent<VFXTrailRenderer>().DoAnim = false;
    }

    [Button("Clip - Disable Snapping")]
    public void DisableSnapping()
    {
        Tube.GetComponent<VFXTrailRenderer>().DoSnap = false;
        Tube.GetComponent<VFXTrailRenderer>().DoAnim = true;
    }

    [Button("Clip - Seek To Body Position")]
    public void SeekToBodyPosition()
    {
        GameObject head = GameObject.FindGameObjectWithTag("MainCamera");
        Tube.GetComponent<VFXTrailRenderer>().SnapTimeScrubberToPosition(head.transform.position);
    }

    [Button("Clip - MarkAsStartPoint")]
    public void MarkAsStartPoint()
    {
        frameStart = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
        UpdateVFXTubeAndMeshTubeAccordingToStartAndEndFrame();

        //Tube.GetComponent<VFXTrailRenderer>().InstantiateOrMoveStartIndicator(frameStart); // update path rendering directly
        //float inper = (frameStart / (float)_H264Video.frameCount);
        //MeshTube.GetComponent<MeshRenderer>().material.SetFloat("_InPer", inper);
        //MeshTube.GetComponent<CustomTubeRenderer>().UpdateTimeTicksRenderingAccordingToFrameRange(frameStart, frameStop);
    }

    [Button("Clip - MarkAsEndPoint")]
    public void MarkAsEndPoint()
    {
        frameStop = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
        UpdateVFXTubeAndMeshTubeAccordingToStartAndEndFrame();
    }

    public void StoreTheAuthoredData()
    {
        WriteMetaDataToFile();
    }

    void UpdateVFXTubeAndMeshTubeAccordingToStartAndEndFrame()
    {
        // for video loop 
        ResetBoundaryControlling();

        // adjust vis 
        Tube.GetComponent<VFXTrailRenderer>().InstantiateOrMoveStartIndicator(frameStart); // update path rendering directly
        Tube.GetComponent<VFXTrailRenderer>().InstantiateOrMoveEndIndicator(frameStop);

        Tube.GetComponent<VFXTrailRenderer>().SwitchRendering(); // apply visual effects by updating to gpu

        //float inper = (frameStart / (float)_H264Video.frameCount);
        //MeshTube.GetComponent<MeshRenderer>().material.SetFloat("_InPer", inper);
        //MeshTube.GetComponent<CustomTubeRenderer>().UpdateTimeTicksRenderingAccordingToFrameRange(frameStart, frameStop);
    }

    public void ResumeTheAuthoredData()
    {
        ReadAndApplyMetaDataFromFile();
    }

    [Button("Clip - MarkAsBranchingPoint")]
    public void MarkAsBranchingPoint()
    {
        int frame = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ActiveBranchingPointPosition =
            Tube.GetComponent<VFXTrailRenderer>().GetPositionGivenFrame(frame);
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ActiveBranchingPointRotation =
            Tube.GetComponent<VFXTrailRenderer>().GetRotationGivenFrame(frame);
    }

    [Button("Clip - SnapToTheBranchingPoint")]
    public void SnapToTheBranchingPoint()
    {
        Vector3 pos = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ActiveBranchingPointPosition;
        //Quaternion quat = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ActiveBranchingPointRotation;

        // snap my start point
        Vector3 clocalp = Tube.GetComponent<VFXTrailRenderer>().pointList[frameStart];
        //Quaternion clocalr = Quaternion.identity;
        ixUtility.AlignGameObjectWithChild_PositionOnly(
            this.transform.parent.transform.Find("VisualEffects").gameObject, clocalp, Quaternion.identity, pos, Quaternion.identity); // position only
    }

    [Button("Clip - CreateARotatingPoint")]
    public void CreateARotatingPoint()
    {
        // set the rotation center to the current active point
        // and rotate with the current

        int frame = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
        Vector3 point = Tube.transform.TransformPoint(Tube.GetComponent<VFXTrailRenderer>().pointList[frame]);
        RotCtrlPoint = Instantiate(DragableRotPointPrefeb, this.transform);
        RotCtrlPoint.transform.position = point;

        DoRotWithCtrlPoint = true;
    }

    [Button("Clip - FixRotatingPoint")]
    public void FixRotatingPoint()
    {
        DoRotWithCtrlPoint = false;
    }



    /////////////////////////////////////////////////////////////////////////////// Run Mode ////

    [Button("Render Run Mode")]
    public void RenderRunMode()
    {
        Previewing_DisableAllCameraTrailView();
        Tube.GetComponent<VFXTrailRenderer>().RenderPathView = true;
        Tube.GetComponent<VFXTrailRenderer>().EncodePathWithSpeed = true;
        Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
    }

    /////////////////////////////////////////////////////////////////////////////// UI Control ////
    public void InitUI()
    {
        if (!ReflactToUI) return;

        if (ProgressBarContent)
        {
            UGUITextedSlider = Instantiate(
                (GameObject)Resources.Load("UGUITextedSlider", typeof(GameObject)),
                ProgressBarContent.transform);

            UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 0;
            UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = "00:00";
        }

        //VFXPreviewControlVaribleNames.Add("ExtractArch");
        //VFXPreviewControlVaribleNames.Add("ExtractHuman");
        //VFXPreviewControlVaribleNames.Add("ExtractDynamicHuman");
        //VFXPreviewControlVaribleNames.Add("ExtractSideBorder");
        //VFXPreviewControlVaribleNames.Add("ExtractThinBorder");

        if (UIContent)
        {
            ixUtility.MakeAButton(UIContent_Previewer, "Read ALL/ RecomputeVis", ReadALLWithoutTex);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ixUtility.MakeSepLine(UIContent_Previewer, "============Previewer===");
            ixUtility.MakeAButton(UIContent_Previewer, "VFXTube - Preview ALL", delegate {
                //
                Tube.GetComponent<VFXTrailRenderer>().RestartVFX();
                //VideoPath.GetComponent<VFXTrailRenderer>().RestartVFX();
                Tube.GetComponent<VFXTrailRenderer>().RefVFX().SetBool("RenderTextures", true);
                Tube.GetComponent<VFXTrailRenderer>().RefVFX().SetBool("RenderTicks", true);

                //
                Tube.SetActive(true);
                //VideoPath.SetActive(true);
            }); // SmallMultiple, TubeVis, PathVis, BorderVis,
            ixUtility.MakeAButton(UIContent_Previewer, "VFXTube - Hide ALL Preview", delegate {
                //
                Tube.GetComponent<VFXTrailRenderer>().StopVFX();
                //VideoPath.GetComponent<VFXTrailRenderer>().StopVFX();

                //
                Tube.SetActive(false);
                //VideoPath.SetActive(false);
            });
            ixUtility.MakeAButton(UIContent_Previewer, "VFXTube - Default (Most Effective)", delegate {
                Tube.GetComponent<VFXTrailRenderer>().RenderColorTrail = true;
                Tube.GetComponent<VFXTrailRenderer>().RenderWithLines = true;

                Tube.GetComponent<VFXTrailRenderer>().RenderTimeTick = false;
                Tube.GetComponent<VFXTrailRenderer>().RenderTexturedFrustumVis = false;
                Tube.GetComponent<VFXTrailRenderer>().RenderTexturedStripVis = false;
                Tube.GetComponent<VFXTrailRenderer>().RenderPointBasedTrail = false;
                Tube.GetComponent<VFXTrailRenderer>().RenderCameraOrientaion_LineBased = false;

                Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
            });

            //
            ixUtility.MakeAButton(UIContent_Previewer, "MeshTube - Preview ALL", delegate {
                MeshTube.SetActive(true);
            });
            ixUtility.MakeAButton(UIContent_Previewer, "MeshTube - Hide ALL", delegate {
                MeshTube.SetActive(false);
            });

            // Semantic, Camera Pose, Time, KeyFrame,
            ixUtility.MakeSepLine(UIContent_Previewer, "===TubeVis Toggles===");
            ixUtility.MakeAPositiveToggleButton(UIContent_Previewer, "Camera Trail View - RenderColorTrail", delegate {
                Tube.GetComponent<VFXTrailRenderer>().RenderColorTrail = !Tube.GetComponent<VFXTrailRenderer>().RenderColorTrail;
                //Tube.GetComponent<VFXTrailRenderer>().RenderTimeTick = true;
                //Tube.GetComponent<VFXTrailRenderer>().RenderTexturedFrustumVis = true;
                //Tube.GetComponent<VFXTrailRenderer>().RenderTexturedStripVis = true;
                //Tube.GetComponent<VFXTrailRenderer>().RenderPointBasedTrail = true;
                //Tube.GetComponent<VFXTrailRenderer>().RenderCameraOrientaion_LineBased = true;

                Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
            }); // Semantic
            ixUtility.MakeAPositiveToggleButton(UIContent_Previewer, "Camera Trail View - RenderPointBasedTrail", delegate {
                Tube.GetComponent<VFXTrailRenderer>().RenderPointBasedTrail =
                    !Tube.GetComponent<VFXTrailRenderer>().RenderPointBasedTrail;
                Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
            }); // Camera Pose
            ixUtility.MakeAPositiveToggleButton(UIContent_Previewer, "Camera Trail View - RenderCameraOrientaion_LineBased", delegate {
                Tube.GetComponent<VFXTrailRenderer>().RenderCameraOrientaion_LineBased =
                    !Tube.GetComponent<VFXTrailRenderer>().RenderCameraOrientaion_LineBased;
                Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
            }); // Camera Pose
            ixUtility.MakeAPositiveToggleButton(UIContent_Previewer, "Camera Trail View - RenderTimeTick", delegate {
                Tube.GetComponent<VFXTrailRenderer>().RenderTimeTick = !Tube.GetComponent<VFXTrailRenderer>().RenderTimeTick;
                Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
            }); // Time
            ixUtility.MakeAPositiveToggleButton(UIContent_Previewer, "Camera Trail View - RenderTexturedStripVis", delegate {
                Tube.GetComponent<VFXTrailRenderer>().RenderTexturedStripVis =
                    !Tube.GetComponent<VFXTrailRenderer>().RenderTexturedStripVis;
                Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
            }); // KeyFrame
            ixUtility.MakeAPositiveToggleButton(UIContent_Previewer, "Camera Trail View - RenderTexturedFrustumVis", delegate {
                Tube.GetComponent<VFXTrailRenderer>().RenderTexturedFrustumVis =
                    !Tube.GetComponent<VFXTrailRenderer>().RenderTexturedFrustumVis;
                Tube.GetComponent<VFXTrailRenderer>().SwitchRendering();
            }); // KeyFrame

            // Smaller scale
            ixUtility.MakeAButton(UIContent_Previewer, "Show SpatialPlacer", ShowSpatialPlacerAndPlaceItInFrontOfMe);
            ixUtility.MakeAButton(UIContent_Previewer, "Align To SpatialPlacer", AlignVFXAccordingToTheSpatialPlacer);
            foreach (var name in VFXPreviewControlVaribleNames)
            {
                GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent_Previewer.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false); // not toggle by default
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = name;
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
                    bool ToggleState = ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf;
                    bool NewState = !ToggleState;
                    ResetBools();
                    ResetBtnUIs();
                    RefVFX().SetBool(name, NewState);
                    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(NewState);

                    StopALLRemoveAllPoints(); // play at the same time!
                    PlayVideo();
                });
                radiouis.Add(ui);
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ixUtility.MakeSepLine(UIContent_SpatialAuthoring, "============Spatial Authoring===");
            // Single Constraint Based
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Starting Direction", InstantiateTrailDirectionIndicatorAtHand); // Single Constraint Based, Start
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Align VFX with Starting Direction", AlignTrajectoryWithIndicator);
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "End Direction", InstantiateTrailEndIndicatorAtHand); // Single Constraint Based, End
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Align VFX with End Direction", AlignTrajectoryWithEndIndicator);
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Middle Point", InstantiateTrailMiddleIndicatorAtHand); // Single Constraint Based, Middle
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Align VFX with Middle Point", AlignTrajectoryWithMiddlePointIndicator);

            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Source Quad", delegate{
                if(!SourceQuad)
                    SourceQuad = Instantiate((GameObject)Resources.Load("SourceQuad", typeof(GameObject)), this.transform.parent.Find("VisualEffects").transform);
                GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
                SourceQuad.transform.position = rhand.transform.position;
            });
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Target Quad", delegate {
                if (!TargetQuad)
                    TargetQuad = Instantiate((GameObject)Resources.Load("TargetQuad", typeof(GameObject)), this.transform.parent.transform);
                GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
                TargetQuad.transform.position = rhand.transform.position;
            });
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Align Them", delegate {
                ixUtility.AlignGameObjectWithChild(this.transform.parent.Find("VisualEffects").gameObject, SourceQuad, TargetQuad);
            });
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Record Transform At Current Frame", delegate {

                if (true)
                {
                    // store position rotation of the vfx in a list - as well as the current time scrubber information
                    if(Tube.GetComponent<VFXTrailRenderer>().TransformationListP.Count == 0)
                    {
                        foreach(var p in Tube.GetComponent<VFXTrailRenderer>().pointList)
                        {
                            Tube.GetComponent<VFXTrailRenderer>().TransformationListP.Add(Vector3.zero);
                            Tube.GetComponent<VFXTrailRenderer>().TransformationListR.Add(Quaternion.identity);
                        }
                        Tube.GetComponent<VFXTrailRenderer>().KeyFrames.Add(0);
                        Tube.GetComponent<VFXTrailRenderer>().KeyFrames.Add(Tube.GetComponent<VFXTrailRenderer>().pointList.Count - 1);
                    }

                    // change the transformation from now on
                    int frame = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
                    Tube.GetComponent<VFXTrailRenderer>().TransformationListP[frame] = this.transform.parent.Find("VisualEffects").position;
                    Tube.GetComponent<VFXTrailRenderer>().TransformationListR[frame] = this.transform.parent.Find("VisualEffects").rotation;

                    // mark as key frame
                    if (Tube.GetComponent<VFXTrailRenderer>().KeyFrames.Contains(frame))
                    {
                        return;
                    }
                    Tube.GetComponent<VFXTrailRenderer>().KeyFrames.Add(frame);
                    Tube.GetComponent<VFXTrailRenderer>().KeyFrames.Sort(); //

                    // update transformation list with lerp
                    for (int key = 0;key< Tube.GetComponent<VFXTrailRenderer>().KeyFrames.Count - 1; key++)
                    {
                        int curKeyFrame = Tube.GetComponent<VFXTrailRenderer>().KeyFrames[key];
                        int nextKeyFrame = Tube.GetComponent<VFXTrailRenderer>().KeyFrames[key + 1];
                        for (int i = curKeyFrame + 1; i< nextKeyFrame - 1; i++)
                        {
                            Vector3 startP = Tube.GetComponent<VFXTrailRenderer>().TransformationListP[curKeyFrame];
                            Vector3 endP = Tube.GetComponent<VFXTrailRenderer>().TransformationListP[nextKeyFrame];
                            Quaternion startQ = Tube.GetComponent<VFXTrailRenderer>().TransformationListR[curKeyFrame];
                            Quaternion endQ = Tube.GetComponent<VFXTrailRenderer>().TransformationListR[nextKeyFrame];
                            float per = ((float)(i - curKeyFrame)) / (nextKeyFrame - curKeyFrame);
                            Vector3 interpolatedPosition = startP + (endP - startP) * per;
                            Quaternion interpolatedRotation = Quaternion.Slerp(startQ, endQ, per);

                            Tube.GetComponent<VFXTrailRenderer>().TransformationListP[i] = interpolatedPosition;
                            Tube.GetComponent<VFXTrailRenderer>().TransformationListR[i] = interpolatedRotation;
                        }
                    }

                    Debug.Log("frame = " + frame + " vfx position = " + Tube.GetComponent<VFXTrailRenderer>().TransformationListP[frame]);
                }

                //TransformCurve.AddKey()

            });
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Toggle Time Based Transform", delegate {
                DoApplyTimelineTransform = !DoApplyTimelineTransform;
            });

            // Multi Constraint Based
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Middle Point", InstantiateTrailMiddleIndicatorAtHand);
            //ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Align VFX with Start and Middle", AlignTrajectoryWithStartPointAndMiddlePoint);

            // Physical Environment Based
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Show SpatialPlacer PhyEnv", ShowSpatialPlacerAndPlaceItInFrontOfMe);
            ixUtility.MakeAButton(UIContent_SpatialAuthoring, "Align To SpatialPlacer PhyEnv", AlignVFXAccordingToTheSpatialPlacer);

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ixUtility.MakeSepLine(UIContent_ClipAuthoring, "============Clip Authoring==="); // -> HandMenu
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Enable Clipping", EnableSnapping); // OnTubeVis + OnTubeTimelineVis
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Disable Clipping", DisableSnapping);
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Seek to Body Position", SeekToBodyPosition); // -> Hand Menu

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ixUtility.MakeSepLine(UIContent_ClipAuthoring, "============Timeline Controller===");
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Mark as Start", delegate { // cut timeline accordingly!
                frameStart = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
                Tube.GetComponent<VFXTrailRenderer>().InstantiateOrMoveStartIndicator(frameStart); // update path rendering directly
            });
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Mark as End", delegate {
                frameStop = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
                Tube.GetComponent<VFXTrailRenderer>().InstantiateOrMoveEndIndicator(frameStop); // update rendering directly
            });
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Mark as Branching Point", delegate {
                int frame = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
                GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ActiveBranchingPointPosition =
                    Tube.GetComponent<VFXTrailRenderer>().GetPositionGivenFrame(frame);
                GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ActiveBranchingPointRotation =
                    Tube.GetComponent<VFXTrailRenderer>().GetRotationGivenFrame(frame);
            });
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Snap to the Branching Point", delegate {
                Vector3 pos = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ActiveBranchingPointPosition;
                //Quaternion quat = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ActiveBranchingPointRotation;

                // snap my start point
                Vector3 clocalp = Tube.GetComponent<VFXTrailRenderer>().pointList[frameStart];
                //Quaternion clocalr = Quaternion.identity;
                ixUtility.AlignGameObjectWithChild_PositionOnly(
                    this.transform.parent.transform.Find("VisualEffects").gameObject, clocalp, Quaternion.identity, pos, Quaternion.identity); // position only

            });
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Create A RotCtrlPoint", delegate {
                // set the rotation center to the current active point
                // and rotate with the current

                int frame = Tube.GetComponent<VFXTrailRenderer>().GetCurrentFrameNumber();
                Vector3 point = Tube.transform.TransformPoint(Tube.GetComponent<VFXTrailRenderer>().pointList[frame]);
                RotCtrlPoint = Instantiate(DragableRotPointPrefeb, this.transform);
                RotCtrlPoint.transform.position = point;

                DoRotWithCtrlPoint = true;
            });
            ixUtility.MakeAButton(UIContent_ClipAuthoring, "Fix RotCtrlPoint", delegate {
                DoRotWithCtrlPoint = false;
            });

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ixUtility.MakeSepLine(UIContent, "============Player==="); // -> HandMenu
            ixUtility.MakeAButton(UIContent, "Play", PlayVideo);
            ixUtility.MakeAButton(UIContent, "ToggleCheckProximity", ToggleCheckProximity);
            //
            //ixUtility.MakeAButton(UIContent, "Pause/ Resume Video", delegate { });
            ixUtility.MakeAButton(UIContent, "Pause/ Resume Video & VFX", PauseContinueVideoPlayAndVFX);
            ixUtility.MakeAButton(UIContent, "Pause/ Resume VFX", PauseResumeVFX);
            ixUtility.MakeAButton(UIContent, "Stop ALL", StopALLRemoveAllPoints);

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ixUtility.MakeSepLine(UIContent_Advanced, "============[Advanced] Extracting===");
                //ixUtility.MakeAButton(UIContent_Advanced, "Load Active Video", LoadActiveVideo);
                ixUtility.MakeAButton(UIContent_Advanced, "Extract Trail", ExtractTrail);
                ixUtility.MakeAButton(UIContent_Advanced, "Write ALL", WriteALL);
                ixUtility.MakeAButton(UIContent_Advanced, "Read ALL ReadALLWithoutTex", ReadALLWithoutTex);

            ixUtility.MakeSepLine(UIContent_Advanced, "============[Advanced] Geometry Encoding===");
            ixUtility.MakeAButton(UIContent_Advanced, "Apply Preview with Geometry",delegate {
                        Tube.GetComponent<VFXTrailRenderer>().AddDummyGeometryIndicator();});

            ixUtility.MakeSepLine(UIContent_Advanced, "============[Advanced] QR Based Video Align===");
            ixUtility.MakeAButton(UIContent_Advanced, "Detect QR From Video", DetectQRFromBibcamAndAlign);
            ixUtility.MakeAButton(UIContent_Advanced, "Align With QR Codes", AlignWithQRCodes);



            //{
            //    GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
            //        "Detect QR From Bibcam";
            //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
            //        delegate { DetectQRFromBibcamAndAlign(); });
            //}
            //{
            //    GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
            //        "Align With QR Codes";
            //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
            //        delegate { AlignWithQRCodes(); });
            //}
            //{
            //    GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
            //        "Stop ALL";
            //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
            //        delegate { StopALLRemoveAllPoints(); });
            //}
            //{
            //    GameObject ui = Instantiate((GameObject)
            //        Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
            //        "Toggle Camera Following";
            //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
            //        FollowMode = !FollowMode;
            //    });
            //}
        }
    }

    public void UpdateUI()
    {
        if (UGUITextedSlider == null) return;

        if (videoFormat == ixUtility.VideoFormat.H264)
        {
            if (_H264Video && _H264Video.isPlaying) {
                UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value =
                    ((float)_H264Video.frame / _H264Video.frameCount);
                UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text =
                    _H264Video.frame.ToString() + "/" + _H264Video.frameCount.ToString();
            }
        }

        if (videoFormat == ixUtility.VideoFormat.HAP)
        {
            if (_HAPVideo && _HAPVideo.isValid) {
                UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value =
                    (float)(_HAPVideo.time / _HAPVideo.streamDuration);
                UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text =
                    _HAPVideo.time.ToString("F2") + "/" + _HAPVideo.streamDuration.ToString("F2");
            }
        }

    }


    ///////////////////////////////////////////////////////////////////// IO ////
    public void LoadVideo(string videoFolderPath, string videoUrl)
    {
        VideoDataFolderPath = videoFolderPath;
        VideoPath = videoUrl;

        ixUtility.LogMe("Playing video from: " + videoUrl);

        //
        if (!System.IO.File.Exists(videoUrl))
        {
            ixUtility.LogMe("Video File Not Exist! ");
            return;
        }

        //
        if (Path.GetExtension(videoUrl).Equals(".mp4") || Path.GetExtension(videoUrl).Equals(".MP4"))
        {
            ixUtility.LogMe("Setting Url and Preparing the H264 videoplayer");
            videoFormat = ixUtility.VideoFormat.H264;
            PrepareH264VideoPlayer(videoUrl);
        }
        else if (Path.GetExtension(videoUrl).Equals(".mov") || Path.GetExtension(videoUrl).Equals(".MOV"))
        {
            ixUtility.LogMe("Setting Url and Preparing the HAP videoplayer");
            videoFormat = ixUtility.VideoFormat.HAP;
            PrepareHapVideoPlayer(videoUrl);
        }
    }

    //public void LoadActiveVideo()
    //{
    //    string videoUrl = GameObject.FindGameObjectWithTag("StringManager")
    //        .GetComponent<StringManager>().curActiveFilePath;
    //    ixUtility.LogMe("Playing video from: " + videoUrl);

    //    //
    //    if (!System.IO.File.Exists(videoUrl))
    //    {
    //        ixUtility.LogMe("Video File Not Exist! ");
    //        return;
    //    }

    //    //
    //    if (Path.GetExtension(videoUrl).Equals(".mp4") || Path.GetExtension(videoUrl).Equals(".MP4"))
    //    {
    //        ixUtility.LogMe("Setting Url and Preparing the H264 videoplayer");
    //        videoFormat = ixUtility.VideoFormat.H264;
    //        PrepareH264VideoPlayer(videoUrl);
    //    }
    //    else if (Path.GetExtension(videoUrl).Equals(".mov") || Path.GetExtension(videoUrl).Equals(".MOV"))
    //    {
    //        ixUtility.LogMe("Setting Url and Preparing the HAP videoplayer");
    //        videoFormat = ixUtility.VideoFormat.HAP;
    //        PrepareHapVideoPlayer(videoUrl);
    //    }
    //}

    public void PrepareH264VideoPlayer(string url)
    {
        _H264Video = gameObject.AddComponent<VideoPlayer>();
        _H264Video.source = VideoSource.Url;
        _H264Video.playOnAwake = false;
        _H264Video.waitForFirstFrame = true;
        _H264Video.isLooping = true; // loop by default
        _H264Video.skipOnDrop = false;
        _H264Video.playbackSpeed = 1;
        _H264Video.renderMode = VideoRenderMode.MaterialOverride;
        //_H264Video.targetMaterialRenderer = targetRenderer;
        _H264Video.targetMaterialProperty = targetMaterialProperty;

        _H264Video.errorReceived += VideoPlayer_errorReceived;
        _H264Video.frameDropped += VideoFrameDrop;
        _H264Video.frameReady += VideoFrameReady;
        _H264Video.sendFrameReadyEvents = true;
        _H264Video.url = url;
        _H264Video.Prepare();

        //
        if (UGUITextedSlider)
        {
            UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 0;
            ixUtility.LogMe("_H264Video.frameCount.ToString() = " + _H264Video.frameCount.ToString());
            UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text =
                _H264Video.frameCount.ToString();
        }

        //
        if (_HAPVideo) Destroy(_HAPVideo);
    }

    public void PrepareHapVideoPlayer(string url)
    {
        if (_HAPVideo) Destroy(_HAPVideo);

        _HAPVideo = gameObject.AddComponent<HapPlayer>();
        //_HAPVideo.targetRenderer = targetRenderer; // disable automatic render texture assign, do it manually
        _HAPVideo.targetMaterialProperty = targetMaterialProperty;
        _HAPVideo._filePath = url;
        _HAPVideo._pathMode = HapPlayer.PathMode.LocalFileSystem;
        _HAPVideo.speed = 0;

        //
        if (UGUITextedSlider)
        {
            UGUITextedSlider.transform.Find("Slider").GetComponent<Slider>().value = 0;
            ixUtility.LogMe("_HAPVideo.frameCount.ToString() = " + _HAPVideo.frameCount.ToString());
            UGUITextedSlider.transform.Find("Per").GetComponent<TextMeshProUGUI>().text =
                _HAPVideo.frameCount.ToString();
        }

        //
        _H264Video.Stop();
    }


    public void ExtractTrail()
    {
        TubePoints.Clear();
        if (Tube) Destroy(Tube);
        TextureAtlas.Clear();
        TextureFrameIdx.Clear();
        //BoundingBoxSize = Vector3.one;
        VideoBounds.SetMinMax(new Vector3(-1, -1, -1), new Vector3(1, 1, 1));

        //
        StopVFX();
        RenderToVFX = false;
        _H264Video.isLooping = false;

        //
        //TextureIn3D = new RenderTexture();

        //
        ExtractingTrail = true;
        Debug.Log("Start extracting, Frame Count = " + (long)_H264Video.frameCount);
    }

    public void TryWriteTexture()
    {
        string folderPath = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curFolderPath;
        string fileName = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curFileName;

        // Create a Texture2D to read the pixels from the RenderTexture
        tex2d = new Texture2D(TextureAtlas[0].width, TextureAtlas[0].height, TextureFormat.RGBA32, false);

        // Read the pixels from the RenderTexture into the Texture2D
        RenderTexture.active = TextureAtlas[0];
        tex2d.ReadPixels(new Rect(0, 0, TextureAtlas[0].width, TextureAtlas[0].height), 0, 0);
        tex2d.Apply();

        string textureFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
        if (!System.IO.Directory.Exists(textureFolder))
            System.IO.Directory.CreateDirectory(textureFolder);
        string fn = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/Test" + ".jpg";
        byte[] imageBytes = tex2d.EncodeToJPG();
        File.WriteAllBytes(fn, imageBytes);
    }

    public void WritetexAtlasToFile(string folderPath, string fileName)
    {
        //string folderPath = GameObject.FindGameObjectWithTag("StringManager")
        //    .GetComponent<StringManager>().curFolderPath;
        //string fileName = GameObject.FindGameObjectWithTag("StringManager")
        //    .GetComponent<StringManager>().curFileName;

        for (int i = 0; i < TextureAtlas.Count; i++)
        {
            // Create a Texture2D to read the pixels from the RenderTexture
            Texture2D texture2D = new Texture2D(TextureAtlas[i].width, TextureAtlas[i].height, TextureFormat.RGBA32, false);

            // Read the pixels from the RenderTexture into the Texture2D
            RenderTexture.active = TextureAtlas[i];
            texture2D.ReadPixels(new Rect(0, 0, TextureAtlas[i].width, TextureAtlas[i].height), 0, 0);
            texture2D.Apply();

            string textureFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
            if (!System.IO.Directory.Exists(textureFolder))
                System.IO.Directory.CreateDirectory(textureFolder);
            string fn = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/" + TextureFrameIdx[i] + ".jpg";
            byte[] imageBytes = texture2D.EncodeToJPG(); // PNG might have dimension issue
            File.WriteAllBytes(fn, imageBytes);
        }
    }

    /// <summary>
    /// Load textures
    /// This have to be called after loading trajectory!
    /// </summary>
    public void TryReadtexAtlasFromFile()
    {
        string folderPath = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curFolderPath;
        string fileName = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curFileName;

        TextureAtlas.Clear();
        TextureFrameIdx.Clear();

        int texID = 0;
        string fn = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/" + texID + ".jpg";

        if (!Tube)
        {
            Debug.Log("Tube not created! ");
            return;
        }

        long numFrames = 0;
        if (RenderWithTubeRenderer)
            numFrames = Tube.GetComponent<CustomTubeRenderer>().pointList.Count;
        if (RenderWithVFXRenderer)
            numFrames = Tube.GetComponent<VFXTrailRenderer>().pointList.Count;

        if (numFrames == 0)
        {
            Debug.Log("Tube has no points! ");
            return;
        }

        for (int i = 0; i < numFrames; i++)
        {
            fn = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/" + i + ".jpg";
            if (System.IO.File.Exists(fn))
            {
                // Load the image data into a Texture2D
                byte[] imageData = File.ReadAllBytes(fn);
                Texture2D texture2D = new Texture2D(2, 2);
                texture2D.LoadImage(imageData);

                // Create a temporary RenderTexture with the same dimensions as the loaded image
                RenderTexture tempRT = new RenderTexture(texture2D.width, texture2D.height, 0, RenderTextureFormat.ARGB32);
                tempRT.enableRandomWrite = true;
                tempRT.Create();

                // Set the target RenderTexture as the active RenderTexture
                RenderTexture.active = tempRT;

                // Copy the Texture2D data into the RenderTexture
                Graphics.Blit(texture2D, tempRT);

                TextureAtlas.Add(tempRT);
                TextureFrameIdx.Add(i);

                // Clean up temporary objects
                Destroy(texture2D);
            }
        }

        CreateTexture2DArray();
    }

    public void CreateTexture2DArray()
    {
        texture2DArray = new Texture2DArray(TextureAtlas[0].width, TextureAtlas[0].height, TextureAtlas.Count,
            TextureFormat.RGBA32, false, false);

        for (int i = 0; i < TextureAtlas.Count; i++)
        {
            Graphics.CopyTexture(TextureAtlas[i], 0, 0, 0, 0, TextureAtlas[i].width, TextureAtlas[i].height, texture2DArray, i, 0, 0, 0);
        }

        //AssetDatabase.CreateAsset(texture2DArray, "Assets/textureArray.asset");


        //texture2DPatternArray = new Texture2DArray

        if (Tube)
        {
            VisualEffect trail = Tube.GetComponent<VisualEffect>();
            trail.SetTexture("TextureArray", texture2DArray);
        }

        //if (VideoPath)
        //{
        //    VisualEffect path = VideoPath.GetComponent<VisualEffect>();
        //    path.SetTexture("TextureArray", texture2DArray);
        //    path.Reinit();
        //}

        //VisualEffect tube = Tube.GetComponent<VisualEffect>();
        // if (trail.HasTexture("TextureArray"))
        // {
        //     Debug.Log("Does have Texture Array");
        //
        // }
        // else
        // {
        //     Debug.Log("VFX does not have texture array property");
        // }


    }

    public void PlaceImagesAlongTrajectory()
    {
        // load prefeb and set position
        for (int i = 0; i < TextureAtlas.Count; i++)
        {
            GameObject quad = Instantiate((GameObject)Resources.Load("Quad", typeof(GameObject)), this.transform);
            quad.transform.localScale = new Vector3(0.02f * 16, 0.02f * 9, 1);
            quad.GetComponent<MeshRenderer>().material.mainTexture = TextureAtlas[i];

            if (RenderWithTubeRenderer)
            {
                Quaternion q = Tube.GetComponent<CustomTubeRenderer>().rotationList[(int)TextureFrameIdx[i]];
                Vector3 newOffset = q * ImageOffset;
                quad.transform.position = Tube.GetComponent<CustomTubeRenderer>().pointList[(int)TextureFrameIdx[i]] + newOffset;
                quad.transform.rotation = q * quad.transform.rotation;
                quads.Add(quad);
            }

            if (RenderWithVFXRenderer)
            {
                Quaternion q = Tube.GetComponent<VFXTrailRenderer>().rotationList[(int)TextureFrameIdx[i]];
                Vector3 newOffset = q * ImageOffset;
                quad.transform.position = Tube.GetComponent<VFXTrailRenderer>().pointList[(int)TextureFrameIdx[i]] + newOffset;
                quad.transform.rotation = q * quad.transform.rotation;
                quads.Add(quad);
            }
        }
    }

    [SerializeField] int NumFrameDropped = 0;

    void VideoFrameDrop(VideoPlayer source)
    {
        NumFrameDropped++;
        Debug.Log("FrameDropped! Total = "  + NumFrameDropped);
        //_H264Video.frame--;
    }

    VideoGenerator videoGenerator = null;
    bool TransCode = false;

    private void VideoFrameReady(VideoPlayer source, long frameIdx)
    {
        if (ExtractingTrail)
            source.Pause();

        {
            if (_H264Video.frame < (long)_H264Video.frameCount - 1)
            {
                if (_H264Video.frame > LastProcessedFrame && _H264Video.isPrepared)
                {
                    _H264Video.Pause();
                    if (_H264Video.frame >= 0)
                    {
                        if (CacheFrames)
                        {
                            // decode
                            videoFeeder.RefDecoder().DecodeSync(_H264Video.texture);
                            videoFeeder.RefDemuxer().Demux(_H264Video.texture, videoFeeder.RefDecoder().Metadata);

                            // save textures
                            if(CacheInterval == 1)
                            {
                                RenderTexture tmpRT = new RenderTexture(
                                    videoFeeder.RefDemuxer().ColorTexture.width,
                                    videoFeeder.RefDemuxer().ColorTexture.height, 1)
                                { wrapMode = TextureWrapMode.Clamp };
                                Graphics.CopyTexture(videoFeeder.RefDemuxer().ColorTexture, tmpRT);
                                CachedTextures.Add(tmpRT);

                                Debug.Log("Extracting Frame = " + _H264Video.frame);
                            }
                            else
                            {
                                if (_H264Video.frame % CacheInterval == 0)
                                {
                                    RenderTexture tmpRT = new RenderTexture(
                                        videoFeeder.RefDemuxer().ColorTexture.width,
                                        videoFeeder.RefDemuxer().ColorTexture.height, 1)
                                    { wrapMode = TextureWrapMode.Clamp };
                                    Graphics.CopyTexture(videoFeeder.RefDemuxer().ColorTexture, tmpRT);
                                    CachedTextures.Add(tmpRT);

                                }
                            }
                        }

                        if (TransCode)
                        {
                            if (videoGenerator == null)
                            {
                                videoGenerator = this.GetComponent<VideoGenerator>();
                                videoGenerator.CreateSession(); // create new video name 
                            }
                            videoGenerator._session.PushFrame(_H264Video.texture);
                        }

                        if(TransCode || CacheFrames)
                            Debug.Log("Processing/Playing Frame: " + _H264Video.frame);
                    }
                    LastProcessedFrame = _H264Video.frame;

                    _H264Video.StepForward();
                    //Invoke("MoveToNextFrame", 1);
                }
                else
                {
                    // wait
                }
            }
            else
            {
                if(CacheFrames) CacheFrames = false;
                if (TransCode) TransCode = false;

                //
                if (TransCode)
                {
                    videoGenerator.StopRecording(); // this will save video to disk 
                }
                Debug.Log("Extraction Finished! Frame Dropped = " + NumFrameDropped);
            }
        }
    }

    void MoveToNextFrame()
    {
        _H264Video.StepForward();
    }


    public void WriteMetaDataToFile()
    {
        //string folderPath = GameObject.FindGameObjectWithTag("StringManager")
        //    .GetComponent<StringManager>().curFolderPath;
        //string fileName = GameObject.FindGameObjectWithTag("StringManager")
        //    .GetComponent<StringManager>().curFileName;

        //string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(filePath) + "_ExtractedData/";
        if (!System.IO.Directory.Exists(VideoDataFolderPath))
            System.IO.Directory.CreateDirectory(VideoDataFolderPath);
        string fileToWrite = VideoDataFolderPath + "/VideoMetaData.csv";

        StreamWriter writer = new StreamWriter(fileToWrite);

        writer.WriteLine("VideoBoundsMin," + VideoBounds.min.x + "," + VideoBounds.min.y + "," + VideoBounds.min.z);
        writer.WriteLine("VideoBoundsMax," + VideoBounds.max.x + "," + VideoBounds.max.y + "," + VideoBounds.max.z);
        writer.WriteLine("VideoStartFrame," + frameStart);
        writer.WriteLine("VideoStopFrame," + frameStop);
        // VisualEffectParent
        writer.WriteLine("VideoPosition," + 
            VisualEffectParent.transform.localPosition.x + "," + 
            VisualEffectParent.transform.localPosition.y + "," + 
            VisualEffectParent.transform.localPosition.z);
        writer.WriteLine("VideoRotation," +
            VisualEffectParent.transform.localRotation.x + "," + 
            VisualEffectParent.transform.localRotation.y + "," + 
            VisualEffectParent.transform.localRotation.z + "," + 
            VisualEffectParent.transform.localRotation.w);
        writer.WriteLine("VideoScale," +
            VisualEffectParent.transform.localScale.x + "," + 
            VisualEffectParent.transform.localScale.y + "," + 
            VisualEffectParent.transform.localScale.z);

        writer.Close();

        ixUtility.LogMe("file saved to: " + fileToWrite);
        ixUtility.ToastMe("file saved to: " + fileToWrite);
    }

    /////////////////////////////////////////////////////////////////////////////// Misc: IO, UI etc. ////
    void InitProcessor()
    {
        if (ProcessingMaterial == null) return;

        _processor = gameObject.AddComponent<VideoPreprocessing>();
        _processor.SetProcessingMaterial(ProcessingMaterial);
    }

    long lastFrameCount;

    bool DoDynamicHumanRenderingAfterTargetFrameHit = false;

    void TargetVideoFrameHit() // not working
    {
        if (DoDynamicHumanRenderingAfterTargetFrameHit)
        {
            DisableAllOtherExtractionVFXBooleans();
            RefVFX().SetBool("ExtractDynamicHuman", true);
            // continue to play!
        }
        else
        {
            VFX.pause = true;
            _H264Video.Pause();
        }
    }


    [Button]
    void SeekToCurrentFrame()
    {
        PauseVideoAndVFXPlay();
        _H264Video.frame = CurrentFrame;
        _H264Video.Prepare();
        ResumeVideoAndVFXPlay();
    }

    bool isBoundaryControling = false;

    void UpdateFrame()
    {
        if (videoFormat == ixUtility.VideoFormat.H264) {

            bool newFrameComes = false;
            if (_H264Video && _H264Video.isPlaying)
            {
                CurrentFrame = (int)_H264Video.frame;
                // 
                if (StopGeneratingPointsForEnvWhenTargetPointCountHit && FrameCountSinceStart > TotalFrameTarget) // check if target frame hit. works?
                {
                    //_H264Video.loopPointReached
                    //videoLoops++;
                    //if (targetVideoLoops > 0)
                    //{
                    //    if (videoLoops > targetVideoLoops)
                    //    {
                    //        TargetVideoFrameHit();
                    //    }
                    //}

                    //_H264Video.frame = frameStart;
                    //_H264Video.Prepare();

                    VFX.pause = true; // pause env rendering vfx
                    VFX.SetBool("DoViewDepCheck", false);
                    Debug.Log("Env Rendering VFX Paused"); // but not work for fuzzy env
                }

                // Boundary Check 
                if (_H264Video.frame >= frameStop - 1)
                {
                    _H264Video.Stop();
                    _H264Video.frame = frameStart + 1; // loop 
                    _H264Video.Prepare();
                    _H264Video.Play();
                    //isBoundaryControling = true; // here we do not need boundary controlling since the jump bet. should be large enough -> no constant seeking 
                }
                if (_H264Video.frame <= frameStart && !isBoundaryControling)
                {
                    _H264Video.frame = frameStart + 1; // snap
                    _H264Video.Prepare();
                    _H264Video.Play();
                    isBoundaryControling = true;
                }

                newFrameComes = _H264Video.frame != lastFrameCount;
                lastFrameCount = _H264Video.frame;
                if (DoPreprocessing)
                    outTexture = _processor.GetProcessedTexture(_H264Video.texture);
                else
                    outTexture = _H264Video.texture;
            }

            if (newFrameComes) //works?
            {
                FrameCountSinceStart++;
                if (RenderToRenderer && targetRenderer)
                    targetRenderer.material.mainTexture = outTexture;
                if (RenderToVFX && videoFeeder)
                    videoFeeder.DecodeFrame(outTexture);
            }
        }

        if (videoFormat == ixUtility.VideoFormat.HAP) {
            if (_HAPVideo && _HAPVideo.isValid) {
                //if (DoFlipWithHap)
                //{
                //    Texture currentTexture = _processor.GetProcessedTexture(_HAPVideo.texture);
                //    outTexture = currentTexture;
                //}
                //else
                {
                    outTexture = _HAPVideo.texture;
                }
            }

            if (RenderToRenderer && targetRenderer)
                targetRenderer.material.mainTexture = outTexture;
            if (RenderToVFX && videoFeeder)
                videoFeeder.DecodeFrame(outTexture);
        }
    }

    void UpdateMRTKPlayspaceFollowMode()
    {
        if (FollowMode && mixedRealityPlayspace && videoFeeder && (videoFeeder.RefDecoder() != null))
        {
            //mixedRealityPlayspace.transform.position =
            //    this.transform.TransformPoint(videoFeeder.RefDecoder().Metadata.CameraPosition);
        }
    }


    void ReadAndApplyMetaDataFromFile()
    {
        //string folderPath = GameObject.FindGameObjectWithTag("StringManager")
        //    .GetComponent<StringManager>().curFolderPath;
        //string fileName = GameObject.FindGameObjectWithTag("StringManager")
        //    .GetComponent<StringManager>().curFileName;

        //string extractedDataFolder = folderPath + "/" + Path.GetFileNameWithoutExtension(fileName) + "_ExtractedData/";
        //if (!System.IO.Directory.Exists(extractedDataFolder))
        //    System.IO.Directory.CreateDirectory(extractedDataFolder);
        string fileToRead = VideoDataFolderPath + "/VideoMetaData.csv";

        Vector3 minPoint = -Vector3.one;
        Vector3 maxPoint = Vector3.one;

        StreamReader reader = new StreamReader(fileToRead);
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var values = line.Split(',');
            if (values[0].ToString() == "VideoBoundsMin")
            {
                minPoint = new Vector3(
                    float.Parse(values[1].ToString()),
                    float.Parse(values[2].ToString()),
                    float.Parse(values[3].ToString()));
            }
            if (values[0].ToString() == "VideoBoundsMax")
            {
                maxPoint = new Vector3(
                    float.Parse(values[1].ToString()),
                    float.Parse(values[2].ToString()),
                    float.Parse(values[3].ToString()));
            }

            // restore the result from temporal authoring 
            if (values[0].ToString() == "VideoStartFrame")
            {
                frameStart = int.Parse(values[1].ToString());
            }
            if (values[0].ToString() == "VideoStopFrame")
            {
                frameStop = int.Parse(values[1].ToString());
            }

            // restore the authored transform - spatial authoring  
            if (values[0].ToString() == "VideoPosition")
            {
                Vector3 thevalue = new Vector3(
                    float.Parse(values[1].ToString()),
                    float.Parse(values[2].ToString()),
                    float.Parse(values[3].ToString()));
                VisualEffectParent.transform.localPosition = thevalue;
            }
            if (values[0].ToString() == "VideoRotation")
            {
                Quaternion thevalue = new Quaternion(
                    float.Parse(values[1].ToString()),
                    float.Parse(values[2].ToString()),
                    float.Parse(values[3].ToString()),
                    float.Parse(values[4].ToString()));
                VisualEffectParent.transform.localRotation = thevalue;
            }
            if (values[0].ToString() == "VideoScale")
            {
                Vector3 thevalue = new Vector3(
                    float.Parse(values[1].ToString()),
                    float.Parse(values[2].ToString()),
                    float.Parse(values[3].ToString()));
                VisualEffectParent.transform.localScale = thevalue;
            }
        }

        VideoBounds.SetMinMax(minPoint, maxPoint);

        UpdateVFXTubeAndMeshTubeAccordingToStartAndEndFrame();
    }

    void WriteALL()
    {
        Tube.GetComponent<VFXTrailRenderer>().WriteToFile(VideoDataFolderPath, VideoPath);
        //Tube.GetComponent<VFXTrailRenderer>().WritePathToFile();
        WritetexAtlasToFile(VideoDataFolderPath, VideoPath);
        WriteMetaDataToFile();
    }

    public void ReadALLWithoutTex()
    {
        VFX.gameObject.SetActive(true);

        // load trail from file
        if (Tube == null) Tube = Instantiate(VFXTrailRendererPrefeb, VFX.transform);
        Tube.GetComponent<VFXTrailRenderer>().TryReadHumanCountFile(); // try read semantic first
        Tube.GetComponent<VFXTrailRenderer>().TryReadFromFile();

        // render 3d mesh based tube
        if (MeshTube == null) MeshTube = Instantiate(TubePrefeb, VFX.transform);
        MeshTube.GetComponent<CustomTubeRenderer>().SetDownScaleFactor(1); //
        MeshTube.GetComponent<CustomTubeRenderer>().CreateTrailBaseOnPointAndRotationList(
            Tube.GetComponent<VFXTrailRenderer>().pointList,
            Tube.GetComponent<VFXTrailRenderer>().rotationList, Color.green);
        MeshTube.SetActive(false); // set as inactive by default 

        // request video control
        Tube.GetComponent<VFXTrailRenderer>().SetVideoReference(_H264Video);
        Tube.GetComponent<VFXTrailRenderer>().SetVideoFeeder(videoFeeder);

        //
        //if (VideoPath == null) VideoPath = Instantiate(VFXPathRendererPrefeb, VFX.transform);
        //VideoPath.GetComponent<VFXTrailRenderer>().TryReadHumanCountFile(); // semantic data only
        //VideoPath.GetComponent<VFXTrailRenderer>().TryReadPathFromFile(); // with GPU Submission

        //
        //if(ReadTex)
        //    TryReadtexAtlasFromFile();
        //ReadAndApplyMetaDataFromFile();
        Invoke("ReadAndApplyMetaDataFromFile", 2.0f);

        //
        Tube.GetComponent<VFXTrailRenderer>().StopVFX();
        //VideoPath.GetComponent<VFXTrailRenderer>().StopVFX();
    }


    public void ReadALLWithTex()
    {
        VFX.gameObject.SetActive(true);

        // load trail from file
        if (Tube == null) Tube = Instantiate(VFXTrailRendererPrefeb, VFX.transform);
        Tube.GetComponent<VFXTrailRenderer>().TryReadHumanCountFile(); // try read semantic first
        Tube.GetComponent<VFXTrailRenderer>().TryReadFromFile();

        // render 3d mesh based tube
        if (MeshTube == null) MeshTube = Instantiate(TubePrefeb, VFX.transform);
        MeshTube.GetComponent<CustomTubeRenderer>().SetDownScaleFactor(1); //
        MeshTube.GetComponent<CustomTubeRenderer>().CreateTrailBaseOnPointAndRotationList(
            Tube.GetComponent<VFXTrailRenderer>().pointList,
            Tube.GetComponent<VFXTrailRenderer>().rotationList, Color.green);

        // request video control
        Tube.GetComponent<VFXTrailRenderer>().SetVideoReference(_H264Video);
        Tube.GetComponent<VFXTrailRenderer>().SetVideoFeeder(videoFeeder);

        //
        //if (VideoPath == null) VideoPath = Instantiate(VFXPathRendererPrefeb, VFX.transform);
        //VideoPath.GetComponent<VFXTrailRenderer>().TryReadHumanCountFile(); // semantic data only
        //VideoPath.GetComponent<VFXTrailRenderer>().TryReadPathFromFile(); // with GPU Submission

        //
        TryReadtexAtlasFromFile();
        //ReadAndApplyMetaDataFromFile();
        Invoke("ReadAndApplyMetaDataFromFile", 2.0f);

        //
        Tube.GetComponent<VFXTrailRenderer>().StopVFX();
        //VideoPath.GetComponent<VFXTrailRenderer>().StopVFX();
    }

    void PreviewAll()
    {
        Tube.GetComponent<VFXTrailRenderer>().RestartVFX();
        //VideoPath.GetComponent<VFXTrailRenderer>().RestartVFX();
    }

    List<string> VFXPreviewControlVaribleNames = new List<string>();

    void ResetBools()
    {
        foreach (var name in VFXPreviewControlVaribleNames)
        {
            RefVFX().SetBool(name, false);
        }
    }

    List<GameObject> radiouis = new List<GameObject>();
    void ResetBtnUIs()
    {
        foreach (var ui in radiouis)
        {
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        }
    }


    AnimationCurve TransformCurve = new AnimationCurve();

    public void UpdaterTimeScrubberRendering(bool b)
    {
        Tube.GetComponent<VFXTrailRenderer>().TimeScrubber.gameObject.SetActive(b);
    }

    [Button]
    void SetExampleFrameStart()
    {
        ResetBoundaryControlling();

        frameStart = (int)((float)_H264Video.frameCount / 4.0f);

        UpdateVFXTubeAndMeshTubeAccordingToStartAndEndFrame();
    }

    [Button]
    void SetExampleFrameStop()
    {
        ResetBoundaryControlling();

        frameStop = (int)((float)_H264Video.frameCount / 2.0f);

        UpdateVFXTubeAndMeshTubeAccordingToStartAndEndFrame();
    }

    [Button]
    void ResetBoundaryControlling() // avoid constantly resetting/ seeking video! 
    {
        isBoundaryControling = false;
    }

    [Button]
    void EditorStoreAuthoredData()
    {
        StoreTheAuthoredData();
    }

    [Button]
    void EditorResumeTheAuthoredData()
    {
        ResumeTheAuthoredData();
    }

    [Button]
    void EnableMeshTube()
    {
        MeshTube.SetActive(true);
        MeshTube.GetComponent<CustomTubeRenderer>().RebuildTrail(); // mesh creation may not complete at the beginning 
    }
}
