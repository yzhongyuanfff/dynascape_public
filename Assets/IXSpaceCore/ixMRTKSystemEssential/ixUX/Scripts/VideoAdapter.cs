using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoAdapter : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public Slider slider;
    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (slider != null)
        {
            slider.value = (float)videoPlayer.frame / videoPlayer.frameCount;
            text.text = "" + videoPlayer.frame + " / " + videoPlayer.frameCount;
        }
    }

    [PunRPC]
    void updateVideoEnabledSync(bool b)
    {
        this.gameObject.SetActive(b);
    }
    public void updateVideoEnabled(bool b)
    {
        this.GetComponent<PhotonView>().RPC("updateVideoEnabledSync", RpcTarget.All, b);
    }


    [PunRPC]
    void updatePlayBackSpeedSync(float speed)
    {
        videoPlayer.playbackSpeed = speed;
    }
    public void updatePlayBackSpeed(float speed) {
        this.GetComponent<PhotonView>().RPC("updatePlayBackSpeedSync", RpcTarget.All, speed);
    }



    [PunRPC]
    void checkPlayBackSpeedSync(bool b)
    {
        if(!b)
            videoPlayer.playbackSpeed = 0;
        else
            videoPlayer.playbackSpeed = 1;
    }
    public void checkPlayBackSpeed(bool b)
    {
        this.GetComponent<PhotonView>().RPC("checkPlayBackSpeedSync", RpcTarget.All, b);
    }
}
