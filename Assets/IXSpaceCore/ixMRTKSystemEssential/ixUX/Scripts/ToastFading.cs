using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToastFading : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartCountDown(float t)
    {
        Invoke("Disappear", t);
    }

    void Disappear()
    {
        this.gameObject.SetActive(false);

        Destroy(this.gameObject);
    }
}
