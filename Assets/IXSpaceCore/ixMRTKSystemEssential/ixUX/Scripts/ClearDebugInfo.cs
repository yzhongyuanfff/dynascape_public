using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ClearDebugInfo : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void clearDebugInfo()
    {
        GameObject.FindGameObjectWithTag("DebugText").GetComponent<TextMeshProUGUI>().text = "<DebugInfo>\n";
    }
}
