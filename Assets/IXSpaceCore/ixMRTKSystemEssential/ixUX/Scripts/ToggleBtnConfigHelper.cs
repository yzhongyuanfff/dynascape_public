using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToggleBtnConfigHelper : MonoBehaviour
{
    public TextMeshProUGUI title;
    public TextMeshProUGUI btnName;
    public GameObject backPlate; 
    public Button btn;
}
