using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpdateAccordingToSlider : MonoBehaviour
{
    public TextMeshProUGUI text;
    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        updateTextAccordingToSlider();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void updateTextAccordingToSlider()
    {
        text.text = slider.value.ToString("F2");
    }
}
