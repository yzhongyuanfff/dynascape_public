using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoLinkSetter : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLinkAsTheActiveFile()
    {
        this.GetComponent<VideoPlayer>().url = 
            GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;

        ixUtility.LogMe("this.GetComponent<VideoPlayer>().url = " + this.GetComponent<VideoPlayer>().url);
    }
}
