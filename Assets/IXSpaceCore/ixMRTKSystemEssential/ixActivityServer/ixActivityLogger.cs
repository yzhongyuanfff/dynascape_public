using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class ixActivityLogger : MonoBehaviour
{
    [Header("------")]
    [SerializeField] bool DoContinuesPostActivity = false;
    [SerializeField] int PostIntervalInSeconds = 60;

    //string PostUrl = "http://zhirongtec.top:5000/acti";
    string PostUrlV2 = "http://zhirongtec.top:5000/actiV2";
    int postIntervalInFrames;
    int currFrame = 0;

    /// <summary>
    /// 
    /// </summary>
    void PostActivity()
    {
        //string m = "Online Device Found: At " + System.DateTime.Now.ToString()
        //    + ", Device ID: " + SystemInfo.deviceUniqueIdentifier
        //    + ", Local IP: " + LocalIPAddress()
        //    + ", Device Model: " + SystemInfo.deviceModel
        //    + ", OS: " + SystemInfo.operatingSystem
        //    + "\n";

        string m = SystemInfo.deviceUniqueIdentifier 
            + "," + SystemInfo.deviceModel
            + "," + SystemInfo.operatingSystem
            + "," + System.DateTime.Now.ToString()
            + "," + LocalIPAddress()
            + "\n";

        //ixLogger.LogMe("[INFO] Activity Log Posted: " + m);

        (new WebClient()).UploadString(PostUrlV2, m);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    string LocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "0.0.0.0";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }

    // Start is called before the first frame update
    void Start()
    {
        postIntervalInFrames = 72 * PostIntervalInSeconds;
        PostActivity();
    }

    // Update is called once per frame
    void Update()
    {
        if (DoContinuesPostActivity)
        {
            currFrame++;
            if (currFrame > postIntervalInFrames)
            {
                currFrame = 0;
                PostActivity();
            }
        }
    }
}
