using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class ServerConnectionManager : MonoBehaviour
{
    public string ServerConnectionTestUrl;
    public int serverCheckFrameInterval = 72 * 30;

    int curFrame = 0;
    string serverInfo;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TryGetDataFromServer(ServerConnectionTestUrl));
    }

    // Update is called once per frame
    void Update()
    {
        curFrame++;
        if (curFrame > serverCheckFrameInterval)
        {
            curFrame = 0;
            StartCoroutine(TryGetDataFromServer(ServerConnectionTestUrl));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void CheckServerInfo()
    {
        StartCoroutine(TryGetDataFromServer(ServerConnectionTestUrl));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    IEnumerator TryGetDataFromServer(string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                serverInfo = "Network Error, Check Internet and VPN connection!";
            }
            else
            {
                if (www.isDone)
                {
                    // handle the result
                    var result = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
             
                     
                    JSONNode n = JSON.Parse(result);
                    if(n.Count > 0)
                        serverInfo = "Connected! Enjoy!";
                    else
                        serverInfo = "Data Error! Get Empty Data From Server.";
                }
                else
                {
                    serverInfo = "Failed to connect to server. Timeout.";
                }

            }
            
            GetComponent<TextMeshProUGUI>().text = serverInfo;
        }

    }
}
