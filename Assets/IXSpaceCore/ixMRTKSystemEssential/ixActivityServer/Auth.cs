using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using TMPro;
using UnityEngine;

public class Auth : MonoBehaviour
{
    public string AuthUrl;
    public int AuthDelay = 2;
    public bool fbDoAuth = false;
    public bool checkAuth = true;
    public List<GameObject> ObjectsToDisableWhenAuthFailed;

    // Start is called before the first frame update
    void Start()
    {
        if(checkAuth)
            Invoke("Authorization", AuthDelay);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public string GetLocalIPv4()
    {
        return Dns.GetHostEntry(Dns.GetHostName())
            .AddressList.First(
                f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            .ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    void GetDeviceInfo()
    {
        LogMe("Device ID: " + SystemInfo.deviceUniqueIdentifier);
        LogMe("LocalIPv4: " + GetLocalIPv4());
        LogMe("SystemInfo.deviceName: " + SystemInfo.deviceName);
        LogMe("SystemInfo.batteryLevel: " + SystemInfo.batteryLevel);
        LogMe("SystemInfo.batteryStatus: " + SystemInfo.batteryStatus);
        LogMe("SystemInfo.deviceModel: " + SystemInfo.deviceModel);
        LogMe("SystemInfo.deviceType: " + SystemInfo.deviceType);
        LogMe("SystemInfo.graphicsDeviceName: " + SystemInfo.graphicsDeviceName);
        LogMe("SystemInfo.graphicsDeviceVendor: " + SystemInfo.graphicsDeviceVendor);
        LogMe("SystemInfo.graphicsMemorySize: " + SystemInfo.graphicsMemorySize);
        LogMe("SystemInfo.hasDynamicUniformArrayIndexingInFragmentShaders: " + SystemInfo.hasDynamicUniformArrayIndexingInFragmentShaders);
        LogMe("SystemInfo.maxComputeBufferInputsCompute: " + SystemInfo.maxComputeBufferInputsCompute);
        LogMe("SystemInfo.maxComputeWorkGroupSize: " + SystemInfo.maxComputeWorkGroupSize);
        LogMe("SystemInfo.maxTextureSize: " + SystemInfo.maxTextureSize);
        LogMe("SystemInfo.operatingSystem: " + SystemInfo.operatingSystem);
        LogMe("SystemInfo.processorCount: " + SystemInfo.processorCount);
        LogMe("SystemInfo.processorFrequency: " + SystemInfo.processorFrequency);
        LogMe("SystemInfo.processorType: " + SystemInfo.processorType);
        LogMe("SystemInfo.supports3DRenderTextures: " + SystemInfo.supports3DRenderTextures);
        LogMe("SystemInfo.supportsAccelerometer: " + SystemInfo.supportsAccelerometer);
        LogMe("SystemInfo.supportsAsyncGPUReadback: " + SystemInfo.supportsAsyncGPUReadback);
        LogMe("SystemInfo.supportsInstancing: " + SystemInfo.supportsInstancing);
        LogMe("SystemInfo.supportsGyroscope: " + SystemInfo.supportsGyroscope);
        LogMe("SystemInfo.systemMemorySize: " + SystemInfo.systemMemorySize);
        LogMe("SystemInfo.supportsRenderTextures: " + SystemInfo.supportsRenderTextures);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator DoAuth()
    {
        string tobeposted = SystemInfo.deviceUniqueIdentifier;
        string authInfo = (new WebClient()).UploadString(AuthUrl, tobeposted);
        Debug.Log("authInfo: " + authInfo);
        if(authInfo == "n")
        {
            foreach(var o in ObjectsToDisableWhenAuthFailed)
            {
                o.gameObject.SetActive(false);
            }
            LogMe("Authorization Failed! Please Contact The Author To Register.");
        }
        else if (authInfo == "y")
        {
            LogMe("Authorization Successful! Enjoy!");
        }
        yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        if (fbDoAuth)
        {
            fbDoAuth = false;
            GetDeviceInfo();
        }

        //if (curFrame > AuthDelay)
        //{
        //    curFrame = 0;
        //    Authorization();
        //    // StartCoroutine(TryGetDataFromServer(ServerConnectionTestUrl));
        //}
        //curFrame++;
    }

    /// <summary>
    /// 
    /// </summary>
    void Authorization()
    {
        GetDeviceInfo();
        StartCoroutine(DoAuth());
    }
}
