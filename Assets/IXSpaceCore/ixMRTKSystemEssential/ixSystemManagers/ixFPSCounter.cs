using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ixFPSCounter : MonoBehaviour
{
    TextMeshProUGUI text;
    float deltaTime = 0.0f;

    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;

        text.text = Math.Ceiling(1.0f / deltaTime) + " FPS";
    }

    void OnGUI()
    {
        text.text = Math.Ceiling(1.0f / deltaTime) + " FPS";
    }
}
