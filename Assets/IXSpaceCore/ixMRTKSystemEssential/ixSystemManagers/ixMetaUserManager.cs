// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Oculus.Platform;
using Oculus.Platform.Models;
using TMPro;

public class ixMetaUserManager : MonoBehaviour
{
    public TextMeshProUGUI DebugInfoPanel;
    [HideInInspector] public ulong OculusUserID;
    [HideInInspector] public User user;

    // Start is called before the first frame update
    void Start()
    {
        //Oculus.Platform.Core.Initialize();
        Core.AsyncInitialize().OnComplete(OnInitializationCallback);
        // Oculus.Platform.Users.GetLoggedInUser().OnComplete(GetLoggedInUserCallback);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    private void OnInitializationCallback(Message<PlatformInitialize> msg)
    {
        if (msg.IsError)
        {
            Debug.LogErrorFormat("Oculus: Error during initialization. Error Message: {0}",
                msg.GetError().Message);
        }
        else
        {
            Entitlements.IsUserEntitledToApplication().OnComplete(OnIsEntitledCallback);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    private void OnIsEntitledCallback(Message msg)
    {
        if (msg.IsError)
        {
            Debug.LogErrorFormat("Oculus: Error verifying the user is entitled to the application. Error Message: {0}",
                msg.GetError().Message);
        }
        else
        {
            GetLoggedInUser();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void GetLoggedInUser()
    {
        Users.GetLoggedInUser().OnComplete(OnLoggedInUserCallback);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    private void OnLoggedInUserCallback(Message<User> msg)
    {
        if (msg.IsError)
        {
            Debug.LogErrorFormat("Oculus: Error getting logged in user. Error Message: {0}",
                msg.GetError().Message);
        }
        else
        {
            user = msg.GetUser();
            string userName = user.OculusID;
            string displayName = user.DisplayName;
            ulong id = user.ID;

            OculusUserID = user.ID;
            Debug.Log("user.OculusID = " + msg.Data.ID.ToString());
            Debug.Log("user.DisplayName = " + user.DisplayName);
            Debug.Log("user.ID = " + user.ID);
            Debug.Log("user.ImageURL = " + user.ImageURL);
            Debug.Log("OVRPlugin.version = " + OVRPlugin.version.ToString());

            string newMsg =
                "user.ID = " + user.ID + "\n"
                + "user.OculusID = " + user.OculusID + "\n"
                + "user.DisplayName = " + user.DisplayName + "\n"
                + "user.ImageURL = " + user.ImageURL + "\n";

            //TextMeshProUGUI DebugInfoPanel = GameObject.FindGameObjectWithTag("DebugText").GetComponent<TextMeshProUGUI>();
            if (DebugInfoPanel)
            {
                DebugInfoPanel.text = newMsg + DebugInfoPanel.text;
                DebugInfoPanel.text = "OVRPlugin.version = " + OVRPlugin.version.ToString() + "\n" + DebugInfoPanel.text;
            }

            //userId = msg.Data.ID.ToString(); // do not use msg.Data.OculusID;
            GetUserProof();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void GetUserProof()
    {
        Users.GetUserProof().OnComplete(OnUserProofCallback);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    private void OnUserProofCallback(Message<UserProof> msg)
    {
        if (msg.IsError)
        {
            Debug.LogErrorFormat("Oculus: Error getting user proof. Error Message: {0}",
                msg.GetError().Message);
        }
        else
        {
            string oculusNonce = msg.Data.Value;
            // Authentication can be performed here
        }
    }

    //private void GetLoggedInUserCallback(Message msg)
    //{
    //    if (!msg.IsError)
    //    {
    //        User user = msg.GetUser();
    //        string userName = user.OculusID;
    //        string displayName = user.DisplayName;
    //        ulong id = user.ID;

    //        Debug.Log("user.OculusID = " + user.OculusID);
    //        Debug.Log("user.DisplayName = " + user.DisplayName);
    //        Debug.Log("user.ID = " + user.ID);
    //        Debug.Log("user.ImageURL = " + user.ImageURL);
    //        Debug.Log("OVRPlugin.version = " + OVRPlugin.version.ToString());

    //        string newMsg = 
    //            "user.ID = " + user.ID + "\n"
    //            + "user.OculusID = " + user.OculusID + "\n"
    //            + "user.DisplayName = " + user.DisplayName + "\n"
    //            + "user.ImageURL = " + user.ImageURL + "\n";

    //        //TextMeshProUGUI DebugInfoPanel = GameObject.FindGameObjectWithTag("DebugText").GetComponent<TextMeshProUGUI>();
    //        if (DebugInfoPanel)
    //        {
    //            DebugInfoPanel.text = newMsg + DebugInfoPanel.text;
    //            DebugInfoPanel.text = "OVRPlugin.version = " + OVRPlugin.version.ToString() + "\n" + DebugInfoPanel.text;
    //        }

    //    }
    //    else
    //    {
    //        Debug.Log(msg.GetError().ToString());
    //    }
    //}

    // Update is called once per frame
}
