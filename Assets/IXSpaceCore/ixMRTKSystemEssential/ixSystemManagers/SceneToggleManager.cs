// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SceneToggleManager : MonoBehaviour
{
    public GameObject UILinePrefeb;
    public GameObject UILineParent;
    public GameObject TargetSceneObjectParent;
    public PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform target in TargetSceneObjectParent.transform)
        {
            if (target.name == "AxisURP")
                continue;

            target.gameObject.SetActive(false);

            GameObject ui = Instantiate(UILinePrefeb, UILineParent.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = target.gameObject.name;
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { onBtnClicked(target.gameObject); });

            // add additional listeners when entering the scene 
            if (target.name == "ixPointCloud: Cantine in APB")
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { toggleRealityLevel(); });
        }
    }

    void onBtnClicked(GameObject t)
    {
        GetComponent<PhotonView>().RPC("PunBtnClicked", RpcTarget.All, t);
    }

    [PunRPC]
    void PunBtnClicked(GameObject t)
    {
        t.SetActive(!t.activeSelf);
    }

    void toggleRealityLevel()
    {
        playerController.ToggleVirtualExperience();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
