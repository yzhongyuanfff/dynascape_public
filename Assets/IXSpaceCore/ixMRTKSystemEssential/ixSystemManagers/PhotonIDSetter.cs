using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonIDSetter : MonoBehaviour
{
    public PhotonIDManager photonIDManager;

    // Start is called before the first frame update
    void Start()
    {
        requestID();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void requestID()
    {
        this.GetComponent<PhotonView>().ViewID = photonIDManager.currentID;
        photonIDManager.currentID++;
    }
}
