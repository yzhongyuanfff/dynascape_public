// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ixSnappinigManager : MonoBehaviour
{
    GameObject[] Snappables;

    // Start is called before the first frame update
    void Start()
    {
        Snappables = GameObject.FindGameObjectsWithTag("Snappable");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SnapAll()
    {
        foreach(GameObject sb in Snappables)
        {
            sb.GetComponent<SnapToTheNearestSpatialAnchor>().SnapOnce();
        }
    }
}
