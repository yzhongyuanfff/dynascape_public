// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using JfranMora.Inspector;
//using SharpPcap;
//using SharpPcap.LibPcap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ixServerExplorer : MonoBehaviour
{
    public GameObject ServerPanelContent;
    public ClientApi clientApi;

    //public string subnet = "192.168.178.";
    List<string> _onlineDevices;
    List<string> _serverList;
    string Subnet;
    string Path = "/info.txt";
    int Timeout = 1;
    int PingTimeout = 10;
    bool PingFinishedAndServerNotScanned = false;

    // Start is called before the first frame update
    void Start()
    {
        _onlineDevices = new List<string>();
        _serverList = new List<string>();
        uiList = new List<GameObject>();

        string ipaddress = LocalIPAddress();
        LogMe("ip.Address.ToString() = " + ipaddress);
        int index = ipaddress.LastIndexOf(".");
        Subnet = ipaddress.Substring(0, index + 1);
        LogMe("subnet = " + Subnet);

        //CheckOnlineDeviceList();
    }

    void Update()
    {
        if (PingFinishedAndServerNotScanned)
        {
            PingFinishedAndServerNotScanned = false;
            //CheckOnlineServerList();
            UpdateUI();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }

    /// <summary>
    /// 
    /// </summary>
    [Button]
    public void CheckOnlineDeviceList()
    {
        PingFinishedAndServerNotScanned = false;
        string ipaddress = LocalIPAddress();
        LogMe("ip.Address.ToString() = " + ipaddress);
        int index = ipaddress.LastIndexOf(".");
        Subnet = ipaddress.Substring(0, index+1);
        LogMe("subnet = " + Subnet);

        //Thread t = new Thread(CheckOnlineDevices);
        //t.Start();

        StartCoroutine(CheckOnlineDevices());
    }

    /// <summary>
    /// 
    /// </summary>
    [Button]
    public void CheckOnlineServerList()
    {
        StartCoroutine(CheckOnlineServerByGetInfo());
    }

    public void CheckOnlineServerListDirect()
    {
        StartCoroutine(CheckOnlineServerDirectly());
    }

    /// <summary>
    /// 
    /// </summary>
    public void ListAllIPsInSubNet()
    {
        LogMe("Listing IPs in Local Network...");
        ClearUI();
        AppendUI("141.76.64.17");

        if (PlayerPrefs.HasKey("LastServerIP"))
        {
            AppendUI(PlayerPrefs.GetString("LastServerIP"));
        }

        for (int i = 2; i < 255; i++) // ignore router 
        {
            string address = Subnet + i;
            AppendUI(address);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public string LocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "0.0.0.0";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckOnlineDevices()
    {
        _onlineDevices.Clear();

        LogMe("Scanning Local Network...");
        ClearUI();
        AppendUI("141.76.64.17");

        for (int i = 2; i < 255; i++) // ignore router 
        {
            string address = Subnet + i;

            SendPing(address);

            //_percentSearching = (float)i / 255;

            // loop every 5 frames
            float waitFrames = Time.deltaTime * 10;
            yield return new WaitForSeconds(waitFrames);
        }

        LogMe("Number Of Online Devices: " + _onlineDevices.Count);

        PingFinishedAndServerNotScanned = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="address"></param>
    public void SendPing(string address)
    {
        System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();

        // 128 TTL and DontFragment
        PingOptions pingOption = new PingOptions(2, false);

        // Once the ping has reached his target (or has timed out), call this function
        //ping.PingCompleted += PingCompleted;

        byte[] buffer = Encoding.ASCII.GetBytes("p");

        try
        {
            PingReply reply = ping.Send(address, PingTimeout, buffer, pingOption);
            //PingReply reply = ping.SendAsync(address, timeout, buffer, pingOption);

            LogMe("Ping Sent at " + address);

            ProcessReply(reply);
        }
        catch (PingException ex)
        {
            LogMe(string.Format("Connection Error: {0}", ex.Message));
        }
        catch (SocketException ex)
        {
            LogMe(string.Format("Connection Error: {0}", ex.Message));
        }
        catch (NotSupportedException ex)
        {
            LogMe(string.Format("Connection Error: {0}", ex.Message));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="reply"></param>
    private void ProcessReply(PingReply reply)
    {
        if (reply != null)
        {
            if (reply.Status == IPStatus.Success)
            {
                IPHostEntry host = Dns.GetHostEntry(reply.Address);
                LogMe("Online Device Detected: " + reply.Address);

                //
                _onlineDevices.Add(reply.Address.ToString());

                AppendUI(reply.Address.ToString());

                //GetServerInfo(reply.Address.ToString(), "/info.txt"); 
            }
        }
        else
        {
            LogMe("No reply");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckOnlineServerDirectly()
    {
        LogMe("Scanning Server In Local Network ...");
        ClearUI();
        AppendUI("141.76.64.17");

        for (int i = 2; i < 255; i++) // ignore router 
        {
            string address = Subnet + i;
            string url = address + Path;
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                www.timeout = Timeout;
                www.useHttpContinue = false;
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    LogMe(www.error);
                    LogMe("URL = " + url);
                    www.Dispose();
                }
                else
                {
                    if (www.isDone)
                    {
                        // handle the result
                        var result = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                        if (result != "")
                        {
                            LogMe("Server Detected!: Url = " + url);
                            LogMe("Server Info: " + result);
                            _serverList.Add(address);
                            AppendUI(address);
                        }
                    }
                    else
                    {
                        //handle the problem
                        LogMe("Error! data couldn't get.");
                    }
                }
            }

            //_percentSearching = (float)i / 255;

            float waitFrames = Time.deltaTime * 10;
            yield return new WaitForSeconds(waitFrames);
        }

        LogMe("Number Of Local Data Server List: " + _serverList.Count);
        foreach (string serURL in _serverList)
        {
            LogMe("Connected Server: " + serURL);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckOnlineServerByGetInfo()
    {
        _serverList.Clear();

        LogMe("Scanning Server In Local Network ...");

        foreach(string address in _onlineDevices)
        {
            string url = address + Path;
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                www.timeout = Timeout;
                www.useHttpContinue = false;
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    LogMe(www.error);
                    LogMe("URL = " + url);
                    www.Dispose();
                }
                else
                {
                    if (www.isDone)
                    {
                        // handle the result
                        var result = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                        if (result != "")
                        {
                            LogMe("Server Detected!: Url = " + url);
                            LogMe("Server Info: " + result);
                            _serverList.Add(address);
                        }
                    }
                    else
                    {
                        //handle the problem
                        LogMe("Error! data couldn't get.");
                    }
                }
            }

            //_percentSearching = (float)i / 255;

            float waitFrames = Time.deltaTime * 10;
            yield return new WaitForSeconds(waitFrames);
        }

        LogMe("Number Of Local Data Server List: " + _serverList.Count);
        foreach(string serURL in _serverList)
        {
            LogMe("Connected Server: " + serURL);
        }

        UpdateUI();
    }

    List<GameObject> uiList;

    public Material ActiveMaterial;
    public Material InActiveMaterial;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="this_ui"></param>
    /// <param name="url"></param>
    void OnActiveClicked(GameObject this_ui, string url)
    {
        foreach (GameObject ui in uiList)
            ui.transform.Find("ChkBtn").transform.Find("Back Plate").GetComponent<Image>().material = InActiveMaterial;
        this_ui.transform.Find("ChkBtn").transform.Find("Back Plate").GetComponent<Image>().material = ActiveMaterial;
        SetServerURL(url);
    }

    /// <summary>
    /// 
    /// </summary>
    void ClearUI()
    {
        foreach (GameObject u in uiList)
            GameObject.Destroy(u);
        uiList.Clear();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serURL"></param>
    void AppendUI(string serURL)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UIServerListPrefeb", typeof(GameObject))
            , ServerPanelContent.transform);

        ui.transform.Find("ChkBtn").GetComponent<Button>().onClick.AddListener(delegate { OnActiveClicked(ui, serURL); });
        ui.transform.Find("ServerLink").GetComponent<TextMeshProUGUI>().text = serURL;
        ui.transform.Find("Type").GetComponent<TextMeshProUGUI>().text = "Windows";
        uiList.Add(ui);
    }

    /// <summary>
    /// 
    /// </summary>
    void UpdateUI()
    {
        ClearUI();

        AppendUI("141.76.64.17");

        foreach (string serURL in _onlineDevices)
        {
            AppendUI(serURL);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="url"></param>
    void SetServerURL(string url)
    {
        //
        PlayerPrefs.SetString("LastServerIP", url);

        //
        clientApi.url = "http://" + url + ":5000/files";
        clientApi.TryFetchFileListFromServer();

        //
        string filePostUrl = "http://" + url + ":5000/fileUpload";
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().FilePostServerPath = filePostUrl;
    }
}
