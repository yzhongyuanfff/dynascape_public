// ------------------------------------------------------------------------------------
// <copyright company="Technische Universitšt Dresden">
//      Copyright (c) Technische Universitšt Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ixMessageManagerUI : MonoBehaviour
{
    public GameObject Content;

    ixMessageManager ixMM;
    List<Tuple<string, Action<GameObject>>> featureList;

    void Start()
    {
        ixMM = GetComponent<ixMessageManager>();
        
        // Add 5.
        featureList = new List<Tuple<string, Action<GameObject>>>();
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_CheckServerClusterInfo", ixMM.RemoteEvent_CheckServerClusterInfo));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_LoadNDISender", ixMM.RemoteEvent_LoadNDISender));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_PlayURLOnNDISneder", ixMM.RemoteEvent_PlayURLOnNDISneder));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_PlayWebcamNDISneder", ixMM.RemoteEvent_PlayWebcamNDISneder));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_PauseContinuePlay", ixMM.RemoteEvent_PauseContinuePlay));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_UpdatePercentageTest_0per", ixMM.RemoteEvent_UpdatePercentageTest_0per));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_UpdatePercentageTest_50per", ixMM.RemoteEvent_UpdatePercentageTest_50per));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_UpdatePlaybackSpeed_Half", ixMM.RemoteEvent_UpdatePlaybackSpeed_Half));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "RemoteEvent_UpdatePlaybackSpeed_Full", ixMM.RemoteEvent_UpdatePlaybackSpeed_Full));

        // Instantiate buttons on UI
        foreach (var t in featureList)
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), Content.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = t.Item1;
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { t.Item2(ui); });
        }
    }
}
