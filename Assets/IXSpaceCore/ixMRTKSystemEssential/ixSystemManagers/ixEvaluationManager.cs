using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ixEvaluationManager : MonoBehaviour
{
    [SerializeField] string RelativeFolderPath = "/Evaluations/";

    Vector3 AnchorNowTrans;
    Quaternion AnchorNowRot;
    Vector3 AnchorRefTrans;
    Quaternion AnchorRefRot;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="p"></param>
    /// <param name="q"></param>
    public void TraceAnchorNow(Vector3 p, Quaternion q)
    {
        AnchorNowTrans = p;
        AnchorNowRot = q;
        ixLogger.LogMe("Anchor Pose Traced! Position = " + p.ToString() + " Rotation = " + q.ToString());
    }

    /// <summary>
    /// where anchor should be 
    /// </summary>
    /// <param name="p"></param>
    /// <param name="q"></param>
    public void TraceAnchorRef(Vector3 p, Quaternion q)
    {
        AnchorRefTrans = p;
        AnchorRefRot = q;
        ixLogger.LogMe("Anchor Pose Traced! Position = " + p.ToString() + " Rotation = " + q.ToString());
    }

    string header = 
        "DateTime,TimeStamp," +
        "AnchorNowPosition,AnchorNowRotation,AnchorRefPosition,AnchorRefRotation," +
        "transOffset,rotOffset," +
        "PairID\n";

    /// <summary>
    /// after setting anchor now and anchor ref, under time, for each pair 
    /// </summary>
    public void MarkAsPair01_ComputeOffsetAndAppendToFile()
    {
        float transOffset = Vector3.Distance(AnchorNowTrans, AnchorRefTrans);
        Vector3 axis;
        float angleAnchorNow = 0;
        AnchorNowRot.ToAngleAxis(out angleAnchorNow, out axis);
        float angleAnchorRef = 0;
        AnchorNowRot.ToAngleAxis(out angleAnchorRef, out axis);

        float rotOffset = Mathf.Abs(angleAnchorNow - angleAnchorRef);

        string line = "";
        line += System.DateTime.Now.ToString() + ",";
        line += (new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()).ToString() + ",";
        line += AnchorNowTrans.ToString() + ",";
        line += AnchorNowRot.ToString() + ",";
        line += AnchorRefTrans.ToString() + ",";
        line += AnchorRefRot.ToString() + ",";

        line += transOffset + ",";
        line += rotOffset + ",";
        line += "1";

        string content = "";
        string fpath = Application.persistentDataPath + RelativeFolderPath + "AnchorEva.txt";
        if (!System.IO.File.Exists(fpath))
        {
            content += header;
        }
        FileStream fs = new FileStream(fpath, FileMode.Append, FileAccess.Write);
        StreamWriter sw = new StreamWriter(fs);
        content += line;
        ixLogger.LogMe("Writing Eva. Values To File: " + line);
        sw.WriteLine(content);
        sw.Flush();
        sw.Close();
    }

    /// <summary>
    /// after setting anchor now and anchor ref, under time, for each pair 
    /// </summary>
    public void MarkAsPair02_ComputeOffsetAndAppendToFile()
    {
        float transOffset = Vector3.Distance(AnchorNowTrans, AnchorRefTrans);
        Vector3 axis;
        float angleAnchorNow = 0;
        AnchorNowRot.ToAngleAxis(out angleAnchorNow, out axis);
        float angleAnchorRef = 0;
        AnchorNowRot.ToAngleAxis(out angleAnchorRef, out axis);

        float rotOffset = Mathf.Abs(angleAnchorNow - angleAnchorRef);

        string line = "";
        line += System.DateTime.Now.ToString() + ",";
        line += (new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()).ToString() + ",";
        line += AnchorNowTrans.ToString() + ",";
        line += AnchorNowRot.ToString() + ",";
        line += AnchorRefTrans.ToString() + ",";
        line += AnchorRefRot.ToString() + ",";

        line += transOffset + ",";
        line += rotOffset + ",";
        line += "2";

        string content = "";
        string fpath = Application.persistentDataPath + RelativeFolderPath + "AnchorEva.txt";
        if (!System.IO.File.Exists(fpath))
        {
            content += header;
        }
        FileStream fs = new FileStream(fpath, FileMode.Append, FileAccess.Write);
        StreamWriter sw = new StreamWriter(fs);
        content += line;
        ixLogger.LogMe("Writing Eva. Values To File: " + line);
        sw.WriteLine(content);
        sw.Flush();
        sw.Close();
    }

    /// <summary>
    /// after setting anchor now and anchor ref, under time, for each pair 
    /// </summary>
    public void MarkAsPair03_ComputeOffsetAndAppendToFile()
    {
        float transOffset = Vector3.Distance(AnchorNowTrans, AnchorRefTrans);
        Vector3 axis;
        float angleAnchorNow = 0;
        AnchorNowRot.ToAngleAxis(out angleAnchorNow, out axis);
        float angleAnchorRef = 0;
        AnchorNowRot.ToAngleAxis(out angleAnchorRef, out axis);

        float rotOffset = Mathf.Abs(angleAnchorNow - angleAnchorRef);

        string line = "";
        line += System.DateTime.Now.ToString() + ",";
        line += (new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()).ToString() + ",";
        line += AnchorNowTrans.ToString() + ",";
        line += AnchorNowRot.ToString() + ",";
        line += AnchorRefTrans.ToString() + ",";
        line += AnchorRefRot.ToString() + ",";

        line += transOffset + ",";
        line += rotOffset + ",";
        line += "3";

        string content = "";
        string fpath = Application.persistentDataPath + RelativeFolderPath + "AnchorEva.txt";
        if (!System.IO.File.Exists(fpath))
        {
            content += header;
        }
        FileStream fs = new FileStream(fpath, FileMode.Append, FileAccess.Write);
        StreamWriter sw = new StreamWriter(fs);
        content += line;
        ixLogger.LogMe("Writing Eva. Values To File: " + line);
        sw.WriteLine(content);
        sw.Flush();
        sw.Close();
    }

    // Start is called before the first frame update
    void Start()
    {
        // create if not exist 
        if (!System.IO.Directory.Exists(Application.persistentDataPath + RelativeFolderPath))
            System.IO.Directory.CreateDirectory(Application.persistentDataPath + RelativeFolderPath);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
