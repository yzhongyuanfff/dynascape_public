// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

//using Eflatun.SceneReference;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ixExperienceConfigurator : MonoBehaviour
{
    //public List<SceneReference> toBeAddToUIGP01;
    //public List<SceneReference> toBeAddToUIGP02;
    //public List<SceneReference> toBeAddToUIGP03;
    //public List<SceneReference> toBeAddToUIGP04;
    //public List<SceneReference> toBeAddToUIGP05;
    //public List<SceneReference> toBeAddToUIGP06;


    // Start is called before the first frame update
    void Start()
    {
        //foreach (SceneReference s in toBeAddToUIGP01)
        //{
        //    if (s!=null)
        //    {
        //        ShowSceneOnGUI(s.Name);
        //    }
        //}
        //foreach (SceneReference s in toBeAddToUIGP02)
        //{
        //    if (s != null)
        //    {
        //        ShowSceneOnGUI(s.Name);
        //    }
        //}
        //foreach (SceneReference s in toBeAddToUIGP03)
        //{
        //    if (s != null)
        //    {
        //        ShowSceneOnGUI(s.Name);
        //    }
        //}
        //foreach (SceneReference s in toBeAddToUIGP04)
        //{
        //    if (s != null)
        //    {
        //        ShowSceneOnGUI(s.Name);
        //    }
        //}
        //foreach (SceneReference s in toBeAddToUIGP05)
        //{
        //    if (s != null)
        //    {
        //        ShowSceneOnGUI(s.Name);
        //    }
        //}
        //foreach (SceneReference s in toBeAddToUIGP06)
        //{
        //    if (s != null)
        //    {
        //        ShowSceneOnGUI(s.Name);
        //    }
        //}
    }

    void onBtnClicked(GameObject ui, string sn)
    {
        onBtnClickedSync(ui, sn);
        this.GetComponent<PhotonView>().RPC("onBtnClickedSync", RpcTarget.Others);
    }

    void onSpawnBtnClicked(string sn)
    {
        onSpawnBtnClickedSync(sn);
        this.GetComponent<PhotonView>().RPC("onSpawnBtnClickedSync", RpcTarget.Others);
    }

    [PunRPC]
    void onBtnClickedSync(GameObject ui, string sn)
    {
        // toggle the ui  
        bool isActive = ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf;
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!isActive);

        //
        if (!isActive)
            SceneManager.LoadSceneAsync(sn, LoadSceneMode.Additive).completed += UpdateCalibrationAcrossScene;
        if (isActive)
            SceneManager.UnloadSceneAsync(sn);
    }

    [PunRPC]
    void onSpawnBtnClickedSync(string sn)
    {
        SceneManager.LoadSceneAsync(sn, LoadSceneMode.Additive).completed += UpdateCalibrationAcrossScene;
    }

    void UpdateCalibrationAcrossScene(AsyncOperation obj)
    {
        Vector3 p = GameObject.FindGameObjectWithTag("ExperienceOrigin").transform.position;
        Quaternion q = GameObject.FindGameObjectWithTag("ExperienceOrigin").transform.rotation;

        // update sub scenes 
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("SceneGeometry"))
        {
            o.transform.position = p;
            o.transform.rotation = q;
        }
    }

    public void ShowSceneOnGUI(string sceneName)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UITextedBtnPrefeb", typeof(GameObject)),
            GameObject.FindGameObjectWithTag("MainContent").transform);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = sceneName;
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { onBtnClicked(ui, sceneName); });
        ui.transform.Find("SpawnBtn").GetComponent<Button>().onClick.AddListener(delegate { onSpawnBtnClicked(sceneName); });
    }

    public void ShowLineOnGUI(string sepLineText)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UISep", typeof(GameObject)),
            GameObject.FindGameObjectWithTag("MainContent").transform);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = 
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text + sepLineText + 
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text;
    }
}
