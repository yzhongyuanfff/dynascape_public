// ------------------------------------------------------------------------------------
// <copyright company="Technische Universitšt Dresden">
//      Copyright (c) Technische Universitšt Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ixFileUploader : MonoBehaviour
{
    [HideInInspector] public string FileFullPath;
    [HideInInspector] public string FileName;

    UnityWebRequest www;

    float UploadStartTime = 0;

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator UploadFileAsync()
    {
        string url = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().FilePostServerPath;
        ixLogger.LogMe("Uploading To: " + url);

        // Create a WWWForm object
        WWWForm form = new WWWForm();

        // Add the file to the form
        byte[] fileBytes = File.ReadAllBytes(FileFullPath);
        form.AddBinaryData("file", fileBytes, FileName);

        // Send the POST request
        www = UnityWebRequest.Post(url, form);
        
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            ixLogger.LogMe(www.error);
        }
        else
        {
            ixLogger.LogMe("File upload complete!");
        }
    }

    void OnUpload()
    {
        UploadStartTime = Time.time;
        StartCoroutine(UploadFileAsync());
    }

    // Start is called before the first frame update
    void Start()
    {
        this.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { OnUpload(); });
    }

    bool UIUpdatedAfterDownload = false;

    // Update is called once per frame
    void Update()
    {
        if (www != null)
        {
            this.transform.Find("Percentage")
                .GetComponent<TextMeshProUGUI>().text = Mathf.Round(100 * www.uploadProgress) + "%";

            if (www.downloadHandler.isDone && !UIUpdatedAfterDownload)
            {
                UIUpdatedAfterDownload = true;

                //UIToExistState();

                float deltaTime = Time.time - UploadStartTime; // s
                ulong LengthInByte = www.downloadedBytes; // B
                ulong LengthInMegaByte = LengthInByte / 1024 / 1024;
                float Speed = LengthInMegaByte / deltaTime;

                //StartCoroutine(PlayOnFinishAudio());

                ixLogger.LogMe("File Uploaded! Avg Speed: " + Speed + " MB/s");
            }
        }
    }
}
