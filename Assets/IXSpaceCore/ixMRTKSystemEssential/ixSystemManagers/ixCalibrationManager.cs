// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ixCalibrationManager : MonoBehaviour
{
    List<GameObject> CalibrationAnchors;

    public void BringMeToStationX(int x, Transform TA)
    {
        if(x<0 || x > CalibrationAnchors.Count)
        {
            Debug.Log("CalibrationAnchors Not Ready.");
            return;
        }

        GameObject playspace = GameObject.FindGameObjectWithTag("MixedRealityPlayspace");
        if(playspace == null)
        {
            Debug.Log("MixedRealityPlayspace Not Found.");
            return;
        }

        Transform TS = CalibrationAnchors[x].transform;

        Transform TP = playspace.transform;

        //Transform TR = TS.localToWorldMatrix * TA.inverse();

        //Vector3 playspacePosition = playspace.transform.position;
        //Quaternion playspaceRotation = playspace.transform.rotation;

        //Vector3 p0 = TS.TransformPoint(playspacePosition);
        //Quaternion r0 = TS.rotation * playspaceRotation;

        //Vector3 p1 = TA.InverseTransformPoint(p0);
        //Quaternion r1 = r0 * Quaternion.Inverse(TA.rotation);

        //playspace.transform.position = TA.transform.position;
        //playspace.transform.rotation = TA.transform.rotation;



        //playspace.transform.position = TS.transform.position - TA.transform.position + TP.transform.position;
        //playspace.transform.rotation = TS.transform.rotation * Quaternion.Inverse(TA.rotation) * TP.rotation;


        GameObject SceneGeometry = GameObject.FindGameObjectWithTag("SceneGeometry");
        if (SceneGeometry == null)
        {
            Debug.Log("SceneGeometry NULL.");
            return;
        }

        //SceneGeometry.transform.position = -TA.transform.position - TS.transform.position;
        //SceneGeometry.transform.rotation = Quaternion.Inverse(TA.transform.rotation) * Quaternion.Inverse(TS.transform.rotation);

        SceneGeometry.transform.rotation = Quaternion.Inverse(TS.transform.rotation) * TA.transform.rotation;
        SceneGeometry.transform.position = TA.transform.position - SceneGeometry.transform.rotation * TS.transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        CalibrationAnchors = new List<GameObject>();
        foreach (Transform c in transform)
        {
            CalibrationAnchors.Add(c.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
