// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using JfranMora.Inspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ixLocalFileExplorer : MonoBehaviour
{
    [SerializeField] GameObject parent;
    [SerializeField] Material ActiveMaterial;
    [SerializeField] Material InActiveMaterial;
    [SerializeField] string RelativeDataPath;
    [SerializeField] GameObject SmallMultiplePreview;
    [SerializeField] GameObject PreviewSlots;
    [SerializeField] GameObject RGBDVideoPrefeb;
    [SerializeField] GameObject RGBDVideoParent;
    [SerializeField] Material ImageMaterialDemo;
    [SerializeField] GameObject VFXTrailRendererPrefeb;
    [SerializeField] GameObject VFXPathRendererPrefeb;
    [SerializeField] int MaxVideoSlots = 5;

    [SerializeField] List<string> DemoClipsSmallMultiple = new List<string>();
    [SerializeField] List<string> DemoClipsForUnderstanding = new List<string>();
    [SerializeField] string DemoClip01 = "";
    [SerializeField] string DemoClip02 = "";
    [SerializeField] string DemoClip03 = "";
    [SerializeField] List<Color> RGBDTubeColor = new List<Color>();

    int currentVideoSlots = 0;
    List<bool> SlotStates = new List<bool>(); // empty or not
    List<GameObject> uiList = new List<GameObject>();
    List<GameObject> videoPreviews = new List<GameObject>();

    private int _rbgdVideoIndex;

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    [Button]
    public void ExtractVideos()
    {
        string FolderAddress = Application.persistentDataPath + "/" + RelativeDataPath;

        Debug.Log("Listing From: " + FolderAddress);
        ixUtility.LogMe("Listing From: " + FolderAddress);
        ixUtility.LogMe("With FilterOption: " + ixUtility.filterOption);

        foreach (string file in GetFiles(FolderAddress))
        {
            ixUtility.LogMe("Path.GetExtension(file) = " + Path.GetExtension(file));

            /*filtering*/
            string ext = Path.GetExtension(file);
            if (ixUtility.filterOption == ixUtility.FilterOption.ALL)
            {
                // do nothing
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.MP4)
            {
                if (ext.Equals(".mp4")
                    || ext.Equals(".MP4")
                ) { }
                else
                    continue;
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.MOV)
            {
                if (ext.Equals(".mov")
                    || ext.Equals(".MOV")
                ) { }
                else
                    continue;
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.PLY)
            {
                if (ext.Equals(".ply")
                    || ext.Equals(".PLY")
                ) { }
                else
                    continue;
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.CSV)
            {
                if (ext.Equals(".csv")
                    || ext.Equals(".CSV")
                ) { }
                else
                    continue;
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.TXT)
            {
                if (ext.Equals(".txt")
                    || ext.Equals(".TXT")
                ) { }
                else
                    continue;
            }

            // folder: FolderAddress
            bool HasExtractedThumbnail = false;
            string TextureFolder = FolderAddress + "/" + Path.GetFileNameWithoutExtension(file) + "_ExtractedData/";
            HasExtractedThumbnail = System.IO.Directory.Exists(TextureFolder);

            //if (HasExtractedThumbnail)
            //{
            //    SmallMultiplePreview.SetActive(true);

            //    /// Load Camera Traj. as 3d preview - for each
            //    GameObject Trail = Instantiate(VFXTrailRendererPrefeb, SmallMultiplePreview.transform);
            //    Trail.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(TextureFolder + "/CameraTrails.csv", Color.green);
            //    GameObject VideoPath = Instantiate(VFXPathRendererPrefeb, SmallMultiplePreview.transform);
            //    VideoPath.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(TextureFolder + "/VideoPath.csv", Color.green);

            //    ///
            //    Trail.GetComponent<VFXTrailRenderer>().LoadTrailTexturesFromFolder(TextureFolder);

            //    GameObject video3d = new GameObject("video3d");
            //    video3d.transform.parent = SmallMultiplePreview.transform;
            //    Trail.transform.parent = video3d.transform;
            //    VideoPath.transform.parent = video3d.transform;
            //    videoPreviews.Add(video3d);

            //    currentVideoSlots++;
            //    if (currentVideoSlots > MaxVideoSlots)
            //        return;
            //}
            if (!HasExtractedThumbnail) {
                GameObject curVideo = Instantiate(RGBDVideoPrefeb, RGBDVideoParent.transform);
                curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().LoadVideo(FolderAddress, file);
                curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().ExtractTrail();
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    [Button("Load Demo Clips For SmallMultiple Preview")]
    void LoadExampleDatasetForSmallMultiplePreview()
    {
        string FolderAddress = Application.persistentDataPath + "/" + RelativeDataPath;

        ixUtility.LogMe("Listing From: " + FolderAddress);
        ixUtility.LogMe("With FilterOption: " + ixUtility.filterOption);

        foreach (string file in GetFiles(FolderAddress))
        {

            foreach(string targetFilename in DemoClipsSmallMultiple)
            {
                if(Path.GetFileNameWithoutExtension(file) == targetFilename) // file exist
                {
                    bool HasExtractedThumbnail = false;
                    string TextureFolder = FolderAddress + "/" + Path.GetFileNameWithoutExtension(file) + "_ExtractedData/";
                    HasExtractedThumbnail = System.IO.Directory.Exists(TextureFolder);

                    if(HasExtractedThumbnail) // file extracted
                    {
                        SmallMultiplePreview.SetActive(true);

                        GameObject Trail = Instantiate(VFXTrailRendererPrefeb, SmallMultiplePreview.transform);
                        Trail.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(TextureFolder + "/CameraTrails.csv", Color.green);
                        //GameObject VideoPath = Instantiate(VFXPathRendererPrefeb, SmallMultiplePreview.transform);
                        //VideoPath.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(TextureFolder + "/VideoPath.csv", Color.green);
                        Trail.GetComponent<VFXTrailRenderer>().LoadTrailTexturesFromFolder(TextureFolder);
                        Trail.GetComponent<VFXTrailRenderer>().LoadMetaDataFromFolder(TextureFolder);

                        //
                        GameObject video3d = new GameObject("video3d");
                        video3d.transform.parent = SmallMultiplePreview.transform;
                        Trail.transform.parent = video3d.transform;
                        //VideoPath.transform.parent = video3d.transform;
                        videoPreviews.Add(video3d);

                        //
                        PushToSlots(ref video3d, Trail.GetComponent<VFXTrailRenderer>().TrailBounds);
                    }
                }
            }
        }
    }

    [Button("Remove All Small Multiple Previews")]
    void RemoveAllSmallMultiplePreviews()
    {
        foreach(Transform sm in SmallMultiplePreview.transform)
        {
            Destroy(sm);
        }
    }

    [Button("Load Videos For Participants To Understand")]
    void LoadVideosForParticipantsToUnderstand()
    {
        string FolderAddress = Application.persistentDataPath + "/" + RelativeDataPath;

        ixUtility.LogMe("Listing From: " + FolderAddress);
        ixUtility.LogMe("With FilterOption: " + ixUtility.filterOption);

        foreach (string file in GetFiles(FolderAddress))
        {

            foreach (string targetFilename in DemoClipsForUnderstanding)
            {
                if (Path.GetFileNameWithoutExtension(file) == targetFilename) // file exist
                {
                    GameObject rgbVideo = Instantiate((GameObject)Resources.Load("conRGBVideo", typeof(GameObject)), SmallMultiplePreview.transform);
                    rgbVideo.transform.Find("VideoPlayer").GetComponent<ixVideoPlayerController>().LoadVideo(FolderAddress, file);
                    rgbVideo.transform.Find("VideoPlayer").GetComponent<ixVideoPlayerController>().PlayVideo();

                    Vector3 randomPosition = new Vector3(UnityEngine.Random.Range(0, 1.0f), UnityEngine.Random.Range(0, 1.0f), UnityEngine.Random.Range(0, 1.0f));
                    rgbVideo.transform.localPosition = randomPosition;
                }
            }
        }

    }

    [Button("Load Demo Clip 01")]
    void LoadDemoDataset01()
    {
        string filePath = Application.persistentDataPath + "/" + RelativeDataPath + "/" + DemoClip01 + ".mp4";
        LoadClip(filePath);
    }

    [Button("Load Demo Clip 02")]
    void LoadDemoDataset02()
    {
        string filePath = Application.persistentDataPath + "/" + RelativeDataPath + "/" + DemoClip02 + ".mp4";
        LoadClip(filePath);
    }

    [Button("Load Demo Clip 03")]
    void LoadDemoDataset03()
    {
        string filePath = Application.persistentDataPath + "/" + RelativeDataPath + "/" + DemoClip03 + ".mp4";
        LoadClip(filePath);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    void InitSlots()
    {
        SlotStates.Clear();
        foreach (Transform slot in PreviewSlots.transform)
        {
            SlotStates.Add(false);
        }
    }

    void PushToSlots(ref GameObject video3d, Bounds videoBounds)
    {
        int emptySlot = 0;
        int i = 0;
        for (;i< PreviewSlots.transform.childCount; i++)
        {
            if(SlotStates[i] == false)
            {
                emptySlot = i;
                break;
            }
        }

        if (i == PreviewSlots.transform.childCount)
            return;

        Debug.Log("Slot found: " + emptySlot);

        SlotStates[emptySlot] = true; // occupuies the slot

        Vector3 targetDimension = PreviewSlots.transform.GetChild(emptySlot).transform.localScale;
        Vector3 originalDimension = videoBounds.size;
        // compute the scaling factor - choose the smallest
        Vector3 scalingFactorVec3 = new Vector3(
            targetDimension.x / originalDimension.x,
            targetDimension.y / originalDimension.y,
            targetDimension.z / originalDimension.z
        );
        float scalingFactor = Mathf.Min(
            Mathf.Min(scalingFactorVec3.x, scalingFactorVec3.y),
            scalingFactorVec3.z);

        video3d.transform.position = PreviewSlots.transform.GetChild(emptySlot).transform.position;
        video3d.transform.rotation = PreviewSlots.transform.GetChild(emptySlot).transform.rotation;
        video3d.transform.localScale = new Vector3(scalingFactor, scalingFactor, scalingFactor);
    }

    [SerializeField] GameObject TimelineParent;

    int LoadMetaDataFromFolderForTimeline(string folderPath)
    {
        string fileToRead = folderPath + "/VideoMetaData.csv";

        Vector3 minPoint = -Vector3.one;
        Vector3 maxPoint = Vector3.one;
        int frameStart = 0;
        int frameStop = int.MaxValue;

        StreamReader reader = new StreamReader(fileToRead);
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var values = line.Split(',');
            if (values[0].ToString() == "VideoBoundsMin")
            {
                minPoint = new Vector3(
                    float.Parse(values[1].ToString()),
                    float.Parse(values[2].ToString()),
                    float.Parse(values[3].ToString()));
            }
            if (values[0].ToString() == "VideoBoundsMax")
            {
                maxPoint = new Vector3(
                    float.Parse(values[1].ToString()),
                    float.Parse(values[2].ToString()),
                    float.Parse(values[3].ToString()));
            }
            if (values[0].ToString() == "VideoStartFrame")
            {
                frameStart = int.Parse(values[1].ToString());
            }
            if (values[0].ToString() == "VideoStopFrame")
            {
                frameStop = int.Parse(values[1].ToString());
            }
        }

        return (frameStop - frameStart);
    }

    void LoadClip(string file)
    {

        OnActiveClicked(file);
        string FolderAddress = Application.persistentDataPath + "/" + RelativeDataPath + "/";
        string TextureFolder = FolderAddress + "/" + Path.GetFileNameWithoutExtension(file) + "_ExtractedData/";

        // disable all UIs for all videos
        foreach (Transform video in RGBDVideoParent.transform)
        {
            video.Find("UIPanels").gameObject.SetActive(false);
        }

        // instantiate video object and UI panels
        bool exists = false;
        GameObject curVideo;
        foreach (Transform video in RGBDVideoParent.transform)
        {
            if (video.name == Path.GetFileNameWithoutExtension(file)) // already loaded
            {
                curVideo = video.gameObject;
                //if (!Application.isEditor)
                    curVideo.transform.Find("UIPanels").gameObject.SetActive(true); //
                exists = true;
                break;
            }
        }
        if (!exists) // not existing
        {
            curVideo = Instantiate(RGBDVideoPrefeb, RGBDVideoParent.transform);
            curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().LoadVideo(TextureFolder, file);
            curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().ReadALLWithoutTex();
            curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().Previewing_DisableAllCameraTrailView();
            //curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().PlayVideo();
            curVideo.name = Path.GetFileNameWithoutExtension(file);

            GameObject timeline = Instantiate((GameObject)Resources.Load("TextedTimeline", typeof(GameObject)), TimelineParent.transform);
            // adjust dimension?
            timeline.transform.Find("Clip").Find("Segment").GetComponent<MeshRenderer>().material.color = UnityEngine.Random.ColorHSV(0f, 1f, 0.8f, 1f, 0.8f, 1f);
            timeline.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = Path.GetFileNameWithoutExtension(file);

            //
            int frames = LoadMetaDataFromFolderForTimeline(TextureFolder);
            float span = (frames / 4000.0f) * 700; // max
            timeline.transform.Find("Clip").Find("EndPoint").localPosition = new Vector3(timeline.transform.Find("Clip").Find("StartPoint").localPosition.x + span, timeline.transform.Find("Clip").Find("EndPoint").localPosition.y, timeline.transform.Find("Clip").Find("EndPoint").localPosition.z);

            // Set GlobalSpatialPlacer
            curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().SpatialPlacer =
                curVideo.transform.parent.parent.Find("GlobalSpatialPlacer").gameObject;

            // Set tube color
            curVideo.transform.Find("VisualEffects/VFX_DynamicScape_Styles_ZY/TubeMeshRendererPrefeb(Clone)").GetComponent<CustomTubeRenderer>().color =
                RGBDTubeColor[_rbgdVideoIndex++];
        }
    }

    void ListFiles()
    {
        foreach (GameObject u in uiList)
            GameObject.Destroy(u);
        uiList.Clear();

        string FolderAddress = Application.persistentDataPath + "/" + RelativeDataPath + "/";

        ixUtility.LogMe("Listing From: " + FolderAddress);
        ixUtility.LogMe("With FilterOption: " +  ixUtility.filterOption);

        foreach (string file in GetFiles(FolderAddress))
        {
            ixUtility.LogMe("Path.GetExtension(file) = " + Path.GetExtension(file));

            /*filtering*/
            string ext = Path.GetExtension(file);
            if (ixUtility.filterOption == ixUtility.FilterOption.ALL)
            {
                // do nothing
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.MP4)
            {
                if (ext.Equals(".mp4")
                    || ext.Equals(".MP4")
                ){}
                else
                    continue;
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.MOV)
            {
                if (ext.Equals(".mov")
                    || ext.Equals(".MOV")
                ) { }
                else
                    continue;
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.PLY)
            {
                if (ext.Equals(".ply")
                    || ext.Equals(".PLY")
                ) { }
                else
                    continue;
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.CSV)
            {
                if (ext.Equals(".csv")
                    || ext.Equals(".CSV")
                ) { }
                else
                    continue;
            }
            if (ixUtility.filterOption == ixUtility.FilterOption.TXT)
            {
                if (ext.Equals(".txt")
                    || ext.Equals(".TXT")
                ) { }
                else
                    continue;
            }

            // folder: FolderAddress
            bool HasExtractedThumbnail = false;
            string TextureFolder = FolderAddress + "/" + Path.GetFileNameWithoutExtension(file) + "_ExtractedData/";
            HasExtractedThumbnail = System.IO.Directory.Exists(TextureFolder);
            string PotentialImageFile = "";
                //= TextureFolder + 30 + ".jpg";

            foreach (string fi in GetFiles(TextureFolder)) {
                if (Path.GetExtension(fi).Equals(".jpg"))
                {
                    PotentialImageFile = fi;
                    break;
                }
            }


            GameObject ui = Instantiate((GameObject)Resources.Load("UILocalFileWithImagePreview", typeof(GameObject)), parent.transform);

            if (HasExtractedThumbnail)
            {
                /// Load 2d preview
                byte[] imageData = File.ReadAllBytes(PotentialImageFile);// Load the image data into a Texture2D
                Texture2D texture = new Texture2D(2, 2);
                texture.LoadImage(imageData);
                Material tmpMat = new Material(ImageMaterialDemo); //
                tmpMat.mainTexture = texture;
                ui.transform.Find("Image").transform.GetComponent<Image>().material = tmpMat; //
                ui.transform.Find("Image").gameObject.SetActive(true);
                ui.transform.Find("ImageSpaceHolder").gameObject.SetActive(false);

                // takes time - move to an additional option
                ///// Load Camera Traj. as 3d preview - for each
                //GameObject Trail = Instantiate(VFXTrailRendererPrefeb, this.transform);
                //Trail.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(TextureFolder + "/CameraTrails.csv", Color.green);
                //GameObject VideoPath = Instantiate(VFXPathRendererPrefeb, this.transform);
                //VideoPath.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(TextureFolder + "/VideoPath.csv", Color.green);

                /////
                //Trail.GetComponent<VFXTrailRenderer>().LoadTrailTexturesFromFolder(TextureFolder);
            }
            else
            {
                ui.transform.Find("Image").gameObject.SetActive(false); // ImageSpaceHolder
                ui.transform.Find("ImageSpaceHolder").gameObject.SetActive(true);
            }

            ui.transform.Find("Btn").gameObject.SetActive(false);
            ui.transform.Find("ChkBtn").gameObject.SetActive(true); //PreviewBtn
            ui.transform.Find("DelBtn").gameObject.SetActive(false);
            ui.transform.Find("Percentage").gameObject.SetActive(true);
            ui.transform.Find("Size").gameObject.SetActive(true);

            if (HasExtractedThumbnail)
            {
                ui.transform.Find("PreviewHolder").gameObject.SetActive(false);
                ui.transform.Find("PreviewBtn").gameObject.SetActive(true);
                ui.transform.Find("PreviewBtn").GetComponent<Button>().onClick.AddListener(delegate { // render a 3d preview
                    SmallMultiplePreview.SetActive(true);

                    GameObject Trail = Instantiate(VFXTrailRendererPrefeb, SmallMultiplePreview.transform);
                    Trail.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(TextureFolder + "/CameraTrails.csv", Color.green);
                    //GameObject VideoPath = Instantiate(VFXPathRendererPrefeb, SmallMultiplePreview.transform);
                    //VideoPath.GetComponent<VFXTrailRenderer>().CreateTrailBaseOnFileAndColor(TextureFolder + "/VideoPath.csv", Color.green);
                    Trail.GetComponent<VFXTrailRenderer>().LoadTrailTexturesFromFolder(TextureFolder);
                    Trail.GetComponent<VFXTrailRenderer>().LoadMetaDataFromFolder(TextureFolder);

                    //
                    GameObject video3d = new GameObject("video3d");
                    video3d.transform.parent = SmallMultiplePreview.transform;
                    Trail.transform.parent = video3d.transform;
                    //VideoPath.transform.parent = video3d.transform;
                    videoPreviews.Add(video3d);

                    //
                    PushToSlots(ref video3d, Trail.GetComponent<VFXTrailRenderer>().TrailBounds);
                });
            }
            else
            {
                ui.transform.Find("PreviewHolder").gameObject.SetActive(true);
                ui.transform.Find("PreviewBtn").gameObject.SetActive(false);
            }

            ui.transform.Find("ChkBtn").GetComponent<Button>().onClick.AddListener(delegate { // activate the video
                OnActiveClicked(file, ui);

                // disable all UIs for all videos
                foreach (Transform video in RGBDVideoParent.transform)
                {
                    video.Find("UIPanels").gameObject.SetActive(false);
                }

                // instantiate video object and UI panels
                bool exists = false;
                GameObject curVideo = null;
                foreach (Transform video in RGBDVideoParent.transform)
                {
                    if (video.name == Path.GetFileNameWithoutExtension(file)) // already loaded
                    {
                        curVideo = video.gameObject;
                        //if(!Application.isEditor)
                        //curVideo.transform.Find("UIPanels").gameObject.SetActive(true); //
                        exists = true;
                        break;
                    }
                }
                if (!exists) // not existing
                {
                    curVideo = Instantiate(RGBDVideoPrefeb, RGBDVideoParent.transform);
                    curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().LoadVideo(TextureFolder, file);
                    curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().ReadALLWithoutTex(); // without loading texture atlas at the beginning 
                    curVideo.name = Path.GetFileNameWithoutExtension(file);

                    // instantiate a time object 
                    GameObject timeline = Instantiate((GameObject)Resources.Load("TextedTimeline", typeof(GameObject)), TimelineParent.transform);
                    // adjust dimension?
                    timeline.transform.Find("Clip").Find("Segment").GetComponent<MeshRenderer>().material.color =      
                        UnityEngine.Random.ColorHSV(0f, 1f, 0.8f, 1f, 0.8f, 1f);
                    timeline.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = 
                        Path.GetFileNameWithoutExtension(file);
                    int frames = LoadMetaDataFromFolderForTimeline(TextureFolder);
                    float span = (frames / 4000.0f) * 700; // max
                    timeline.transform.Find("Clip").Find("EndPoint").localPosition = new Vector3(timeline.transform.Find("Clip").Find("StartPoint").localPosition.x + span, timeline.transform.Find("Clip").Find("EndPoint").localPosition.y, timeline.transform.Find("Clip").Find("EndPoint").localPosition.z);

                    // Set GlobalSpatialPlacer
                    curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().SpatialPlacer =
                        curVideo.transform.parent.parent.Find("GlobalSpatialPlacer").gameObject;

                    // Set tube color
                    curVideo.transform.Find("VisualEffects/VFX_DynamicScape_Styles_ZY/TubeMeshRendererPrefeb(Clone)").GetComponent<CustomTubeRenderer>().color =
                        RGBDTubeColor[_rbgdVideoIndex++];
                }
                else
                {
                    if (curVideo != null)
                        Destroy(curVideo);

                    // remove from global timeline 
                }
            });
            ui.transform.Find("WithTexBtn").GetComponent<Button>().onClick.AddListener(delegate { // activate the video
                OnActiveClicked(file, ui);

                // disable all UIs for all videos
                foreach (Transform video in RGBDVideoParent.transform)
                {
                    video.Find("UIPanels").gameObject.SetActive(false);
                }

                // instantiate video object and UI panels
                bool exists = false;
                GameObject curVideo = null;
                foreach (Transform video in RGBDVideoParent.transform)
                {
                    if (video.name == Path.GetFileNameWithoutExtension(file)) // already loaded
                    {
                        curVideo = video.gameObject;
                        //if(!Application.isEditor)
                        //curVideo.transform.Find("UIPanels").gameObject.SetActive(true); //
                        exists = true;
                        break;
                    }
                }
                if (!exists) // not existing
                {
                    curVideo = Instantiate(RGBDVideoPrefeb, RGBDVideoParent.transform);
                    curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().LoadVideo(TextureFolder, file);
                    curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().ReadALLWithTex(); // without loading texture atlas at the beginning 
                    curVideo.name = Path.GetFileNameWithoutExtension(file);

                    // instantiate a time object 
                    GameObject timeline = Instantiate((GameObject)Resources.Load("TextedTimeline", typeof(GameObject)), TimelineParent.transform);
                    // adjust dimension?
                    timeline.transform.Find("Clip").Find("Segment").GetComponent<MeshRenderer>().material.color =
                        UnityEngine.Random.ColorHSV(0f, 1f, 0.8f, 1f, 0.8f, 1f);
                    timeline.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                        Path.GetFileNameWithoutExtension(file);
                    int frames = LoadMetaDataFromFolderForTimeline(TextureFolder);
                    float span = (frames / 4000.0f) * 700; // max
                    timeline.transform.Find("Clip").Find("EndPoint").localPosition = new Vector3(timeline.transform.Find("Clip").Find("StartPoint").localPosition.x + span, timeline.transform.Find("Clip").Find("EndPoint").localPosition.y, timeline.transform.Find("Clip").Find("EndPoint").localPosition.z);

                    // Set GlobalSpatialPlacer
                    curVideo.transform.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().SpatialPlacer =
                        curVideo.transform.parent.parent.Find("GlobalSpatialPlacer").gameObject;

                    // Set tube color
                    curVideo.transform.Find("VisualEffects/VFX_DynamicScape_Styles_ZY/TubeMeshRendererPrefeb(Clone)").GetComponent<CustomTubeRenderer>().color =
                        RGBDTubeColor[_rbgdVideoIndex++];
                }
                else
                {
                    if (curVideo != null)
                        Destroy(curVideo);

                    // remove from global timeline 
                }
            });
            ui.transform.Find("ActivateUIBtn").GetComponent<Button>().onClick.AddListener(delegate {

                // disable all UIs for all videos
                foreach (Transform video in RGBDVideoParent.transform)
                {
                    video.Find("UIPanels").gameObject.SetActive(false);
                }

                // activete the UI panel of the current video given name 
                foreach (Transform video in RGBDVideoParent.transform)
                {
                    if (video.name == Path.GetFileNameWithoutExtension(file)) // already loaded
                    {
                        video.transform.Find("UIPanels").gameObject.SetActive(true);

                        Debug.Log("video UI activated: " + video.name);
                        break;
                    }
                }

            });
                
            ui.transform.Find("FileLink").GetComponent<TextMeshProUGUI>().text = Path.GetFileNameWithoutExtension(file);
            ui.transform.Find("Percentage").GetComponent<TextMeshProUGUI>().text = "100%";
            var fileinfo = new System.IO.FileInfo(file);
            float fileSizeInMB = fileinfo.Length / 1024.0f / 1024.0f;
            ui.transform.Find("Size").GetComponent<TextMeshProUGUI>().text = fileSizeInMB.ToString("F2") + " MB";

            uiList.Add(ui);
        }
    }

    static IEnumerable<string> GetFiles(string path)
    {
        Queue<string> queue = new Queue<string>();
        queue.Enqueue(path);
        while (queue.Count > 0)
        {
            path = queue.Dequeue();
            try
            {
                foreach (string subDir in Directory.GetDirectories(path))
                {
                    queue.Enqueue(subDir);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            string[] files = null;
            try
            {
                files = Directory.GetFiles(path);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            if (files != null)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    yield return files[i];
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //uiList = new List<GameObject>();
        //ListFiles();
        InitSlots();
        ListFiles_FilterMP4();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Disable3DPreviews()
    {
        SmallMultiplePreview.gameObject.SetActive(false);
    }

    // adjust UI and global active file
    void OnActiveClicked(string fn, GameObject this_ui = null)
    {
        if (this_ui)
        {
            foreach (GameObject ui in uiList)
                ui.transform.Find("ChkBtn").transform.Find("Back Plate").GetComponent<Image>().material = InActiveMaterial;
            this_ui.transform.Find("ChkBtn").transform.Find("Back Plate").GetComponent<Image>().material = ActiveMaterial;
        }

        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().SetCurActiveFilePath(fn);
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curFolderPath = Path.GetDirectoryName(fn);
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curFileName = Path.GetFileName(fn);
    }

    public void ListFiles_FilterMP4()
    {
        ixUtility.filterOption = ixUtility.FilterOption.MP4;
        ListFiles();
    }
    public void ListFiles_FilterMOV()
    {
        ixUtility.filterOption = ixUtility.FilterOption.MOV;
        ListFiles();
    }
    public void ListFiles_FilterPLY()
    {
        ixUtility.filterOption = ixUtility.FilterOption.PLY;
        ListFiles();
    }
    public void ListFiles_FilterCSV()
    {
        ixUtility.filterOption = ixUtility.FilterOption.CSV;
        ListFiles();
    }
    public void ListFiles_FilterTXT()
    {
        ixUtility.filterOption = ixUtility.FilterOption.TXT;
        ListFiles();
    }
    public void ListFiles_ALL()
    {
        ixUtility.filterOption = ixUtility.FilterOption.ALL;
        ListFiles();
    }
}
