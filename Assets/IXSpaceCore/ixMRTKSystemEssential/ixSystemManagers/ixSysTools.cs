// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ixSysTools: MonoBehaviour
{
    //public ixMetaUserManager MetaUserManager;
    public GameObject Content;

    List<Tuple<string, Action<GameObject>>> featureList;

    private void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }

    private void Start()
    {
        featureList = new List<Tuple<string, Action<GameObject>>>();
        //featureList.Add(new Tuple<string, Action<GameObject>>("LogUserInfo", LogUserInfo)); // 
        featureList.Add(new Tuple<string, Action<GameObject>>("DumpFileContent", DumpFileContent));
        featureList.Add(new Tuple<string, Action<GameObject>>("LoadActiveAssetBundle", LoadActiveAssetBundle));
        featureList.Add(new Tuple<string, Action<GameObject>>("UnloadJustLoadScene", UnloadJustLoadScene));
        featureList.Add(new Tuple<string, Action<GameObject>>("ShowAllLoadedScenes", ShowAllLoadedScenes));
        featureList.Add(new Tuple<string, Action<GameObject>>("UnloadAllInActiveScenes", UnloadAllInActiveScenes));

        // Instantiate buttons 
        foreach (var t in featureList)
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), Content.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = t.Item1;
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { t.Item2(ui); });
        }
    }

    int LineLimit = 100;
    int NumOfLines = 0;

    void DumpFileContent(GameObject ui)
    {
        string fpath = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;

        StreamReader reader = new StreamReader(fpath);
        string line = reader.ReadLine();
        NumOfLines = 0;
        while (line != null)
        {
            LogMe(line);
            LogMe("");

            //
            NumOfLines++;
            if (NumOfLines > LineLimit)
            {
                break;
            }

            // Read next line
            line = reader.ReadLine();
        }
            
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void LoadActiveAssetBundle(GameObject ui)
    {
        GetComponent<ixAssetBundleLoader>().LoadActiveSceneBundle();
        string fpath = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
        LogMe("Load AssetBundle From: " + fpath);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void UnloadJustLoadScene(GameObject ui)
    {
        GetComponent<ixAssetBundleLoader>().UnloadJustLoadScene();
        LogMe("Scene Removed!");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void ShowAllLoadedScenes(GameObject ui)
    {
        int countLoaded = SceneManager.sceneCount;
        LogMe("Num Of Loaded Scenes = " + countLoaded);
        Scene[] loadedScenes = new Scene[countLoaded];
        //Scene activeScene = SceneManager.GetActiveScene();
        for (int i = 0; i < countLoaded; i++)
        {
            loadedScenes[i] = SceneManager.GetSceneAt(i);
            LogMe("Scene Found! Name = " + loadedScenes[i].name);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void UnloadAllInActiveScenes(GameObject ui)
    {
        int countLoaded = SceneManager.sceneCount;
        LogMe("Num Of Loaded Scenes = " + countLoaded);
        Scene[] loadedScenes = new Scene[countLoaded];
        Scene activeScene = SceneManager.GetActiveScene();
        for (int i = 0; i < countLoaded; i++)
        {
            //loadedScenes[i] = SceneManager.GetSceneAt(i);
            //LogMe("Scene Found! Name = " + loadedScenes[i].name);
            if (loadedScenes[i] != activeScene)
            {
                SceneManager.UnloadSceneAsync(loadedScenes[i], UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            }
        }
        LogMe("Done.");
    }
}
