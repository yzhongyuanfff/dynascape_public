// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;

public class ixDeviceInfoManager : MonoBehaviour
{
    /// <summary>
    /// 
    /// </summary>
    void GetDeviceInfo()
    {
        LogMe("Application.GetBuildTags().Length: " + Application.GetBuildTags().Length);
        //LogMe("Application.GetBuildTags().ToString(): " + Application.GetBuildTags()[0]);
        LogMe("Device ID: " + SystemInfo.deviceUniqueIdentifier);
        LogMe("LocalIPv4: " + GetLocalIPv4());
        LogMe("SystemInfo.deviceName: " + SystemInfo.deviceName);
        LogMe("SystemInfo.batteryLevel: " + SystemInfo.batteryLevel);
        LogMe("SystemInfo.batteryStatus: " + SystemInfo.batteryStatus);
        LogMe("SystemInfo.deviceModel: " + SystemInfo.deviceModel);
        LogMe("SystemInfo.deviceType: " + SystemInfo.deviceType);
        LogMe("SystemInfo.graphicsDeviceName: " + SystemInfo.graphicsDeviceName);
        LogMe("SystemInfo.graphicsDeviceVendor: " + SystemInfo.graphicsDeviceVendor);
        LogMe("SystemInfo.graphicsMemorySize: " + SystemInfo.graphicsMemorySize);
        LogMe("SystemInfo.hasDynamicUniformArrayIndexingInFragmentShaders: " + SystemInfo.hasDynamicUniformArrayIndexingInFragmentShaders);
        LogMe("SystemInfo.maxComputeBufferInputsCompute: " + SystemInfo.maxComputeBufferInputsCompute);
        LogMe("SystemInfo.maxComputeWorkGroupSize: " + SystemInfo.maxComputeWorkGroupSize);
        LogMe("SystemInfo.maxTextureSize: " + SystemInfo.maxTextureSize);
        LogMe("SystemInfo.operatingSystem: " + SystemInfo.operatingSystem);
        LogMe("SystemInfo.processorCount: " + SystemInfo.processorCount);
        LogMe("SystemInfo.processorFrequency: " + SystemInfo.processorFrequency);
        LogMe("SystemInfo.processorType: " + SystemInfo.processorType);
        LogMe("SystemInfo.supports3DRenderTextures: " + SystemInfo.supports3DRenderTextures);
        LogMe("SystemInfo.supportsAccelerometer: " + SystemInfo.supportsAccelerometer);
        LogMe("SystemInfo.supportsAsyncGPUReadback: " + SystemInfo.supportsAsyncGPUReadback);
        LogMe("SystemInfo.supportsInstancing: " + SystemInfo.supportsInstancing);
        LogMe("SystemInfo.supportsGyroscope: " + SystemInfo.supportsGyroscope);
        LogMe("SystemInfo.systemMemorySize: " + SystemInfo.systemMemorySize);
        LogMe("SystemInfo.supportsRenderTextures: " + SystemInfo.supportsRenderTextures);
        //LogMe("System Version: " + sysVersion);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public string GetLocalIPv4()
    {
        return Dns.GetHostEntry(Dns.GetHostName())
            .AddressList.First(
                f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            .ToString();
    }


    // Start is called before the first frame update
    void Start()
    {
        Invoke("GetDeviceInfo", 2);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
