// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using JfranMora.Inspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ixAudioRecorder : MonoBehaviour
{
    private AudioSource audioSourceA;

    private void Start()
    {
        FrameSpan = FrameSpanSec * 72;
        Invoke("LateStart", StartFrame / 72.0f);
        _ixSavWav = gameObject.AddComponent(typeof(ixSavWav)) as ixSavWav;
    }

    void LateStart()
    {
        ixLogger.LogMe("===ixAudioRecorder===");
        //List all input mics to get the device ID.
        foreach (var device in Microphone.devices)
            ixLogger.LogMe("Microphone Found! Name: " + device);

        //
        audioSourceA = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        audioSourceA.clip = Microphone.Start(Microphone.devices[0], true, FrameSpanSec, 44100);
    }

    int frameCount = 0;
    int StartFrame = 72;
    public int FrameSpanSec = 10;
    int FrameSpan;
    public bool EnableAutoRecord = true;

    /// <summary>
    /// 
    /// </summary>
    public void PauseContinueRecording()
    {
        EnableAutoRecord = !EnableAutoRecord;
        ixLogger.LogMe("Audio: EnableAutoRecord = " + EnableAutoRecord);

        //
        //if(EnableAutoRecord == false)
        //{
        //    Microphone.End(Microphone.devices[0]);
        //}
        //else
        //{
        //    audioSourceA = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        //    audioSourceA.clip = Microphone.Start(Microphone.devices[0], true, FrameSpanSec, 44100);
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if (EnableAutoRecord)
        {
            if (frameCount > (StartFrame + FrameSpan))
            {
                ForceWriteToFileAndRestartRecording();
            }
        }

        frameCount++;
    }

    ixSavWav _ixSavWav;

    public void SaveToDisk()
    {
        string fpath = Application.persistentDataPath + "/CollectedData/Audios/";

        if (!System.IO.Directory.Exists(fpath))
            System.IO.Directory.CreateDirectory(fpath);

        string timestamp = 
              System.DateTime.Now.Hour.ToString() + "_"
            + System.DateTime.Now.Minute.ToString() + "_"
            + System.DateTime.Now.Second.ToString() + "_" 
            + System.DateTime.Now.Date.Day.ToString() + "_" 
            + System.DateTime.Now.Date.Month.ToString() + "_" 
            + System.DateTime.Now.Date.Year.ToString();

        //(new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()).ToString()

        string fn = "/TracedAudio_" + timestamp + ".wav";

        string path = fpath + fn;

        ixLogger.LogMe("Saving: Number of Samples = " + audioSourceA.clip.samples * 2);
        _ixSavWav.Save(path, audioSourceA.clip);
    }

    [Button]
    void ForceWriteToFileAndRestartRecording()
    {
        SaveToDisk();
        frameCount = 0;

        //
        audioSourceA = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        audioSourceA.clip = Microphone.Start(Microphone.devices[0], true, FrameSpanSec, 44100);
    }

}
