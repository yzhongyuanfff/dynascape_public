// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.AccessControl;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FileDownloader : MonoBehaviour
{
    [HideInInspector] public int FileID = 0;
    [HideInInspector] public string HttpAddress;
    [HideInInspector] public string DiskAddress;
    [HideInInspector] public string FolderAddress;
    [HideInInspector] public string FileNameWithoutExtension;
    [HideInInspector] public bool EnableDel = true;

    public Material ActiveMaterial;
    public Material ActiveLinkMaterial;
    public Material InActiveMaterial;
    //public AudioClip OnFinishAudio;

    UnityWebRequest www;

    //GameObject Btn;
    //GameObject ChkBtn;
    //GameObject DelBtn;

    bool UIUpdatedAfterDownload = false;
    bool DefaultStateChkBtn = false;

    bool isActive = false;
    float DownloadStartTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { StartDownload(); });
        //this.transform.Find("ChkBtn").GetComponent<Button>().onClick.AddListener(delegate { });
        this.transform.Find("DelBtn").GetComponent<Button>().onClick.AddListener(delegate { DeleteFile(); });
        if(this.transform.Find("Speed"))
            this.transform.Find("Speed").GetComponent<TextMeshProUGUI>().text = "";
    }

    public void SetActive()
    {
        this.transform.Find("ChkBtn").transform.Find("Back Plate").GetComponent<Image>().material = ActiveMaterial;
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().SetCurActiveFilePath(DiskAddress);
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curFolderPath = FolderAddress;
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curFileName = FileNameWithoutExtension;
    }

    public void SetUseLink()
    {
        this.transform.Find("SetBtn").transform.Find("Back Plate").GetComponent<Image>().material = ActiveLinkMaterial;
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().SetCurActiveFilePath(HttpAddress);
        ixLogger.LogMe("Set StringManager path to: " + HttpAddress);
    }

    /// <summary>
    /// 
    /// </summary>
    public void SetInActive()
    {
        this.transform.Find("ChkBtn").transform.Find("Back Plate").GetComponent<Image>().material = InActiveMaterial;
    }

    /// <summary>
    /// 
    /// </summary>
    public void SetUnused()
    {
        this.transform.Find("SetBtn").transform.Find("Back Plate").GetComponent<Image>().material = InActiveMaterial;
        GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().SetCurActiveFilePath("");
    }

    void UIToExistState()
    {
        //
        this.transform.Find("SetBtn").gameObject.SetActive(true);
        this.transform.Find("ChkBtn").gameObject.SetActive(true);
        if (EnableDel)
            this.transform.Find("DelBtn").gameObject.SetActive(true);
        else
            this.transform.Find("DelBtn").gameObject.SetActive(false);
        this.transform.Find("Percentage").GetComponent<TextMeshProUGUI>().text = "100%";

        //
        this.transform.Find("Btn").gameObject.SetActive(false);
        this.transform.Find("Speed").gameObject.SetActive(false);
    }

    void UIToNonExistState()
    {
        //
        this.transform.Find("SetBtn").gameObject.SetActive(true);
        this.transform.Find("ChkBtn").gameObject.SetActive(DefaultStateChkBtn);
        this.transform.Find("DelBtn").gameObject.SetActive(false);
        this.transform.Find("Percentage").GetComponent<TextMeshProUGUI>().text = "0%";

        //
        this.transform.Find("Btn").gameObject.SetActive(true);
        this.transform.Find("Speed").gameObject.SetActive(true);
    }

    ulong downloadedBytesLastFrame = 0;
    float timeLastFrame = 0;

    int frameInterval = 30;
    int frameCount = 0;

    // Update is called once per frame
    void Update()
    {
        if (www != null)
        {
            // realtime percentage 
            this.transform.Find("Percentage")
                .GetComponent<TextMeshProUGUI>().text = Mathf.Round(100 * www.downloadProgress)  + "%";

            // realtime speed
            frameCount++;
            if(frameCount> frameInterval)
            {
                //
                frameCount = 0;

                //
                float deltaTime = Time.time - timeLastFrame;
                ulong LengthInByte = www.downloadedBytes - downloadedBytesLastFrame;
                float Speed = LengthInByte/ 1024.0f / 1024.0f / deltaTime;
                this.transform.Find("Speed").GetComponent<TextMeshProUGUI>().text = Speed.ToString("F2") + " MB/s";

                //
                timeLastFrame = Time.time;
                downloadedBytesLastFrame = www.downloadedBytes;
            }

            //
            if (www.downloadHandler.isDone && !UIUpdatedAfterDownload)
            {
                UIUpdatedAfterDownload = true;

                UIToExistState();

                float deltaTime = Time.time - DownloadStartTime; // s
                ulong LengthInByte = www.downloadedBytes; // B
                ulong LengthInMegaByte = LengthInByte / 1024 / 1024;
                float Speed = LengthInMegaByte / deltaTime;

                StartCoroutine(PlayOnFinishAudio());

                ixLogger.LogMe("File Downloaded! Avg Speed: " + Speed + " MB/s");
            }
        }
    }

    IEnumerator PlayOnFinishAudio()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (audio.clip)
        {
            audio.Play();
            yield return new WaitForSeconds(audio.clip.length);
        }
    }

    [HideInInspector] public bool UseLegacyDownloadFunc = false;

    Task IOTask;

    IEnumerator GetData()
    {
        www = UnityWebRequest.Get(HttpAddress);
        // rqlist.Add(www);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            ixLogger.LogMe(www.error);
        }
        else
        {
            byte[] results = www.downloadHandler.data;
            ulong numbytes = www.downloadedBytes;

            ixUtility.LogMe("Download Successfully To Memory! www.downloadedBytes = " + numbytes);
            ixUtility.LogMe("Start Writing To Disk!");

            //
            if (ixUtility.IsWinPlatform())
            {
                /*Will block IO on oculus quest platforms*/
                IOTask = File.WriteAllBytesAsync(DiskAddress, results);
                StartCoroutine(LogIOTaskState()); // start a Coroutine to check task state
            }
            else
            {
                /*Downloading with Coroutine on oculus quest platforms - no threading support*/
                sw1.Restart();
                StartCoroutine(SyncWriteV2(DiskAddress, results, numbytes));
                /*Wrong Impl. */
                //SavetoDisk(DiskAddress, results, (int)www.downloadedBytes);
            }

        }
    }


    IEnumerator LogIOTaskState()
    {
        while (!IOTask.IsCompleted)
        {
            ixUtility.LogMe("IOTask.Status: " + IOTask.Status);
            yield return new WaitForSeconds(0.5f);
        }
        ixUtility.LogMe("File Downloaded! IOTask.IsCompletedSuccessfully: " + IOTask.IsCompletedSuccessfully);
    }


    Stopwatch sw1 = new Stopwatch();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="bytes"></param>
    /// <param name="numbytes"></param>
    /// <returns></returns>
    IEnumerator SyncWriteV2(string path, byte[] bytes, ulong numbytes)
    {
        int frameIntervalCoroutine =
            GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().FrameIntervalCoroutine;

        ulong ChunkSize =
            GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().ChunkSize;

        // ulong ChunkSize = 1024 * 1024; // 1MB
        ulong NumChunks = numbytes / ChunkSize;
        if (numbytes % ChunkSize > 0) NumChunks++;
        ixLogger.LogMe("ChunkSize = " + ChunkSize);
        ixLogger.LogMe("NumChunks = " + NumChunks);

        FileStream stream = new FileStream(path, FileMode.Append, FileAccess.Write);
        if (NumChunks == 1)
            stream.Write(bytes, 0, bytes.Length);
        else
            stream.Write(bytes, 0, (int)ChunkSize);
        ixLogger.LogMe("writing, yield for few frames ... writing file to IO takes a while ...");
        yield return new WaitForSeconds(frameIntervalCoroutine / 70.0f); // wait for few frames

        //
        if (NumChunks > 1)
        {
            for (ulong cid = 1; cid < NumChunks - 1; cid++)
            {
                ulong offset = cid * ChunkSize;
                stream.Write(bytes, (int)offset, (int)ChunkSize);
                //ixLogger.LogMe("writing, yield for few frames ... ");
                yield return new WaitForSeconds(frameIntervalCoroutine / 70.0f); // wait for few frames
            }

            //
            stream.Write(bytes, (int)(ChunkSize * (NumChunks - 1)), (int)(numbytes % ChunkSize));
        }

        sw1.Stop();
        var s1 = sw1.Elapsed;
        ixLogger.LogMe("s1.TotalMilliseconds = " + s1.TotalMilliseconds);
        yield return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="bytes"></param>
    /// <param name="NumOfBytes"></param>
    /// <returns></returns>
    IEnumerator SavetoDisk(string path, byte[] bytes, int NumOfBytes)
    {
        int ChunkSize = 1024 * 1024;
        int NumOfChunks = NumOfBytes / ChunkSize;
        var file = new FileStream(path, FileMode.Append, FileAccess.Write);
        for (int i = 0; i < ChunkSize; i++)
        {
            file.Write(bytes, i * ChunkSize, NumOfChunks + 1);

            float FrameInterval = 30;
            float FrameRate = 70;
            yield return new WaitForSeconds(FrameInterval/ FrameRate);
        }
        file.Close();
    }

    [PunRPC]
    void StartDownloadNetworked(string httpAddress, string diskAddress, string folderAddress, string fileNameWithoutExtension)
    {
        ixUtility.LogMe("Excuting StartDownloadNetworked()");

        // 
        HttpAddress = httpAddress;
        DiskAddress = diskAddress;
        FolderAddress = folderAddress;
        FileNameWithoutExtension = fileNameWithoutExtension;

        //
        DownloadStartTime = Time.time;
        UIUpdatedAfterDownload = false;
        StartCoroutine(GetData());
    }

    // Networked Download - Should All of the guests download the current data!
    public void StartDownload()
    {
        // Local 
        StartDownloadNetworked(HttpAddress, DiskAddress, FolderAddress, FileNameWithoutExtension);

        // Remote 
        if (this.GetComponent<PhotonView>().ViewID != 0) 
            this.GetComponent<PhotonView>().RPC("StartDownloadNetworked", RpcTarget.Others, HttpAddress, DiskAddress, FolderAddress, FileNameWithoutExtension);
    }

    // define the initial state of the UI 
    public void RefreshUI()
    {
        bool e = System.IO.File.Exists(DiskAddress);
        if (e)
        {
            UIToExistState();
        }
        else // does not exist 
        {
            UIToNonExistState();
        }
    }

    void DeleteFile()
    {
        UIToNonExistState();

        if (System.IO.File.Exists(DiskAddress))
        {
            System.IO.File.Delete(DiskAddress);
        }
    }
}
