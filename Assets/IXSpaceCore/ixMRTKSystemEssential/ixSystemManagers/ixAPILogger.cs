// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

//using SharpPcap;
//using SharpPcap.LibPcap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// Write a function & reflect it to UI 
public class ixAPILogger : MonoBehaviour
{
    //public ixMetaUserManager MetaUserManager;
    public GameObject Content;

    List<Tuple<string, Action<GameObject>>> featureList;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    private void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
        featureList = new List<Tuple<string, Action<GameObject>>>();
        featureList.Add(new Tuple<string, Action<GameObject>>("LogOculusSDKInfo", LogOculusSDKInfo)); 
        featureList.Add(new Tuple<string, Action<GameObject>>("LogDeviceInfo", LogDeviceInfo));
        featureList.Add(new Tuple<string, Action<GameObject>>("ThreadingTest", ThreadingTest));
        featureList.Add(new Tuple<string, Action<GameObject>>("QueryThreadNumberCount", QueryThreadNumberCount));
        featureList.Add(new Tuple<string, Action<GameObject>>("StopTheThread", StopTheThread));
        featureList.Add(new Tuple<string, Action<GameObject>>("WriteALargeFileToDisk", WriteALargeFileToDisk));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "FileRWTest_InPersistentDataPath", FileRWTest_InPersistentDataPath));
        featureList.Add(new Tuple<string, Action<GameObject>>(
            "FileRWTest_ExternalStorage", FileRWTest_ExternalStorage));
        featureList.Add(new Tuple<string, Action<GameObject>>("LoggingTest", LoggingTest));
        featureList.Add(new Tuple<string, Action<GameObject>>("DeleteALLTracedData", DeleteALLTracedData));

        // Instantiate buttons 
        foreach (var t in featureList ){
            GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), Content.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = t.Item1;
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { t.Item2(ui); });
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void LogUserInfo(GameObject ui)
    {
        // toggle the ui  
        //bool isActive = ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf;
        //ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!isActive);

        //

        //LogMe("==========User Info===");
        //if (MetaUserManager.user != null)
        //{
        //    LogMe("MetaUserManager.user.ID = " + MetaUserManager.user.ID);
        //    LogMe("MetaUserManager.user.DisplayName = " + MetaUserManager.user.DisplayName);
        //    LogMe("MetaUserManager.user.ImageURL = " + MetaUserManager.user.ImageURL);
        //    LogMe("MetaUserManager.user.InviteToken = " + MetaUserManager.user.InviteToken);
        //    LogMe("MetaUserManager.user.Presence = " + MetaUserManager.user.Presence);
        //    LogMe("MetaUserManager.user.ToString() = " + MetaUserManager.user.ToString());
        //}
        //else
        //{
        //    LogMe("User Info Query Failed! Check Your Login. ");
        //}

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void LogDeviceInfo(GameObject ui)
    {
        LogMe("==========Camera Devices===");
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
            LogMe("Camera Devices[" + i + "].name = " + devices[i].name);
        LogMe("==========SharpPcap Info===");
        LogSharpPcapInfo();

        //
        LogMe("DotNetVersion = "  + Environment.Version.ToString());
        LogMe("Environment.UserName = " + Environment.UserName);
        LogMe("Environment.MachineName = " + Environment.MachineName);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void LogOculusSDKInfo(GameObject ui)
    {
        LogMe("==========OculusSDK Info===");
        // toggle the ui  
        //bool isActive = ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf;
        //ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!isActive);

        /*
         CameraStatus
         */

        // version 
        LogMe("OVRPlugin.version.ToString()= " + OVRPlugin.version.ToString());
        LogMe("OVRPlugin.nativeXrApi.ToString()= " + OVRPlugin.nativeXrApi.ToString());

        // camera 
        LogMe("OVRPlugin.GetExternalCameraCount()= " + OVRPlugin.GetExternalCameraCount());
        LogMe("OVRPlugin.Tracker.Count= " + OVRPlugin.Tracker.Count);

        // cpu gpu info
        float? floatValue;
        int? intValue;
        floatValue = OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics.App_CpuTime_Float);
        LogMe("App_CpuTime_Float= " + floatValue.GetValueOrDefault());
        floatValue = OVRPlugin.GetPerfMetricsFloat(OVRPlugin.PerfMetrics.App_GpuTime_Float);
        LogMe("App_GpuTime_Float= " + floatValue.GetValueOrDefault());

        // hand 
        OVRPlugin.Skeleton skel_LH;
        OVRPlugin.GetSkeleton(OVRPlugin.SkeletonType.HandLeft, out skel_LH);
        LogMe("skel_LH.NumBones= " + skel_LH.NumBones);



    }

    int TN = 0;
    void ThreadCode()
    {
        while (true)
        {
            TN++;
        }
    }

    Thread t;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void ThreadingTest(GameObject ui)
    {
        LogMe("Performing Threading Test! ");
        t = new Thread(ThreadCode);
        t.Start();
        LogMe("ThreadState: Running = 0," +
            "StopRequested = 1," +
            "SuspendRequested = 2," +
            "Background = 4," +
            "Unstarted = 8," +
            "Stopped = 16, " +
            "WaitSleepJoin = 32," +
            "Suspended = 64," +
            "AbortRequested = 128," +
            "Aborted = 256");
        LogMe("t.ThreadState.ToString() = " + t.ThreadState.ToString());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void QueryThreadNumberCount(GameObject ui)
    {
        LogMe("t.ThreadState.ToString() = " + t.ThreadState.ToString());
        LogMe("TN = " + TN);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void StopTheThread(GameObject ui)
    {
        t.Abort();
    }

    Stopwatch sw1 = new Stopwatch();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="bytes"></param>
    void DirectWrite(string path, byte[] bytes)
    {
        sw1.Restart();
        using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write))
        {
            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
            stream.Close();
        }
        sw1.Stop();
        var s1 = sw1.Elapsed;
        LogMe("s1.TotalMilliseconds = " + s1.TotalMilliseconds);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="bytes"></param>
    /// <param name="numbytes"></param>
    /// <returns></returns>
    IEnumerator SyncWrite(string path, byte[] bytes, ulong numbytes)
    {
        FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write);
        stream.Write(bytes, 0, bytes.Length);
        //yield return new WaitForSeconds(10.0f/72); // 10 frames
        sw1.Stop();
        var s1 = sw1.Elapsed;
        LogMe("s1.TotalMilliseconds = " + s1.TotalMilliseconds);
        yield return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="bytes"></param>
    /// <param name="numbytes"></param>
    /// <returns></returns>
    IEnumerator SyncWriteV2(string path, byte[] bytes, ulong numbytes)
    {

        ulong ChunkSize = 1024 * 1024; // 1MB
        ulong NumChunks = numbytes / ChunkSize;
        if (numbytes % ChunkSize > 0) NumChunks++;
        LogMe("ChunkSize = " + ChunkSize);
        LogMe("NumChunks = " + NumChunks);

        FileStream stream = new FileStream(path, FileMode.Append, FileAccess.Write);
        if(NumChunks == 1)
            stream.Write(bytes, 0, bytes.Length);
        else
            stream.Write(bytes, 0, (int)ChunkSize);
        LogMe("writing, yield for few frames ... ");
        yield return new WaitForSeconds(10.0f / 72); // 10 frames

        //
        if (NumChunks > 1)
        {
            for (ulong cid = 1; cid < NumChunks - 1; cid++)
            {
                ulong offset = cid * ChunkSize;
                stream.Write(bytes, (int)offset, (int)ChunkSize);
                LogMe("writing, yield for few frames ... ");
                yield return new WaitForSeconds(10.0f / 72); // 10 frames
            }

            //
            stream.Write(bytes, (int)(ChunkSize * (NumChunks - 1)), (int)(numbytes % ChunkSize));
        }

        sw1.Stop();
        var s1 = sw1.Elapsed;
        LogMe("s1.TotalMilliseconds = " + s1.TotalMilliseconds);
        yield return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void WriteALargeFileToDisk(GameObject ui)
    {
        string path = Application.persistentDataPath + "/IXSpaceData/Record_0524_1233_53.MP4";
        //ulong numbytes = 1024 * 1024 * 51; // 50 MB
        //byte[] bytes = new byte[numbytes]; 
        //for (ulong i = 0; i < numbytes; i++)
        //    bytes[i] = (byte)(i % 255);
        var fileinfo = new System.IO.FileInfo(path);
        byte[] bytes = File.ReadAllBytes(path);
        ulong numbytes = (ulong)fileinfo.Length;

        string writepath = Application.persistentDataPath + "/Out_Record_0524_1233_53.MP4";

        sw1.Restart();
        StartCoroutine(SyncWriteV2(writepath, bytes, numbytes));
    }

    /// <summary>
    /// 
    /// </summary>
    void LogSharpPcapInfo()
    {
        //var ver = Pcap.SharpPcapVersion;
        //LogMe("SharpPcap Version:" + ver);

        //// Retrieve the device list
        //var devices = LibPcapLiveDeviceList.Instance;

        //// If no devices were found print an error
        //if (devices.Count < 1)
        //{
        //    Console.WriteLine("No devices were found on this machine");
        //    return;
        //}

        //LogMe("The following devices are available on this machine:");

        //int i = 0;

        //// Print out the available devices
        //foreach (var dev in devices)
        //{
        //    //Console.WriteLine("{0}) {1} {2}", );
        //    LogMe("Device " + i + ": "+ dev.Name + " dev.Description: " + dev.Description);
        //    i++;
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void FileRWTest_InPersistentDataPath(GameObject ui)
    {
        string fpath = Application.persistentDataPath +
            GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().RelativeDataFolderPath;

        LogMe("fpath = " + fpath);

        if(!System.IO.Directory.Exists(fpath))
            System.IO.Directory.CreateDirectory(fpath);

        string path = fpath + "/Test.txt";

        string content = "Test Writing Content!";
        LogMe("Data Write = " + content);
        StreamWriter sw = new StreamWriter(path);
        sw.WriteLine(content);
        sw.Flush();
        sw.Close();


        StreamReader sr = new StreamReader(path);
        string readback = sr.ReadLine();
        LogMe("Data Readback = " + readback);
        if (readback.Equals(content))
        {
            LogMe("Writing Test Success!");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void FileRWTest_ExternalStorage(GameObject ui)
    {
        string fpath = Application.persistentDataPath + "/../../";
        //Application.persistentDataPath + GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().RelativeDataFolderPath;

        // List File Test
        ixUtility.LogMe("Files in folder: " + fpath);
        var info = new DirectoryInfo(fpath);
        var fileInfo = info.GetFiles();
        foreach (var file in fileInfo) 
            ixUtility.LogMe("file: " + file.FullName);

        // RW test 
        if (!System.IO.Directory.Exists(fpath))
            System.IO.Directory.CreateDirectory(fpath);
        string path = fpath + "/Test.txt";
        string content = "Test Writing Content!";
        LogMe("Data Write = " + content);
        StreamWriter sw = new StreamWriter(path);
        sw.WriteLine(content);
        sw.Flush();
        sw.Close();


        StreamReader sr = new StreamReader(path);
        string readback = sr.ReadLine();
        LogMe("Data Readback = " + readback);
        if (readback.Equals(content))
        {
            LogMe("Writing Test Success!");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void LoggingTest(GameObject ui)
    {
        ixLogger.LogMe("Logging From Static ixLogger class!");
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    void DeleteALLTracedData(GameObject ui)
    {
        ixUtility.DeleteDirectory(Application.persistentDataPath + "/CollectedData/");
        ixUtility.DeleteDirectory(Application.persistentDataPath + "/Evaluations/");

        ixLogger.LogMe("Files in folder CollectedData/ and Evaluations/ deleted!");
    }
}
