// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class SceneConfigure : MonoBehaviour
{
    public GameObject emptySceneObjects; // 0
    public GameObject passthroughSceneObjects; // 1
    public GameObject foyerSceneObjects; //2
    public GameObject room2067SceneObjects; //3 

    int lastScene = 0;
    // Start is called before the first frame update
    void Start()
    {
        passthroughSceneObjects.SetActive(true);
        lastScene = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CallExperiencePanel()
    {
        if (GameObject.FindGameObjectWithTag("ExperiencePanel"))
        {
            GameObject.FindGameObjectWithTag("ExperiencePanel").SetActive(true);
            GameObject.FindGameObjectWithTag("ExperiencePanel").GetComponent<ComeInFrontOfMe>().comeToMe();
        }
    }

    [PunRPC]
    public void configureSceneSync(int which)
    {
        if (which == 0) // empty 
        {
            if (passthroughSceneObjects) passthroughSceneObjects.SetActive(false);
            if (emptySceneObjects) emptySceneObjects.SetActive(true);
            if (foyerSceneObjects) foyerSceneObjects.SetActive(false);
            if (room2067SceneObjects) room2067SceneObjects.SetActive(false);

            if (GameObject.FindGameObjectWithTag("Player"))
                GameObject.FindGameObjectWithTag("Player").GetComponent<OVRPassthroughLayer>().textureOpacity = 0;

            if (GameObject.FindGameObjectWithTag("MainCamera"))
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<UniversalAdditionalCameraData>().renderPostProcessing = true;
        }
        if (which == 1) // passthrough 
        {
            if (passthroughSceneObjects) passthroughSceneObjects.SetActive(true);
            if (emptySceneObjects) emptySceneObjects.SetActive(false);
            if (foyerSceneObjects) foyerSceneObjects.SetActive(false);
            if (room2067SceneObjects) room2067SceneObjects.SetActive(false);

            if (GameObject.FindGameObjectWithTag("Player"))
                GameObject.FindGameObjectWithTag("Player").GetComponent<OVRPassthroughLayer>().textureOpacity = 0.2f;

            if (GameObject.FindGameObjectWithTag("MainCamera"))
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<UniversalAdditionalCameraData>().renderPostProcessing = false;
        }
        if (which == 2) // virtual apb
        {
            foyerSceneObjects.SetActive(true);
            emptySceneObjects.SetActive(false);
            passthroughSceneObjects.SetActive(false);
            room2067SceneObjects.SetActive(false);

            if (GameObject.FindGameObjectWithTag("Player"))
                GameObject.FindGameObjectWithTag("Player").GetComponent<OVRPassthroughLayer>().textureOpacity = 0;
        }
        if (which == 3) // room2067SceneObjects
        {
            foyerSceneObjects.SetActive(false);
            emptySceneObjects.SetActive(false);
            passthroughSceneObjects.SetActive(false);
            room2067SceneObjects.SetActive(true);

            if (GameObject.FindGameObjectWithTag("Player"))
                GameObject.FindGameObjectWithTag("Player").GetComponent<OVRPassthroughLayer>().textureOpacity = 0;
        }

        lastScene = which;
    }
    public void configureScene(int which)
    {
        this.GetComponent<PhotonView>().RPC("configureSceneSync", RpcTarget.All, which);
    }

    public void disablePassthroughAndBackToLastScene()
    {
        configureScene(lastScene);
    }
}
