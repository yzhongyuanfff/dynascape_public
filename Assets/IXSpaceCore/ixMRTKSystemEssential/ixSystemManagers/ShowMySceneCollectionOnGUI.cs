// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShowMySceneCollectionOnGUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //gameObject.SetActive(false);
        //this.transform.Find("Content").gameObject.SetActive(false);
    }

    public void showMySceneOnGUI(GameObject parent)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UITextedBtnPrefeb", typeof(GameObject)),
            parent.transform);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = gameObject.name;
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { onBtnClicked(ui); });
        ui.transform.Find("SpawnBtn").gameObject.SetActive(false); // disable the spawn button 
           // GetComponent<Button>().onClick.AddListener(delegate { onSpawnBtnClicked(); });
    }

    void onBtnClicked(GameObject ui)
    {
        //
        bool isActive = ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf;
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!isActive);

        //
        onBtnClickedRPC(!isActive);
        this.GetComponent<PhotonView>().RPC("onBtnClickedRPC", RpcTarget.Others, !isActive);
    }

    [PunRPC]
    void onBtnClickedRPC(bool DoLoad)
    {
        if (DoLoad)
        {
            foreach(Transform c in transform)
            {
                SceneManager.LoadSceneAsync(c.name, LoadSceneMode.Additive).completed += UpdateCalibrationAcrossScene;
            }
        }
        if(!DoLoad)
        {
            foreach (Transform c in transform)
            {
                SceneManager.UnloadSceneAsync(c.name);
            }
        }
    }

    void UpdateCalibrationAcrossScene(AsyncOperation obj)
    {
        Vector3 p = GameObject.FindGameObjectWithTag("ExperienceOrigin").transform.position;
        Quaternion q = GameObject.FindGameObjectWithTag("ExperienceOrigin").transform.rotation;

        // update sub scenes 
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("SceneRoot"))
        {
            o.transform.position = p;
            o.transform.rotation = q;
        }
    }
}
