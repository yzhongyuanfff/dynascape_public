using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class ixGlobalSettingsManager : MonoBehaviour
{
    [Header("----------")]
    [SerializeField] GameObject UIContent;
    [SerializeField] ixCamCapManager camCapManager;
    [SerializeField] GameObject DefaultDirectionalLights;
    [SerializeField] GameObject PointLightAtHead;

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool PostProcessingAllowed = false;

    void InitUI()
    {
        if (UIContent)
        {
            {
                ixUtility.MakeAButton(UIContent, 
                    "Toggle Post Processing Effect", delegate {
                        PostProcessingAllowed = !PostProcessingAllowed;

                        UniversalAdditionalCameraData cameraData = Camera.main.GetComponent<Camera>().GetUniversalAdditionalCameraData();
                        cameraData.renderPostProcessing = PostProcessingAllowed;
                    });
            }
            {
                ixUtility.MakeAButton(UIContent,
                    "Toggle Default Directional Lights", delegate {
                        DefaultDirectionalLights.SetActive(!DefaultDirectionalLights.activeSelf);
                    });
            }
            {
                ixUtility.MakeAButton(UIContent,
                    "Toggle PointLight at Head", delegate {
                        PointLightAtHead.SetActive(!PointLightAtHead.activeSelf);
                    });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Smaller SceneGeometry";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    GameObject[] scenes = GameObject.FindGameObjectsWithTag("SceneGeometry");
                    for(int i = 0; i < scenes.Length; i++)
                    {
                        scenes[i].transform.localScale = scenes[i].transform.localScale / 2.0f;
                        Transform handle = scenes[i].transform.Find("Handle");
                        if (handle)
                        {
                            handle.transform.localScale = handle.transform.localScale * 2.0f;
                        }
                    }
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Larger SceneGeometry";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    GameObject[] scenes = GameObject.FindGameObjectsWithTag("SceneGeometry");
                    for (int i = 0; i < scenes.Length; i++)
                    {
                        scenes[i].transform.localScale = scenes[i].transform.localScale * 2.0f;
                        Transform handle = scenes[i].transform.Find("Handle");
                        if (handle)
                        {
                            handle.transform.localScale = handle.transform.localScale / 2.0f;
                        }
                    }
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Hide All Handles";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    GameObject[] scenes = GameObject.FindGameObjectsWithTag("SceneGeometry");
                    for (int i = 0; i < scenes.Length; i++)
                    {
                        GameObject handle = scenes[i].transform.Find("Handle").gameObject;
                        if (handle)
                        {
                            handle.SetActive(false);
                        }
                    }
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Show All Handles";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    GameObject[] scenes = GameObject.FindGameObjectsWithTag("SceneGeometry");
                    for (int i = 0; i < scenes.Length; i++)
                    {
                        GameObject handle = scenes[i].transform.Find("Handle").gameObject;
                        if (handle)
                        {
                            handle.SetActive(true);
                        }
                    }
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Toggle Frame Sending";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    camCapManager.ToggleFrameSending();
                    ixUtility.LogMe("Sending State:" + camCapManager.GetFrameSendingState());
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Resize Frame To 480p";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    camCapManager.UpdateFrameDimention(Resolution.Res480p);
                    ixUtility.LogMe("Sending Frame with Resolution.Res480p");
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Resize Frame To Res720p";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    camCapManager.UpdateFrameDimention(Resolution.Res720p);
                    ixUtility.LogMe("Sending Frame with Resolution.Res720p");
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Resize Frame To Res1080p";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    camCapManager.UpdateFrameDimention(Resolution.Res1080p);
                    ixUtility.LogMe("Sending Frame with Resolution.Res1080p");
                });
            }
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Resize Frame To Res1440p";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    camCapManager.UpdateFrameDimention(Resolution.Res1440p);
                    ixUtility.LogMe("Sending Frame with Resolution.Res1440p");
                });
            }
        }
    }
}
