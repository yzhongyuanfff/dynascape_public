// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ixApplicationManager : MonoBehaviour
{
    public GameObject QuitPanel;
    public List<GameObject> ToBeDisabledToShowTheQuitPanel;

    /// <summary>
    /// 
    /// </summary>
    public void CallQuitPanel()
    {
        if(QuitPanel && QuitPanel.GetComponent<ComeInFrontOfMe>())
        {
            QuitPanel.SetActive(true);
            QuitPanel.GetComponent<ComeInFrontOfMe>().comeToMe();
            foreach (GameObject o in ToBeDisabledToShowTheQuitPanel)
            {
                o.SetActive(false);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void ResumeToApp()
    {
        if (QuitPanel) QuitPanel.SetActive(false);
        foreach (GameObject o in ToBeDisabledToShowTheQuitPanel)
        {
            o.SetActive(true);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void QuitApp()
    {
        Application.Quit();
    }

    // Start is called before the first frame update
    void Start()
    {
        QuitPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
