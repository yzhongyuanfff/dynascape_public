using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugInfoManager : MonoBehaviour
{
    TextMeshProUGUI debugInfo;
    string lastText;
    int line = 0;

    // Start is called before the first frame update
    void Start()
    {
        debugInfo = GetComponent<TextMeshProUGUI>();
    }

    public void AppendText(string newtext)
    {
        if (debugInfo)
        {
            lastText = debugInfo.text;
            debugInfo.text = System.DateTime.Now.ToString() + ": " + newtext + "\n" + debugInfo.text;
            line++;
            Vector2 rt = debugInfo.GetComponent<RectTransform>().sizeDelta;
            rt = new Vector2(rt.x, rt.y + 30);
            debugInfo.GetComponent<RectTransform>().sizeDelta = rt;
        }
    }

    public void GoBackToLastText()
    {
        debugInfo.text = lastText;
    }

    public void ReplaceText(string newtext)
    {
        lastText = debugInfo.text;
        debugInfo.text = newtext;
    }

    public void ClearText()
    {
        lastText = debugInfo.text;
        debugInfo.text = "";
    }
}
