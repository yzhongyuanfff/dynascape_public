using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ixExperiencePanelConfigurator : MonoBehaviour
{
    // ref
    public TextMeshPro title;

    // assign
    public string Title;

    // Start is called before the first frame update
    void Start()
    {
        title.text = Title;
        //SceneManager.GetActiveScene
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
