// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperiencesConfiger : MonoBehaviour
{
    [SerializeField] GameObject MainPanelContent;

    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform c in this.transform)
        {
            ShowMeOnGUI smUI = c.GetComponent<ShowMeOnGUI>();
            if (smUI) smUI.showMeOnGUI(MainPanelContent);
            ShowSepLineOnGUI sspUI = c.GetComponent<ShowSepLineOnGUI>();
            if (sspUI)
            {
                sspUI.showLineOnGUI(MainPanelContent);
                foreach (Transform c1 in sspUI.transform)
                {
                    ShowMySceneOnGUI ssogui1 = c1.GetComponent<ShowMySceneOnGUI>();
                    if (ssogui1) ssogui1.showMySceneOnGUI(MainPanelContent);

                    ShowMySceneCollectionOnGUI scgui = c1.GetComponent<ShowMySceneCollectionOnGUI>();
                    if (scgui) scgui.showMySceneOnGUI(MainPanelContent);
                }
            }
            ShowMySceneOnGUI ssogui = c.GetComponent<ShowMySceneOnGUI>();
            if (ssogui) ssogui.showMySceneOnGUI(MainPanelContent);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
