using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ixLightManager : MonoBehaviour
{
    List<GameObject> lights = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// 
    /// </summary>
    public void RemoveAllPointLights()
    {
        foreach(var l in lights)
        {
            Destroy(l);
        }
        lights.Clear();
    }

    /// <summary>
    /// this will generate a point light at right hand 
    /// </summary>
    public void CreatePointLight()
    {
        GameObject pl = Instantiate((GameObject)Resources.Load("PointLightPrefeb", typeof(GameObject)), this.transform);
        GameObject rhand = GameObject.FindGameObjectWithTag("RHand");
        pl.transform.position = rhand.transform.position;
        lights.Add(pl);
    }
}
