// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class StringManager : MonoBehaviour
{
    public string serverAddress { set; get; }
    public string filePathForScenes { set; get; }
    public string curActiveFilePath { set; get; } // full file path 
    public string curFolderPath { set; get; }
    public string curFileName { set; get; }

    public string RelativeDataFolderPath = "/Data/";

    public string FilePostServerPath = "http://141.76.64.17:5000/fileUpload";

    public string FileListServerPath = "http://141.76.64.17:5000/files";

    string lastServerIP;

    public int FrameIntervalCoroutine = 30;

    public ulong ChunkSize = 512 * 1024; // 512k

    public float ActiveScalingFactor = 0.1f;
    public Vector3 ActivePOIPosition;
    public Quaternion ActivePOIRotation;

    public Vector3 ActiveBranchingPointPosition;
    public Quaternion ActiveBranchingPointRotation;

    public void SetLastServerIP()
    {
        PlayerPrefs.SetString("LastServerIP", lastServerIP);
    }

    public string GetLastServerIP()
    {
        return PlayerPrefs.GetString("LastServerIP");
    }

    public void SetCurActiveFilePath(string s)
    {
        curActiveFilePath = s;
        PlayerPrefs.SetString("curActiveFilePath", s);
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("curActiveFilePath"))
        {
            curActiveFilePath = PlayerPrefs.GetString("curActiveFilePath");
            ixLogger.LogMe("Reading string from PlayerPrefs: set curActiveFilePath to: " + curActiveFilePath);
        }
    }
}
