// ------------------------------------------------------------------------------------
// <copyright company="Technische Universitšt Dresden">
//      Copyright (c) Technische Universitšt Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using ExitGames.Client.Photon;
using JfranMora.Inspector;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

/*
    Add a new remote control function:
    1. add event code 
    2. add sender function
    3. port sender function to UI 
    4. server side receive code 
    5. call server side API
 */

/// <summary>
/// Basically used to send/ receive message bet. users, store client/server relations etc.
/// </summary>
public class ixMessageManager : MonoBehaviourPunCallbacks, IOnEventCallback
{
    // Add 1.
    enum Event
    {
        CheckInfo,
        InfoBack,
        LoadSender,
        PlayURL,
        PauseContinuePlay, 
        UpdateSpeed,
        UpdatePercentage
    }

    enum SupportedPlatforms
    {
        Win, 
        Andriod, 
        Linux,
        MacOS
    }

    bool IsWindowsServer = true;

    [Button]
    public void RemoteEvent_CheckServerClusterInfo(GameObject ui)
    {
        RaiseRemoteEvent(-1, (byte)Event.CheckInfo, "");
    }

    [Button]
    public void RemoteEvent_LoadNDISender(GameObject ui)
    {
        RaiseRemoteEvent(-1, (byte)Event.LoadSender, ""); 
    }

    [Button]
    public void RemoteEvent_PlayURLOnNDISneder(GameObject ui)
    {
        string activeUrl = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
        RaiseRemoteEvent(-1, (byte)Event.PlayURL, activeUrl);
    }

    public void RemoteEvent_PlayWebcamNDISneder(GameObject ui)
    {
        RaiseRemoteEvent(-1, (byte)Event.PlayURL, "Webcam");
    }

    [Button]
    public void LogPhotonUserInfo()
    {
        ixUtility.LogMe("PhotonNetwork.LocalPlayer.ActorNumber = " + PhotonNetwork.LocalPlayer.ActorNumber);
        ixUtility.LogMe("PhotonNetwork.LocalPlayer.IsMasterClient = " + PhotonNetwork.LocalPlayer.IsMasterClient);
        ixUtility.LogMe("PhotonNetwork.LocalPlayer.NickName = " + PhotonNetwork.LocalPlayer.NickName);
        ixUtility.LogMe("PhotonNetwork.LocalPlayer.UserId = " + PhotonNetwork.LocalPlayer.UserId);
        ixUtility.LogMe("PhotonNetwork.LocalPlayer.CustomProperties = " + PhotonNetwork.LocalPlayer.CustomProperties);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    // Add 2.
    public void RemoteEvent_PauseContinuePlay(GameObject ui)
    {
        RaiseRemoteEvent(-1, (byte)Event.PauseContinuePlay, "");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    public void RemoteEvent_UpdatePercentageTest_0per(GameObject ui)
    {
        RaiseRemoteEvent(-1, (byte)Event.UpdatePercentage, "0");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    public void RemoteEvent_UpdatePercentageTest_50per(GameObject ui)
    {
        RaiseRemoteEvent(-1, (byte)Event.UpdatePercentage, "0.5");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    public void RemoteEvent_UpdatePlaybackSpeed_Half(GameObject ui)
    {
        RaiseRemoteEvent(-1, (byte)Event.UpdateSpeed, "0.5");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ui"></param>
    public void RemoteEvent_UpdatePlaybackSpeed_Full(GameObject ui)
    {
        RaiseRemoteEvent(-1, (byte)Event.UpdateSpeed, "1");
    }

    /// <summary>
    /// Send a message to other players in the same room
    /// </summary>
    /// <param name="targetClientID"></param>
    /// <param name="eventCode"></param>
    /// <param name="parameter"></param>
    public void RaiseRemoteEvent(int targetClientID, byte eventCode, string parameter)
    {
        int sender = PhotonNetwork.LocalPlayer.ActorNumber;
        object[] data = new object[] { targetClientID, parameter, sender}; // Create the data to be sent
        RaiseEventOptions options = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // Specify the receivers for the message
        PhotonNetwork.RaiseEvent(eventCode, data, options, SendOptions.SendReliable);

        ixUtility.LogMe("RaiseRemoteEvent Called");
    }

    /// <summary>
    /// Handle the message received from other players
    /// </summary>
    /// <param name="photonEvent"></param>
    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        // TEMP
        if (eventCode == (byte)Event.UpdatePercentage)
        {
            object[] data = (object[])photonEvent.CustomData;
            int targetClientID = (int)data[0];
            string parameter = (string)data[1];
            int senderID = (int)data[2];
            if ((targetClientID == PhotonNetwork.LocalPlayer.ActorNumber) || (targetClientID == -1))
            {
                if (IsSuitableServerPlatform()) // only apply on windows platforms now 
                {
                    //
                    Server_UpdateVideoPlayPercentage(float.Parse(parameter));

                    //
                    RaiseRemoteEvent(senderID, (byte)Event.InfoBack, "Server_UpdateVideoPlayPercentage Called On Server Side!");
                }
            }
        }


        // 
        if (eventCode == (byte)Event.UpdateSpeed)
        {
            object[] data = (object[])photonEvent.CustomData;
            int targetClientID = (int)data[0];
            string parameter = (string)data[1];
            int senderID = (int)data[2];
            if ((targetClientID == PhotonNetwork.LocalPlayer.ActorNumber) || (targetClientID == -1))
            {
                if (IsSuitableServerPlatform()) // only apply on windows platforms now 
                {
                    //
                    Server_UpdateVideoplaybackSpeed(float.Parse(parameter));

                    //
                    RaiseRemoteEvent(senderID, (byte)Event.InfoBack, "Server_UpdateVideoplaybackSpeed Called On Server Side!");
                }
            }
        }

        //
        if (eventCode == (byte)Event.LoadSender)
        { 
            object[] data = (object[])photonEvent.CustomData; 
            int targetClientID = (int)data[0]; 
            string parameter = (string)data[1];
            int senderID = (int)data[2];
            if ((targetClientID == PhotonNetwork.LocalPlayer.ActorNumber) || (targetClientID == -1))
            {
                if (IsSuitableServerPlatform()) // only apply on windows platforms now 
                {
                    // Perform operations on server here
                    ixUtility.LogMe("Instantiating NDISender!");


                    GlobalSenderID = senderID;

                    Server_InstantiateNDISender();
                }
            }
        }

        //
        if (eventCode == (byte)Event.PlayURL) 
        {
            object[] data = (object[])photonEvent.CustomData;
            int targetClientID = (int)data[0];
            string parameter = (string)data[1];
            int senderID = (int)data[2];
            if ((targetClientID == PhotonNetwork.LocalPlayer.ActorNumber) || (targetClientID == -1))
            {
                if (IsSuitableServerPlatform()) // only apply on windows platforms now 
                {
                    //
                    Server_PlayURL(senderID, parameter);

                    // 
                    RaiseRemoteEvent(senderID, (byte)Event.InfoBack, "Server_PlayURL Called On Server Side!");
                }
            }
        }

        //
        if (eventCode == (byte)Event.CheckInfo)
        {
            if (IsSuitableServerPlatform())
            {
                object[] data = (object[])photonEvent.CustomData;
                int senderID = (int)data[2];

                string serverInfo = "";
                serverInfo += "Server UserId = " + PhotonNetwork.LocalPlayer.UserId + "\n";
                serverInfo += "Server ActorNumber = " + PhotonNetwork.LocalPlayer.ActorNumber + "\n";

                ixUtility.IsServer = true;

                // send info back 
                RaiseRemoteEvent(senderID, (byte)Event.InfoBack, serverInfo);
            }
        }

        // 
        if (eventCode == (byte)Event.InfoBack)
        {
            object[] data = (object[])photonEvent.CustomData;
            int targetClientID = (int)data[0];
            string parameter = (string)data[1];

            if (targetClientID == PhotonNetwork.LocalPlayer.ActorNumber)
            {
                ixUtility.LogMe("Hear Back From Server: " + parameter);
            }
        }

        // Add 3.
        //
        if (eventCode == (byte)Event.PauseContinuePlay)
        {
            object[] data = (object[])photonEvent.CustomData;
            int targetClientID = (int)data[0];
            string parameter = (string)data[1];
            int senderID = (int)data[2];
            if ((targetClientID == PhotonNetwork.LocalPlayer.ActorNumber) || (targetClientID == -1))
            {
                if (IsSuitableServerPlatform()) // only apply on windows platforms now 
                {
                    // 
                    Server_PauseContinuePlay();

                    // call back event
                    RaiseRemoteEvent(senderID, (byte)Event.InfoBack, "Server_PauseContinuePlay Called On Server Side!");
                }
            }
        }
    }

    //
    bool IsSuitableServerPlatform()
    {
        SupportedPlatforms platform = SupportedPlatforms.Andriod;

        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.WindowsPlayer)
        {
            platform = SupportedPlatforms.Win;
        }

        else if (Application.platform == RuntimePlatform.Android)
        {
            platform = SupportedPlatforms.Andriod;
        }
        else
        {
            Debug.Log("Current platform is not Windows or Android.");
        }

        if (platform == SupportedPlatforms.Win)
            return true;
        else
            return false;
    }

    int GlobalSenderID;

    /// <summary>
    /// 
    /// </summary>
    void Server_InstantiateNDISender()
    {
        SceneManager.LoadSceneAsync("ixNDISender", LoadSceneMode.Additive).completed+= OnNDISenderLoaded;
    }
    void OnNDISenderLoaded(AsyncOperation obj)
    {
        RaiseRemoteEvent(GlobalSenderID, (byte)Event.InfoBack, "NDISender Loaded On Server!");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="url"></param>
    void Server_PlayURL(int senderID, string url)
    {
        GameObject sender = GameObject.FindGameObjectWithTag("NDISender");
        if (sender)
        {
            ixNDIController ndiController = GameObject.FindGameObjectWithTag("NDISender").GetComponent<ixNDIController>();
            if (url.Equals("Webcam"))
                ndiController.SetNdiTex_WebcamTex();
            else
                ndiController.SetNdiTex_VideoTexFromUrl(url);

            //
            RaiseRemoteEvent(senderID, (byte)Event.InfoBack, 
                "[Server INFO] Res = " + ndiController.RTToSend.width + " x " + ndiController.RTToSend.height);
        }
    }

    /// <summary>
    /// Add 4.
    /// </summary>
    void Server_PauseContinuePlay()
    {
        GameObject sender = GameObject.FindGameObjectWithTag("NDISender");
        if (sender)
        {
            ixVideoPlayerController videoController = 
                GameObject.FindGameObjectWithTag("NDISender").GetComponent<ixVideoPlayerController>();
            videoController.PauseContinueVideoPlayAndVFX();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="per"></param>
    void Server_UpdateVideoPlayPercentage(float per)
    {
        GameObject sender = GameObject.FindGameObjectWithTag("NDISender");
        if (sender)
        {
            ixVideoPlayerController videoController =
                GameObject.FindGameObjectWithTag("NDISender").GetComponent<ixVideoPlayerController>();
            videoController.UpdateVideoPlayPercentage(per);
        }
    }

    void Server_UpdateVideoplaybackSpeed(float speed)
    {
        GameObject sender = GameObject.FindGameObjectWithTag("NDISender");
        if (sender)
        {
            ixVideoPlayerController videoController =
                GameObject.FindGameObjectWithTag("NDISender").GetComponent<ixVideoPlayerController>();
            videoController.UpdateVideoPlaySpeed(speed);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
