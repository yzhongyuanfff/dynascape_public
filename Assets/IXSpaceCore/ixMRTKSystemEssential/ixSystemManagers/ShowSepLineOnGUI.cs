// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowSepLineOnGUI : MonoBehaviour
{
    public void showLineOnGUI(GameObject parent)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UISep", typeof(GameObject)),
            parent.transform);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text + this.gameObject.name +
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text;
    }
}
