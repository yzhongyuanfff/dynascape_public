using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExperienceLoadingHelper : MonoBehaviour
{

    public List<string> experienceNames;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void loadScene(int scene_i)
    {
        SceneManager.LoadScene(experienceNames[scene_i], LoadSceneMode.Additive);
    }

    public void unloadScene(int scene_i)
    {
        SceneManager.UnloadScene(experienceNames[scene_i]);
    }
}
