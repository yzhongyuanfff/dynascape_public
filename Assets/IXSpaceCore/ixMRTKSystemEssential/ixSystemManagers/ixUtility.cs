using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static UnityEngine.UI.Slider;

public static class ixUtility
{
    public static bool IsServer = false;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
	public static void LogMe(string msg)
	{
		if (GameObject.FindGameObjectWithTag("LogInfo"))
			GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
				.AppendText(msg);
	}

    public static void ToastMe(string msg)
    {
        GameObject toast = GameObject.Instantiate((GameObject)Resources.Load("TextedToast", typeof(GameObject)));
        toast.transform.Find("Text").GetComponent<TextMeshPro>().text = msg;
        toast.GetComponent<ComeInFrontOfMe>().comeToMe(0.6f);
        toast.GetComponent<ToastFading>().StartCountDown(4.0f);
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="target_dir"></param>
    public static void DeleteDirectory(string target_dir)
    {
        string[] files = Directory.GetFiles(target_dir);
        string[] dirs = Directory.GetDirectories(target_dir);

        foreach (string file in files)
        {
            File.SetAttributes(file, FileAttributes.Normal);
            try
            {
                File.Delete(file);
            }
            catch (IOException e)
            {
                ixLogger.LogMe("Ignore processing files and continue! File: " + file);
                continue;
            }
        }

        foreach (string dir in dirs)
        {
            DeleteDirectory(dir);
        }

        try
        {
            Directory.Delete(target_dir, false);
        }
        catch (IOException e)
        {
            ixLogger.LogMe("Giving up folder deletion");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static bool IsWinPlatform()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.WindowsPlayer)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum FilterOption
    {
        MP4,
        Bytes,
        MOV,
        PLY,
        CSV,
        TXT,
        WinAB,
        AndroidAB,
        ZIP,
        SelectedClips,
        Time,
        ALL
    }

    public enum VideoFormat
    {
        H264,
        HAP
    }

    public static FilterOption filterOption = FilterOption.ALL;

    static string ReadFileContent(string fpath)
    {
        StreamReader reader = new StreamReader(fpath);
        return reader.ReadToEnd();
    }

    //
    static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
    static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
    static char[] TRIM_CHARS = { '\"' };

    public static List<Dictionary<string, object>> ReadCSV(string file)
    {
        var list = new List<Dictionary<string, object>>();
        //TextAsset data = Resources.Load(file) as TextAsset;

        StreamReader reader = new StreamReader(file);

        var lines = Regex.Split(reader.ReadToEnd(), LINE_SPLIT_RE);

        if (lines.Length <= 1) return list;

        string[] header = Regex.Split(lines[0], SPLIT_RE);
        for (var i = 1; i < lines.Length; i++)
        {

            var values = Regex.Split(lines[i], SPLIT_RE);
            if (values.Length == 0 || values[0] == "") continue;

            var entry = new Dictionary<string, object>();
            for (var j = 0; j < header.Length && j < values.Length; j++)
            {
                string value = values[j];
                value = value.TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "");
                object finalvalue = value;
                int n;
                float f;
                if (int.TryParse(value, out n))
                {
                    finalvalue = n;
                }
                else if (float.TryParse(value, out f))
                {
                    finalvalue = f;
                }
                entry[header[j]] = finalvalue;
            }
            list.Add(entry);
        }

        return list;
    }

    /// <summary>
    ///  Assume csv file format: time, pointx, pointy, pointz, etc. 
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    public static List<Vector3> ReadPointsFromCsv(string file)
    {
        List<Vector3> pointList = new List<Vector3>();

        StreamReader reader = new StreamReader(file);
        var header = reader.ReadLine();
        while (!reader.EndOfStream) {
            var line = reader.ReadLine();
            var values = line.Split(',');

            Vector3 point = new Vector3(
                float.Parse(values[1].ToString()), 
                float.Parse(values[2].ToString()), 
                float.Parse(values[3].ToString()));

            pointList.Add(point);
        }

        return pointList;
    }

    static void ResetToggleState(GameObject ui)
    {
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// +Toggle
    public static void MakeAButton(GameObject parent, string text, UnityAction callback)
    {
        GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), parent.transform);
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = text;
        //ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
        //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(true);
        //    Thread.Sleep(10);
        //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        //});
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(callback);
    }

    public static GameObject MakeANegativeToggleButton(GameObject parent, string text, UnityAction callback)
    {
        GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), parent.transform);
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = text;
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
        {
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf);
        });
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(callback);
        return ui;
    }
    public static GameObject MakeAPositiveToggleButton(GameObject parent, string text, UnityAction callback)
    {
        GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), parent.transform);
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(true);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = text;
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
        {
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf);
        });
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(callback);
        return ui;
    }

    public static void MakeSepLine(GameObject parent, string text)
    {
        GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("UGUIText", typeof(GameObject)), parent.transform);
        ui.GetComponent<TextMeshProUGUI>().text = text;
    }

    public static void MakeASlider(GameObject parent, string text, UnityAction<float> onValueChange)
    {
        GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("UGUITextedSlider_DetailPanel", typeof(GameObject)), parent.transform);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = text;
        ui.transform.Find("Slider").GetComponent<Slider>().value = 0;
        ui.transform.Find("Slider").GetComponent<Slider>().onValueChanged.AddListener(onValueChange);
        ui.transform.Find("Slider").GetComponent<Slider>().onValueChanged.AddListener(delegate(float f) {
            float newValue = 100 * ui.transform.Find("Slider").GetComponent<Slider>().value;
            ui.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = newValue.ToString("F0");
            });
        ui.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = "0%";
    }
    public static void MakeASlider(GameObject parent, string text, Vector2 range, UnityAction<float> onValueChange)
    {
        GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("UGUITextedSlider_DetailPanel", typeof(GameObject)), parent.transform);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = text;
        ui.transform.Find("Slider").GetComponent<Slider>().value = 0;
        ui.transform.Find("Slider").GetComponent<Slider>().onValueChanged.AddListener(onValueChange);
        ui.transform.Find("Slider").GetComponent<Slider>().onValueChanged.AddListener(delegate (float f) {
            float scale = range.y - range.x;
            float newValue = range.x + scale * ui.transform.Find("Slider").GetComponent<Slider>().value;
            ui.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = newValue.ToString();
        });
        ui.transform.Find("Per").GetComponent<TextMeshProUGUI>().text = "-";
    }

    static void AlignOnce(GameObject parentObject, GameObject childObject, GameObject target)
    {
        Vector3 desiredPosition = target.transform.position;
        Quaternion desiredRotation = target.transform.rotation;

        // Determine the local position and rotation of the child object
        Vector3 childLocalPosition = childObject.transform.localPosition;
        Quaternion childLocalRotation = childObject.transform.localRotation;

        // Calculate the offset needed to align the child object
        Vector3 offset = desiredPosition - parentObject.transform.TransformPoint(childLocalPosition);
        Quaternion rotationOffset = Quaternion.Inverse(parentObject.transform.rotation) * desiredRotation * Quaternion.Inverse(childLocalRotation);

        // Apply the offset to the parent GameObject's position and rotation
        parentObject.transform.position += offset;
        parentObject.transform.rotation *= rotationOffset;
    }
    static void AlignOnce(GameObject parentObject, Vector3 childLocalPosition, Quaternion childLocalRotation, GameObject target)
    {
        Vector3 desiredPosition = target.transform.position;
        Quaternion desiredRotation = target.transform.rotation;

        //// Determine the local position and rotation of the child object
        //Vector3 childLocalPosition = childObject.transform.localPosition;
        //Quaternion childLocalRotation = childObject.transform.localRotation;

        // Calculate the offset needed to align the child object
        Vector3 offset = desiredPosition - parentObject.transform.TransformPoint(childLocalPosition);
        Quaternion rotationOffset = Quaternion.Inverse(parentObject.transform.rotation) * desiredRotation * Quaternion.Inverse(childLocalRotation);

        // Apply the offset to the parent GameObject's position and rotation
        parentObject.transform.position += offset;
        parentObject.transform.rotation *= rotationOffset;
    }
    static void AlignOnce(GameObject parentObject, Vector3 childLocalPosition, Quaternion childLocalRotation, 
        Vector3 desiredPosition, Quaternion desiredRotation)
    {
        //Vector3 desiredPosition = target.transform.position;
        //Quaternion desiredRotation = target.transform.rotation;

        //// Determine the local position and rotation of the child object
        //Vector3 childLocalPosition = childObject.transform.localPosition;
        //Quaternion childLocalRotation = childObject.transform.localRotation;

        // Calculate the offset needed to align the child object
        Vector3 offset = desiredPosition - parentObject.transform.TransformPoint(childLocalPosition);
        Quaternion rotationOffset = Quaternion.Inverse(parentObject.transform.rotation) * desiredRotation * Quaternion.Inverse(childLocalRotation);

        // Apply the offset to the parent GameObject's position and rotation
        parentObject.transform.position += offset;
        parentObject.transform.rotation *= rotationOffset;
    }
    static void AlignOnce_PositionOnly(GameObject parentObject, Vector3 childLocalPosition, Quaternion childLocalRotation,
        Vector3 desiredPosition, Quaternion desiredRotation)
    {
        //Vector3 desiredPosition = target.transform.position;
        //Quaternion desiredRotation = target.transform.rotation;

        //// Determine the local position and rotation of the child object
        //Vector3 childLocalPosition = childObject.transform.localPosition;
        //Quaternion childLocalRotation = childObject.transform.localRotation;

        // Calculate the offset needed to align the child object
        Vector3 offset = desiredPosition - parentObject.transform.TransformPoint(childLocalPosition);
        //Quaternion rotationOffset = Quaternion.Inverse(parentObject.transform.rotation) * desiredRotation * Quaternion.Inverse(childLocalRotation);

        // Apply the offset to the parent GameObject's position and rotation
        parentObject.transform.position += offset;
        //parentObject.transform.rotation *= rotationOffset;
    }

    public static void AlignGameObjectWithChild(GameObject parentObject, GameObject childObject, GameObject target)
    {
        AlignOnce(parentObject, childObject, target);
        AlignOnce(parentObject, childObject, target);
    }
    public static void AlignGameObjectWithChild(GameObject parentObject, Vector3 childLocalPosition, Quaternion childLocalRotation, GameObject target)
    {
        AlignOnce(parentObject, childLocalPosition, childLocalRotation, target);
        AlignOnce(parentObject, childLocalPosition, childLocalRotation, target);
    }
    public static void AlignGameObjectWithChild(GameObject parentObject, Vector3 childLocalPosition, Quaternion childLocalRotation, Vector3 targetGlobalPosition, Quaternion targetGlobalRotation)
    {
        AlignOnce(parentObject, childLocalPosition, childLocalRotation, targetGlobalPosition, targetGlobalRotation);
        AlignOnce(parentObject, childLocalPosition, childLocalRotation, targetGlobalPosition, targetGlobalRotation);
    }
    public static void AlignGameObjectWithChild_PositionOnly(GameObject parentObject, Vector3 childLocalPosition, Quaternion childLocalRotation, Vector3 targetGlobalPosition, Quaternion targetGlobalRotation)
    {
        AlignOnce_PositionOnly(parentObject, childLocalPosition, childLocalRotation, targetGlobalPosition, targetGlobalRotation);
        AlignOnce_PositionOnly(parentObject, childLocalPosition, childLocalRotation, targetGlobalPosition, targetGlobalRotation);
    }


    public static void RotateParentAroundCenterSoThatChildHitsPosition(GameObject parent, Vector3 center, Vector3 childposition, Vector3 targetposition)
    {
        // Calculate the direction from the center point to the target position
        Vector3 targetDirection = targetposition - center;

        // Rotate the parent object to align the child object with the target position
        parent.transform.rotation = Quaternion.LookRotation(targetDirection, Vector3.up);

        // Move the parent object so that the child object is at the target position
        parent.transform.position += targetposition - childposition;
    }
}
