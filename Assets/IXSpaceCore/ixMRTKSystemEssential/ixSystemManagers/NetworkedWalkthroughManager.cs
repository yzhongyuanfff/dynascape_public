using JfranMora.Inspector;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using Udar.SceneManager;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkedWalkthroughManager : MonoBehaviour
{
    [SerializeField] 
    List<GameObject> Prefebs;

    //[SerializeField] 
    List<GameObject> LoadedPrefebs = new List<GameObject>();

    [SerializeField]
    List<string> AssetBundles;

    [SerializeField] 
    List<SceneField> Scenes;

    //[SerializeField] 
    List<string> LoadedScenes = new List<string>();

    [SerializeField] int PrefebID = 0;
    [SerializeField] int BundleID = 0;
    [SerializeField] int SceneID = 0;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    [Button]
    void LoadPrefeb()
    {
        LoadPrefebSync(); // do local
        this.GetComponent<PhotonView>().RPC("LoadPrefebSync", RpcTarget.Others); // remote 
    }

    [PunRPC]
    void LoadPrefebSync()
    {
        //PrefebManager.LoadPrefebAsync(Prefebs[PrefebID], LoadPrefebMode.Additive);
        GameObject o = Instantiate(Prefebs[PrefebID], this.transform);
        LoadedPrefebs.Add(o);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    [Button]
    void LoadScene()
    {
        LoadSceneSync(); // do local
        this.GetComponent<PhotonView>().RPC("LoadSceneSync", RpcTarget.Others); // remote 
    }

    [PunRPC]
    void LoadSceneSync()
    {
        SceneManager.LoadSceneAsync(Scenes[SceneID].Name, LoadSceneMode.Additive);
        LoadedScenes.Add(Scenes[SceneID].Name);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    [Button]
    void LoadAssetBundle()
    {
        LoadAssetBundleSync(); // do local
        this.GetComponent<PhotonView>().RPC("LoadAssetBundleSync", RpcTarget.Others); // remote 
    }

    [PunRPC]
    void LoadAssetBundleSync()
    {
        StartCoroutine(LoadSceneFromBundle(AssetBundles[BundleID]));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    [Button]
    void UnLoadALLPrefeb()
    {
        UnLoadAllPrefebSync(); // do local
        this.GetComponent<PhotonView>().RPC("UnLoadAllPrefebSync", RpcTarget.Others); // remote 
    }

    void UnLoadAllPrefebSync()
    {
        //PrefebManager.UnloadPrefebAsync(Prefebs[PrefebID].Name);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator LoadSceneFromBundle(string fn)
    {
        AssetBundleCreateRequest asyncBundleRequest = AssetBundle.LoadFromFileAsync(fn);
        yield return asyncBundleRequest;

        AssetBundle localAssetBundle = asyncBundleRequest.assetBundle;

        if (localAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle! localAssetBundle = null");
            yield break;
        }

        if (localAssetBundle.isStreamedSceneAssetBundle)
        {
            string[] scenePaths = localAssetBundle.GetAllScenePaths();
            string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
            string loadSceneName = sceneName;
            Debug.Log("Loading Scene: " + sceneName);
            SceneManager.LoadSceneAsync(loadSceneName, LoadSceneMode.Additive);
        }

        localAssetBundle.Unload(false);
        Debug.Log("Scene Loaded!");
    }
}
