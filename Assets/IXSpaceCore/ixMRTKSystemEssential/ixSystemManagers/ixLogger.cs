using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ixLogger
{
	public static void LogMe(string msg)
	{
		if (GameObject.FindGameObjectWithTag("LogInfo"))
			GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
				.AppendText(msg);
    }
}
