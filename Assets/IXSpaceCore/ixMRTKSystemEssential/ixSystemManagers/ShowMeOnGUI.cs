// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShowMeOnGUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //gameObject.SetActive(false);
        this.transform.Find("Content").gameObject.SetActive(false);
    }

    public void showMeOnGUI(GameObject parent)
    {
        GameObject ui = Instantiate((GameObject)Resources.Load("UITextedBtnPrefeb", typeof(GameObject)), parent.transform);
        ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = gameObject.name;
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { onBtnClicked(ui); });
        ui.transform.Find("SpawnBtn").GetComponent<Button>().onClick.AddListener(delegate { onSpawnBtnClicked(); });
    }

    void onBtnClicked(GameObject ui)
    {
        onBtnClickedSync(ui);
        this.GetComponent<PhotonView>().RPC("onBtnClickedSync", RpcTarget.Others);
    }

    void onSpawnBtnClicked()
    {
        onSpawnBtnClickedSync();
        this.GetComponent<PhotonView>().RPC("onSpawnBtnClickedSync", RpcTarget.Others);
    }

    [PunRPC]
    void onBtnClickedSync(GameObject ui)
    {
        // toggle the ui  
        bool isActive = ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf;
        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!isActive);

        //
        this.transform.Find("Content").gameObject.SetActive(!this.transform.Find("Content").gameObject.activeSelf);
    }

    [PunRPC]
    void onSpawnBtnClickedSync()
    {
        Instantiate((GameObject)Resources.Load(gameObject.name, typeof(GameObject)));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
