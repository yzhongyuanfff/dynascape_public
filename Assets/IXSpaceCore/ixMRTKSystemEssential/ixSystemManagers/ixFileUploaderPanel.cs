// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using JfranMora.Inspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ixFileUploaderPanel : MonoBehaviour
{
    public GameObject parent;
    public Material ActiveMaterial;
    public Material InActiveMaterial;
    public string RelativeDataPath;
    List<GameObject> uiList;

    // Start is called before the first frame update
    void Start()
    {
        uiList = new List<GameObject>();
        //ListFiles();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnActiveClicked(GameObject this_ui, string fn)
    {
        foreach (GameObject ui in uiList)
            ui.transform.Find("ChkBtn").transform.Find("Back Plate").GetComponent<Image>().material = InActiveMaterial;
        this_ui.transform.Find("ChkBtn").transform.Find("Back Plate").GetComponent<Image>().material = ActiveMaterial;

    }

    [Button]
    public void ListFiles()
    {
        foreach (GameObject u in uiList)
            GameObject.Destroy(u);
        uiList.Clear();

        string FolderAddress = Application.persistentDataPath + "/" + RelativeDataPath;

        ixLogger.LogMe("Listing From: " + FolderAddress);

        foreach (string file in GetFiles(FolderAddress))
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("UIUploadBtnPrefeb", typeof(GameObject)), parent.transform);

            //ui.transform.Find("Btn").gameObject.SetActive(false);
            //ui.transform.Find("Placeholder").gameObject.SetActive(false);
            //ui.transform.Find("ChkBtn").gameObject.SetActive(true);
            //ui.transform.Find("DelBtn").gameObject.SetActive(false);
            //ui.transform.Find("Percentage").gameObject.SetActive(true);
            //ui.transform.Find("Size").gameObject.SetActive(true);

            ui.transform.GetComponent<ixFileUploader>().FileFullPath = file;
            ui.transform.GetComponent<ixFileUploader>().FileName = Path.GetFileName(file);

            //ui.transform.Find("ChkBtn").GetComponent<Button>().onClick.AddListener(delegate { OnActiveClicked(ui, file); });
            ui.transform.Find("FileLink").GetComponent<TextMeshProUGUI>().text = file;
            ui.transform.Find("Percentage").GetComponent<TextMeshProUGUI>().text = "0%";
            var fileinfo = new System.IO.FileInfo(file);
            float fileSizeInMB = fileinfo.Length / 1024.0f / 1024.0f;
            ui.transform.Find("Size").GetComponent<TextMeshProUGUI>().text = fileSizeInMB.ToString("F2") + " MB";

            uiList.Add(ui);
        }
    }

    static IEnumerable<string> GetFiles(string path)
    {
        Queue<string> queue = new Queue<string>();
        queue.Enqueue(path);
        while (queue.Count > 0)
        {
            path = queue.Dequeue();
            try
            {
                foreach (string subDir in Directory.GetDirectories(path))
                {
                    queue.Enqueue(subDir);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            string[] files = null;
            try
            {
                files = Directory.GetFiles(path);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            if (files != null)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    yield return files[i];
                }
            }
        }
    }

}
