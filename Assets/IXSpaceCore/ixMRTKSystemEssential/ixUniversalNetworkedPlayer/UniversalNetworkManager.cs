using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalNetworkManager : MonoBehaviourPunCallbacks
{
    public string JoinRoomName = "ixOculusMRDev";
    public string RemotePlayerObjectName = "RemotePlayer";

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to Master Server. \n");
        PhotonNetwork.JoinRoom(JoinRoomName);
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        float playerCount = PhotonNetwork.IsConnected 
            && PhotonNetwork.CurrentRoom != null ? PhotonNetwork.CurrentRoom.PlayerCount : 0;
        Debug.Log("Connected players : " + playerCount);
    }
    public override void OnJoinedRoom()
    {

        Debug.Log("Joined Room. Creating Remote Player Representation.");

        // Network Instantiate the object used to represent our player. This will have a View on it and represent the player         
        GameObject player = PhotonNetwork.Instantiate(RemotePlayerObjectName, new Vector3(0f, 0f, 0f), Quaternion.identity, 0);
        UNetworkPlayer np = player.GetComponent<UNetworkPlayer>();
        if (np)
        {
            np.transform.name = "MyRemotePlayer";
            np.AssignPlayerObjects();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Disconnected from PUN due to cause : " + cause);
        if (!PhotonNetwork.ReconnectAndRejoin())
        {
            Debug.Log("Reconnect and Joined.");
        }
        base.OnDisconnected(cause);
    }
}
