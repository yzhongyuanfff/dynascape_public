using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.UI;
using JfranMora.Inspector;

public class PhotonNetworkManager : MonoBehaviourPunCallbacks
{
    [SerializeField] GameObject LobbyUIContent;
    [SerializeField] string DefaultRoomName = "IXSpaceDefault";
    public bool bListUsers = false;

    [HideInInspector] public List<string> RoomList = new List<string>();
    [HideInInspector] public static PhotonNetworkManager instance;
    [HideInInspector] public UNetworkPlayer np;
    string RemotePlayerObjectName = "RemotePlayer";

    private void Awake()
    {
        RoomList.Add(DefaultRoomName);
        RoomList.Add("IXSpace_PointcloudMR");
        RoomList.Add("IXSpace_ViewR");
        RoomList.Add("IXSpace_EduMR");
        RoomList.Add("IXSpace_Robotron");
        CurrentRoomName = DefaultRoomName;
        InitUI();
    }

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();   // -> OnConnectedToMaster
    }
    public override void OnConnectedToMaster()
    {
        JoinRoom(CurrentRoomName);
    }

    void JoinRoom(string roomName)
    {
        PhotonNetwork.NickName = "GUEST " + Random.Range(1, 1000);
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.PublishUserId = true;
        roomOptions.IsVisible = false;
        roomOptions.MaxPlayers = 20;
        bool success = PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default); // -> OnJoinedRoom
        Debug.Log("connected to masterserver with " + success);
    }


    public override void OnJoinedRoom()
    {
        Debug.Log("Room Max number: " + PhotonNetwork.CurrentRoom.MaxPlayers);
        Player[] player = PhotonNetwork.PlayerList;

        for (int i = 0; i < player.Length; i++)
        {
            Debug.Log((i).ToString() + " : " + player[i].NickName + " ID = " + player[i].UserId);
        }


        // Network Instantiate the object used to represent our player. This will have a View on it and represent the player         
        GameObject p = PhotonNetwork.Instantiate(RemotePlayerObjectName, new Vector3(0f, 0f, 0f), Quaternion.identity, 0);
        np = p.GetComponent<UNetworkPlayer>();
        if (np)
        {
            np.transform.name = "MyRemotePlayer";
            np.AssignPlayerObjects();
        }

        //
        UpdateRoomInfoUI();
        ixUtility.LogMe("Joind Room: " + CurrentRoomName + ", Number Of Users Upon Join: " + (PhotonNetwork.PlayerList.Length - 1));
    }

    // Update is called once per frame
    void Update()
    {
        if (bListUsers)
        {
            bListUsers = false;
            foreach (var p in PhotonNetwork.PlayerList)
            {
                Debug.Log(p.UserId);
            }
        }
        if(np)
            if (!np.PlayerHeadTransform || !np.PlayerLeftHandTransform || !np.PlayerRightHandTransform)
                np.AssignPlayerObjects();
    }

    GameObject RoomInfoTextUI;
    string CurrentRoomName;

    void UpdateRoomInfoUI()
    {
        RoomInfoTextUI.GetComponent<TextMeshProUGUI>().text = "CurrentRoom: " + CurrentRoomName + ", Number Of Users Upon Join: " + (PhotonNetwork.PlayerList.Length - 1);
    }

    void InitUI()
    {
        //
        {
            RoomInfoTextUI = Instantiate((GameObject)Resources.Load("UGUIText", typeof(GameObject)), LobbyUIContent.transform);
            RoomInfoTextUI.GetComponent<TextMeshProUGUI>().text = "CurrentRoom: " + "-";
        }

        //
        foreach (string room in RoomList)
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), LobbyUIContent.transform);
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                "Room: " + room;
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
            {
                PhotonNetwork.LeaveRoom();
                CurrentRoomName = room;
                JoinRoom(room);
            });
        }
    }

    [Button]
    public void JoinRecordRoom()
    {
        PhotonNetwork.LeaveRoom();
        CurrentRoomName = "RecordRoom";
        JoinRoom("RecordRoom");
    }
}
