using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UNetworkPlayer : MonoBehaviourPunCallbacks, IPunObservable, IPunOwnershipCallbacks
{

    [Tooltip("Transform of the remote player's head. This will be updated during Update")]
    public Transform RemoteHeadTransform;

    // Store positions to move between updates
    private Vector3 _syncHeadStartPosition = Vector3.zero;
    private Vector3 _syncHeadEndPosition = Vector3.zero;
    private Quaternion _syncHeadStartRotation = Quaternion.identity;
    private Quaternion _syncHeadEndRotation = Quaternion.identity;

    [Tooltip("Transform of the remote player's left hand / controller. This will be updated during Update")]
    public Transform RemoteLeftHandTransform;

    // Store positions to move between updates
    private Vector3 _syncLHandStartPosition = Vector3.zero;
    private Vector3 _syncLHandEndPosition = Vector3.zero;
    private Quaternion _syncLHandStartRotation = Quaternion.identity;
    private Quaternion _syncLHandEndRotation = Quaternion.identity;

    [Tooltip("Transform of the remote player's right hand / controller. This will be updated during Update")]
    public Transform RemoteRightHandTransform;

    // Store positions to move between updates
    private Vector3 _syncRHandStartPosition = Vector3.zero;
    private Vector3 _syncRHandEndPosition = Vector3.zero;
    private Quaternion _syncRHandStartRotation = Quaternion.identity;
    private Quaternion _syncRHandEndRotation = Quaternion.identity;

    [Tooltip("Transform of the local player's head to track. This will be applied to the Remote Player's Head Transform")]
    public Transform PlayerHeadTransform;
    public Transform PlayerLeftHandTransform;
    public Transform PlayerRightHandTransform;
    public Transform ExperienceOrigin;

    bool disabledObjects = false;

    // Interpolation values
    private float _lastSynchronizationTime = 0f;
    private float _syncDelay = 0f;
    private float _syncTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        //Transform t = this.transform;
        //PlayerHeadTransform = t;
        //PlayerLeftHandTransform = t;
        //PlayerRightHandTransform = t;
    }

    // Update is called once per frame
    void Update()
    {
        // Remote Player
        if (!photonView.IsMine)
        {

            if (disabledObjects)
            {
                toggleObjects(true);
            }

            // Keeps latency in mind to keep player in sync
            _syncTime += Time.deltaTime;
            float synchValue = _syncTime / _syncDelay;

            // Update Head and Hands Positions
            updateRemotePositionRotation(RemoteHeadTransform, 
                _syncHeadStartPosition, _syncHeadEndPosition, 
                _syncHeadStartRotation, _syncHeadEndRotation, synchValue);
            updateRemotePositionRotation(RemoteLeftHandTransform, 
                _syncLHandStartPosition, _syncLHandEndPosition, 
                _syncLHandStartRotation, _syncLHandEndRotation, synchValue);
            updateRemotePositionRotation(RemoteRightHandTransform, 
                _syncRHandStartPosition, _syncRHandEndPosition, 
                _syncRHandStartRotation, _syncRHandEndRotation, synchValue);
        }else
        {
            if (!disabledObjects)
            {
                toggleObjects(false);
            }
        }
    }

    void updateRemotePositionRotation(Transform moveTransform, Vector3 startPosition, Vector3 endPosition, Quaternion syncStartRotation, Quaternion syncEndRotation, float syncValue)
    {
        float dist = Vector3.Distance(startPosition, endPosition);

        // If far away just teleport there
        if (dist > 0.5f)
        {
            moveTransform.position = endPosition;
            moveTransform.rotation = syncEndRotation;
        }
        else
        {
            moveTransform.position = Vector3.Lerp(startPosition, endPosition, syncValue);
            moveTransform.rotation = Quaternion.Lerp(syncStartRotation, syncEndRotation, syncValue);
        }
    }

    public void AssignPlayerObjects()
    {
        if (GameObject.FindGameObjectWithTag("MainCamera"))
        {
            PlayerHeadTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
            //Debug.Log("MainCamera Found!");
        }
        if (GameObject.FindGameObjectWithTag("LHand"))
        {
            PlayerLeftHandTransform = GameObject.FindGameObjectWithTag("LHand").transform;
            //Debug.Log("LHand Found!");
        }
        if (GameObject.FindGameObjectWithTag("RHand"))
        {
            PlayerRightHandTransform = GameObject.FindGameObjectWithTag("RHand").transform;
            //Debug.Log("RHand Found!");
        }
        if (GameObject.FindGameObjectWithTag("ExperienceOrigin"))
        {
            ExperienceOrigin = GameObject.FindGameObjectWithTag("ExperienceOrigin").transform;
            //Debug.Log("ExperienceOrigin Found!");
        }
    }

    void toggleObjects(bool enableObjects)
    {
        RemoteHeadTransform.gameObject.SetActive(enableObjects);
        RemoteLeftHandTransform.gameObject.SetActive(enableObjects);
        RemoteRightHandTransform.gameObject.SetActive(enableObjects);
        disabledObjects = !enableObjects;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (!ExperienceOrigin) 
            ExperienceOrigin = GameObject.FindGameObjectWithTag("ExperienceOrigin").transform;

        // This is our player, send our positions to the other players
        if (stream.IsWriting)
        {
            // Player Head / Hand Information
            if (PlayerHeadTransform)
            {
                Transform t = PlayerHeadTransform;
                Vector3 plocal = ExperienceOrigin.InverseTransformPoint(t.position);
                Quaternion rlocal = t.rotation * Quaternion.Inverse(ExperienceOrigin.rotation);
                stream.SendNext(plocal);
                stream.SendNext(rlocal);
            }
            else
            {
                stream.SendNext(Vector3.zero);
                stream.SendNext(Quaternion.identity);
            }
            if (PlayerLeftHandTransform)
            {
                Transform t = PlayerLeftHandTransform;
                Vector3 plocal = ExperienceOrigin.InverseTransformPoint(t.position);
                Quaternion rlocal = t.rotation * Quaternion.Inverse(ExperienceOrigin.rotation);
                stream.SendNext(plocal);
                stream.SendNext(rlocal);
            }
            else
            {
                stream.SendNext(Vector3.zero);
                stream.SendNext(Quaternion.identity);
            }
            if (PlayerRightHandTransform)
            {
                Transform t = PlayerRightHandTransform;
                Vector3 plocal = ExperienceOrigin.InverseTransformPoint(t.position);
                Quaternion rlocal = t.rotation * Quaternion.Inverse(ExperienceOrigin.rotation);
                stream.SendNext(plocal);
                stream.SendNext(rlocal);
            }
            else
            {
                stream.SendNext(Vector3.zero);
                stream.SendNext(Quaternion.identity);
            }
        }
        else
        {
            // Remote player, receive data
            // Head
            this._syncHeadStartPosition = RemoteHeadTransform.position;
            this._syncHeadEndPosition = ExperienceOrigin.TransformPoint((Vector3)stream.ReceiveNext());
            this._syncHeadStartRotation = RemoteHeadTransform.rotation;
            this._syncHeadEndRotation = ExperienceOrigin.rotation * (Quaternion)stream.ReceiveNext();

            // Left Hand
            this._syncLHandStartPosition = RemoteLeftHandTransform.position;
            this._syncLHandEndPosition = ExperienceOrigin.TransformPoint((Vector3)stream.ReceiveNext());
            this._syncLHandStartRotation = RemoteLeftHandTransform.rotation;
            this._syncLHandEndRotation = ExperienceOrigin.rotation * (Quaternion)stream.ReceiveNext();

            // Right Hand
            this._syncRHandStartPosition = RemoteRightHandTransform.position;
            this._syncRHandEndPosition = ExperienceOrigin.TransformPoint((Vector3)stream.ReceiveNext());
            this._syncRHandStartRotation = RemoteRightHandTransform.rotation;
            this._syncRHandEndRotation = ExperienceOrigin.rotation * (Quaternion)stream.ReceiveNext();
        }


        _syncTime = 0f;
        _syncDelay = Time.time - _lastSynchronizationTime;
        _lastSynchronizationTime = Time.time;
    }

    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
        //throw new System.NotImplementedException();
    }

    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {
        //throw new System.NotImplementedException();
    }

    public void OnOwnershipTransferFailed(PhotonView targetView, Player senderOfFailedRequest)
    {
        //throw new System.NotImplementedException();
    }
}
