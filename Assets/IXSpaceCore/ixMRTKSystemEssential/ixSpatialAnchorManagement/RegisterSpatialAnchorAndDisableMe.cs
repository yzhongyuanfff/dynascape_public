using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterSpatialAnchorAndDisableMe : MonoBehaviour
{
    public GameObject ixAnchorPrefeb;

    /// <summary>
    /// 
    /// </summary>
    public void register()
    {
        GameObject anchor = Instantiate(ixAnchorPrefeb, this.transform.position, this.transform.rotation); // rhand.transform.rotation
        anchor.GetComponent<Anchor>().OnSaveLocalButtonPressed();

        //spatialAnchors.Add(anchor);
        if (GameObject.FindGameObjectWithTag("SpatialAnchorManager") && GameObject.FindGameObjectWithTag("SpatialAnchorManager").GetComponent<SpatialAnchorManager>())
        {
            GameObject.FindGameObjectWithTag("SpatialAnchorManager").GetComponent<SpatialAnchorManager>().AnchorPreviews.Add(anchor);
        }

        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// 
    /// </summary>
    public void delete()
    {
        this.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
