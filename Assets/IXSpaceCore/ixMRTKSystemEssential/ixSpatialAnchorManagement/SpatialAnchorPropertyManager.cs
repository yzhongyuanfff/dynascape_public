using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnchorUsage
{
    Calibration,
    Snapping
}

public class SpatialAnchorPropertyManager : MonoBehaviour
{
    [HideInInspector] public bool IsActive = false;
    [HideInInspector] public bool IsSurface = false;
    public AnchorUsage anchorUsage = AnchorUsage.Calibration;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggleSpatialAnchorProperty()
    {
        IsActive = !IsActive;
    }

    public void setSpatialAnchorProperty(bool b)
    {
        IsActive = b;
    }

    /// <summary>
    /// 
    /// </summary>
    public void MarkMeAsAnchorNow()
    {
        GameObject.FindGameObjectWithTag("EvaluationManager")
            .GetComponent<ixEvaluationManager>().TraceAnchorNow(this.transform.position, this.transform.rotation);
    }


    /// <summary>
    /// 
    /// </summary>
    public void MarkMeAsAnchorRef()
    {
        GameObject.FindGameObjectWithTag("EvaluationManager")
            .GetComponent<ixEvaluationManager>().TraceAnchorRef(this.transform.position, this.transform.rotation);
    }
}
