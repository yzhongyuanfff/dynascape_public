using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ixAnchorHelper : MonoBehaviour
{
    public GameObject ixAnchorPreview;

    public void deregister()
    {
        delete();
        GameObject anchorPreview = Instantiate(ixAnchorPreview, 
            this.transform.position, this.transform.rotation); // rhand.transform.rotation
    }

    public void delete()
    {
        //TODO: spatialAnchors.remove(anchor);

        this.GetComponent<Anchor>().OnEraseButtonPressed();
        this.gameObject.SetActive(false);
    }

    public void Hide()
    {
        foreach(Transform t in this.transform)
        {
            t.gameObject.SetActive(false);
        }
    }

    public void Show()
    {
        foreach (Transform t in this.transform)
        {
            t.gameObject.SetActive(true);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
