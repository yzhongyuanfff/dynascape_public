using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterToManager : MonoBehaviour
{
    [SerializeField] bool HideAfterRegister = false;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Register", 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Register()
    {
        GameObject.FindGameObjectWithTag("SpatialAnchorManager").GetComponent<SpatialAnchorManager>().AnchorPreviews.Add(this.gameObject);

        //if(HideAfterRegister)
        //    this.GetComponent
    }
}
