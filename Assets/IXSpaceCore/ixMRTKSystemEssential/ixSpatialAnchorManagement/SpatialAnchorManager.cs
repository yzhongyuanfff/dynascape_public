using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;

public class SpatialAnchorManager : MonoBehaviour
{
    public ixMetaUserManager MetaUserManager;
    public SnapToTheNearestSpatialAnchor snapToTheNearestSpatialAnchor;
    public GameObject _anchorPrefab;
    public GameObject _anchorPreviewPrefeb;
    public string postUrl;
    public string uuidGetUrl;
    public bool fbPostToServer = false;
    public bool fbFetchUUidsFromServer = false;
    public bool KeepTryingLoadSpatialAnchors = true;
    public int LoadingInterval = 10; // in seconds 
    public int FirstLoadInterval = 3;
    public int TimesTryLoadSpatialAnchor = 4;
    [SerializeField] bool HideBydefault = false;

    public List<GameObject> spatialAnchors; // created Spatial Anchors 
    public List<GameObject> loadSpatialAnchorsCloud;
    public List<GameObject> loadSpatialAnchorsLocal;
    public List<GameObject> AnchorPreviews;

    GameObject rhand;
    JSONNode uuidListJsonNode;
    List<Guid> createdUuidList;
    JSONNode createdSpaceAndUuid;
    int curFrame = 0;
    int timesLoaded = 0;
    bool IsHidden = false;
    float timeShareBegin;
    float timeLoadBegin;
    bool ForceLoadSpatialAnchors = false;

    public List<GameObject> GetAllSpatialAnchorRefs()
    {
        List<GameObject> SAGameObjects = new List<GameObject>();

        foreach (var sa in spatialAnchors)
            SAGameObjects.Add(sa);
        foreach (var sa in loadSpatialAnchorsCloud)
            SAGameObjects.Add(sa);
        foreach (var sa in loadSpatialAnchorsLocal)
            SAGameObjects.Add(sa);
        foreach (var sa in AnchorPreviews)
            SAGameObjects.Add(sa);

        return SAGameObjects;
    }

    // Start is called before the first frame update
    void Start()
    {
        spatialAnchors = new List<GameObject>();
        loadSpatialAnchorsCloud = new List<GameObject>();
        loadSpatialAnchorsLocal = new List<GameObject>();
        AnchorPreviews = new List<GameObject>();
        createdUuidList = new List<Guid>();
        LoadSpatialAnchors();

        curFrame = LoadingInterval * 70 - FirstLoadInterval * 70;
        LoadingInterval = 70 * LoadingInterval;
    }


    // Update is called once per frame
    void Update()
    {
        if (fbPostToServer)
        {
            fbPostToServer = false;
            StartCoroutine(PostToServer());
        }

        if (fbFetchUUidsFromServer)
        {
            fbFetchUUidsFromServer = false;
            LoadAllSpatialAnchorsFromServer();
        }

        if(timesLoaded > TimesTryLoadSpatialAnchor && !ForceLoadSpatialAnchors)
        {
            KeepTryingLoadSpatialAnchors = false;
        }

        if(ForceLoadSpatialAnchors)
            KeepTryingLoadSpatialAnchors = true;

        if (KeepTryingLoadSpatialAnchors)
        {
            if (curFrame > LoadingInterval)
            {
                curFrame = 0;
                LoadSpatialAnchors();
                timesLoaded++;
            }
            curFrame++;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void HideShowAllSpatialAnchors()
    {
        if (IsHidden)
        {
            foreach (GameObject o in spatialAnchors)
            {
                ixAnchorHelper anchorhelper = o.GetComponent<ixAnchorHelper>();
                if (anchorhelper) anchorhelper.Show();
            }
            foreach (GameObject o in loadSpatialAnchorsLocal)
            {
                ixAnchorHelper anchorhelper = o.GetComponent<ixAnchorHelper>();
                if (anchorhelper) anchorhelper.Show();
            }
            foreach (GameObject o in loadSpatialAnchorsCloud)
            {
                ixAnchorHelper anchorhelper = o.GetComponent<ixAnchorHelper>();
                if (anchorhelper) anchorhelper.Show();
            }
        }
        else
        {
            foreach (GameObject o in spatialAnchors)
            {
                ixAnchorHelper anchorhelper = o.GetComponent<ixAnchorHelper>();
                if (anchorhelper) anchorhelper.Hide();
            }
            foreach (GameObject o in loadSpatialAnchorsLocal)
            {
                ixAnchorHelper anchorhelper = o.GetComponent<ixAnchorHelper>();
                if (anchorhelper) anchorhelper.Hide();
            }
            foreach (GameObject o in loadSpatialAnchorsCloud)
            {
                ixAnchorHelper anchorhelper = o.GetComponent<ixAnchorHelper>();
                if (anchorhelper) anchorhelper.Hide();
            }
        }
        IsHidden = !IsHidden;
    }

    /// <summary>
    /// 
    /// </summary>
    public void LoadAllSpatialAnchorsFromServer()
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText("Loading Spatial Anchors From Server... This can take a while...");

        timeLoadBegin = Time.time;

        StartCoroutine(GetUUidsAndLoadCloudSpatialAnchors(uuidGetUrl));
    }

    /// <summary>
    /// instantinate a _anchorPreviewPrefeb and move around 
    /// </summary>
    public void createSpatialAnchor()
    {
        if(!rhand) rhand = GameObject.FindGameObjectWithTag("RHand");
        GameObject anchorPreview;
        if (rhand)
            anchorPreview = Instantiate(_anchorPreviewPrefeb, rhand.transform.position, Quaternion.identity);
        else
            anchorPreview = Instantiate(_anchorPreviewPrefeb, Vector3.zero, Quaternion.identity);
        AnchorPreviews.Add(anchorPreview);
    }

    /// <summary>
    /// Anchors are not able to be changed after registered 
    /// </summary>
    public void registerSpatialAnchors()
    {
        foreach (GameObject anchorPreview in AnchorPreviews)
        {
            GameObject anchor = Instantiate(_anchorPrefab, anchorPreview.transform.position, anchorPreview.transform.rotation); // rhand.transform.rotation
            spatialAnchors.Add(anchor);
            Destroy(anchorPreview);
        }
    }

    /// <summary>
    /// save to cloud. save to local by default. 
    /// </summary>
    public void saveALLSpatialAnchorsLocally()
    {
        OVRSpatialAnchor.SaveOptions saveOptions;
        saveOptions.Storage = OVRSpace.StorageLocation.Local;

        //
        foreach (GameObject anchor in spatialAnchors)
        {
            //anchor.GetComponent<Anchor>().OnSaveLocalButtonPressed();
            OVRSpatialAnchor _spatialAnchor = anchor.GetComponent<OVRSpatialAnchor>();
            Anchor _anchor = anchor.GetComponent<Anchor>();
            _spatialAnchor.Save(saveOptions, (anchor, success) =>
            {
                if (!success) return;

                // Enables save icon on the menu
                _anchor.ShowSaveIcon = true;

                // Write uuid of saved anchor to file
                if (!PlayerPrefs.HasKey(Anchor.NumUuidsPlayerPref))
                {
                    PlayerPrefs.SetInt(Anchor.NumUuidsPlayerPref, 0);
                }

                int playerNumUuids = PlayerPrefs.GetInt(Anchor.NumUuidsPlayerPref);
                PlayerPrefs.SetString("uuid" + playerNumUuids, anchor.Uuid.ToString());
                PlayerPrefs.SetInt(Anchor.NumUuidsPlayerPref, ++playerNumUuids);

                LogMe("Anchor with Uuid " + anchor.Uuid.ToString() + " Saved Locally!");
            });
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void ShareAllSpatialAnchors()
    {
        LogMe("Sharing Spatial Anchors To Server... This takes a moment...");

        timeShareBegin = Time.time;

        createdUuidList.Clear();
        OVRSpatialAnchor.SaveOptions saveOptions;
        saveOptions.Storage = OVRSpace.StorageLocation.Cloud;

        // share created spatial anchors 
        foreach (GameObject anchor in spatialAnchors)
        {
            OVRSpatialAnchor curAnchor = anchor.GetComponent<OVRSpatialAnchor>();
            curAnchor.Save(saveOptions, (spatialAnchor, isSuccessful) =>
            {
                if (isSuccessful)
                {
                    LogMe("Saved To Cloud! CurAnchor: " + curAnchor.Uuid);
                    createdUuidList.Add(curAnchor.Uuid);

                    LogMe("Sharing With: " + MetaUserManager.OculusUserID);
                    ICollection<OVRSpaceUser> spaceUserList = new List<OVRSpaceUser>();
                    spaceUserList.Add(new OVRSpaceUser(MetaUserManager.OculusUserID));
                    OVRSpatialAnchor.Share(new List<OVRSpatialAnchor> { spatialAnchor }, spaceUserList, OnShareComplete);

                    //SampleController.Instance.AddSharedAnchorToLocalPlayer(this);
                }
                else
                {
                    LogMe("Failed To Save The Anchor To Cloud!");
                    //SampleController.Instance.Log("OnShareButtonPressed: failed to save anchors to cloud with user ids: " + userIds.Length);
                }
            });
        }

        // also share the locally load sdpatial anchors 
        foreach (GameObject anchor in loadSpatialAnchorsLocal) 
        {
            OVRSpatialAnchor curAnchor = anchor.GetComponent<OVRSpatialAnchor>();
            curAnchor.Save(saveOptions, (spatialAnchor, isSuccessful) =>
            {
                if (isSuccessful)
                {
                    LogMe("Saved To Cloud! CurAnchor: " + curAnchor.Uuid);
                    createdUuidList.Add(curAnchor.Uuid);

                    LogMe("Sharing With: " + MetaUserManager.OculusUserID);
                    ICollection<OVRSpaceUser> spaceUserList = new List<OVRSpaceUser>();
                    spaceUserList.Add(new OVRSpaceUser(MetaUserManager.OculusUserID));
                    OVRSpatialAnchor.Share(new List<OVRSpatialAnchor> { spatialAnchor }, spaceUserList, OnShareComplete);

                    //SampleController.Instance.AddSharedAnchorToLocalPlayer(this);
                }
                else
                {
                    LogMe("Failed To Save The Anchor To Cloud!");
                    //SampleController.Instance.Log("OnShareButtonPressed: failed to save anchors to cloud with user ids: " + userIds.Length);
                }
            });
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void PostUuidsToServer()
    {
        StartCoroutine(PostToServer());
    }

    /// <summary>
    /// 
    /// </summary>
    public void LoadAnchorsByUuid()
    {
        // Get number of saved anchor uuids
        if (!PlayerPrefs.HasKey(Anchor.NumUuidsPlayerPref))
        {
            PlayerPrefs.SetInt(Anchor.NumUuidsPlayerPref, 0);
        }

        var playerUuidCount = PlayerPrefs.GetInt("numUuids");
        // LogMe($"Attempting to load {playerUuidCount} saved anchors."); // too many logs 
        if (playerUuidCount == 0)
            return;

        var uuids = new Guid[playerUuidCount];
        for (int i = 0; i < playerUuidCount; ++i)
        {
            var uuidKey = "uuid" + i;
            var currentUuid = PlayerPrefs.GetString(uuidKey);
            // LogMe("QueryAnchorByUuid: " + currentUuid); // too many logs

            uuids[i] = new Guid(currentUuid);
        }

        Load(new OVRSpatialAnchor.LoadOptions
        {
            Timeout = 0,
            StorageLocation = OVRSpace.StorageLocation.Local,
            Uuids = uuids
        });
    }

    /// <summary>
    /// 
    /// </summary>
    public void LoadSpatialAnchors()
    {
        //
        if (snapToTheNearestSpatialAnchor)
            snapToTheNearestSpatialAnchor.SnapOnce(2); // seconds delay
        LogMe("Num of Spatial Anchors Before Loading: " + loadSpatialAnchorsLocal.Count);

        //
        //GetComponent<SpatialAnchorLoader>().LoadAnchorsByUuid();
        LoadAnchorsByUuid();
    }

    /// <summary>
    /// 
    /// </summary>
    public void SaveLoadSpatialAnchorsFromCloudToLocal()
    {
        OVRSpatialAnchor.SaveOptions saveOptions;
        saveOptions.Storage = OVRSpace.StorageLocation.Local; // save to local
        foreach (GameObject anchor in loadSpatialAnchorsCloud)
        {
            //anchor.GetComponent<Anchor>().OnSaveLocalButtonPressed();
            OVRSpatialAnchor _spatialAnchor = anchor.GetComponent<OVRSpatialAnchor>();
            Anchor _anchor = anchor.GetComponent<Anchor>();
            _spatialAnchor.Save(saveOptions, (anchor, success) =>
            {
                if (!success) return;

                // Enables save icon on the menu
                //_anchor.ShowSaveIcon = true;
                _anchor.OnHoverStart(); // reuse the highlight yellow

                // Write uuid of saved anchor to file
                if (!PlayerPrefs.HasKey(Anchor.NumUuidsPlayerPref))
                {
                    PlayerPrefs.SetInt(Anchor.NumUuidsPlayerPref, 0);
                }

                int playerNumUuids = PlayerPrefs.GetInt(Anchor.NumUuidsPlayerPref);
                PlayerPrefs.SetString("uuid" + playerNumUuids, anchor.Uuid.ToString());
                PlayerPrefs.SetInt(Anchor.NumUuidsPlayerPref, ++playerNumUuids);

                LogMe("Saved Locally!");
            });
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void deleteSpatialAnchors()
    {
        //
        foreach (GameObject anchor in spatialAnchors)
        {
            anchor.GetComponent<Anchor>().OnEraseButtonPressed();
        }

        // Get number of saved anchor uuids
        if (PlayerPrefs.HasKey(Anchor.NumUuidsPlayerPref))
        {
            PlayerPrefs.SetInt(Anchor.NumUuidsPlayerPref, 0);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public GameObject getAvtiveSpatialAnchor()
    {
        foreach (GameObject sa in spatialAnchors)
        {
            if (sa.GetComponent<SpatialAnchorPropertyManager>().IsActive)
            {
                return sa;
            }
        }

        return null;
    }

    /// <summary>
    /// 
    /// </summary>
    void SimpleJSONTest()
    {
        JSONObject jSpatialAnchor1 = new JSONObject();
        jSpatialAnchor1.Add("uuid", "uuid1-xxx-xxx-xxx");
        jSpatialAnchor1.Add("space", "space1-xxx-xxx-xxx");

        JSONObject jSpatialAnchor2 = new JSONObject();
        jSpatialAnchor2.Add("uuid", "uuid2-xxx-xxx-xxx");
        jSpatialAnchor2.Add("space", "space2-xxx-xxx-xxx");

        JSONObject jNode = new JSONObject();
        jNode.Add("sa1", jSpatialAnchor1);
        jNode.Add("sa2", jSpatialAnchor2);

        Debug.Log("jNode.ToString() = " + jNode.ToString());

        Debug.Log("jNode[0]['uuid'] = " + jNode[0]["uuid"]);
        Debug.Log("jNode[0]['space'] = " + jNode[0]["space"]);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_spatialAnchors"></param>
    /// <param name="result"></param>
    private void OnShareComplete(ICollection<OVRSpatialAnchor> _spatialAnchors, OVRSpatialAnchor.OperationResult result) {
        LogMe("Share Result: " + result);

        if (result != OVRSpatialAnchor.OperationResult.Success)
        {
            LogMe("Sharing Failed! Check Account Info, Server Connection etc. ");
            LogMe(@"Error Info: \n 
                Success = 0, \n
                Failure = -1000, \n
                Failure_SpaceCloudStorageDisabled = -2000, \n
                Failure_SpaceMappingInsufficient = -2001, \n
                Failure_SpaceLocalizationFailed = -2002, \n
                Failure_SpaceNetworkTimeout = -2003, \n
                Failure_SpaceNetworkRequestFailed = -2004 \n");
            return;
        }

        var uuids = new Guid[_spatialAnchors.Count];
        var uuidIndex = 0;

        foreach (var spatialAnchor in _spatialAnchors)
        {
            LogMe("OnShareComplete: space: " + spatialAnchor.Space.Handle + ", uuid: " + spatialAnchor.Uuid);

            uuids[uuidIndex] = spatialAnchor.Uuid;
            ++uuidIndex;

            //JSONString tmpNode = new JSONString("");

            //createdSpaceAndUuid.Add()

            // visual feedback after sharing 
            spatialAnchor.GetComponent<Anchor>().ShowSaveIcon = true;
        }

        // final one 
        if(createdUuidList.Count == spatialAnchors.Count)
        {
            StartCoroutine(PostToServer());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator PostToServer()
    {
        string tobeposted = "";

        int num = 0;
        foreach(Guid uuid in createdUuidList)
        {
            tobeposted += uuid.ToString();
            if (num != (createdUuidList.Count - 1))
                tobeposted += ",";
            num++;
        }

        Debug.Log("uuid upload complete!");

        float timeInBetween = Time.time - timeShareBegin;
        LogMe("Spatial Anchor Share Complete. Uuid upload complete! In " + timeInBetween.ToString("F2") + " Seconds.");

        yield return (new WebClient()).UploadString(postUrl, tobeposted);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    IEnumerator GetUUidsAndLoadCloudSpatialAnchors(string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    // handle the result
                    var result = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    //Debug.Log(result);

                    //var enemy = JsonUtility.FromJson<Enemy>(result);

                    uuidListJsonNode = JSON.Parse(result);

                    //enemyViewController.DisplayEnemyData(enemy.name, enemy.health.ToString(), enemy.attack.ToString());

                    //for (int i = 0; i < uuidListJsonNode.Count; i++)
                    //{
                    //    Debug.Log("Load uuid: " + uuidListJsonNode[i]);
                    //}

                    LoadCloudSpatialAnchorsAfteruuidLoading();

                    //UpdateUI();

                    //Debug.Log("file name = " + fileList[0]["name"]);
                }
                else
                {
                    //handle the problem
                    Debug.Log("Error! data couldn't get.");
                }
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    void LogMe(string msg)
    {
        if (GameObject.FindGameObjectWithTag("LogInfo"))
            GameObject.FindGameObjectWithTag("LogInfo").GetComponent<DebugInfoManager>()
                .AppendText(msg);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="options"></param>
    private void Load(OVRSpatialAnchor.LoadOptions options) => OVRSpatialAnchor.LoadUnboundAnchors(options, anchors =>
    {
        if (anchors == null)
        {
            //Log("Query failed.");
            return;
        }

        foreach (var anchor in anchors)
        {
            if (anchor.Localized)
            {
                OnLocalized(anchor, true);
            }
            else if (!anchor.Localizing)
            {
                anchor.Localize(OnLocalized);
            }
        }
    });

    /// <summary>
    /// 
    /// </summary>
    /// <param name="unboundAnchor"></param>
    /// <param name="success"></param>
    private void OnLocalized(OVRSpatialAnchor.UnboundAnchor unboundAnchor, bool success)
    {
        if (!success)
        {
            //Log($"{unboundAnchor} Localization failed!");
            return;
        }

        var pose = unboundAnchor.Pose;
        GameObject spatialAnchor = Instantiate(_anchorPrefab, pose.position, pose.rotation);
        unboundAnchor.BindTo(spatialAnchor.GetComponent<OVRSpatialAnchor>());
        loadSpatialAnchorsLocal.Add(spatialAnchor);

        if (spatialAnchor.TryGetComponent<Anchor>(out var anchor))
        {
            // We just loaded it, so we know it exists in persistent storage.
            anchor.ShowSaveIcon = true;
        }

        // hide by default
        if (HideBydefault)
        {
            ixAnchorHelper anchorhelper = spatialAnchor.GetComponent<ixAnchorHelper>();
            if (anchorhelper) anchorhelper.Hide();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void LoadCloudSpatialAnchorsAfteruuidLoading()
    {
        loadSpatialAnchorsCloud.Clear();

        OVRManager.SpaceQueryComplete += OnSpaceQueryComplete;
        var uuids = new HashSet<Guid>();

        //uuids.Add(Guid.Parse("03458fc9-e136-29c9-2625-d235568a50f2"));
        for (int i = 0; i < uuidListJsonNode.Count; i++)
        {
            LogMe("Load uuid From Server: " + uuidListJsonNode[i]);
            uuids.Add(Guid.Parse(uuidListJsonNode[i]));
        }


        var uuidInfo = new OVRPlugin.SpaceFilterInfoIds { NumIds = uuids.Count, Ids = uuids.ToArray() };

        var queryInfo = new OVRPlugin.SpaceQueryInfo()
        {
            QueryType = OVRPlugin.SpaceQueryType.Action,
            MaxQuerySpaces = 100,
            Timeout = 0,
            Location = OVRPlugin.SpaceStorageLocation.Cloud,
            ActionType = OVRPlugin.SpaceQueryActionType.Load,
            FilterType = OVRPlugin.SpaceQueryFilterType.Ids,
            IdInfo = uuidInfo
        };

        var didSuccessfullyQuerySpaces = OVRPlugin.QuerySpaces(queryInfo, out var requestId);
        //LogMe("LowLevelMessage: \n" + OVRPlugin.LowLevelMessage);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="requestId"></param>
    /// <param name="isSuccessful"></param>
    private void OnSpaceQueryComplete(ulong requestId, bool isSuccessful)
    {
        LogMe("OnSpaceQueryComplete: requestId: " + requestId + " isSuccessful: " + isSuccessful);

        if (!isSuccessful)
        {
            //return;
        }

        var didSuccessfullyRetrieveResults = OVRPlugin.RetrieveSpaceQueryResults(requestId, out var queryResults);

        if (!didSuccessfullyRetrieveResults)
        {
            LogMe("RetrieveSpaceQueryResults: failed to query requestId: " 
                + requestId + " version: " + OVRPlugin.version);
            return;
        }

        LogMe("RetrieveSpaceQueryResults: requestId: " + requestId + " result count: " + queryResults.Length);

        foreach (var queryResult in queryResults)
        {
            LogMe("Loaded Space: " + queryResult.space + ", uuid: " + queryResult.uuid);
            GameObject spatialAnchor = Instantiate(_anchorPrefab);
            spatialAnchor.GetComponent<OVRSpatialAnchor>().InitializeFromExisting(queryResult.space, queryResult.uuid);
            loadSpatialAnchorsCloud.Add(spatialAnchor);
        }

        float timeinBetween = Time.time - timeLoadBegin;

        LogMe("Load Complete! In " + timeinBetween.ToString("F2") + " Seconds.");
    }

}
