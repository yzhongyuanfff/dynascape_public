using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToTheActivatedSpatialAnchor : MonoBehaviour
{
    public SpatialAnchorManager spatialAnchorManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void snapToTheActiveSpatialAnchor()
    {
        GameObject activeSA = spatialAnchorManager.getAvtiveSpatialAnchor();
        if (activeSA)
        {
            this.transform.position = activeSA.transform.position;
            this.transform.rotation = activeSA.transform.rotation;
        }
    }
}
