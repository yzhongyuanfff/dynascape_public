using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToTheNearestSpatialAnchorPlane: MonoBehaviour
{
    [SerializeField] bool DoSnapWhenStarts = false;
    [SerializeField] float SnapThreshold = 0.2f;
    [SerializeField] bool DoSnap = true;

    SpatialAnchorManager spatialAnchorManager;
    List<Transform> SAPoseList;

    // Visuals
    //GameObject MyPoseIndicator_NN;
    //GameObject SAIndicator_NN;
    //GameObject NearestSA_NN;
    //GameObject SnapSource;
    //List<GameObject> SAIndicatorList;

    // Start is called before the first frame update
    void Start()
    {
        // get ref
        spatialAnchorManager = GameObject.FindGameObjectWithTag("SpatialAnchorManager").GetComponent<SpatialAnchorManager>();
        //SnapSource = GameObject.FindGameObjectWithTag("SnapSource");

        //
        SAPoseList = new List<Transform>();
        //SAIndicatorList = new List<GameObject>();

        // init
        if (DoSnapWhenStarts)
            SnapOnce(2);
    }

    // Update is called once per frame
    void Update()
    {

    }

    bool EnableAutoSnap = true;

    public void UpdateAutoSnap(bool b)
    {
        EnableAutoSnap = b;
    }

    public void AutoSnapOnce()
    {
        if (EnableAutoSnap)
            SnapOnce();
    }

    public void SnapOnce()
    {
        Snap();
    }

    public void SnapOnce(float delay)
    {
        Invoke("Snap", delay);
    }

    void Snap()
    {
        if (spatialAnchorManager == null) return;
        if (DoSnap == false) return;

        //if (SnapSource == null) return;

        //// visual: show my pose before align
        //if (MyPoseIndicator_NN)
        //    MyPoseIndicator_NN.transform.position = this.transform.position;

        // 
        SAPoseList.Clear();
        foreach (GameObject sa in spatialAnchorManager.AnchorPreviews)
            SAPoseList.Add(sa.transform);
        foreach (GameObject sa in spatialAnchorManager.spatialAnchors)
            SAPoseList.Add(sa.transform);
        foreach (GameObject sa in spatialAnchorManager.loadSpatialAnchorsLocal)
            SAPoseList.Add(sa.transform);
        foreach (GameObject sa in spatialAnchorManager.loadSpatialAnchorsCloud)
            SAPoseList.Add(sa.transform);

        //// visual: show all traced anchor points 
        //foreach (var si in SAIndicatorList)
        //{
        //    Destroy(si);
        //}
        //SAIndicatorList.Clear();
        //if (SAIndicator_NN)
        //{
        //    foreach (Transform sap in SAPoseList)
        //    {
        //        GameObject sai = Instantiate(SAIndicator_NN);
        //        sai.transform.position = sap.position;
        //        SAIndicatorList.Add(sai);
        //    }
        //}

        //
        //SnapSource.transform.position = this.transform.position; // change the snap source first 

        // find the nearest SA
        Vector3 neartestSpatialAnchorPosition = transform.position;
        Quaternion neartestSpatialAnchorRotation = transform.rotation;
        float minDist = float.MaxValue;
        foreach (Transform sap in SAPoseList)
        {
            //Vector3 projPosition = this.transform.InverseTransformPoint(sa.transform.position);
            float dist = Vector3.Distance(this.transform.position, sap.position);
            ixUtility.LogMe("Current Dist = " + dist + " minDist = " + minDist);
            if (dist < minDist)
            {
                minDist = dist;
                neartestSpatialAnchorPosition = sap.position;
                neartestSpatialAnchorRotation = sap.rotation;
            }
        }
        ixUtility.LogMe("Final minDist = " + minDist);
        ixUtility.LogMe("neartestSpatialAnchor position = " + neartestSpatialAnchorPosition);

        //
        if(minDist > SnapThreshold)
        {
            ixUtility.LogMe("Too large distance to the nearest spatial anchor! ");
            return;
        }

        //
        this.transform.position = neartestSpatialAnchorPosition;
        this.transform.rotation = neartestSpatialAnchorRotation;

        //// for the Panels that are not snapping at the beginning, do not move scene  
        //if (DoSnapWhenStarts)
        //    UpdateCalibrationAcrossScene(neartestSpatialAnchor.transform.position, neartestSpatialAnchor.transform.rotation);

        //// visual 
        //if (NearestSA_NN)
        //    NearestSA_NN.transform.position = neartestSpatialAnchor.transform.position;
    }

    void UpdateCalibrationAcrossScene(Vector3 p, Quaternion q)
    {
        // update sub scenes 
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("SceneGeometry"))
        {
            o.transform.position = p;
            o.transform.rotation = q;
        }
    }
}
