using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToTheNearestSpatialAnchor : MonoBehaviour
{
    [SerializeField] bool DoSnapWhenStarts = false;
    [SerializeField] float SnapThreshold = 1.0f;
    [SerializeField] bool DoSnap = true;

    SpatialAnchorManager spatialAnchorManager;
    List<GameObject> SAGameObjectList;

    // Start is called before the first frame update
    void Start()
    {
        // get ref
        spatialAnchorManager = GameObject.FindGameObjectWithTag("SpatialAnchorManager").GetComponent<SpatialAnchorManager>();

        //
        SAGameObjectList = new List<GameObject>();

        // init
        if (DoSnapWhenStarts)
            SnapOnce(2);
    }

    // Update is called once per frame
    void Update()
    {

    }

    bool EnableAutoSnap = true;

    public void UpdateAutoSnap(bool b)
    {
        EnableAutoSnap = b;
    }

    public void AutoSnapOnce()
    {
        if (EnableAutoSnap)
            SnapOnce();
    }

    public void SnapOnce()
    {
        Snap();
    }

    public void SnapOnce(float delay)
    {
        Invoke("Snap", delay);
    }

    public void UpdateThreshold(float sh)
    {
        SnapThreshold = sh;
    }

    void Snap()
    {
        if (spatialAnchorManager == null) return;
        if (DoSnap == false) return;

        // 
        SAGameObjectList.Clear();
        foreach (GameObject sa in spatialAnchorManager.AnchorPreviews)
            SAGameObjectList.Add(sa);
        foreach (GameObject sa in spatialAnchorManager.spatialAnchors)
            SAGameObjectList.Add(sa);
        foreach (GameObject sa in spatialAnchorManager.loadSpatialAnchorsLocal)
            SAGameObjectList.Add(sa);
        foreach (GameObject sa in spatialAnchorManager.loadSpatialAnchorsCloud)
            SAGameObjectList.Add(sa);

        // find the nearest SA
        Vector3 neartestSpatialAnchorPosition = transform.position;
        Quaternion neartestSpatialAnchorRotation = transform.rotation;
        float minDist = float.MaxValue;
        GameObject nearestSpatialAnchor = null;
        foreach (GameObject sap in SAGameObjectList)
        {
            //Vector3 projPosition = this.transform.InverseTransformPoint(sa.transform.position);
            float dist = Vector3.Distance(this.transform.position, sap.transform.position);
            ixUtility.LogMe("Current Dist = " + dist + " minDist = " + minDist);
            if (dist < minDist)
            {
                minDist = dist;
                nearestSpatialAnchor = sap;
                neartestSpatialAnchorPosition = sap.transform.position;
                neartestSpatialAnchorRotation = sap.transform.rotation;
            }
        }
        ixUtility.LogMe("Final minDist = " + minDist);
        ixUtility.LogMe("neartestSpatialAnchor position = " + neartestSpatialAnchorPosition);

        //
        if(minDist > SnapThreshold)
        {
            ixUtility.LogMe("Too large distance to the nearest spatial anchor! ");
            return;
        }


        // Test - snap to xy plane 
        if (nearestSpatialAnchor && nearestSpatialAnchor.GetComponent<ixSnapSurface>())
        {
            //
            if (nearestSpatialAnchor.GetComponent<ixSnapSurface>().snapMode == SnapMode.Point)
            {
                ixUtility.LogMe("snap to the point");

                this.transform.position = neartestSpatialAnchorPosition;
                this.transform.rotation = neartestSpatialAnchorRotation;
            }

            //
            if (nearestSpatialAnchor.GetComponent<ixSnapSurface>().snapMode == SnapMode.Plane)
            {
                ixUtility.LogMe("snap to plane");

                Vector3 planePoint = neartestSpatialAnchorPosition; // new Vector3(x_p, y_p, z_p);
                Vector3 planeNormal = nearestSpatialAnchor.transform.rotation * new Vector3(0, 0, 1); // for the xy plane

                Vector3 pointToProject = this.transform.position;

                // Find the vector from the plane point to the point to project
                Vector3 pq = pointToProject - planePoint;

                // Find the scalar projection of pq onto the plane normal
                float scalarProjection = Vector3.Dot(pq, planeNormal);

                // Find the vector projection of pq onto the plane normal
                Vector3 pn = scalarProjection * planeNormal;

                // Find the projected point
                Vector3 projectedPoint = pointToProject - pn;

                Quaternion projectedRotation = neartestSpatialAnchorRotation;

                this.transform.position = projectedPoint;
                this.transform.rotation = projectedRotation;
            }

            //
            if (nearestSpatialAnchor.GetComponent<ixSnapSurface>().snapMode == SnapMode.Surface)
            {
                ixUtility.LogMe("snap to the surface, not impl.");
            }
        }
        else
        {
            ixUtility.LogMe("snap to the point");

            this.transform.position = neartestSpatialAnchorPosition;
            this.transform.rotation = neartestSpatialAnchorRotation;
        }
    }

    void UpdateCalibrationAcrossScene(Vector3 p, Quaternion q)
    {
        // update sub scenes 
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("SceneGeometry"))
        {
            o.transform.position = p;
            o.transform.rotation = q;
        }
    }
}
