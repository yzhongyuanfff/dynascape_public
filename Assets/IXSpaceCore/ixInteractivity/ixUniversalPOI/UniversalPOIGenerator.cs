// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalPOIGenerator : MonoBehaviour
{
    public PlayerController playerController;
    public GameObject POIPrefeb;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// generate POI at hand 
    /// </summary>
    public void generatePOI()
    {
        GameObject poi = Instantiate(POIPrefeb, this.transform);
        if(playerController.player) 
            poi.transform.position = playerController.player.transform.position; // move to anchor 
    }
}
