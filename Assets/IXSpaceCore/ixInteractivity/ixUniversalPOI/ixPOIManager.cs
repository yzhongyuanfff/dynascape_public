using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ixPOIManager : MonoBehaviour
{
    [Header("------")]
    [SerializeField] GameObject UIContent;

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitUI()
    {
        if (UIContent)
        {
            {
                GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                    "Scale SceneGeometry To Me";
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
                {
                    GameObject[] scenes = GameObject.FindGameObjectsWithTag("SceneGeometry");
                    ixUtility.LogMe("Num. SceneGeometry found: " + scenes.Length);
                    for (int i = 0; i < scenes.Length; i++)
                    {
                        ixUtility.LogMe("Scene " + i + " Found: " + scenes[i].name);
                        Debug.Log("Scene " + i + " Found: " + scenes[i].name);
                        if (!scenes[i].GetComponent<BoxCollider>())
                        {
                            ixUtility.LogMe("Scene Do Not Have a Box Collider!: " + scenes[i].name);
                            continue;
                        }
                        ixUtility.LogMe("Scene has a Box Collider!: " + scenes[i].name);

                        //
                        Vector3 origScale = scenes[i].GetComponent<BoxCollider>().size;
                        ixUtility.LogMe("Original Scale: " + origScale.ToString());
                        Vector3 myScale = transform.Find("AOIIllustrator").localScale;
                        ixUtility.LogMe("My Scale: " + myScale.ToString());

                        // compute the scaling factor - choose the smallest 
                        Vector3 scalingFactorVec3 = new Vector3(
                            myScale.x / origScale.x,
                            myScale.y / origScale.y,
                            myScale.z / origScale.z
                        );
                        float scalingFactor = Mathf.Min(
                            Mathf.Min(scalingFactorVec3.x, scalingFactorVec3.y), 
                            scalingFactorVec3.z);
                        ixUtility.LogMe("ScalingFactor: " + scalingFactor.ToString());
                        GameObject.Find("StringManager").GetComponent<StringManager>().ActiveScalingFactor = scalingFactor;
                        GameObject.Find("StringManager").GetComponent<StringManager>().ActivePOIPosition = this.transform.position;
                        GameObject.Find("StringManager").GetComponent<StringManager>().ActivePOIRotation = this.transform.rotation;

                        //
                        scenes[i].transform.localScale = new Vector3(scalingFactor, scalingFactor, scalingFactor);
                        scenes[i].transform.position = this.transform.position;
                        scenes[i].transform.rotation = this.transform.rotation;

                        // hide handles 
                        Transform handle = scenes[i].transform.Find("Handle");
                        if (handle) handle.gameObject.SetActive(false);
                    }
                });
            }
        }
    }
}
