﻿///////////////////////////////////////////////////////////////////////////////
// Author: Federico Garcia Garcia
// License: GPL-3.0
// Created on: 04/06/2020 23:00
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Text;
using TMPro;
using UnityEngine.UI;

public class TubeGeneratorFromObj : TubeGenerator
{
	public string path; // Path or URL to file
	public TextAsset data;
	public GameObject loading; // A GameObject that is disabled after the data is generated

	public bool loadOnStart = true;
	public bool fbrRebuild = false;

	GameObject experienceOrigin;

	void Start()
	{
		//if(loadOnStart) buildTraj();
		InitializeUI();
	}

	public void Update()
	{
		base.Update();
	}

	/// <summary>
	/// 
	/// </summary>
	public void AppendTrailBaseOnCurActiveFile()
	{
		path = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;

		// If its URL, download data in a coroutine, then in
		if (path.StartsWith("https:") || path.StartsWith("http:"))
		{
			StartCoroutine(LoadFromURL(path));
		}
		else if (path != "")// If its file, load data in a thread
		{
			if (ixUtility.IsWinPlatform())
			{
				//thread = new Thread(() => LoadFromPath());
				//thread.Start();
				StartCoroutine(LoadFromPath());
			}
			else
			{
				StartCoroutine(LoadFromPath());
			}
		}
		else
		{
			//Thread thread = new Thread(()=>LoadFromFile());
			//thread.Start();
			StartCoroutine(LoadFromFile()); // directly load from the file assigned 
		}
	}

	Thread thread;

	public void ClearAllTrails()
	{
		foreach (Transform t in transform)
		{
			Destroy(t.gameObject);
		}
	}


	void AppendTrailFromFile(string p)
	{
		path = p;
		StartCoroutine(LoadFromPath());
	}

	/// <summary>
	///  we assume the data file is csv for now 
	/// </summary>
	/// <param name="flist"></param>
	/// <returns></returns>
	IEnumerator LoadFromFileList(List<string> flist)
	{
		if (flist.Count == 0) yield return null;

		//if (Path.GetExtension(flist[0]).Equals(".csv") || Path.GetExtension(flist[0]).Equals(".CSV"))
		{
			// create one polyline for all 
			Vector3[][] polylines = new Vector3[flist.Count][]; 
			int idx = 0;
			foreach(var fn in flist)
            {
				List<Dictionary<string, object>> csvdata = ixUtility.ReadCSV(fn);
				polylines[idx] = new Vector3[csvdata.Count];
				for (int i = 0; i < csvdata.Count; i++) // number of samples = csvdata.Count
				{
					polylines[idx][i] = new Vector3(
						float.Parse(csvdata[i]["PositionX"].ToString()),
						float.Parse(csvdata[i]["PositionY"].ToString()),
						float.Parse(csvdata[i]["PositionZ"].ToString()));
				}
				idx++;
			}

			//
			Debug.Log("Start Building Trajectories,  polylines.Length = " + polylines.Length
				+ " , polylines[0].Length = " + polylines[0].Length);
			ixUtility.LogMe("Start Building Trajectories,  polylines.Length = " + polylines.Length
				+ " , polylines[0].Length = " + polylines[0].Length);

			// Make sure to lock to avoid multithreading problems
			lock (_enque)
			{
				// Run the generation of polylines in the Main Thread
				ExecuteOnMainThread.Enqueue(() => { StartCoroutine(AfterLoading()); });
				ExecuteOnMainThread.Enqueue(() => { StartCoroutine(Generate(polylines, "From FileList")); });
			};
		}

		yield return null;
	}

	IEnumerator LoadFromPath()
	{
		string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);
		if (Path.GetExtension(path).Equals(".txt") || Path.GetExtension(path).Equals(".TXT"))
		{
			// Create OBJ reader
			ObjReader objReader = new ObjReader();

			// Convert MemoryStream to StreamReader
			StreamReader reader = new StreamReader(path);

			Vector3[][] polylines = objReader.GetPolylinesFromStreamReader(reader);

			Debug.Log("Start Building Trajectories,  polylines.Length = " + polylines.Length 
				+ " , polylines[0].Length = " + polylines[0].Length);


			// Make sure to lock to avoid multithreading problems
			lock (_enque)
			{
				// Run the generation of polylines in the Main Thread
				ExecuteOnMainThread.Enqueue(() => { StartCoroutine(AfterLoading()); });
				ExecuteOnMainThread.Enqueue(() => { StartCoroutine(Generate(polylines, fileNameWithoutExtension)); });
			};
		}
		else if (Path.GetExtension(path).Equals(".csv") || Path.GetExtension(path).Equals(".CSV"))
		{
			List<Dictionary<string, object>> csvdata = ixUtility.ReadCSV(path); // takes some time 
																				//for (var i = 0; i < 100; i++)
																				//{
																				//    ixUtility.LogMe("In csvdata, PositionX = " + csvdata[i]["PositionX"].ToString());
																				//}
			Vector3[][] polylines = new Vector3[1][];
			polylines[0] = new Vector3[csvdata.Count];
			for (int i = 0; i < csvdata.Count; i++)
			{
				polylines[0][i] = new Vector3(
					float.Parse(csvdata[i]["PositionX"].ToString()),
					float.Parse(csvdata[i]["PositionY"].ToString()),
					float.Parse(csvdata[i]["PositionZ"].ToString()));
			}

			Debug.Log("Start Building Trajectories,  polylines.Length = " + polylines.Length
				+ " , polylines[0].Length = " + polylines[0].Length);
			ixUtility.LogMe("Start Building Trajectories,  polylines.Length = " + polylines.Length
				+ " , polylines[0].Length = " + polylines[0].Length);

			// Make sure to lock to avoid multithreading problems
			lock (_enque)
			{
				// Run the generation of polylines in the Main Thread
				ExecuteOnMainThread.Enqueue(() => { StartCoroutine(AfterLoading()); });
				ExecuteOnMainThread.Enqueue(() => { StartCoroutine(Generate(polylines, fileNameWithoutExtension)); });
			};
		}

		yield return null;
	}

	IEnumerator LoadFromFile()
	{
		// Create OBJ reader
		ObjReader objReader = new ObjReader();

		//Vector3 [][] polylines = objReader.GetPolylinesFromFilePath(path);

		// Get bytes from file
		byte[] byteArray = Encoding.UTF8.GetBytes(data.text);

		// Create memory stream
		MemoryStream stream = new MemoryStream(byteArray);

		// Convert MemoryStream to StreamReader
		StreamReader reader = new StreamReader(stream);

		Vector3[][] polylines = objReader.GetPolylinesFromStreamReader(reader);

		Debug.Log("Start Building Trajectories,  polylines.Length = " + polylines.Length + " , polylines[0].Length = " + polylines[0].Length);

		// Make sure to lock to avoid multithreading problems
		lock (_enque)
		{
			// Run the generation of polylines in the Main Thread
			ExecuteOnMainThread.Enqueue(() => { StartCoroutine(AfterLoading()); });
			ExecuteOnMainThread.Enqueue(() => { StartCoroutine(Generate(polylines, data.name)); });
		};

		yield return null;
	}

	IEnumerator LoadFromURL(string url)
	{
		UnityWebRequest www = UnityWebRequest.Get(url);
		yield return www.SendWebRequest();

		if (www.isNetworkError || www.isHttpError)
		{
			Debug.Log(www.error);
		}
		else
		{
			// Or retrieve results as binary data
			//byte[] byteArray = www.downloadHandler.data;

			// Get bytes from file
			byte[] byteArray = Encoding.UTF8.GetBytes(www.downloadHandler.text);

			// Create memory stream
			MemoryStream stream = new MemoryStream(byteArray);

			// Convert MemoryStream to StreamReader
			StreamReader reader = new StreamReader(stream);

			// Run this in a thread
			Thread thread = new Thread(() => LoadFromStream(reader));
			thread.Start();

		}
	}

	void LoadFromStream(StreamReader reader)
	{

		// Create OBJ reader
		ObjReader objReader = new ObjReader();

		Vector3[][] polylines = objReader.GetPolylinesFromStreamReader(reader);

		// Make sure to lock to avoid multithreading problems
		lock (_enque)
		{
			// Run the generation of polylines in the Main Thread
			ExecuteOnMainThread.Enqueue(() => { StartCoroutine(AfterLoading()); });
			ExecuteOnMainThread.Enqueue(() => { StartCoroutine(Generate(polylines, "Streamed")); });
		};
	}

	IEnumerator AfterLoading()
	{
		if (loading != null)
			loading.SetActive(false);

		yield return null;
	}

	List<Tuple<string, Action<GameObject>>> featureList;
	public GameObject UIContent;

	void InitializeUI()
    {
		featureList = new List<Tuple<string, Action<GameObject>>>();
		featureList.Add(new Tuple<string, Action<GameObject>>("Clear All Trails", delegate {
			ClearAllTrails();
		}));
		featureList.Add(new Tuple<string, Action<GameObject>>("Load Active Trail", delegate {
			AppendTrailBaseOnCurActiveFile();
		}));
		featureList.Add(new Tuple<string, Action<GameObject>>("Scale The Trail To 1%", delegate {
			this.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
		}));
		featureList.Add(new Tuple<string, Action<GameObject>>("Unzip Active File and Render", delegate {
			UnzipActiveFileAndRender();
		}));

		// Instantiate buttons 
		foreach (var t in featureList)
		{
			GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
			ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = t.Item1;
			ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
			ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate { t.Item2(ui); });
		}
	}

	void UnzipActiveFileAndRender()
    {
		//
		string zipfile = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curActiveFilePath;
		string targetPathBase = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFolderPath;
		string fileNameWithoutExtension = GameObject.FindGameObjectWithTag("StringManager")
			.GetComponent<StringManager>().curFileName;

		//
		fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileNameWithoutExtension);
		string targetPath = targetPathBase + "/" + fileNameWithoutExtension + "/";

		List<string> fplist = new List<string>();
		if (!Directory.Exists(targetPath))
        {
			Directory.CreateDirectory(targetPath);
			fplist = ZipFile.UnZip(targetPath, zipfile);
			ixUtility.LogMe("Unzipped! zipfile = "+ zipfile + " targetPath = " + targetPath);
        }
		else {
			var info = new DirectoryInfo(targetPath);
			var fileInfo = info.GetFiles();
			foreach (var file in fileInfo)
				fplist.Add(file.FullName);
		}

		//
		foreach(string f in fplist)
        {
			ixUtility.LogMe("Loading File: " + f);
			Debug.Log("Loading File: " + f);
		}
		StartCoroutine(LoadFromFileList(fplist));
	}
}