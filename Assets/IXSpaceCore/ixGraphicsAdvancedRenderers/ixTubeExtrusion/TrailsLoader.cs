// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TrailsLoader : MonoBehaviour
{
    public GameObject tubePrefeb;
    public GameObject experienceOrigin;
    string trailsFolder;

    // Start is called before the first frame update
    void Start()
    {
        trailsFolder = Application.persistentDataPath + "/CollectedData/Trails/";
        //reloadDataset();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //
    public void reloadDataset()
    {
        //
        foreach(Transform t in this.transform)
        {
            Destroy(t.gameObject);
        }
        this.transform.position = Vector3.zero;
        this.transform.rotation = Quaternion.identity;

        //
        DirectoryInfo d = new DirectoryInfo(trailsFolder);
        FileInfo[] Files = d.GetFiles("*.txt");
        Debug.Log("Loading Trails: " + Files.Length);
        foreach (FileInfo file in Files)
        {
            GameObject tube = Instantiate(tubePrefeb, this.transform);
            tube.GetComponent<TubeGeneratorFromObj>().loadOnStart = false;
            tube.GetComponent<TubeGeneratorFromObj>().path = trailsFolder + file.Name;
            tube.GetComponent<TubeGeneratorFromObj>().AppendTrailBaseOnCurActiveFile();
        }

    }

    public void alignWithTheSystem()
    {
        //
        this.transform.position = experienceOrigin.transform.position;
        this.transform.rotation = experienceOrigin.transform.rotation;
    }

    public void deleteAllData()
    {
        //
        foreach (Transform t in this.transform)
        {
            Destroy(t.gameObject);
        }

        //
        DirectoryInfo d = new DirectoryInfo(trailsFolder);
        FileInfo[] Files = d.GetFiles("*.txt");
        Debug.Log("Loading Trails: " + Files.Length);
        foreach (FileInfo file in Files)
        {
            File.Delete(trailsFolder + file.Name);
        }
    }
}
