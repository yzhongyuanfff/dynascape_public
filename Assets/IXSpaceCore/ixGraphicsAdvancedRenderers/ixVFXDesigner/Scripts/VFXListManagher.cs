using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public class VFXListManagher : MonoBehaviour
{
    public GameObject UIContent;
    public GameObject VFXParent;
    ixVideoPlayerController videoPlayerController;

    List<VisualEffect> VFXList;


    // Start is called before the first frame update
    void Start()
    {
        videoPlayerController = GetComponent<ixVideoPlayerController>();
        CollectVFXList();
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CollectVFXList()
    {
        if (VFXParent == null) return;

        VFXList = new List<VisualEffect>();
        foreach (Transform c in VFXParent.transform)
        {
            VisualEffect vfx = c.GetComponent<VisualEffect>();
            if (vfx)
                VFXList.Add(vfx);
        }
    }


    void InitUI()
    {
        if (UIContent == null) return;

        foreach(VisualEffect vfx in VFXList)
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Use VFX: " + vfx.name;
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(
                delegate { videoPlayerController.SwitchVFX(vfx); });
        }
    }
}
