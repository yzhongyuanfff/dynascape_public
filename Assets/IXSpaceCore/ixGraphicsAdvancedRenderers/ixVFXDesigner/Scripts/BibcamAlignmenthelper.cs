using Bibcam.Decoder;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BibcamAlignmenthelper : MonoBehaviour
{
    public GameObject alignmentTarget;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.GetComponent<BibcamMetadataDecoder>().AlignDataUpdatedButNotApplied)
        {
            transform.GetComponent<BibcamMetadataDecoder>().AlignDataUpdatedButNotApplied = false;
            Debug.Log("Performing Alignment! ");
            updateAlignment();
        }
    }

    public void updateAlignment()
    {
        //Quaternion r0 = transform.GetComponent<BibcamMetadataDecoder>().Metadata.MarkerRotation.GetInverse();
        //Vector3 t0 = transform.GetComponent<BibcamMetadataDecoder>().Metadata.MarkerPosition;
        //Vector3 rotated_t0 = r0 * t0;

        //alignmentTarget.transform.position = -rotated_t0;
        //alignmentTarget.transform.rotation = r0;
    }
}
