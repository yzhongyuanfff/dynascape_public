using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoFrameControl : MonoBehaviour
{

    //public long animFrameStart = 10;
    //public long animFrameStop = 100;

    VideoPlayer _video;
    public bool bGoToTargetFrame = false;

    //
    public long targetFrame = 100;
    long lastframe = 100;
    public bool enableSnapping = false;
    public GameObject snapTarget;

    public BufferedPoints bufferedPoints;

    // Start is called before the first frame update
    void Start()
    {
        _video = GetComponent<VideoPlayer>();
        _video.frameReady += OnFrameReady;
        _video.sendFrameReadyEvents = true;
    }

    private void OnFrameReady(VideoPlayer source, long frameIdx)
    {
        _video.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        //if (_video.texture != null)
        //{
        //    if (_video.frame < animFrameStart) _video.frame = animFrameStart;
        //    if (_video.frame > animFrameStop) _video.frame = animFrameStart;
        //}

        if (bGoToTargetFrame)
        {
            bGoToTargetFrame = false;
            _video.frame = targetFrame;
            _video.Play();
        }

        if (enableSnapping)
        {
            int nearestFrameIdx = 0;
            float dist = float.MaxValue;
            int idx = 0;
            foreach(var p in bufferedPoints._cam_positions)
            {
                float d = Vector3.Distance(p, this.transform.InverseTransformPoint(snapTarget.transform.position));
                if (d < dist)
                {
                    dist = d;
                    nearestFrameIdx = idx;
                }
                idx++;
            }
            
            targetFrame = nearestFrameIdx;
            _video.frame = targetFrame;
            lastframe = _video.frame;
            if(targetFrame != lastframe)
                _video.Play();
        }
    }
}
