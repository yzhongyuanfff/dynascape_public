// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.VFX;
using Pcx;
using UnityEngine.Rendering;
using UnityEngine.Video;
using Bibcam.Decoder;
using System.Text;
using TMPro;

struct Point
{
    public Vector3 position;
    public uint color;
}

public class BufferedPoints : MonoBehaviour
{
    //
    [SerializeField] public BibcamMetadataDecoder _decoder = null;
    [SerializeField] public BibcamTextureDemuxer _demuxer = null;
    public string fileName = "tree_orange_big21";
    public string camposeFileName = "campose";
    public string outputFolderPath;

    // 
    Vector3[] points;
    Color[] colors;
    int[] indices;
    const int elementSize = sizeof(float) * 4;
    public GraphicsBuffer _position_buffer;
    public GraphicsBuffer _color_buffer;

    //
    VisualEffect _vfx;
    VideoPlayer _video;
    Matrix4x4 transformMat;
    bool frameReadyAndNotProcessed = false;

    // showing on editor 
    public Texture currentTexture;
    public long currentFrame = -1;
    public long numOfFrames = 0;

    //
    public Vector3 pointOffset = new Vector3(0, 0, 0);
    public int numPoints = 0;
    public bool recenter = true;
    public int frameStart = 10;
    public int frameStop = 500;
    public int resolution = 20;
    public float gridSize = 100;

    //
    public Texture2D DepthMap;
    public Texture2D ColorMap;
    public int actualNumOfPoints = 0;
    public int estimatedNumPoints = 4000000;

    public TextMeshProUGUI infoText;
    void readCamPositions()
    {
        _cam_positions = new List<Vector3>();
        _cam_rotations = new List<Quaternion>();

        string file = Application.streamingAssetsPath + "/" + camposeFileName + ".bin";
        // Check file exists
        if (!File.Exists(file))
        {
            return;
        }
        BinaryReader reader = new BinaryReader(File.Open(file, FileMode.Open));
        int fileLength = (int)reader.BaseStream.Length;
        string buildingLine = "";
        int charSize = sizeof(char);

        int numOfPoses = reader.ReadInt32();

        float x, y, z;
        for (int i = 0; i < numOfPoses; i++)
        {
            x = reader.ReadSingle();
            y = reader.ReadSingle();
            z = reader.ReadSingle();

            _cam_positions.Add(new Vector3(x, y, z));
        }
    }

    public int ReadPointsFromPLY(string file)
    {
        // Check file exists
        if (!File.Exists(file))
        {
            return 1;
        }

        try
        {
            // Interpret File
            using (BinaryReader reader = new BinaryReader(File.Open(file, FileMode.Open)))
            {
                int fileLength = (int)reader.BaseStream.Length;
                string buildingLine = "";
                int charSize = sizeof(char);

                // read the header
                int numRead = 0;
                while ((numRead += charSize) < fileLength)
                {
                    char nextChar = reader.ReadChar();
                    if (nextChar == '\n')
                    {
                        if (buildingLine.Contains("end_header"))
                        {
                            break;
                        }
                        else if (buildingLine.Contains("element vertex"))
                        {
                            string[] array = Regex.Split(buildingLine, @"\s+");
                            if (array.Length - 2 > 0)
                            {
                                int target = Array.IndexOf(array, "vertex") + 1;
                                numPoints = Convert.ToInt32(array[target]);
                                buildingLine = "";
                            }
                            else
                            {
                                return 1;
                            }
                        }
                    }
                    else
                    {
                        buildingLine += nextChar;
                    }
                }

                // Read the content 
                points = new Vector3[numPoints];
                colors = new Color[numPoints];
                indices = new int[numPoints];

                float x, y, z;
                float r, g, b;

                for (int i = 0; i < numPoints; i++)
                {
                    // Read the position
                    // convert from the centimeters to meters, and flip z and y (RHS to LHS)
                    x = reader.ReadSingle();
                    y = reader.ReadSingle();
                    z = reader.ReadSingle();

                    r = reader.ReadByte() / 255.0f;
                    g = reader.ReadByte() / 255.0f;
                    b = reader.ReadByte() / 255.0f;

                    // Package the position and signal strength into a single 4 element vector
                    points[i] = new Vector3(x, y, z);
                    colors[i] = new Color(r, g, b);
                    indices[i] = i;
                }
            }
            return 0;
        }
        catch (Exception e)
        {
            return 1;
        }
    }

    public void RecenterPoints()
    {
        //if (recenter)
        {
            Vector3 centre = new Vector3();
            foreach (var p in points)
            {
                centre += p;
            }
            centre /= points.Length;

            //
            for (int i = 0; i < points.Length; i++)
            {
                points[i] = points[i] - centre;
            }
        }

        updateGPUGraphicsBuffer();
    }

    void discretizeAndRemoveDuplicate()
    {

    }

    public void UpdateSubsamplingPercentage(float v)
    {
        _vfx.SetFloat("ReducingPercentage", v);
    }

    public void updateGPUGraphicsBuffer()
    {
        ClearText();

        _vfx.Stop();
        _vfx.Reinit();

        numPoints = actualNumOfPoints;

        _position_buffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, numPoints, sizeof(float) * 3);
        _position_buffer.SetData(points, 0, 0, numPoints);
        _vfx.SetGraphicsBuffer("PointBuffer", _position_buffer);

        _color_buffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, numPoints, sizeof(float) * 4);
        _color_buffer.SetData(colors, 0, 0, numPoints);
        _vfx.SetGraphicsBuffer("ColorBuffer", _color_buffer);

        _vfx.SetInt("PointCount", numPoints);
        _vfx.Play();

        float percentage = _vfx.GetFloat("ReducingPercentage");

        AppendText("Active File: " +
            GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curFileName);
        AppendText("Original Points: " + numPoints);
        AppendText("Estimated Rendered Points: " + (int)(numPoints * percentage));
    }

    public void updateColor()
    {
        float threshhold_r = 0.5f;
        // count 
        for (int i = 0; i < numPoints; i++)
        {
            if (colors[i].r > threshhold_r)
            {
                colors[i].r = 1.0f;
            }
        }

        // update GPU buffer 
        _color_buffer.SetData(colors);
        _vfx.SetGraphicsBuffer("ColorBuffer", _color_buffer);
    }

    void Start()
    {
        _vfx = this.GetComponent<VisualEffect>();
        _vfx.Stop();
        _video = this.GetComponent<VideoPlayer>();
        if (_video)
        {
            _video.frameReady += videoPrepared;
            _video.sendFrameReadyEvents = true;
        }
    }

    void OnDestroy()
    {
        _position_buffer?.Dispose();
        _position_buffer = null;

        _color_buffer?.Dispose();
        _color_buffer = null;
    }


    public void fetchTextureFromGPU()
    {

        // depth is 960 x 540 while colormap is 960 x 1080 -> found from vfx graph 
        DepthMap = new Texture2D(_demuxer.DepthTexture.width, _demuxer.DepthTexture.height, TextureFormat.RGBA32, false);
        ColorMap = new Texture2D(_demuxer.ColorTexture.width, _demuxer.ColorTexture.height, TextureFormat.RGBA32, false);

        // copy from GPU to CPU
        RenderTexture.active = _demuxer.DepthTexture;
        DepthMap.ReadPixels(new Rect(0, 0, _demuxer.DepthTexture.width, _demuxer.DepthTexture.height), 0, 0);
        DepthMap.filterMode = FilterMode.Point;
        DepthMap.Apply();

        RenderTexture.active = _demuxer.ColorTexture;
        ColorMap.ReadPixels(new Rect(0, 0, _demuxer.ColorTexture.width, _demuxer.ColorTexture.height), 0, 0);
        ColorMap.filterMode = FilterMode.Point;
        ColorMap.Apply();
    }
    private float DepthHueMargin = 0.01f;
    private float DepthHuePadding = 0.01f;

    /*
     float RGB2Hue(float3 c)
    {
        float minc = min(min(c.r, c.g), c.b);
        float maxc = max(max(c.r, c.g), c.b);
        float div = 1 / (6 * max(maxc - minc, 1e-5));
        float r = (c.g - c.b) * div;
        float g = 1.0 / 3 + (c.b - c.r) * div;
        float b = 2.0 / 3 + (c.r - c.g) * div;
        float d = lerp(r, lerp(g, b, c.g < c.b), c.r < max(c.g, c.b));
        return frac(d + 1 - DepthHuePadding / 2) + DepthHuePadding / 2;
    }

     */

    float RGB2Hue(Color c)
    {
        float minc = Mathf.Min(Mathf.Min(c.r, c.g), c.b);
        float maxc = Mathf.Max(Mathf.Max(c.r, c.g), c.b);
        float div = 1 / (6 * Mathf.Max(maxc - minc, 1e-5f));
        float r = (c.g - c.b) * div;
        float g = 1.0f / 3 + (c.b - c.r) * div;
        float b = 2.0f / 3 + (c.r - c.g) * div;
        float test1;
        if (c.g < c.b)
            test1 = 1;
        else
            test1 = 0;
        float test2;
        if (c.r < Mathf.Max(c.g, c.b))
            test2 = 1;
        else
            test2 = 0;
        float d = Mathf.Lerp(r, Mathf.Lerp(g, b, test1), test2);
        float aux3 = d + 1 - DepthHuePadding / 2;
        return (aux3 - Mathf.Floor(aux3)) + DepthHuePadding / 2;
    }

    float DecodeDepth(Color rgb, Vector2 range)
    {
        // Hue decoding
        float depth = RGB2Hue(rgb);
        // Padding/margin
        depth = (depth - DepthHueMargin) / (1 - DepthHueMargin * 2);
        depth = (depth - DepthHuePadding) / (1 - DepthHuePadding * 2);
        // Depth range
        return Mathf.Lerp(range.x, range.y, depth);
    }

    // Inversion projection into the world space - from the vfx graph
    Vector3 DepthMapToCameraLocalPosition(Vector2 uv, float d, in Vector4 rayParams)
    {

        Vector3 ray = new Vector3();
        ray.x = Mathf.Lerp(-1, 1, uv.x);
        ray.y = Mathf.Lerp(-1, 1, uv.y);
        ray.z = 1;
        //new Vector3((uv - new Vector2(1, 1)).x, (uv - new Vector2(1, 1)).y, 1); // Lerp
        Vector2 tmp01 = (new Vector2(ray.x, ray.y) + new Vector2(rayParams.x, rayParams.y)) * new Vector2(rayParams.z, rayParams.w);
        ray.x = tmp01.x;
        ray.y = tmp01.y;
        return new Vector3((ray * d).x, (ray * d).y, (ray * d).z);
        //Vector4 tmp02 = inverseView * new Vector4((ray * d).x, (ray * d).y, (ray * d).z, 1);
        //return new Vector3(tmp02.x, tmp02.y, tmp02.z);
    }

    Vector3 LocalToWorldPosition(Vector3 localPosition, in Matrix4x4 inverseView)
    {
        Vector4 tmp02 = inverseView * new Vector4(localPosition.x, localPosition.y, localPosition.z, 1);
        return new Vector3(tmp02.x, tmp02.y, tmp02.z);
    }

    private byte[] ConvertToBytes(Vector3[] vertices, Color[] colors)
    {
        var len = vertices.Length;
        var pointInfoByteSize = sizeof(float) * 3 + 3;
        var result = new byte[pointInfoByteSize * len];
        for (int i = 0; i < len; i++)
        {
            var idx = i * pointInfoByteSize;

            var x = BitConverter.GetBytes(vertices[i].x);
            result[idx + 0] = x[0];
            result[idx + 1] = x[1];
            result[idx + 2] = x[2];
            result[idx + 3] = x[3];

            var y = BitConverter.GetBytes(vertices[i].y);
            result[idx + 4] = y[0];
            result[idx + 5] = y[1];
            result[idx + 6] = y[2];
            result[idx + 7] = y[3];

            var z = BitConverter.GetBytes(vertices[i].z);
            result[idx + 8] = z[0];
            result[idx + 9] = z[1];
            result[idx + 10] = z[2];
            result[idx + 11] = z[3];

            result[idx + 12] = (byte)(colors[i].r * 255);
            result[idx + 13] = (byte)(colors[i].g * 255);
            result[idx + 14] = (byte)(colors[i].b * 255);
        }

        return result;
    }

    public void writeCurrentFrameToPLY()
    {
        string file = outputFolderPath + "/" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff_" + _video.frame) + ".ply";

        using (var sw = new StreamWriter(file, false, Encoding.ASCII))
        {
            sw.Write("ply\n" +
                     "format binary_little_endian 1.0\n" +
                     "element vertex " + actualNumOfPoints + "\n" +
                     "property float32 x\n" +
                     "property float32 y\n" +
                     "property float32 z\n");

            sw.Write("property uchar red\n" +
                        "property uchar green\n" +
                        "property uchar blue\n");

            sw.Write("end_header\n");
        }
        var fs = File.Open(file, FileMode.Append);
        using (var wr = new BinaryWriter(fs))
        {
            wr.Write(ConvertToBytes(points, colors));
        }

        Debug.Log("has been written to: " + file);
    }

    //////////////////////////////////////////////////////////

    void InsertTextOnTop(string t)
    {
        if (infoText == null) return;
        infoText.text = t + "\n" + infoText.text;
    }

    void AppendText(string t)
    {
        if (infoText == null) return;
        infoText.text = infoText.text + t + "\n";
    }

    void ReplaceText(string t)
    {
        if (infoText == null) return;
        infoText.text = t;
    }

    void ClearText()
    {
        if (infoText == null) return;
        infoText.text = "";
    }

    void LoadPointCloudsFromFile()
    {
        ReadPointsFromPLY(Application.streamingAssetsPath + "/" + fileName + ".ply");
        //ReplaceText("Pointcloud Loaded: " + Application.streamingAssetsPath + "/" + fileName + ".ply");
        Debug.Log("Loading " + Application.streamingAssetsPath + "/" + fileName + ".ply");
        Debug.Log("numPoints = " + numPoints);
        actualNumOfPoints = numPoints;
        //preprocessing();
        updateGPUGraphicsBuffer();

    }

    public void LoadPointcloudFromStringManager() {
        string fn = GameObject.FindGameObjectWithTag("StringManager").GetComponent<StringManager>().curActiveFilePath;
        ReadPointsFromPLY(fn);
        //ReplaceText("Pointcloud Loaded: " + fn);
        Debug.Log("Loading " + fn);
        Debug.Log("numPoints = " + numPoints);
        actualNumOfPoints = numPoints;
        updateGPUGraphicsBuffer();
    }

    void InitializeBuffers()
    {
        numPoints = estimatedNumPoints;
        points = new Vector3[estimatedNumPoints];
        colors = new Color[estimatedNumPoints];
        indices = new int[estimatedNumPoints];
        _cam_positions = new List<Vector3>();
        _cam_rotations = new List<Quaternion>();
    }

    //int resolution = 20;
    //int ratioW = 16;
    //int ratioH = 9;
    public int stepX = 3;
    public int stepY = 3;
    public bool sequential = true;

    Vector3 DepthMapCoordiToWorld(int x, int y)
    {
        int w = _demuxer.DepthTexture.width; // 960
        int h = _demuxer.DepthTexture.height; // 540

        if (x < 0) x = 0; // safty
        if (x > w) x = w;
        if (y < 0) y = 0;
        if (y > h) y = h;

        Color p = DepthMap.GetPixel(x, y); // 
        Vector3 pLocal = DepthMapToCameraLocalPosition(
            new Vector2((float)x / w, (float)y / h),
            //DecodeDepth(p, _decoder.Metadata.DepthRange),
            p.r,
            BibcamRenderUtils.RayParams(_decoder.Metadata));
        Vector3 pWorld = LocalToWorldPosition(
            pLocal, BibcamRenderUtils.InverseView(_decoder.Metadata)); // world position! 
        return pWorld;
    }

    bool CheckIfIsFlat(int x, int y, float delta)
    {
        float dx = Vector3.Distance(DepthMapCoordiToWorld(x - 1, y), DepthMapCoordiToWorld(x + 1, y));
        float dy = Vector3.Distance(DepthMapCoordiToWorld(x, y - 1), DepthMapCoordiToWorld(x, y + 1));
        return (dx < delta) && (dy < delta);
    }

    [HideInInspector] public List<Vector3> _cam_positions;
    [HideInInspector] public List<Quaternion> _cam_rotations;

    void AccumulateCameraPointsFromFrames()
    {
        _cam_positions.Add(_decoder.Metadata.CameraPosition);
        _cam_rotations.Add(_decoder.Metadata.CameraRotation);
    }

    private byte[] ArrayListToBytes(List<Vector3> array)
    {
        var len = array.Count;
        var VectorByteSize = sizeof(float) * 3;
        var result = new byte[4 + VectorByteSize * len]; // sizeof int = 4
        var num = BitConverter.GetBytes(len);
        var idx = 0;
        result[idx + 0] = num[0];
        result[idx + 1] = num[1];
        result[idx + 2] = num[2];
        result[idx + 3] = num[3];
        idx += 4;
        for (int i = 0; i < len; i++)
        {
            var x = BitConverter.GetBytes(array[i].x);
            result[idx + 0] = x[0];
            result[idx + 1] = x[1];
            result[idx + 2] = x[2];
            result[idx + 3] = x[3];

            var y = BitConverter.GetBytes(array[i].y);
            result[idx + 4] = y[0];
            result[idx + 5] = y[1];
            result[idx + 6] = y[2];
            result[idx + 7] = y[3];

            var z = BitConverter.GetBytes(array[i].z);
            result[idx + 8] = z[0];
            result[idx + 9] = z[1];
            result[idx + 10] = z[2];
            result[idx + 11] = z[3];

            idx += VectorByteSize;
        }

        return result;
    }


    void writeCamPositionsToBinFile()
    {
        string file = outputFolderPath + "/" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff_campose") + ".bin";
        var fs = File.Open(file, FileMode.OpenOrCreate);
        using (var wr = new BinaryWriter(fs))
            wr.Write(ArrayListToBytes(_cam_positions));
    }

    public GameObject cameraPosePrefeb;

    void debugCamPositionsByRenderingThem()
    {
        foreach (var p in _cam_positions)
        {
            Instantiate(cameraPosePrefeb, this.transform.TransformPoint(p), Quaternion.identity);
        }
    }


    void AccumulatePointsFromFrames()
    {
        fetchTextureFromGPU();

        //int hPixels = resolution * ratioH;
        //int wPixels = resolution * ratioW;
        //int h = hPixels < _demuxer.DepthTexture.height ? hPixels : _demuxer.DepthTexture.height;
        //int w = wPixels < _demuxer.DepthTexture.width ? wPixels : _demuxer.DepthTexture.width;

        int h = _demuxer.DepthTexture.height; // 540
        int w = _demuxer.DepthTexture.width; // 960

        int pointsCurrentFrame = w * h;
        if((actualNumOfPoints + pointsCurrentFrame) < estimatedNumPoints)
        {
            if (sequential)
            {
                for (var y = 0; y < h; y += stepY)
                {
                    for (var x = 0; x < w; x += stepX)
                    {
                        Color p = DepthMap.GetPixel(x, y);
                        Vector3 pWorld = DepthMapCoordiToWorld(x, y);

                        Camera.main.transform.position = _decoder.Metadata.CameraPosition;
                        Camera.main.transform.rotation = _decoder.Metadata.CameraRotation;
                        Camera.main.projectionMatrix = _decoder.Metadata.ReconstructProjectionMatrix(Camera.main.projectionMatrix);
                        Vector3 pView = Camera.main.WorldToViewportPoint(pWorld);
                        if ((pView.x >= 0.1f && pView.x <= 0.9f) && (pView.y >= 0.1f && pView.y <= 0.9f) && (pView.z >= 0 && pView.z <= 10.0f))
                        {
                            continue;
                        }
                        Color c = ColorMap.GetPixel(x, 2 * y); // just use half of the color pixels
                        if (c.a >= 0.015f)
                        {
                            continue;
                        }
                        if (!CheckIfIsFlat(x, y, 0.02f))
                        {
                            continue;
                        }

                        // Grid Snap
                        //Vector3 GridPoint = new Vector3(
                        //    (float)Math.Floor(100 * gridSize * pWorld.x), 
                        //    (float)Math.Floor(100 * gridSize * pWorld.y), 
                        //    (float)Math.Floor(100 * gridSize * pWorld.z));
                        //Vector3 Final = GridPoint * 0.01f * (1.0f/ gridSize);

                        points[actualNumOfPoints] = pWorld; // world position! 
                        colors[actualNumOfPoints] = c;
                        actualNumOfPoints++;
                    }
                }
            }

            if (!sequential)
            {
                int numOfPointsCurrentFrame = resolution * 16 * 9;
                for (int idx = 0; idx < numOfPointsCurrentFrame; idx++)
                {
                    //UnityEngine.Random.InitState(idx);
                    int x = (int)(UnityEngine.Random.Range(0.0f, 1.0f) * w);
                    int y = (int)(UnityEngine.Random.Range(0.0f, 1.0f) * h);
                    Color p = DepthMap.GetPixel(x, y);
                    Vector3 pWorld = DepthMapCoordiToWorld(x, y);

                    Camera.main.transform.position = _decoder.Metadata.CameraPosition;
                    Camera.main.transform.rotation = _decoder.Metadata.CameraRotation;
                    Camera.main.projectionMatrix = _decoder.Metadata.ReconstructProjectionMatrix(Camera.main.projectionMatrix);
                    Vector3 pView = Camera.main.WorldToViewportPoint(pWorld);
                    if ((pView.x >= 0.2f && pView.x <= 0.8f) && (pView.y >= 0.1f && pView.y <= 0.9f) && (pView.z >= 0 && pView.z <= 10.0f))
                    {
                        continue;
                    }
                    Color c = ColorMap.GetPixel(x, 2 * y); // just use half of the color pixels
                    if (c.a >= 0.015f)
                    {
                        continue;
                    }
                    if (!CheckIfIsFlat(x, y, 0.02f))
                    {
                        continue;
                    }

                    // Grid Snap
                    //Vector3 GridPoint = new Vector3(
                    //    (float)Math.Floor(100 * gridSize * pWorld.x), 
                    //    (float)Math.Floor(100 * gridSize * pWorld.y), 
                    //    (float)Math.Floor(100 * gridSize * pWorld.z));
                    //Vector3 Final = GridPoint * 0.01f * (1.0f/ gridSize);

                    points[actualNumOfPoints] = pWorld; // world position! 
                    colors[actualNumOfPoints] = c;
                    actualNumOfPoints++;
                }
            }
        }
        else // point limit reached
        {
            writeCurrentFrameToPLY();
            bAccumulatePointsFromFrames = false;
            Debug.Log("we are: " + _video.frame);
        }
    }

    void ReplaceAllPointsWithCurrentFrame()
    {
        fetchTextureFromGPU();

        // reset points on CPU
        int h = _demuxer.DepthTexture.height;
        int w = _demuxer.DepthTexture.width;
        int pointsCurrentFrame = w * h;
        actualNumOfPoints = 0;
        if ((actualNumOfPoints + pointsCurrentFrame) < estimatedNumPoints)
        {
            for (var y = 0; y < h; y++)
            {
                for (var x = 0; x < w; x++)
                {
                    Color p = DepthMap.GetPixel(x, y);
                    Vector3 pw = // decode depth on CPU to avoid depth value normalization, and correct world position
                        LocalToWorldPosition(
                            DepthMapToCameraLocalPosition(
                                new Vector2((float)x / w, (float)y / h),
                                DecodeDepth(p, _decoder.Metadata.DepthRange),
                                BibcamRenderUtils.RayParams(_decoder.Metadata)),
                        BibcamRenderUtils.InverseView(_decoder.Metadata));
                    points[actualNumOfPoints] = pw; // world position! 

                    Color c = ColorMap.GetPixel(x, 2 * y); // just use half of the color pixels
                    colors[actualNumOfPoints] = c;
                    actualNumOfPoints++;
                }
            }
        }

        // upload again to GPU 
        updateGPUGraphicsBuffer();
    }

    //////////////////////////////////////////////////////////
    private void videoPrepared(VideoPlayer source, long frameIdx)
    {
        frameReadyAndNotProcessed = true;
    }

    public int numCamPoses = 0;

    public bool fbLoadPointCloudsFromFile = false;
    public bool fbLoadPointCloudsFromVideo = false;
    public bool fbUpdateGPUGraphicsBuffer = false;
    public bool bAccumulatePointsFromFrames = false;
    public bool bAccumulatePointsFromFramesJustOnce = false;
    public bool bAccumulateCameraPointsFromFrames = false;
    public bool bReplaceAllPointsWithCurrentFrame = false;
    public bool fbWriteCurrentFrameToPLY = false;
    public bool fbDebugCamPositionsByRenderingThem = false;
    public bool fbWriteCamPositionsToBinFile = false;
    public bool fbReadCamPositions = false;

    void Update()
    {
        if (fbLoadPointCloudsFromFile)
        {
            fbLoadPointCloudsFromFile = false;
            LoadPointCloudsFromFile();
        }

        if (fbLoadPointCloudsFromVideo)
        {
            if (_video.frame <= (long)(frameStop + frameStart)) { 
                if (_video.frame == -1) // at the beginning 
                {
                    _video.frame = (long)frameStart;
                    _video.Play();
                    numOfFrames = (long)_video.frameCount;
                    InitializeBuffers();
                }
                if (frameReadyAndNotProcessed)
                {
                    frameReadyAndNotProcessed = false;
                    _video.Pause();

                    // do some processing 
                    // decoding 
                    _decoder.DecodeSync(_video.texture); // decode the meta data 
                    _demuxer.Demux(_video.texture, _decoder.Metadata); // demux the color and depth images 
                    currentTexture = _demuxer.DepthTexture;
                    currentFrame = _video.frame;

                    //
                    if(bAccumulatePointsFromFrames)
                    {
                        AccumulatePointsFromFrames();
                        if (bAccumulatePointsFromFramesJustOnce) 
                            bAccumulatePointsFromFrames = false;
                    }

                    //
                    if(bReplaceAllPointsWithCurrentFrame)
                        ReplaceAllPointsWithCurrentFrame();

                    //
                    if (bAccumulateCameraPointsFromFrames)
                    {
                       // bAccumulateCameraPointsFromFrames = false;
                        AccumulateCameraPointsFromFrames();
                        numCamPoses = _cam_positions.Count;
                        if(_video.frame > (long)_video.frameCount - 1) 
                            bAccumulateCameraPointsFromFrames = false;
                    }

                    _video.frame++;
                    _video.Play();
                }
            }
        }

        if (fbUpdateGPUGraphicsBuffer)
        {
            fbUpdateGPUGraphicsBuffer = false;
            updateGPUGraphicsBuffer();
        }

        if (fbWriteCurrentFrameToPLY)
        {
            fbWriteCurrentFrameToPLY = false;
            writeCurrentFrameToPLY();
        }

        if(_video)
            if(_video.frame == (long)_video.frameCount)
            {
                writeCurrentFrameToPLY();
                bAccumulatePointsFromFrames = false;
            }

        if (fbWriteCamPositionsToBinFile)
        {
            fbWriteCamPositionsToBinFile = false;
            writeCamPositionsToBinFile();
        }

        if (fbDebugCamPositionsByRenderingThem)
        {
            fbDebugCamPositionsByRenderingThem = false;
            debugCamPositionsByRenderingThem();
        }

        if (fbReadCamPositions)
        {
            fbReadCamPositions = false;
            readCamPositions();
        }
    }
}
