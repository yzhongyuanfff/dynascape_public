using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum VisualStyles
{
    Normal, 
    HighSamplingRate, 
    LowSamplingRate, 
    ColorHighlighted, 
    LongLifetime, 
    ShortLifetime
}
public class StyleStatus
{
    // frame centered 
    public VisualStyles HumanStyle;
    public VisualStyles ForegroundStyle;
    public VisualStyles BackgroundStyle;
    public VisualStyles BorderStyle;

    // space 
    public VisualStyles FloorStyle;
    public VisualStyles PhysicalBoxStyle;
}

public class VFXTune : MonoBehaviour
{
    [SerializeField] GameObject UIContent;
    [SerializeField] GameObject DashboardUIContent;
    [SerializeField] GameObject VFXFineTuneUIContent;
    [SerializeField] GameObject BoxIllustrator; // PhyEnvPointEffect

    ixVideoPlayerController videoPlayerController;

    List<string> RadioControlVaribleNames = new List<string>(); // scene extraction etc. 

    StyleStatus styleStatus = new StyleStatus(); // 
    List<string> segmentedObjectNames = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        // Radio Buttons 
        RadioControlVaribleNames.Add("ExtractArch");
        RadioControlVaribleNames.Add("ExtractHuman");
        RadioControlVaribleNames.Add("ExtractSideBorder");
        RadioControlVaribleNames.Add("ExtractThinBorder");


        videoPlayerController = GetComponent<ixVideoPlayerController>();
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        PassOBB();
    }

    void PassOBB()
    {
        //BoxIllustrator.transform.position
        videoPlayerController.RefVFX().SetVector3("OBB_center", BoxIllustrator.transform.position);
        videoPlayerController.RefVFX().SetVector3("OBB_angles", BoxIllustrator.transform.rotation.eulerAngles);
        videoPlayerController.RefVFX().SetVector3("OBB_size", BoxIllustrator.transform.localScale);
    }

    void ResetBools()
    {
        foreach (var name in RadioControlVaribleNames)
        {
            videoPlayerController.RefVFX().SetBool(name, false);
        }
    }

    //void HighlightingVariousParts()
    //{
    //    ResetBools();
    //    videoPlayerController.RefVFX().SetBool("Highlighting", true);
    //}

    //void ExtractArchitecturalInformation()
    //{
    //    ResetBools();
    //    videoPlayerController.RefVFX().SetBool("ExtractArch", true);
    //}


    //void ExtractHumanInformation()
    //{
    //    ResetBools();
    //    videoPlayerController.RefVFX().SetBool("ExtractHuman", true);
    //}

    //void ExtractBorder()
    //{
    //    ResetBools();
    //    videoPlayerController.RefVFX().SetBool("ExtractBorder", true);
    //}

    void ShowResult()
    {
        foreach (FieldInfo fi in styleStatus.GetType().GetFields())
        {
            ixUtility.LogMe(fi.GetValue(styleStatus).ToString());
        }
    }

    void ToggleCallBack(string name, GameObject uui)
    {

    }

    List<GameObject> radiouis = new List<GameObject>();

    void ResetBtnUIs()
    {
        foreach(var ui in radiouis)
        {
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        }
    }

    void InitUI()
    {
        //ixUtility.MakeAButton(UIContent, "Highlighting Various Parts", HighlightingVariousParts);
        //ixUtility.MakeAButton(UIContent, "Extract Architectural Information", ExtractArchitecturalInformation);
        //ixUtility.MakeAButton(UIContent, "Extract Human Information", ExtractHumanInformation);
        //ixUtility.MakeAButton(UIContent, "Extract Border", ExtractBorder);

        //RadioControlVaribleNames.Add("ExtractArch");
        //RadioControlVaribleNames.Add("ExtractHuman");
        //RadioControlVaribleNames.Add("ExtractSideBorder");
        //RadioControlVaribleNames.Add("ExtractThinBorder");

        // Radio button group - rare
        foreach (var name in RadioControlVaribleNames)
        {
            GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false); // not toggle by default 
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = name;
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate{
                bool ToggleState = ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf;
                bool NewState = !ToggleState;
                ResetBools();
                ResetBtnUIs();
                videoPlayerController.RefVFX().SetBool(name, NewState);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(NewState);
            });
            radiouis.Add(ui);
        }

        // visual effects toggle - most cases
        {
            List<string> VaribleNames = new List<string>();
            VaribleNames.Add("Highlighting");
            VaribleNames.Add("ApplyPointSizeVaryingEffect");
            VaribleNames.Add("DoTrailRemoval");
            foreach (var name in VaribleNames)
            {
                GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), VFXFineTuneUIContent.transform);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false); // not toggle by default 
                //if(name == "DoTrailRemoval")
                //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(true); // excepts
                ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = name;
                ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
                    bool ToggleState = ui.transform.Find("Btn").Find("ToggleState").gameObject.activeSelf;
                    bool NewState = !ToggleState;
                    videoPlayerController.RefVFX().SetBool(name, NewState);
                    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(NewState);
                });
            }
        }

        // visual effect bools no toggle feedback - see log panel for feedback/ audio - special 
        {
            ixUtility.MakeAButton(VFXFineTuneUIContent, "Render With Quads", delegate {
                videoPlayerController.RefVFX().SetInt("WhichPrimitive", 0);
                ixUtility.LogMe("Rendering With Quads");
            });
            ixUtility.MakeAButton(VFXFineTuneUIContent, "Render With Cubes", delegate {
                videoPlayerController.RefVFX().SetInt("WhichPrimitive", 1);
                ixUtility.LogMe("Rendering With Cubes");
            });
            ixUtility.MakeAButton(VFXFineTuneUIContent, "Render With Spheres", delegate {
                videoPlayerController.RefVFX().SetInt("WhichPrimitive", 2);
                ixUtility.LogMe("Rendering With Spheres");
            });
        }

        Vector2 range = new Vector2(1, 2);
        ixUtility.MakeASlider(VFXFineTuneUIContent, "Lifetime", range, delegate(float f) {
            Debug.Log("Here value = " + range.x + f * (range.y - range.x));

        });

        //ixUtility.MakeASlider(VFXFineTuneUIContent, "", delegate { });

        //foreach (int i in Enum.GetValues(typeof(VisualStyles)))
        //{
        //    GameObject uibtn = Instantiate((GameObject)Resources.Load("UIBtn",
        //            typeof(GameObject)), DashboardUIContent.transform);
        //    uibtn.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = Enum.GetName(typeof(VisualStyles), i);
        //    uibtn.transform.Find("ToggleState").gameObject.SetActive(false);

        //    // call back 
        //    uibtn.GetComponent<Button>().onClick.AddListener(delegate {
        //        condition.GetType().GetField(type.Name.ToLower()).SetValue(condition, i);// cast?
        //        uibtn.transform.Find("ToggleState").gameObject.SetActive(true);
        //        ShowCondition();
        //    });
        //}

        //segmentedObjectNames.Add(typeof(StyleStatus).get);
        //segmentedObjectNames.Add(typeof(VisualStyles));
        //segmentedObjectNames.Add(typeof(VisualStyles));
        //segmentedObjectNames.Add(typeof(VisualStyles));
        //segmentedObjectNames.Add(typeof(VisualStyles));
        //segmentedObjectNames.Add(typeof(VisualStyles));

        //VisualStyles
        //StyleStatus styleStatus = new StyleStatus();
        //List<string> segmentedObjectNames = new List<string>();

        foreach (FieldInfo fi in typeof(StyleStatus).GetFields())
        {
            //ixUtility.LogMe(fi.Name.ToString());
            segmentedObjectNames.Add(fi.Name.ToString());
        }


        foreach (string styleName in segmentedObjectNames)
        {
            GameObject ui = Instantiate((GameObject)Resources.Load("UIConditionOptionPrefeb",
                typeof(GameObject)), DashboardUIContent.transform);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = styleName;

            List<GameObject> conditionBtnList = new List<GameObject>();

            var type = typeof(VisualStyles);
            var list = Enum.GetValues(type);
            foreach (int i in Enum.GetValues(type))
            {
                GameObject uibtn = Instantiate((GameObject)Resources.Load("UIBtn",
                    typeof(GameObject)), ui.transform);
                uibtn.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = Enum.GetName(type, i);

                // init the btn toggle state 
                if ((int)styleStatus.GetType().GetField(styleName).GetValue(styleStatus) == i)
                    uibtn.transform.Find("ToggleState").gameObject.SetActive(true);
                else
                    uibtn.transform.Find("ToggleState").gameObject.SetActive(false);

                // call back 
                uibtn.GetComponent<Button>().onClick.AddListener(delegate {
                    styleStatus.GetType().GetField(styleName).SetValue(styleStatus, i);// cast?
                    uibtn.transform.Find("ToggleState").gameObject.SetActive(true);
                    ShowResult();
                });
                conditionBtnList.Add(uibtn);
            }

            foreach (GameObject currentBtn in conditionBtnList)// disable the other btns 
            {
                currentBtn.GetComponent<Button>().onClick.AddListener(delegate {
                    foreach (GameObject btn in conditionBtnList)
                    {
                        if (!btn.Equals(currentBtn))
                        {
                            btn.transform.Find("ToggleState").gameObject.SetActive(false);
                        }
                    }
                });
            }
        }
    }
}
