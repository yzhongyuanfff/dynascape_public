using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;
using UnityEngine.VFX;

public class VFXCSharpAdapter : MonoBehaviour
{
    VisualEffect vfx; 
    // Start is called before the first frame update

    //public GameObject[] renderPrimitivesVisual;

    public Mesh[] meshes;

    void Start()
    {
        vfx = this.gameObject.GetComponent<VisualEffect>();
        //renderPrimitivesVisual[0].transform.Find("BackPlateToggleState").gameObject.SetActive(true);
    }

    // floats 
    public void updateParticleSize(float eventData)
    {
        float value = 15 * eventData;
        this.GetComponent<VisualEffect>().SetFloat("ParticleSize", value);
    }
    public void updateHumanPersistenceTime(float eventData)
    {
        float value = 15 * eventData;
        this.GetComponent<VisualEffect>().SetFloat("HumanPersistenceTime", value);
    }
    public void updateGridSize(float eventData)
    {
        float value = 15 * eventData;
        this.GetComponent<VisualEffect>().SetFloat("GridSize", value);
    }
    public void updateDepthMin(float eventData)
    {
        float value = 15 * eventData;
        this.GetComponent<VisualEffect>().SetFloat("DepthMin", value);
    }
    public void updateDepthMax(float eventData)
    {
        float value = 15 * eventData;
        this.GetComponent<VisualEffect>().SetFloat("DepthMax", value);
    }
    public void updateLifetime(float eventData)
    {
        float value = 15 * eventData;
        this.GetComponent<VisualEffect>().SetFloat("Lifetime", value);
    }
    public void updateUpdateRate(float eventData)
    {
        float value = 15 * eventData;
        this.GetComponent<VisualEffect>().SetFloat("UpdateRate", value);
    }
    public void updateResolution(float eventData)
    {
        float value = 15 * eventData;
        this.GetComponent<VisualEffect>().SetFloat("Resolution", value);
    }

    // int 

    // bools 
    public void updateCubeRendering(bool b)
    {
        if(b)
            this.GetComponent<VisualEffect>().SetMesh("Primitive", meshes[0]);
    }
    public void updateSphereRendering(bool b)
    {
        if (b)
            this.GetComponent<VisualEffect>().SetMesh("Primitive", meshes[1]);
    }
    public void updateQuadRendering(bool b)
    {
        if (b)
            this.GetComponent<VisualEffect>().SetMesh("Primitive", meshes[2]);
    }
    public void updateHumanFade(bool b)
    {
        this.GetComponent<VisualEffect>().SetBool("HumanFade", b);
    }
    public void updateIsGridOn(bool b)
    {
        this.GetComponent<VisualEffect>().SetBool("isGridOn", b);
    }
    public void updateIsSequential(bool b)
    {
        this.GetComponent<VisualEffect>().SetBool("isSequential", b);
    }
    public void updateBibcamPersistence(bool b)
    {
        this.GetComponent<VisualEffect>().SetBool("BibcamPersistence", b);
    }
    public void updateEnableAlgorithm(bool b)
    {
        this.GetComponent<VisualEffect>().SetBool("EnableAlgorithm", b);
    }


    //
    public void updateEnabled(bool b)
    {
        if(b)
            vfx.Reinit();
        else
            vfx.Stop();
    }
}
