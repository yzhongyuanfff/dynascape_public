// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Video;

public class BibcamRGBDVideoConfigure : MonoBehaviour
{
    public VideoPlayer VideoPlayer;
    public VisualEffect visualEffect;

    public void setEnabled(bool b)
    {
        if (b)
        {
            VideoPlayer.Play();
            visualEffect.Play();
            this.gameObject.SetActive(true);
        }
        else
        {
            VideoPlayer.Stop();
            visualEffect.Stop();
            this.gameObject.SetActive(false);
        }
    }
}
