// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using Bibcam.Decoder;
using Klak.Hap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class HAPRGBDVideoController : MonoBehaviour
{
    HapPlayer _video;
    VideoPreprocessing _processor;

    // input
    [SerializeField] BibcamMetadataDecoder _decoder = null;
    [SerializeField] BibcamTextureDemuxer _demuxer = null;
    [SerializeField] public VisualEffect _vfx;

    // control 
    public bool bGoToTargetFrame = false;
    // public bool bDecodeCurrentFrame = false;
    public long targetFrame = 100;
    public float framerate = 60;

    //
    bool AllowResume = false;
    float _playbackSpeed = 1; 
    public ulong frameStart = 0;
    public ulong frameStop = 0;

    public Material ProcessingMaterial; 
    
    public RenderTexture rt;
    public Renderer targetRenderer;
    public string targetMaterialProperty = "_MainTex";

    // Start is called before the first frame update
    void Start()
    {
        //_video = GetComponent<HapPlayer>();
        //_processor = GetComponent<VideoPreprocessing>();

        _processor = gameObject.AddComponent<VideoPreprocessing>();
        _processor.SetProcessingMaterial(ProcessingMaterial);
    }

    /// <summary>
    /// 
    /// </summary>
    public void OpenActiveVideo()
    {
        string fpath = GameObject.FindGameObjectWithTag("StringManager")
            .GetComponent<StringManager>().curActiveFilePath;

        if (_video) Destroy(_video);

        _video = gameObject.AddComponent<HapPlayer>();
        _video.targetTexture = rt;
        _video.targetRenderer = targetRenderer;
        _video.targetMaterialProperty = targetMaterialProperty;
        _video._filePath = fpath;
        _video._pathMode = HapPlayer.PathMode.LocalFileSystem;
        _video.speed = 0;

        //_processor.SetHapPlayer(_video);

        //CreateHAPComponent();
        //if (_video) _video.Open(activeFile, HapPlayer.PathMode.LocalFileSystem);
    }

    // Update is called once per frame
    void Update()
    {
        if (bGoToTargetFrame && _video)
        {
            bGoToTargetFrame = false;
            float targetTime = targetFrame / framerate;
            _video.time = targetTime;
            Debug.Log("seeking!");
        }

        //if (_video.time > frameStop / framerate)
        //{
        //    _video.time = frameStart / framerate;
        //}

        if(_video)
            if (_video.speed > 0) // is playing back the video, might receive new frame constantly 
                DecodeCurrentFrameAndSendToVFX();
    }

    void DecodeCurrentFrameAndSendToVFX()
    {
        Texture currentTexture = _processor.GetProcessedTexture(_video.texture);
        _decoder.DecodeSync(currentTexture);
        _demuxer.Demux(currentTexture, _decoder.Metadata);
        _vfx.SendEvent("OnPlay");
    }

    public void UpdatePlaybackSpeed(float v)
    {
        _playbackSpeed = v;
        _video.speed = v;
    }

    public void StartTheVideo()
    {
        _vfx.Stop();
        _vfx.Reinit();
        _video.time = frameStart / framerate;
        _video.speed = _playbackSpeed;
    }

    public void PauseTheVideo()
    {
        _video.speed = 0;
        _vfx.pause = true;
    }

    public void DeepPauseTheVideo()
    {
        AllowResume = false;
        PauseTheVideo();
    }

    public void ResumeTheVideo()
    {
        if (AllowResume)
        {
            _video.speed = _playbackSpeed;
            _vfx.pause = false;
        }
    }

    public void DeepResumeTheVideo()
    {
        AllowResume = true;
        ResumeTheVideo();
    }

    public void UpdateStartFrame(float sv)
    {
        frameStart = (ulong)(sv * _video.frameCount);
    }

    public void UpdateStopFrame(float sv)
    {
        frameStop = (ulong)(sv * _video.frameCount);
    }
}
