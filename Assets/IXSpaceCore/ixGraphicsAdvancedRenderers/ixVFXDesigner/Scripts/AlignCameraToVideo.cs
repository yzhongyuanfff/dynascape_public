using Bibcam.Decoder;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AlignCameraToVideo : MonoBehaviour
{
    public GameObject UIContent;
    [SerializeField] BibcamMetadataDecoder _decoder = null;

    //
    GameObject mixedRealityPlayspace;
    bool alignCameraToVideo = false;

    // Start is called before the first frame update
    void Start()
    {
        mixedRealityPlayspace = GameObject.FindGameObjectWithTag("MixedRealityPlayspace");
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (alignCameraToVideo && mixedRealityPlayspace && (_decoder != null))
        {
            mixedRealityPlayspace.transform.position = 
                this.transform.TransformPoint(_decoder.Metadata.CameraPosition);
        }
    }

    void ToggleCameraFollowing()
    {
        alignCameraToVideo = !alignCameraToVideo;
    }

    void InitUI()
    {
        if (UIContent == null) return;

        {
            GameObject ui = Instantiate((GameObject)
                Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text =
                "ToggleCameraFollowing";
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
                ToggleCameraFollowing();
            });
        }
    }
}
