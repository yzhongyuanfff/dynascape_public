using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class SpatialMaskBinder : MonoBehaviour
{
    VisualEffect _vfx;
    public List<GameObject> spheres;

    // Start is called before the first frame update
    void Start()
    {
        _vfx = GetComponent<VisualEffect>();
    }

    // Update is called once per frame
    void Update()
    {
        int idx = 1;
        foreach(GameObject s in spheres)
        {
            Vector3 sphereInOriginSpace = this.transform.InverseTransformPoint(s.transform.position);
            Vector4 composedparameter = new Vector4(
                sphereInOriginSpace.x,
                sphereInOriginSpace.y,
                sphereInOriginSpace.z,
                s.transform.localScale.x / 2.0f);
            _vfx.SetVector4("SpatialMask" + idx.ToString(), composedparameter);
            idx++;
        }
    }
}
