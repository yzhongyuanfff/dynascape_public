using Klak.Hap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPreprocessing : MonoBehaviour
{

    CustomRenderTexture _processedTexture;
    Material _preprocessingMaterial;


    // Start is called before the first frame update
    void Start()
    {
        _processedTexture = new CustomRenderTexture(1920, 1080, RenderTextureFormat.Default, RenderTextureReadWrite.Default);
        _processedTexture.updateMode = CustomRenderTextureUpdateMode.Realtime;
        _processedTexture.initializationSource = CustomRenderTextureInitializationSource.Material;
        _processedTexture.initializationMode = CustomRenderTextureUpdateMode.Realtime;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_pm"></param>
    public void SetProcessingMaterial(Material _pm)
    {
        _preprocessingMaterial = _pm;
    }

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <returns></returns>
    //public Texture GetProcessedVideoFrame()
    //{

    //    if (_preprocessingMaterial != null)
    //    {
    //        _preprocessingMaterial.mainTexture = _video.texture;
    //        _processedTexture.initializationMaterial = _preprocessingMaterial;
    //        //_processedTexture.initializationMaterial.SetFloat("_Width", width);
    //    }

    //    return _processedTexture;
    //}

    public Texture GetProcessedTexture(Texture raw)
    {
        if (_preprocessingMaterial != null)
        {
            _preprocessingMaterial.mainTexture = raw;
            _processedTexture.initializationMaterial = _preprocessingMaterial;
            //_processedTexture.initializationMaterial.SetFloat("_Width", width);
        }

        return _processedTexture;
    }
}
