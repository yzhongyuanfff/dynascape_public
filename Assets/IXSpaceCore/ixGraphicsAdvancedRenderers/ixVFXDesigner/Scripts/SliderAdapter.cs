using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public class SliderAdapter : MonoBehaviour
{
    public VisualEffect _vfx;

    Slider s;
    // Start is called before the first frame update
    void Start()
    {
        s.value = _vfx.GetFloat("ReducingPercentage");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
