// ------------------------------------------------------------------------------------
// <copyright company="Technische Universität Dresden">
//      Copyright (c) Technische Universität Dresden.
//      Licensed under the MIT License.
// </copyright>
// <author>
//      Zhongyuan Yu <zhongyuan.yu@tu-dresden.de>
// </author>
// ------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class AdaptivePointControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            this.GetComponent<VisualEffect>().SetInt(Shader.PropertyToID("PointLimit"), 18000);
        }
        else
        {
            this.GetComponent<VisualEffect>().SetInt(Shader.PropertyToID("PointLimit"), 1000000);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
