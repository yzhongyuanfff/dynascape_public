using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.UI;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.Rendering.Universal.Internal;

public class VFXStyleSliders : MonoBehaviour
{
    [SerializeField]
    private VFXStyleObject vfxStyle;
    
    [SerializeField]
    private int vfxStyleNumber;

    public PinchSlider shapeSlider;
    public PinchSlider sizeSlider;
    public PinchSlider alphaSlider;
    public PinchSlider lifetimeSlider;
    public PinchSlider turbulenceSlider;
    public PinchSlider dragSlider;
    public PinchSlider replacementRateSlider;
    public PinchSlider replacementTimeSlider;
    public PinchSlider samplingRateSlider;

    private void Awake()
    {
        UpdateSliderValues();
    }

    public void SetVFXStyleNumber(int number)
    {
        vfxStyleNumber = number;
        UpdateSliderValues();
    }

    public void UpdateSliderValues()
    {
        var coipy = vfxStyle.StyleParamsList[vfxStyleNumber];
        
        shapeSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].shape / 3.0f;
        sizeSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].size;
        alphaSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].alpha;
        lifetimeSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].lifeTime / 100.0f;
        turbulenceSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].turbelenceStrength;
        dragSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].linearDrag;
        replacementRateSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].replacementRate;
        replacementTimeSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].replacementTime / 10.0f;
        samplingRateSlider.SliderValue = vfxStyle.StyleParamsList[vfxStyleNumber].samplingRate;


    }


    public void OnSliderUpdatedShape(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.shape = (int) (eventData.NewValue * 3);
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
    
    public void OnSliderUpdatedSize(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.size = eventData.NewValue;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
    
    public void OnSliderUpdatedAlpha(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.alpha = eventData.NewValue;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
    
    public void OnSliderUpdatedLifetime(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.lifeTime = eventData.NewValue * 100;
        styleCopy.updateLifetime = 1;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
        StartCoroutine(ResetLifetime());


    }

    IEnumerator ResetLifetime()
    {
        yield return new WaitForSeconds(.1f);
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.updateLifetime = 0;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
    
    public void OnSliderUpdatedTurbulenceStrength(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.turbelenceStrength = eventData.NewValue;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
    
    public void OnSliderUpdatedLinearDrag(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.linearDrag = (int) eventData.NewValue;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
    
    public void OnSliderUpdatedReplacementrate(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.replacementRate = eventData.NewValue;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
    
    public void OnSliderUpdatedReplacementTime(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.replacementTime = eventData.NewValue * 10.0f;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
    
    public void OnSliderUpdatedSamplingRate(SliderEventData eventData)
    {
        var styleCopy = vfxStyle.StyleParamsList[vfxStyleNumber];
        styleCopy.samplingRate = eventData.NewValue;
        vfxStyle.StyleParamsList[vfxStyleNumber] = styleCopy;
    }
}
