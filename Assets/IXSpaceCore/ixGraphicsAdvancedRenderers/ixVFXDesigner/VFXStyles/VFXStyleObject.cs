using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "VFXStyle", menuName = "ScriptableObjects/VFXStyle", order = 1)]
[System.Serializable]
public class VFXStyleObject : ScriptableObject
{
    public List<StyleParams> StyleParamsList;
}
