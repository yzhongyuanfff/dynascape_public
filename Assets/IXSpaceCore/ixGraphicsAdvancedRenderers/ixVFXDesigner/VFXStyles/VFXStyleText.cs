﻿using Microsoft.MixedReality.Toolkit.UI;
using TMPro;
using UnityEngine;

public class VFXStyleText : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro textMesh = null;

    public float factor = 1.0f;

    public void OnSliderUpdated(SliderEventData eventData)
    {
        if (textMesh == null)
        {
            textMesh = GetComponent<TextMeshPro>();
        }

        if (textMesh != null)
        {
            textMesh.text = $"{eventData.NewValue * factor:F2}";
        }
    }
}

