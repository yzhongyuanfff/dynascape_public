using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class VFXStyle : MonoBehaviour
{
    [SerializeField]
    public VFXStyleObject currentStyles;

    private GraphicsBuffer styleBuffer;

    [HideInInspector]
    public VisualEffect VFX;

    [SerializeField] AnimationCurve EnvLCurve;
    [SerializeField] AnimationCurve HumanLCurve;

    [SerializeField] public bool UseStylesFromScript = false;
    VFXStyleObject scriptStyle;

    private void Awake()
    {
        VFX = GetComponent<VisualEffect>();
        styleBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, currentStyles.StyleParamsList.Count, sizeof(float) * 17 + sizeof(int) * 3);
        styleBuffer.SetData(currentStyles.StyleParamsList);
        VFX.SetGraphicsBuffer("StyleBuffer", styleBuffer);
    }

    private void Update()
    {
        if (UseStylesFromScript)
            styleBuffer.SetData(scriptStyle.StyleParamsList);
        else
            styleBuffer.SetData(currentStyles.StyleParamsList);
    }

    public void UpdatePreviewHERate(float rate)
    {
        float HL = HumanLCurve.Evaluate(rate);
        float EL = EnvLCurve.Evaluate(rate);
        //List<StyleParams> paraList = currentStyles.StyleParamsList;
        StyleParams paraHuman = currentStyles.StyleParamsList[0];
        StyleParams paraBkg = currentStyles.StyleParamsList[1];
        StyleParams paraFore = currentStyles.StyleParamsList[2];
        paraHuman.lifeTime = HL;
        paraBkg.lifeTime = 0;
        paraFore.lifeTime = EL;

        if(scriptStyle == null)
        {
            scriptStyle = new VFXStyleObject();
            scriptStyle.StyleParamsList = new List<StyleParams>();
            scriptStyle.StyleParamsList.Add(paraHuman);
            scriptStyle.StyleParamsList.Add(paraBkg);
            scriptStyle.StyleParamsList.Add(paraFore);
        }
        else
        {
            scriptStyle.StyleParamsList[0] = paraHuman;
            scriptStyle.StyleParamsList[1] = paraBkg;
            scriptStyle.StyleParamsList[2] = paraFore;
        }

    }

    public void UpdateLifetime()
    {
        StartCoroutine(ResetLifetime());

    }
    
    IEnumerator ResetLifetime()
    {
        VFX.SetInt("UpdateLifetime", 1);
        yield return new WaitForSeconds(.1f);
        VFX.SetInt("UpdateLifetime", 0);
    }
}


[VFXType(VFXTypeAttribute.Usage.GraphicsBuffer)]
[System.Serializable]
public struct StyleParams
    {

        [Range(0,3)]
        public int shape;
        
        [Range(0.0f, 10.0f)]
        public float size;
        [Range(0.0f, 1.0f)]
        public float alpha;
        [Range(0.0f, 1.0f)]
        public float softness;

        [Range(0.0f, 100.0f)]
        public float lifeTime;
        
        [Range(0.0f, 10.0f)]
        public float turbelenceStrength;

        [Range(0,1)]
        public int linearDrag;

        public Vector3 Force;
        //
        [Range(0.0f, 1.0f)]
         public float samplingRate;

         [Range(0.0f, 1.0f)]
        public float replacementRate;
                 
        [Range(0.0f, 10.0f)] 
        public float replacementTime;
        
        [Range(-0.5f, 1.5f)] 
        public float viewDependenceMin;
        
        [Range(-0.5f, 1.5f)] 
        public float ViewDependenceMax;
        
        public Color color;

        [Range(0, 1)] 
        public int updateLifetime;


    }
