using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayerAdapter : MonoBehaviour
{
    ixVideoPlayerController videoPlayerController;

    // for the quad 
    [SerializeField] Material SourceQuadMaterial;
    [SerializeField] Material TargetQuadMaterial;
    [SerializeField] MeshRenderer QuadMeshRenderer;

    public void SetVideoPlayerController(ixVideoPlayerController video)
    {
        videoPlayerController = video;
    }

    //
    public void DoAlignAsStartPoint()
    {
        videoPlayerController.AlignAsStartPoint(this.gameObject);
    }

    public void DoAlignAsEndPoint()
    {
        videoPlayerController.AlignAsEndPoint(this.gameObject);
    }

    //
    public void SetMeAsSourceQuad()
    {
        videoPlayerController.SetMeAsSourceQuad(this.gameObject);
        QuadMeshRenderer.material = SourceQuadMaterial;
    }

    public void SetMeAsTargetQuad()
    {
        videoPlayerController.SetMeAsTargetQuad(this.gameObject);
        QuadMeshRenderer.material = TargetQuadMaterial;
    }

    public void AlignMeToTarget()
    {
        videoPlayerController.AlignQuads();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
