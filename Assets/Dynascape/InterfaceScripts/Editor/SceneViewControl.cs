using UnityEditor;
using UnityEngine;

public class SceneViewControl : EditorWindow
{
    [MenuItem("IXLAB/Scene View Control")]
    public static void ShowWindow()
    {
        GetWindow<SceneViewControl>("Scene View Control");
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Move To MainCamera"))
        {
            MoveSceneViewCamera();
        }

        if (GUILayout.Button("Rotate Scene View Camera"))
        {
            RotateSceneViewCamera();
        }
    }

    private void MoveSceneViewCamera()
    {
        SceneView sceneView = SceneView.lastActiveSceneView;

        GameObject head = GameObject.FindGameObjectWithTag("MainCamera");

        if (sceneView != null)
        {
            // Move the camera to a new position
            sceneView.camera.transform.position = head.transform.position; //new Vector3(5f, 2f, 5f);
            sceneView.Repaint();
        }
    }

    private void RotateSceneViewCamera()
    {
        SceneView sceneView = SceneView.lastActiveSceneView;

        if (sceneView != null)
        {
            // Rotate the camera around the pivot point
            sceneView.LookAt(sceneView.pivot, Quaternion.Euler(30f, 45f, 0f));
            sceneView.Repaint();
        }
    }
}
