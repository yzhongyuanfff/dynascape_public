using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Walkthrough : MonoBehaviour
{
    [SerializeField] GameObject UIContent;

    [Header("====")]
    [SerializeField] GameObject ExperiencePlayer;
    //[SerializeField] GameObject ExperienceFileBrowser;
    [Header("====")]
    [SerializeField] List<GameObject> paneluis;
    //[SerializeField] GameObject FileBrowserUI;
    //[SerializeField] GameObject ExperiencePreviewer;
    //[SerializeField] GameObject ExperienceSpatialAuthoring;
    //[SerializeField] GameObject ExperienceClipAuthoring;
    //[SerializeField] GameObject ExperienceAdvanced;
    //[SerializeField] GameObject ExperienceVFXSwitcher;
    //[SerializeField] GameObject ExperienceAppearanceManager; // button based 
    //[SerializeField] GameObject ExperienceVFXFineTune;

    List<GameObject> btnuis = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        ExperiencePlayer.SetActive(false);
       // ExperienceFileBrowser.SetActive(false);
        DisableAllOtherUIs();
        InitUI();
    }

    // include panelui and btn ui
    void DisableAllOtherUIs()
    {
        //ExperienceFileBrowser.SetActive(false);
        foreach(var ui in paneluis)
        {
            ui.SetActive(false);
        }
        foreach (var ui in btnuis)
        {
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        }
    }

    void InitUI()
    {
        //{
        //    GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
        //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = ExperienceFileBrowser.name;
        //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
        //    {
        //        bool mystate = ExperienceFileBrowser.activeSelf;
        //        DisableAllOtherUIs();
        //        ExperienceFileBrowser.SetActive(!mystate);
        //        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
        //        ExperienceFileBrowser.GetComponent<ixLocalFileExplorer>().ListFiles_FilterMP4(); // spec
        //    });
        //    btnuis.Add(ui);
        //}
        {
            GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = ExperiencePlayer.name;
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
            {
                bool mystate = ExperiencePlayer.activeSelf;
                ExperiencePlayer.SetActive(!mystate);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
            });
            // no radio 
        }
        foreach (GameObject pui in paneluis)
        {
            GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
            ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
            ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = pui.name;
            ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
            {
                bool mystate = pui.activeSelf;
                DisableAllOtherUIs();
                pui.SetActive(!mystate);
                ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
            });
            btnuis.Add(ui);
        }

        ////
        // FileBrowserUI
        //{
        //    GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
        //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "File Browser";
        //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate
        //    {
        //        bool mystate = FileBrowserUI.activeSelf;
        //        DisableAllOtherUIs();
        //        FileBrowserUI.SetActive(!mystate);
        //        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
        //        if (FileBrowserUI.activeSelf)
        //            FileBrowserUI.GetComponent<ixLocalFileExplorer>().ListFiles_FilterMP4(); // 
        //    });
        //    btnuis.Add(ui);
        //}

        ////
        //ixUtility.MakeSepLine(UIContent, "============1: Spatial Authoring============");
        //// ExperienceMain
        //{
        //    GameObject curUI = ExperienceMain;
        //    string showname = "Experience Main";
        //    GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
        //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = showname;
        //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
        //        bool mystate = curUI.activeSelf;
        //        DisableAllOtherUIs();
        //        curUI.SetActive(!mystate);
        //        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
        //    });
        //    btnuis.Add(ui);
        //}

        ////
        //ixUtility.MakeSepLine(UIContent, "============2: Clip Authoring============");

        ////
        //ixUtility.MakeSepLine(UIContent, "============3: Appearance Authoring============");
        ////ixUtility.MakeANegativeToggleButton(UIContent, "Experience VFX Switcher", delegate {
        ////    DisableAllOtherUIs();
        ////    ExperienceVFXSwitcher.SetActive(!ExperienceVFXSwitcher.activeSelf); }); // Toggle, false by default
        ////ixUtility.MakeANegativeToggleButton(UIContent, "Experience VFX Previewer", delegate {
        ////    DisableAllOtherUIs();
        ////    ExperienceVFXPreviewer.SetActive(!ExperienceVFXPreviewer.activeSelf); }); // Toggle, false by default
        ////ixUtility.MakeANegativeToggleButton(UIContent, "Experience VFX Fine Tune", delegate {
        ////    DisableAllOtherUIs();
        ////    ExperienceVFXFineTune.SetActive(!ExperienceVFXFineTune.activeSelf); }); // Toggle, false by default
        ////ixUtility.MakeANegativeToggleButton(UIContent, "Experience Appearance Manager", delegate {
        ////    DisableAllOtherUIs();
        ////    ExperienceAppearanceManager.SetActive(!ExperienceAppearanceManager.activeSelf); }); // Toggle, false by default

        //// ExperienceVFXSwitcher
        //{
        //    GameObject curUI = ExperienceVFXSwitcher;
        //    string showname = "Experience VFX Switcher";
        //    GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
        //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = showname;
        //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
        //        bool mystate = curUI.activeSelf;
        //        DisableAllOtherUIs();
        //        curUI.SetActive(!mystate);
        //        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
        //    });
        //    btnuis.Add(ui);
        //}
        //// ExperienceVFXPreviewer
        //{
        //    //GameObject curUI = ExperienceVFXPreviewer;
        //    //string showname = "Experience VFX Previewer";
        //    //GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
        //    //ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        //    //ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = showname;
        //    //ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
        //    //    bool mystate = curUI.activeSelf;
        //    //    DisableAllOtherUIs();
        //    //    curUI.SetActive(!mystate);
        //    //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
        //    //});
        //    //btnuis.Add(ui);
        //}
        //// ExperienceVFXFineTune
        //{
        //    GameObject curUI = ExperienceVFXFineTune;
        //    string showname = "Experience VFX Fine Tune";
        //    GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
        //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = showname;
        //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
        //        bool mystate = curUI.activeSelf;
        //        DisableAllOtherUIs();
        //        curUI.SetActive(!mystate);
        //        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
        //    });
        //    btnuis.Add(ui);
        //}
        //// ExperienceAppearanceManager
        //{
        //    GameObject curUI = ExperienceAppearanceManager;
        //    string showname = "Experience Appearance Manager";
        //    GameObject ui = GameObject.Instantiate((GameObject)Resources.Load("DebugUITextedBtnPrefeb", typeof(GameObject)), UIContent.transform);
        //    ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(false);
        //    ui.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = showname;
        //    ui.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(delegate {
        //        bool mystate = curUI.activeSelf;
        //        DisableAllOtherUIs();
        //        curUI.SetActive(!mystate);
        //        ui.transform.Find("Btn").Find("ToggleState").gameObject.SetActive(!mystate);
        //    });
        //    btnuis.Add(ui);
        //}
    }
}
