using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceUIHelper : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CallExpUI()
    {
        foreach (Transform video in this.transform)
        {
            video.transform.Find("UIPanels").GetComponent<ComeInFrontOfMe>().comeToMe();
        }
    }

    public void DisableAllExpUI()
    {

    }
}
