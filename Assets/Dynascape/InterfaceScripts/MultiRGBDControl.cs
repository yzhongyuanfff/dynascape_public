using JfranMora.Inspector;
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class MultiRGBDControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    [SerializeField] bool isRunMode = false;
    [SerializeField] bool isDoCheckProximity = false;

    [Button]
    void EnterRunMode()
    {
        //
        foreach(Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.DisableSnapping();
            video.UpdaterTimeScrubberRendering(false);
            video.RenderRunMode();
            video.DoCheckProximity = true;
            video.StopALLRemoveAllPoints();
            video.ResetPlayParameters();
            video.PlayVideo();
        }

        // post effect
        UniversalAdditionalCameraData cameraData = Camera.main.GetComponent<Camera>().GetUniversalAdditionalCameraData();
        cameraData.renderPostProcessing = true;

        isRunMode = true;
    }

    [Button]
    void ToggleProximityCheck()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.DoCheckProximity = !video.DoCheckProximity;
            isDoCheckProximity = video.DoCheckProximity;
        }
    }

    [Button]
    void ExitRunMode()
    {

        //
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            //video.EnableSnapping();
            video.UpdaterTimeScrubberRendering(true);
            //video.RenderRunMode();
            video.DoCheckProximity = false;
            video.StopALLRemoveAllPoints();
            video.ResetPlayParameters();
            //video.PlayVideo();
            video.PauseVideoAndVFXPlay();
        }

        // post effect
        UniversalAdditionalCameraData cameraData = Camera.main.GetComponent<Camera>().GetUniversalAdditionalCameraData();
        cameraData.renderPostProcessing = false;

        //
        isRunMode = false;
    }

    public void MoveOutOfSpatialPlacer()
    {
        // TODO: implement this
        //throw new System.NotImplementedException();
    }

    public void MoveIntoSpatialPlacer(GameObject sp)
    {
        foreach (Transform c in this.transform)
        {
            c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>().AlignVFXAccordingToTheSpatialPlacer(sp);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    public void ForAllRGBDs_StartMediaPlayback()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.StopALLRemoveAllPoints();
            video.ResetPlayParameters();
            video.PlayVideo();
        }
    }

    public void ForAllRGBDs_PauseVideoAndVFXPlay()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.PauseVideoAndVFXPlay();
        }
    }

    public void ForAllRGBDs_ResumeVideoAndVFXPlay()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.ResumeVideoAndVFXPlay();
        }
    }

    public void ForAllRGBDs_StopMediaPlayback()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.StopALLRemoveAllPoints();
        }
    }

    public void ForAllRGBDs_SnapToMe()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.SeekToBodyPosition();
        }
    }

    public void ForAllRGBDs_DirectionIndicator()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.InstantiateDirectionIndicatorPrefeb();
        }
    }
    public void ForAllRGBDs_PlaneIndicator()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.InstantiateQuadPrefeb();
        }
    }

    public void ForAllRGBDs_SetCurrentPointerAsStart()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.MarkAsStartPoint();
        }
    }

    public void ForAllRGBDs_SetCurrentPointerAsEnd()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.MarkAsEndPoint();
        }
    }

    public void ForAllRGBDs_StoreTheAuthoredData()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.StoreTheAuthoredData();
        }
    }

    public void ForAllRGBDs_ResumeTheAuthoredData()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.ResumeTheAuthoredData();
        }
    }

    public void UpdatePreviewHERate(SliderEventData eventData)
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.UpdatePreviewHERate(eventData.NewValue);
        }
    }

    [Button]
    public void ToggleAllowHandSeek()
    {
        foreach (Transform c in this.transform)
        {
            ixVideoPlayerController video = c.Find("BibcamVideoPlayer").GetComponent<ixVideoPlayerController>();
            video.Tube.GetComponent<VFXTrailRenderer>().AllowHandSeek = !video.Tube.GetComponent<VFXTrailRenderer>().AllowHandSeek;
        }
    }

    [Button]
    void ToastTest()
    {
        ixUtility.ToastMe("Test Message! ");
    }
}
