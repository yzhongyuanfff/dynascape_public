using UnityEngine;
using UnityEngine.Serialization;

public class TimeTickController : MonoBehaviour
{
    public TMPro.TMP_Text timeText;
    public int FrameID;

    private Camera _mainCamera;

    private void Start()
    {
        _mainCamera = Camera.main;
    }

    private void LateUpdate()
    {
        var newRotation = _mainCamera.transform.eulerAngles;
        newRotation.x = 0;
        newRotation.z = 0;
        transform.eulerAngles = newRotation;
    }
}
