Shader "IndirectDrawShader"
{
    Properties
	{
	    _BgDistThreshold ("Background distance threshold", Float) = 9
	    _TrailDepthThreshold ("Trail removal depth threshold", Float) = 0.1

        _HumanAlpha ("Human alpha", Float) = 1
        _ForegroundAlpha ("Foreground alpha", Float) = 1
        _BackgroundAlpha ("Background alpha", Float) = 1

	    _HumanTintColor ("Human tint color", Color) = (1, 1, 1, 1)
	    _BackgroundTintColor ("Background tint color", Color) = (1, 1, 1, 1)
	    _ForegroundTintColor ("Foreground tint color", Color) = (1, 1, 1, 1)

	    _RenderCircle ("Render point as circle", Int) = 1
	    _CircleRadiusSize ("Circle radius size", Float) = 1
	    _FeatherStrength ("Feather strength", Float) = 1

        _RenderAllHumans ("Render all humans", Int) = 1
        _HumanCurrentFrame ("Render only human at current frame", Int) = 0
        _HumanPrevFrames ("Render only human at current frame plus previous frames", Int) = 30
	}

    SubShader
    {
        Tags {"RenderType"="Transparent" "Queue"="Transparent"}
        Blend SrcAlpha OneMinusSrcAlpha
        AlphaToMask On

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #define UNITY_INDIRECT_DRAW_ARGS IndirectDrawIndexedArgs
            #include "UnityIndirect.cginc"
            #include "DataPoint.cginc"

            // Vertex shader outputs ("vertex to fragment")
            struct v2f
            {
                float4 pos : SV_POSITION;
                fixed2 uv: TEXCOORD0;
                float4 color : COLOR0;
                bool ignore : IGNORE;
            };

            StructuredBuffer<DataPoint> _Points;

            float _BgDistThreshold;
            float _TrailDepthThreshold;

            float _HumanAlpha;
            float _ForegroundAlpha;
            float _BackgroundAlpha;

            float4 _HumanTintColor;
            float4 _BackgroundTintColor;
            float4 _ForegroundTintColor;

            int _RenderCircle;
            float _CircleRadiusSize;
            float _FeatherStrength;

            int _RenderAllHumans;
            int _HumanCurrentFrame;
            int _HumanPrevFrames;

            v2f vert(appdata_base v, uint svInstanceID : SV_InstanceID)
            {
                InitIndirectDrawArgs(0);
                v2f o;

                // Default values
                o.uv = v.texcoord;
                o.ignore = false;

                // StructuredBuffer data index
                uint cmdID = GetCommandID(0);
                uint instanceID = GetIndirectInstanceID(svInstanceID);
                uint idx = cmdID * 1023 + instanceID;

                const uint frame = _Points[idx].frame;
                const float3 position = _Points[idx].position;
                const float4 color = _Points[idx].color;
                const uint objectId = _Points[idx].objectId;
                const float depth = _Points[idx].depth;
                const float4 deltaDepths = _Points[idx].deltaDepths;

                // Null data handler
                if (position.x == 0 &&
                    position.y == 0 &&
                    position.z == 0)
                {
                    o.ignore = true;
                    return o;
                }

                // Trail removal
                if (deltaDepths.x > _TrailDepthThreshold ||
                    deltaDepths.y > _TrailDepthThreshold ||
                    deltaDepths.z > _TrailDepthThreshold ||
                    deltaDepths.w > _TrailDepthThreshold)
                {
                    o.ignore = true;
                    return o;
                }

                // Billboard mesh towards camera
				float3 vpos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz);
				float4 worldCoord = float4(unity_ObjectToWorld._m03, unity_ObjectToWorld._m13,
				                           unity_ObjectToWorld._m23, 1) +
				                    float4(position, 0);
				float4 viewPos = mul(UNITY_MATRIX_V, worldCoord) + float4(vpos, 0);
				o.pos = mul(UNITY_MATRIX_P, viewPos);

                // Handle foreground vs background
                if (depth < _BgDistThreshold)
                    o.color = float4(_ForegroundTintColor.rgb * color.rgb, _ForegroundAlpha); // Foreground
                else
                    o.color = float4(_BackgroundTintColor.rgb * color.rgb, _BackgroundAlpha); // Background

                // Handle human
                if (objectId == 1)
                {
                    if (_RenderAllHumans == 1 ||
                        (_HumanCurrentFrame >= frame && _HumanCurrentFrame - _HumanPrevFrames <= frame))
                    {
                        o.color = float4(_HumanTintColor.rgb * color.rgb,_HumanAlpha); // Human
                    }
                    else
                    {
                        o.ignore = true;
                        return o;
                    }
                }

                return o;
            }

            float4 frag(v2f i) : SV_Target
            {
                if (i.ignore)
                    discard;

                if (i.color.a == 0)
                    discard;

                if (!_RenderCircle) return i.color;

                const float originalAlpha = i.color.a;
                float alpha;
                const float uvFromCenter = length(i.uv - float2(0.5, 0.5));
                const float maxUvFromCenter = _CircleRadiusSize / 2;
                if (uvFromCenter > maxUvFromCenter)
                {
                    alpha = 0.0;
                }
                else
                {
                    const float subtractor = maxUvFromCenter - maxUvFromCenter * _FeatherStrength;
                    const float multiplier = originalAlpha / (maxUvFromCenter - subtractor);

                    if (uvFromCenter < subtractor)
                        alpha = originalAlpha;
                    else
                        alpha = originalAlpha - (uvFromCenter - subtractor) * multiplier;
                }

                return float4(i.color.rgb, alpha);
            }
            ENDCG
        }
    }
}
