struct DataPoint
{
    uint frame;
    float3 position;
    float4 color;
    uint objectId;
    float depth;
    float4 deltaDepths;
};
