using System.IO;
using Unity.Mathematics;
using UnityEngine;
using Application = UnityEngine.Application;

public class StaticPointsPlayer : MonoBehaviour
{
    // ComputeBuffer: GPU data buffer for use with compute shaders
    private ComputeBuffer _pointBuffer;

    [Space(10)]

    [SerializeField] private string fileName = "tunnel_4_m.dat";

    [Space(10)]

    // Material and mesh for RenderMeshIndirect
    [SerializeField] private Material material;
    [SerializeField] private Mesh mesh;

    [Space(10)]

    [SerializeField, Range(0.0f, 10.0f)] private float bgDistThreshold = 9.0f;
    [SerializeField, Range(0.0f, 0.5f)] private float trailDepthThreshold = 0.5f;

    [Space(10)]

    [SerializeField, Range(0.0f, 1.0f)] private float humanAlpha = 1.0f;
    [SerializeField, Range(0.0f, 1.0f)] private float backgroundAlpha = 1.0f;
    [SerializeField, Range(0.0f, 1.0f)] private float foregroundAlpha = 1.0f;

    [Space(10)]

    [SerializeField] private Color humanTintColor = Color.white;
    [SerializeField] private Color backgroundTintColor = Color.white;
    [SerializeField] private Color foregroundTintColor = Color.white;

    [Space(10)]

    [SerializeField] private bool renderCircle = true;
    [SerializeField, Range(0.0f, 1.0f)] private float circleRadiusSize = 1.0f;
    [SerializeField, Range(0.0f, 1.0f)] private float featherStrength = 1.0f;

    [Space(10)]

    [SerializeField] private bool renderAllHumans = true;
    [SerializeField, Range(0.0f, 1.0f)] private float humanCurrentFrame;
    [SerializeField, Range(1, 300)] private int humanPrevFrames = 30;

    // Command buffer for RenderMeshIndirect
    private GraphicsBuffer _commandBuf;
    private GraphicsBuffer.IndirectDrawIndexedArgs[] _commandData;

    private readonly string _filePath = Application.dataPath + "/StaticPoints/export~/";
    private bool _fileLoaded;
    private int _pointCount;
    private int _frameCount;
    private int _numberOfIndirectDrawCommands;

    private void OnDestroy()
    {
        ReleaseBuffers();
    }

    private void Start()
    {
        ReadBinaryFile();
    }

    private void Update()
    {
        if (!_fileLoaded) return;

        // Renders multiple instances of a small cube mesh using GPU instancing
        RenderParams rp = new RenderParams(material);
        rp.worldBounds = new Bounds(Vector3.zero, 10000 * Vector3.one); // use tighter bounds for better FOV culling
        rp.matProps = new MaterialPropertyBlock();
        rp.matProps.SetBuffer("_Points", _pointBuffer);

        rp.matProps.SetFloat("_TrailDepthThreshold", trailDepthThreshold);
        rp.matProps.SetFloat("_BgDistThreshold", bgDistThreshold);

        rp.matProps.SetFloat("_HumanAlpha", humanAlpha);
        rp.matProps.SetFloat("_BackgroundAlpha", backgroundAlpha);
        rp.matProps.SetFloat("_ForegroundAlpha", foregroundAlpha);

        rp.matProps.SetColor("_HumanTintColor", humanTintColor);
        rp.matProps.SetColor("_BackgroundTintColor", backgroundTintColor);
        rp.matProps.SetColor("_ForegroundTintColor", foregroundTintColor);


        rp.matProps.SetInt("_RenderCircle", renderCircle ? 1 : 0);
        rp.matProps.SetFloat("_CircleRadiusSize", circleRadiusSize);
        rp.matProps.SetFloat("_FeatherStrength", featherStrength);

        rp.matProps.SetInt("_RenderAllHumans", renderAllHumans ? 1 : 0);
        rp.matProps.SetInt("_HumanCurrentFrame", (int)math.round(humanCurrentFrame * _frameCount));
        rp.matProps.SetInt("_HumanPrevFrames", humanPrevFrames);

        for (var commandIdx = 0; commandIdx < _numberOfIndirectDrawCommands; commandIdx++)
        {
            _commandData[commandIdx].indexCountPerInstance = mesh.GetIndexCount(0);
            _commandData[commandIdx].instanceCount = 1023;
        }
        _commandBuf.SetData(_commandData);

        Graphics.RenderMeshIndirect(rp, mesh, _commandBuf, _numberOfIndirectDrawCommands);
    }

    private void ReleaseBuffers()
    {
        _pointBuffer?.Release();
        _pointBuffer = null;
        _commandBuf?.Release();
        _commandBuf = null;
    }

    private void ReadBinaryFile()
    {
        using FileStream fileStream = File.Open(_filePath + fileName, FileMode.Open);
        using BinaryReader binaryReader = new(fileStream);

        _pointCount = binaryReader.ReadInt32();
        _frameCount = binaryReader.ReadInt32();

        var pointArray = new DataPoint[_pointCount];

        for (var i = 0; i < _pointCount; i++)
        {
            var point = new DataPoint
            {
                Frame = binaryReader.ReadInt32(),
                Position = new float3(
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle()
                ),
                Color = new float4(
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle(),
                    1.0f
                ),
                ObjectId = binaryReader.ReadInt32(),
                Depth = binaryReader.ReadSingle(),
                DeltaDepths = new float4(
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle()
                )
            };

            pointArray[i] = point;
        }

        ReleaseBuffers();

        _pointBuffer = new ComputeBuffer(_pointCount, sizeof(uint) +   // frame
                                                      3 * sizeof(float) +   // position
                                                      4 * sizeof(float) +   // color
                                                      sizeof(uint) +        // objectId
                                                      sizeof(float) +       // depth
                                                      4 * sizeof(float));   // deltaDepths

        _pointBuffer.SetData(pointArray);

        _numberOfIndirectDrawCommands = _pointCount / 1023 + 1;
        _commandBuf = new GraphicsBuffer(GraphicsBuffer.Target.IndirectArguments, _numberOfIndirectDrawCommands, GraphicsBuffer.IndirectDrawIndexedArgs.size);
        _commandData = new GraphicsBuffer.IndirectDrawIndexedArgs[_numberOfIndirectDrawCommands];

        _fileLoaded = true;
    }
}
