using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Bibcam.Decoder;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Video;
using Application = UnityEngine.Application;
using Random = Unity.Mathematics.Random;

public class StaticPointsExtractor : MonoBehaviour
{
    [SerializeField] private ComputeShader computeShader;

    [Space(10)]

    [SerializeField] private BibcamMetadataDecoder decoder;
    [SerializeField] private BibcamTextureDemuxer demux;
    [SerializeField] private VideoPlayer videoPlayer;

    [Space(10)]

    [SerializeField] private int pointCloudColumns = 960;
    [SerializeField] private int pointCloudRows = 540;
    [SerializeField] private int pointCount = 1000000;
    [SerializeField] private string fileName = "tunnel_4_m.dat";
    [SerializeField] private bool exportPointCloud;

    [Space(10)]

    [SerializeField] private bool renderCentroids;
    [SerializeField] private GameObject centroidPrefab;
    [SerializeField] private bool exportCentroids;

    [Space(10)]

    // Material and mesh for RenderMeshIndirect
    [SerializeField] private bool renderPreview = true;
    [SerializeField] private Material material;
    [SerializeField] private Mesh mesh;

    [Space(10)]

    [SerializeField, Range(0.0f, 10.0f)] private float bgDistThreshold = 9.0f;
    [SerializeField, Range(0.0f, 0.5f)] private float trailDepthThreshold = 0.5f;

    [Space(10)]

    [SerializeField, Range(0.0f, 1.0f)] private float humanAlpha = 1.0f;
    [SerializeField, Range(0.0f, 1.0f)] private float backgroundAlpha = 1.0f;
    [SerializeField, Range(0.0f, 1.0f)] private float foregroundAlpha = 1.0f;

    [Space(10)]

    [SerializeField] private Color humanTintColor = Color.white;
    [SerializeField] private Color backgroundTintColor = Color.white;
    [SerializeField] private Color foregroundTintColor = Color.white;

    private readonly int2 _depthMapRes = new(960, 540);
    private List<float3> _centroidPool = new();

    private readonly string _filePath = Application.dataPath + "/StaticPoints/export~/";
    private bool _videoPlayerEndReached;
    private bool _videoPlayerReady;
    private bool _frameProcessed;
    private int _frameCount;
    private int _perFramePointCount;
    private int _numberOfIndirectDrawCommands;
    private int _frame = -1;

    private int _mainKernel; // 0

    // For generating random sampled UVs
    private Random _randomGenerator;

    // Compute buffers holding the extracted data
    private ComputeBuffer _pointBuffer;

    // Compute buffer for random sampled UVs
    private ComputeBuffer _sampledUvBuffer;

    // Command buffer for RenderMeshIndirect
    private GraphicsBuffer _commandBuffer;
    private GraphicsBuffer.IndirectDrawIndexedArgs[] _commandData;

    // Array to hold generated random sampled UVs in CPU
    private NativeArray<float2> _sampledUvNativeArray;

    private void Awake()
    {
        // Get compute shader kernel indices
        _mainKernel = computeShader.FindKernel("Main");

        videoPlayer.prepareCompleted += OnVideoPrepared;
        videoPlayer.seekCompleted += SeekCompleted;
        videoPlayer.loopPointReached += VideoPlayerEndReached;
    }

    private void OnDestroy()
    {
        // Release buffers
        _pointBuffer?.Release();
        _pointBuffer = null;
        _commandBuffer?.Release();
        _commandBuffer = null;
    }

    private void Start()
    {
        // Prepare and set buffers

        _pointBuffer = new ComputeBuffer(pointCount, sizeof(uint) +   // frame
                                                     3 * sizeof(float) +   // position
                                                     4 * sizeof(float) +   // color
                                                     sizeof(uint) +        // objectId
                                                     sizeof(float) +       // depth
                                                     4 * sizeof(float));   // deltaDepths

        computeShader.SetBuffer(_mainKernel, "points", _pointBuffer);

        _numberOfIndirectDrawCommands = pointCount / 1023 + 1;
        _commandBuffer = new GraphicsBuffer(GraphicsBuffer.Target.IndirectArguments, _numberOfIndirectDrawCommands, GraphicsBuffer.IndirectDrawIndexedArgs.size);
        _commandData = new GraphicsBuffer.IndirectDrawIndexedArgs[_numberOfIndirectDrawCommands];

        videoPlayer.playOnAwake = false;
        videoPlayer.playbackSpeed = 0f;
        videoPlayer.Prepare();

        _randomGenerator = new Random();
    }

    void Update()
    {
        if (!_videoPlayerReady) return;

        if (_frameProcessed) ProcessNextFrame();

        // Renders multiple instances of a small cube mesh using GPU instancing
        if (renderPreview)
        {
            RenderParams rp = new RenderParams(material);
            rp.worldBounds = new Bounds(Vector3.zero, 10000 * Vector3.one); // use tighter bounds for better FOV culling
            rp.matProps = new MaterialPropertyBlock();
            rp.matProps.SetBuffer("_Points", _pointBuffer);

            rp.matProps.SetFloat("_TrailDepthThreshold", trailDepthThreshold);
            rp.matProps.SetFloat("_BgDistThreshold", bgDistThreshold);

            rp.matProps.SetFloat("_HumanAlpha", humanAlpha);
            rp.matProps.SetFloat("_BackgroundAlpha", backgroundAlpha);
            rp.matProps.SetFloat("_ForegroundAlpha", foregroundAlpha);

            rp.matProps.SetColor("_HumanTintColor", humanTintColor);
            rp.matProps.SetColor("_BackgroundTintColor", backgroundTintColor);
            rp.matProps.SetColor("_ForegroundTintColor", foregroundTintColor);

            for (var commandIdx = 0; commandIdx < _numberOfIndirectDrawCommands; commandIdx++)
            {
                _commandData[commandIdx].indexCountPerInstance = mesh.GetIndexCount(0);
                _commandData[commandIdx].instanceCount = 1023;
            }
            _commandBuffer.SetData(_commandData);

            Graphics.RenderMeshIndirect(rp, mesh, _commandBuffer, _numberOfIndirectDrawCommands);
        }
    }

    private void OnVideoPrepared(VideoPlayer source)
    {
        // Minus 2 frames, because somehow the processed frames is missing 2 frames
        _frameCount = (int)videoPlayer.frameCount - 2;

        _perFramePointCount = pointCount / _frameCount;

        _sampledUvBuffer = new ComputeBuffer(_perFramePointCount, 2 * sizeof(float));
        _sampledUvNativeArray = new NativeArray<float2>(_perFramePointCount, Allocator.Persistent);

        ProcessNextFrame();

        _videoPlayerReady = true;

        Debug.Log("Total Video Frames: " + _frameCount);
        Debug.Log("Perframe Point Count: " + _perFramePointCount);
    }

    private void ProcessNextFrame()
    {
        // You should pause while you seek for better stability
        videoPlayer.Pause();

        videoPlayer.frame = ++_frame;
        _frameProcessed = false;
    }

    void SeekCompleted(VideoPlayer source)
    {
        StartCoroutine(WaitToUpdateRenderTextureBeforeEndingSeek());
    }

    IEnumerator WaitToUpdateRenderTextureBeforeEndingSeek()
    {
        yield return new WaitForEndOfFrame();
        ProcessFrame();
        _frameProcessed = true;
    }

    private void VideoPlayerEndReached(VideoPlayer source)
    {
        _frame = -1;
        ProcessNextFrame();

        if (_videoPlayerEndReached) return;

        _videoPlayerEndReached = true;

        renderCentroids = false;

        if (exportCentroids)
        {
            WriteCentroidsToFile();
            exportCentroids = false;
        }

        if (!exportPointCloud) return;

        WriteBinaryDataPool();

        exportPointCloud = false;
    }

    private void ProcessFrame()
    {
        // Generate random sample locations
        _randomGenerator.InitState((uint)DateTime.Now.Ticks);
        for (var i = 0; i < _perFramePointCount; i++)
            _sampledUvNativeArray[i] = _randomGenerator.NextFloat2();
        _sampledUvBuffer.SetData(_sampledUvNativeArray);

        // Get metadata
        var meta = decoder.Metadata;
        if (!meta.IsValid) return;

        // Camera parameters
        var ray = BibcamRenderUtils.RayParams(meta);
        var iView = BibcamRenderUtils.InverseView(meta);

        // Property update
        computeShader.SetTexture(_mainKernel, "colorMap", demux.ColorTexture);
        computeShader.SetTexture(_mainKernel, "depthMap", demux.DepthTexture);
        computeShader.SetVector("rayParams", ray);
        computeShader.SetMatrix("inverseView", iView);
        computeShader.SetVector("depthRange", meta.DepthRange);
        computeShader.SetInt("pointCloudColumns", pointCloudColumns);
        computeShader.SetInt("pointCloudRows", pointCloudRows);
        computeShader.SetVector("depthMapRes", new Vector2(_depthMapRes.x, _depthMapRes.y));
        computeShader.SetBuffer(_mainKernel, "sampledUVs", _sampledUvBuffer);
        computeShader.SetInt("frame", _frame); // (int)videoPlayer.frame
        computeShader.SetInt("perFramePointCount", _perFramePointCount);

        // Dispatch the compute shader kernel
        computeShader.Dispatch(_mainKernel, _perFramePointCount / 8 + 1, 1, 1);

        // Compute centroid
        var centroid = ComputeCentroid(ray, iView);
        _centroidPool.Add(centroid);

        // Render centroid
        if (renderCentroids)
            Instantiate(centroidPrefab, centroid, Quaternion.identity, transform);
    }

    private void WriteBinaryDataPool()
    {
        Debug.Log("Writing the binary file. This might take a while ...");

        // Readback positions and colors point cloud data from GPU
        var pointArray = new DataPoint[pointCount];
        _pointBuffer.GetData(pointArray);

        using var fileStream = new FileStream(_filePath + fileName, FileMode.Create, FileAccess.Write, FileShare.None);
        using var binaryWriter = new BinaryWriter(fileStream);

        // Number of points
        binaryWriter.Write(pointCount);

        // Number of frames
        binaryWriter.Write(_frameCount);

        for (var i = 0; i < pointCount; i++)
        {
            var point = pointArray[i];

            // Data size per point = 13 x 4 bytes = 52 bytes
            binaryWriter.Write(point.Frame);
            binaryWriter.Write(point.Position.x);
            binaryWriter.Write(point.Position.y);
            binaryWriter.Write(point.Position.z);
            binaryWriter.Write(point.Color.x);
            binaryWriter.Write(point.Color.y);
            binaryWriter.Write(point.Color.z);
            binaryWriter.Write(point.ObjectId);
            binaryWriter.Write(point.Depth);
            binaryWriter.Write(point.DeltaDepths.x);
            binaryWriter.Write(point.DeltaDepths.y);
            binaryWriter.Write(point.DeltaDepths.z);
            binaryWriter.Write(point.DeltaDepths.w);
        }

        Debug.Log("Writing the binary file done.");
    }

    private float3 ComputeCentroid(Vector4 rayParams, Matrix4x4 inverseView)
    {
        var cornerPoints = new float2[]
        {
            new(-1f, -1f),
            new(-1f, 1f),
            new(1f, 1f),
            new(1f, -1f),
        };

        float3 sumPosition = new(0f, 0f, 0f);

        foreach (var cornerPoint in cornerPoints)
        {
            // Add camera CenterShift
            float2 addCenterShift = new(rayParams.x + cornerPoint.x, rayParams.y + cornerPoint.y);

            // Multiply with camera FieldOfView
            float4 mulFieldOfView = new(addCenterShift.x * rayParams.z, addCenterShift.y * rayParams.w, 1f, 1f);

            // Calculate final position using transformation matrix
            float4 position = math.mul(inverseView, mulFieldOfView);

            sumPosition += new float3(position.x, position.y, position.z);
        }

        return (sumPosition / 4f);
    }

    private void WriteCentroidsToFile()
    {
        Debug.Log("Writing centroid csv file. This might take a while ...");

        using(var writer = new StreamWriter(_filePath + "centroid.csv", false))
        {
            foreach (var centroid in _centroidPool)
            {
                writer.WriteLine(centroid.x + "," +
                                 centroid.y + "," +
                                 centroid.z);
            }
        }

        Debug.Log("Writing centroid csv file done.");
    }
}
