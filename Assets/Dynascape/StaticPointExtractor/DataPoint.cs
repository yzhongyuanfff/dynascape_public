using Unity.Mathematics;

public struct DataPoint
{
    public int Frame;
    public float3 Position;
    public float4 Color;
    public int ObjectId;
    public float Depth;
    public float4 DeltaDepths;
};
