StructuredBuffer<float4> _PathColor;

void SampleGraphicsBuffer_float(float ID, out float3 Num){
	Num = _PathColor[(int)(ID/3.0)].xyz;
}