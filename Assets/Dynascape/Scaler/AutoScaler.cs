using UnityEngine;

public class AutoScaler : MonoBehaviour
{
    [SerializeField] private float checkRate = 1f;
    [SerializeField] private float distanceThreshold = 1.5f;
    [SerializeField] private Transform headTransform;

    private MultiRGBDControl _multiRGBDControl;
    private bool _isInSpatialPlacer;

    private void Awake()
    {
        _multiRGBDControl = transform.parent.GetComponentInChildren<MultiRGBDControl>();
    }

    private void Start()
    {
        InvokeRepeating("DistanceCheck", checkRate, checkRate);
    }

    private void DistanceCheck()
    {
        var distance = Vector3.Distance(headTransform.position, transform.position);

        switch (_isInSpatialPlacer)
        {
            case true when distance > distanceThreshold:
                _multiRGBDControl.MoveOutOfSpatialPlacer();
                _isInSpatialPlacer = false;
                break;
            case false when distance <= distanceThreshold:
                _multiRGBDControl.MoveIntoSpatialPlacer(this.gameObject); // pass the placer itself 
                _isInSpatialPlacer = true;
                break;
        }
    }
}
