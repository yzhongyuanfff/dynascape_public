# Dynascape : Immersive Authoring of Real-World Dynamic Scenes with Spatially Tracked RGB-D Videos
This is the accompanying code repository for the work “Dynascape : Immersive Authoring of Real-World Dynamic Scenes with Spatially Tracked RGB-D Videos” by Zhongyuan Yu, Daniel Zeidler, Victor Victor, Matthew Mcginity

This work has been presented at the ACM VRST 2023 conference and published as Open Access in the ACM DL (see https://dl.acm.org/doi/10.1145/3611659.3615718).

Feel free to cite our paper if our work benefits your research. 
```
@inproceedings{10.1145/3611659.3615718,
author = {Yu, Zhongyuan and Zeidler, Daniel and Victor, Victor and Mcginity, Matthew},
title = {Dynascape : Immersive Authoring of Real-World Dynamic Scenes with Spatially Tracked RGB-D Videos},
year = {2023},
isbn = {9798400703287},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
url = {https://doi.org/10.1145/3611659.3615718},
doi = {10.1145/3611659.3615718},
abstract = {In this paper, we present Dynascape, an immersive approach to the composition and playback of dynamic real-world scenes in mixed and virtual reality. We use spatially tracked RGB-D cameras to capture point cloud representations of arbitrary dynamic real-world scenes. Dynascape provides a suite of tools for spatial and temporal editing and composition of such scenes, as well as fine control over their visual appearance. We also explore strategies for spatiotemporal navigation and different tools for the in situ authoring and viewing of mixed and virtual reality scenes. Dynascape is intended as a research platform for exploring the creative potential of dynamic point clouds captured with mobile, tracked RGB-D cameras. We believe our work represents a first attempt to author and playback spatially tracked RGB-D video in an immersive environment, and opens up new possibilities for involving dynamic 3D scenes in virtual space.},
booktitle = {Proceedings of the 29th ACM Symposium on Virtual Reality Software and Technology},
articleno = {10},
numpages = {12},
keywords = {Immersive Authoring, Data Visualization, Human Computer Interaction},
location = {Christchurch, New Zealand},
series = {VRST '23}
}
```

# Welcome to Dynascape prototype 
This repository contains the prototype implementation of our proposed concepts. Feel free to contact me (zhongyuan.yu@tu-dresden.de) for further details. 
This prototype is being developed for research use. It may contain bugs, including critical security flaws. Do not use this prototype for mission critical purposes.  
